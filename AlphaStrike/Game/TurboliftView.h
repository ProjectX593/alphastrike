#pragma once

#include "View.h"

class HomeBase;

class TurboliftView : public View
{
public:
	TurboliftView(HomeBase& homeBase, AnimationRegister& animationRegister);
	~TurboliftView();

	void Initialize();
	void HackSave();

private:
	HomeBase& mHomeBase;
};

