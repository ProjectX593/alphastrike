#pragma once

#include "cereal\types\polymorphic.hpp"
#include "Vector2.h"

class Bullet;

class BulletBehavior
{
public:
	BulletBehavior(float duration = 0.0f);
	virtual ~BulletBehavior();

	virtual std::shared_ptr<BulletBehavior> Copy();
	virtual void Run(float timestepSeconds);
	virtual void Initialize(std::shared_ptr<Bullet> bullet);
	virtual void Reset();
	virtual std::string GetDisplayString();
	bool Done();
	virtual void Reflect();

	float GetDuration() const { return mDuration; }
	void SetDuration(float duration) { mDuration = duration; }

	template <class Archive>
	void serialize(Archive& archive, const std::uint32_t version)
	{
		int errorStopper = 0;	// cereal doesn't like not serializing things...
		archive(errorStopper);
	}

protected:
	float mDuration;
	float mTimeLeft;
	std::shared_ptr<Bullet> mBullet;

	void TurnTowardsTarget(Vector2 targetVelocity, float turnRate, float timestepSeconds);
};

CEREAL_CLASS_VERSION(BulletBehavior, 1);