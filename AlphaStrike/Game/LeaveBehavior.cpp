/*
LeaveBehavior

Causes an enemy to leave the screen
*/

#include "EnemyShip.h"
#include "LeaveBehavior.h"
#include "RandomGenerator.h"
#include "World.h"

#include "cereal\archives\binary.hpp"
#include "cereal\archives\json.hpp"
CEREAL_REGISTER_TYPE(LeaveBehavior);
CEREAL_REGISTER_DYNAMIC_INIT(leaveB);

LeaveBehavior::LeaveBehavior():
	mType(LeaveType::none),
	mAcceleration(1.0f),
	mVelocity(0.0f),
	mXSpawnPos(0.5f)
{
}

LeaveBehavior::LeaveBehavior(LeaveType type, float acceleration, float xSpawnPos):
	mType(type),
	mAcceleration(acceleration),
	mVelocity(0.0f),
	mXSpawnPos(xSpawnPos)
{

}

LeaveBehavior::~LeaveBehavior()
{
}

std::shared_ptr<EnemyBehavior> LeaveBehavior::Copy()
{
	return std::make_shared<LeaveBehavior>(mType, mAcceleration, mXSpawnPos);
}

void LeaveBehavior::Run(float timestepSeconds)
{
	mVelocity += mAcceleration * timestepSeconds;

	Vector2 moveVec;
	if(mType == LeaveType::up)
	{
		moveVec = Vector2(0.0f, -mVelocity);
	}
	else if(mType == LeaveType::down)
	{
		moveVec = Vector2(0.0f, mVelocity);
	}
	else if(mType == LeaveType::left || (mType == LeaveType::split && mEnemyShip->GetPos().x < mXSpawnPos))
	{
		moveVec = Vector2(-mVelocity, 0.0f);
	}
	else if(mType == LeaveType::right || (mType == LeaveType::split && mEnemyShip->GetPos().x >= mXSpawnPos))
	{
		moveVec = Vector2(mVelocity, 0.0f);
	}
	else if(mType == LeaveType::radiate)
	{
		moveVec = mEnemyShip->GetPos() - Vector2(mXSpawnPos, 0.5f);
		moveVec.Normalize();
		moveVec *= mVelocity;
	}
	mEnemyShip->MoveBy(moveVec);
}

// Static stuff
const std::map<std::string, LeaveType>& LeaveBehavior::GetLeaveTypeMap()
{
	static std::map<std::string, LeaveType> map;

	if(map.size() == 0)
	{
		map["none"] = LeaveType::none;
		map["up"] = LeaveType::up;
		map["down"] = LeaveType::down;
		map["left"] = LeaveType::left;
		map["right"] = LeaveType::right;
		map["radiate"] = LeaveType::radiate;
		map["split"] = LeaveType::split;
	}

	return map;
}

LeaveType LeaveBehavior::LeaveTypeFromStr(const std::string& str)
{
	auto& map = GetLeaveTypeMap();
	
	if(map.find(str) != map.end())
		return map.at(str);
	else
		return LeaveType::none;
}

std::string LeaveBehavior::StrFromLeaveType(LeaveType type)
{
	auto& map = GetLeaveTypeMap();
	for(auto& pair : map)
	{
		if(pair.second == type)
			return pair.first;
	}

	return "";
}