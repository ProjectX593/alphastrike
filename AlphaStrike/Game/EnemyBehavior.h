#pragma once

#include <memory>
#include <stdint.h>

#include "cereal\types\polymorphic.hpp"

class EnemyShip;

class EnemyBehavior
{
public:
	EnemyBehavior();
	virtual ~EnemyBehavior();

	virtual std::shared_ptr<EnemyBehavior> Copy();
	virtual void Run(float timestepSeconds);
	virtual void Initialize(EnemyShip* enemyShip);
	bool IsBlocking();
	virtual void Reset();
	virtual void SetLeaderBehavior(std::weak_ptr<EnemyBehavior> behavior);
	virtual std::string GetDisplayString() { return "EB BASE"; }

	template<class Archive>
	void serialize(Archive& archive, const std::uint32_t version)
	{
		int errorStopper = 0;	// cereal doesn't like not serializing things...
		archive(errorStopper);
	}

protected:
	bool mBlocking;	// if true, will stop a SequenceBehavior from moving on to the next step
	EnemyShip* mEnemyShip;
};

CEREAL_CLASS_VERSION(EnemyBehavior, 1);