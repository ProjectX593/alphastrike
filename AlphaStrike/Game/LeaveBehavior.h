#pragma once

#include "EnemyBehavior.h"

enum class LeaveType : int
{
	none,
	up,
	down,
	left,
	right,
	radiate,
	split
};

class LeaveBehavior : public EnemyBehavior
{
public:
	LeaveBehavior();
	LeaveBehavior(LeaveType type, float mAcceleration, float xSpawnPos);
	~LeaveBehavior();

	static LeaveType LeaveTypeFromStr(const std::string& str);
	static std::string StrFromLeaveType(LeaveType type);

	virtual std::shared_ptr<EnemyBehavior> Copy();
	virtual void Run(float timestepSeconds);
	virtual std::string GetDisplayString() { return "Leave"; }

	template <class Archive>
	void serialize(Archive& archive, const std::uint32_t version)
	{
		archive(mType, mAcceleration, cereal::base_class<EnemyBehavior>(this));
	}

private:
	LeaveType mType;
	float mVelocity;
	float mAcceleration;
	float mXSpawnPos;

	static const std::map<std::string, LeaveType>& GetLeaveTypeMap();
};

CEREAL_CLASS_VERSION(LeaveBehavior, 1);
CEREAL_FORCE_DYNAMIC_INIT(leaveB);