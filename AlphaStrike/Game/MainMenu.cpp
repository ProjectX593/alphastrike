/*
MainMenu

The game's main menu screen.
*/

#include "AnimationRegister.h"
#include "Button.h"
#include "GameGlobals.h"
#include "MainMenu.h"
#include "UtilityServiceLocator.h"

MainMenu::MainMenu(AnimationRegister& animationRegister, std::function<void(const std::string&)> newGameCallback, std::function<void()> quitCallback):
	View(animationRegister),
	mNewGameCallback(newGameCallback),
	mQuitCallback(quitCallback)
{
}

void MainMenu::Initialize()
{
	LoadFromJson("assets\\screens\\mainMenu.json", PIXELS_IN_UNIT);

	GetChild<Button>("newGameButton")->SetMouseReleaseCallback(std::bind(&MainMenu::OnNewGameButton, this));
	GetChild<Button>("continueButton")->SetMouseReleaseCallback(std::bind(&MainMenu::OnContinueButton, this));
	GetChild<Button>("optionsButton")->SetMouseReleaseCallback(std::bind(&MainMenu::OnOptionsButton, this));
	GetChild<Button>("quitButton")->SetMouseReleaseCallback(mQuitCallback);
}

void MainMenu::OnNewGameButton()
{
	mNewGameCallback("");
	Close();
}

void MainMenu::OnContinueButton()
{
	mNewGameCallback(UtilityServiceLocator::GetPlatformUtil().GetWorkingDirectory() + "saves\\default.sav");
	Close();
}

void MainMenu::OnOptionsButton()
{

}