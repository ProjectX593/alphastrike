#pragma once

#include "EnemyBehavior.h"
#include <vector>

class RandomGenerator;

class SequenceBehavior : public EnemyBehavior
{
public:
	SequenceBehavior();
	SequenceBehavior(const std::vector<std::vector<std::shared_ptr<EnemyBehavior>>>& behaviors, bool isRandom = false, int runCount = 0);
	virtual ~SequenceBehavior();

	virtual std::shared_ptr<EnemyBehavior> Copy();
	virtual void Run(float timestepSeconds);
	virtual void Initialize(EnemyShip* enemyShip);
	virtual void Reset();
	virtual void SetLeaderBehavior(std::weak_ptr<EnemyBehavior> leaderBehavior);
	const std::vector<std::vector<std::shared_ptr<EnemyBehavior>>>& GetBehaviors() { return mBehaviors; }
	std::vector<std::vector<std::shared_ptr<EnemyBehavior>>>& GetEditableBehaviors() { return mBehaviors; }
	virtual std::string GetDisplayString() { return "Sequence"; }

	int GetRunCount(){ return mRunCount; }
	bool GetIsRandom(){ return mIsRandom; }
	void SetRunCount(int runCount){ mRunCount = runCount; }
	void SetIsRandom(bool isRandom){ mIsRandom = isRandom; }

	template <class Archive>
	void serialize(Archive& archive, const std::uint32_t version)
	{
		archive(mBehaviors, mRunCount, mIsRandom, cereal::base_class<EnemyBehavior>(this));
	}

private:
	std::vector<std::vector<std::shared_ptr<EnemyBehavior>>> mBehaviors;
	int mCurrentGroup;
	int mRunCount;
	int mRunsLeft;
	bool mIsRandom;
	bool mActive;

	bool RunBehaviors(std::vector<std::shared_ptr<EnemyBehavior>>& behaviors, float timestepSeconds);
};

CEREAL_CLASS_VERSION(SequenceBehavior, 1);
CEREAL_FORCE_DYNAMIC_INIT(sequenceB);