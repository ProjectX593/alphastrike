/*
TurretVisibilityBehavior

A behavior that will swap the visibility of series of turrets on an enemyship. The change only occurs once until the behavior is reset.
Might be extended in the future to have transition animations or a time for an alpha tween or something, rather than just causing the turrets to pop in and out.
*/

#include "EnemyShip.h"
#include "TurretVisibilityBehavior.h"

#include "cereal\archives\binary.hpp"
#include "cereal\archives\json.hpp"
CEREAL_REGISTER_TYPE(TurretVisibilityBehavior);
CEREAL_REGISTER_DYNAMIC_INIT(turretVisibilityB);

TurretVisibilityBehavior::TurretVisibilityBehavior():
	mVisible(true),
	mActive(true)
{
}

TurretVisibilityBehavior::TurretVisibilityBehavior(const std::vector<int> turretsToChange, bool visible):
	mTurretsToChange(turretsToChange),
	mVisible(visible),
	mActive(true)
{
}

TurretVisibilityBehavior::~TurretVisibilityBehavior()
{
}

std::shared_ptr<EnemyBehavior> TurretVisibilityBehavior::Copy()
{
	return std::make_shared<TurretVisibilityBehavior>(mTurretsToChange, mVisible);
}

void TurretVisibilityBehavior::Run(float timestepSeconds)
{
	if(mActive)
	{
		mActive = false;
		auto& turrets = mEnemyShip->GetTurrets();

		for(int turretIndex : mTurretsToChange)
			turrets.at(turretIndex).GetSprite()->SetVisible(mVisible);
	}
}

void TurretVisibilityBehavior::Reset()
{
	mActive = true;
}