/*
SpawnEnemyBehavior

Causes a ship to spawn a new enemy at the given position. If a turret index is passed in, the enemies will spawn only
if the turret is still alive.
*/

#include "EnemyShip.h"
#include "GameGlobals.h"
#include "SpawnEnemyBehavior.h"
#include "World.h"

#include "cereal\archives\binary.hpp"
#include "cereal\archives\json.hpp"
CEREAL_REGISTER_TYPE(SpawnEnemyBehavior);
CEREAL_REGISTER_DYNAMIC_INIT(spawnEB);

using std::string;

SpawnEnemyBehavior::SpawnEnemyBehavior():
	mRequiredTurretIndex(-1)
{
	mBlocking = true;
}

SpawnEnemyBehavior::SpawnEnemyBehavior(const string& enemyId, const string& initialBehaviorSetId, const Vector2& relativePos, int requiredTurretIndex):
	mEnemyId(enemyId),
	mInitialBehaviorSetId(initialBehaviorSetId),
	mRelativePos(relativePos),
	mRequiredTurretIndex(requiredTurretIndex)
{
	mBlocking = true;
}

SpawnEnemyBehavior::~SpawnEnemyBehavior()
{
}

std::shared_ptr<EnemyBehavior> SpawnEnemyBehavior::Copy()
{
	return std::make_shared<SpawnEnemyBehavior>(mEnemyId, mInitialBehaviorSetId, mRelativePos, mRequiredTurretIndex);
}

void SpawnEnemyBehavior::Run(float timestepSeconds)
{
	if(mRequiredTurretIndex >= 0 && mEnemyShip->GetTurrets().at(mRequiredTurretIndex).IsDead())
		return;

	if(mBlocking)
	{
		mEnemyShip->GetWorld()->SpawnEnemy(mEnemyId, mInitialBehaviorSetId, mEnemyShip->GetSprite()->GetPos() + (mRelativePos / PIXELS_IN_UNIT));

		mBlocking = false;
	}
}

void SpawnEnemyBehavior::Reset()
{
	mBlocking = true;
}