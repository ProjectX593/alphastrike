/*
Planet

Represents a single 'planet' in the game. Each planet has several levels, and the game
consists of progressing through several planets until the final boss planet is reached and defeated.
*/

#include "Config.h"
#include <exception>
#include "FileUtil.h"
#include "Planet.h"
#include "RandomGenerator.h"

#include "cereal/cereal.hpp"
#include "cereal/archives/binary.hpp"
#include "cereal/archives/json.hpp"

using std::string;
using std::vector;

//---------------------------- PlanetConfig ----------------------------//
// PlanetConfig represents the configuration for a single planet. A series of PlanetConfig objects
// will live inside Config at all times, and as such, the arrays contained within may be referenced instead of copied.

PlanetConfig::PlanetConfig()
{
}

PlanetConfig::PlanetConfig(const string& planetId, const string& description, const string& bgImage, const vector<string>& spawnGroupIds, LevelDifficulty minDifficulty, LevelDifficulty maxDifficulty, const std::vector < std::vector < std::string >> &backgroundLayerImages):
	mPlanetId(planetId),
	mDescription(description),
	mBGImage(bgImage),
	mSpawnGroupIds(spawnGroupIds),
	mMinDifficulty(minDifficulty),
	mMaxDifficulty(maxDifficulty),
	mBackgroundLayerImages(backgroundLayerImages)
{
	if(backgroundLayerImages.size() != BACKGROUND_LAYER_COUNT)
		throw std::runtime_error("ERROR: Wrong number of layers for planet config!");
}

LevelDifficulty PlanetConfig::GetRandomDifficulty(RandomGenerator& randomGenerator) const
{
	int min = static_cast<int>(mMinDifficulty);
	int max = static_cast<int>(mMaxDifficulty);

	return static_cast<LevelDifficulty>(randomGenerator.RandomInRange(min, max));
}

//---------------------------- Planet ----------------------------//

// It's possible to initialize planets without the config, and set it later. This is used for serialization.
Planet::Planet():
	mConfig(nullptr),
	mDifficulty(LevelDifficulty::easy)
{
}

Planet::Planet(const Config* config):
	mConfig(config),
	mDifficulty(LevelDifficulty::easy)
{
}

Planet::Planet(const PlanetConfig& planetConfig, RandomGenerator& randomGenerator, const Config* config):
	mConfig(config),
	mDifficulty(planetConfig.GetRandomDifficulty(randomGenerator)),
	mPlanetId(planetConfig.GetPlanetId())
{
}

Planet::~Planet()
{
}

// sets the config and pulls data on the planet from it, use this after loading the planet from a file
void Planet::SetConfig(const Config* config)
{
	if(mPlanetId == "")
		throw new std::runtime_error("ERROR: Planet set config called before it was deserialized.");

	mConfig = config;
	const PlanetConfig& conf = mConfig->GetPlanetConfig(mPlanetId);
}

const PlanetConfig& Planet::GetPlanetConfig() const
{
	return mConfig->GetPlanetConfig(mPlanetId);
}