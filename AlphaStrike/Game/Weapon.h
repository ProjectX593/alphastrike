#pragma once

#include "Bullet.h"
#include <functional>
#include "Item.h"
#include "TextureInfo.h"
#include "WeaponAction.h"

#include "cereal/types/vector.hpp"

class AnimatedSprite;
class BulletBehavior;

class Weapon : public Item
{
public:
	Weapon();
	Weapon(float cooldown, const std::vector<WeaponAction>& actions);
	Weapon(int guid, const std::string& name, const std::string& description, const std::string& thumbnail, int recycleValue, bool isPrimaryWeapon, bool isPlayerWeapon, float cooldown, const std::vector<WeaponAction>& actions, int ammoRequired = 0, int extraDamage = 0);
	virtual ~Weapon();

	float mCooldown;
	int mAmmoRequired;

	virtual std::shared_ptr<Weapon> Clone() const;
	virtual void Simulate(float timestepSeconds, bool firePressed, std::shared_ptr<AnimatedSprite> shipSprite, World* world, const Vector2& offset = Vector2());
	const std::vector<WeaponAction>& GetActions() const { return mActions; }
	std::vector<WeaponAction>& GetEditableActions() { return mActions; }
	bool IsPrimaryWeapon() { return mIsPrimaryWeapon; }
	bool CanFire();
	int GetExtraDamage() { return mExtraDamage; }
	void SetExtraDamage(int extraDamage) { mExtraDamage = extraDamage; }
	void StopActions();
	void TakeWeaponConfigValues(const Weapon* weapon);
	virtual ItemType GetType();

	template<class Archive> 
	void serialize(Archive& archive, const std::uint32_t version)
	{
		archive(mCooldown, mIsPrimaryWeapon, mIsPlayerWeapon, mExtraDamage, mAmmoRequired, mActions, cereal::base_class<Item>(this));
		mCurrentActionIndex = mActions.size();
	}

protected:
	bool mIsPlayerWeapon;

private:
	bool mIsPrimaryWeapon;
	bool mFireReleased;
	float mTimeUntilFire;
	int mExtraDamage;
	std::vector<WeaponAction> mActions;
	int mCurrentActionIndex;
	float mActionTimeElapsed;

	void ExecuteAction(int actionIndex, std::shared_ptr<AnimatedSprite> shipSprite, World* world, const Vector2& offset);
};

CEREAL_CLASS_VERSION(Weapon, 2);
CEREAL_FORCE_DYNAMIC_INIT(wep);