#pragma once
#include "TimedBehavior.h"

class ShootBehavior : public TimedBehavior
{
public:
	ShootBehavior();
	ShootBehavior(int weaponIndex, float duration = 0.0f);
	virtual ~ShootBehavior();

	virtual std::shared_ptr<EnemyBehavior> Copy();
	virtual void Run(float timestepSeconds);
	virtual void Reset();
	virtual std::string GetDisplayString() { return "Shoot - " + std::to_string(mWeaponIndex); }

	int GetWeaponIndex() { return mWeaponIndex; }
	void SetWeaponIndex(int weaponIndex) { mWeaponIndex = weaponIndex; }

	template <class Archive>
	void serialize(Archive& archive, const std::uint32_t version)
	{
		archive(mWeaponIndex, cereal::base_class<TimedBehavior>(this));
	}

private:
	int mWeaponIndex;
};

CEREAL_CLASS_VERSION(ShootBehavior, 1);
CEREAL_FORCE_DYNAMIC_INIT(shootB);