/*
TimedBehavior

A class that any behavior may extend in order to have behavior that stops after
a specific amount of time. 

By default these behaviors stop blocking and go inactive but this can be overwritten by
overwriting Done().

Passing a duration of 0 (or leaving the default) will cause the timed behavior
to do nothing.
*/

#include <functional>
#include "TimedBehavior.h"

#include "cereal\archives\binary.hpp"
#include "cereal\archives\json.hpp"
CEREAL_REGISTER_TYPE(TimedBehavior);
CEREAL_REGISTER_DYNAMIC_INIT(timedB);

using std::shared_ptr;

TimedBehavior::TimedBehavior(float duration):
	mTimer(duration, std::bind(&TimedBehavior::Done, this))
{
	if(duration != 0.0f)
		mBlocking = true;
}

TimedBehavior::~TimedBehavior()
{
}

TimedBehavior::TimedBehavior(const TimedBehavior& other):
	mTimer(other.mTimer.GetDuration(), std::bind(&TimedBehavior::Done, this))
{
	mBlocking = mTimer.GetDuration() > 0.0001f;
}

void TimedBehavior::SetDuration(float duration)
{
	mTimer.SetDuration(duration);
	mBlocking = duration > 0.0001;
}

shared_ptr<EnemyBehavior> TimedBehavior::Copy()
{
	return std::make_shared<TimedBehavior>(mTimer.GetDuration());
}

void TimedBehavior::Run(float timestepSeconds)
{
	mTimer.Run(timestepSeconds);
}

void TimedBehavior::Reset()
{
	mTimer.Reset();
	if(mTimer.GetDuration() > 0.0001f)
		mBlocking = true;
}

void TimedBehavior::Done()
{
	mBlocking = false;
}
