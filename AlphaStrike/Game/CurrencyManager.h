#pragma once

#include "cereal/archives/binary.hpp"
#include "cereal/archives/json.hpp"
#include "cereal/cereal.hpp"

class CurrencyManager
{
public:
	CurrencyManager();
	~CurrencyManager();

	void AddScrap(int scrap);
	void AddEnergy(int energy);
	void ConvertScrapToEnergy(int scrap);
	void ConvertEnergyToAmmo(int energy);
	bool TrySpendAmmo(int ammo);
	float GetAmmoPercent() const;
	int GetScrap() const { return mScrap; }
	int GetEnergy() const { return mEnergy; }
	int GetAmmo() const { return mAmmo; }
	int GetScrapPerEnergy() const;

	template<class Archive>
	void serialize(Archive& archive, const std::uint32_t version)
	{
		archive(mScrap, mEnergy);
	};

private:
	int mScrap;
	int mEnergy;
	int mAmmo;

	void Convert(int amount, int& fromCurrency, int& toCurrency, int ratio);
};

CEREAL_CLASS_VERSION(CurrencyManager, 1);