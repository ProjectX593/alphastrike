#pragma once

#include "View.h"

class HomeBase;

class BridgeView : public View
{
public:
	BridgeView(HomeBase& homeBase, AnimationRegister& animationRegister);
	~BridgeView();

	void Initialize();

private:
	HomeBase& mHomeBase;
	bool mShopSelected;
	int mSelectedPlanetIndex;
	std::shared_ptr<View> mMainView;
	std::shared_ptr<View> mConsoleView;

	void ShowConsoleView();
	void HideConsoleIcons();
	void ShowPlanetState();
	void ShowMissionState();
	void ClickPlanet(int planetIndex);
	void ClickShop();
	void OnGoButton();
	void UpdateGoButton(int planetIndexOverride = -1);
	std::shared_ptr<Button> GetButtonForPlanet(int planetIndex);
	std::shared_ptr<AnimatedSprite> GetMissionIcon(int missionIndex, int columnIndex);
};

