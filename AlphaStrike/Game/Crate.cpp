/*
Crate

Crates are objects that appear in the game. Their only purpose is to be destroyed,
and spawn a drop when they die.
*/

#include "AnimatedSprite.h"
#include "AnimationRegister.h"
#include "Config.h"
#include "Crate.h"
#include "GameGlobals.h"
#include "Layer.h"
#include "RandomGenerator.h"
#include "TextureInfo.h"
#include "Vector2.h"
#include "World.h"

using std::shared_ptr;
using std::string;

const int CRATE_HEALTH = 5;
const float CRATE_SPEED = 0.15f;

Crate::Crate(const Vector2& initialPos, const std::string& dropTableId, World* world):
	Ship(CRATE_HEALTH, std::make_shared<AnimatedSprite>(initialPos, D2DSize(0.0f, 0.0f, 11)), world, 0.0f, 0.0f),
	mDropTableId(dropTableId),
	mPos(initialPos)
{
	mShipSprite->LoadAndPlayAnimation(mWorld->GetAnimationRegister().RegisterAnimation("crate"), DEFAULT_ANIMATION, true, PIXELS_IN_UNIT);
	mWorld->GetWorldLayer()->AddChild(mShipSprite);
}

Crate::~Crate()
{
	
}

void Crate::OnDie(World* world)
{
	Ship::OnDie();

	const auto& dropTable = mWorld->GetConfig()->GetDropTables().at(mDropTableId);
	float dropProb = (float)mWorld->GetRandomGenerator().RandomInRange(0.0f, 1.0f);
	for(const auto& drop : dropTable)
	{
		dropProb -= drop.mProbability;
		if(dropProb <= 0.0f)
		{
			world->SpawnItemPowerup(mPos, drop.mItemId, drop.mType);
			return;
		}
	}
	//TODO: Assert here
}

void Crate::Simulate(float timestepSeconds)
{
	Ship::Simulate(timestepSeconds);

	mPos.y += CRATE_SPEED * timestepSeconds;
	mShipSprite->SetPos(mPos);
}