#pragma once

#include "EnemyBehavior.h"

class SetChangeBehavior : public EnemyBehavior
{
public:
	SetChangeBehavior();
	SetChangeBehavior(const std::string& newBehaviorSetId, int healthThreshold, bool changeWhenTurretsDead = false);
	~SetChangeBehavior();

	virtual std::shared_ptr<EnemyBehavior> Copy();
	virtual void Run(float timestepSeconds);
	virtual std::string GetDisplayString() { return "Behavior Set Change"; }

	const std::string& GetNewBehaviorSetId() const { return mNewBehaviorSetId; }
	void SetNewBehaviorSetId(const std::string& newBehaviorSetId) { mNewBehaviorSetId = newBehaviorSetId; }
	int GetHealthThreshold() const { return mHealthThreshold; }
	void SetHealthThreshold(int healthThreshold){ mHealthThreshold = healthThreshold; }
	bool GetChangeWhenTurretsDead() const { return mChangeWhenTurretsDead; }
	void SetChangeWhenTurretsDead(bool changeWhenTurretsDead) { mChangeWhenTurretsDead = changeWhenTurretsDead; }

	template <class Archive>
	void serialize(Archive& archive, std::uint32_t version)
	{
		archive(mNewBehaviorSetId, mHealthThreshold, mChangeWhenTurretsDead, cereal::base_class<EnemyBehavior>(this));
	}

private:
	std::string mNewBehaviorSetId;
	int mHealthThreshold;
	bool mChangeWhenTurretsDead;
};

CEREAL_CLASS_VERSION(SetChangeBehavior, 1);
CEREAL_FORCE_DYNAMIC_INIT(setChangeB);