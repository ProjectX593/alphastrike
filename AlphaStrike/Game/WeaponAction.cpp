#include "BulletBehavior.h"
#include "WeaponAction.h"

WeaponAction::WeaponAction():
	mDamage(1),
	mInitialVelocity(0.0f, 1.0f),
	mRotates(true),
	mDelay(0.0f)
{

}

WeaponAction::WeaponAction(int damage, const Vector2& centerPosInPixels, const Vector2& initialVelocity, bool rotates, float delay, const std::vector<std::shared_ptr<BulletBehavior>>& behaviors, const std::string& bulletId):
	mDamage(damage),
	mCenterPosInPixels(centerPosInPixels),
	mInitialVelocity(initialVelocity),
	mRotates(rotates),
	mDelay(delay),
	mBulletId(bulletId)
{
	CopyBehaviors(behaviors);
}

WeaponAction::WeaponAction(const WeaponAction& other):
	mDamage(other.mDamage),
	mCenterPosInPixels(other.mCenterPosInPixels),
	mInitialVelocity(other.mInitialVelocity),
	mRotates(other.mRotates),
	mDelay(other.mDelay),
	mBulletId(other.mBulletId)
{
	CopyBehaviors(other.mBulletBehaviors);
}

WeaponAction& WeaponAction::operator=(const WeaponAction& other)
{
	mDamage = other.mDamage;
	mCenterPosInPixels = other.mCenterPosInPixels;
	mInitialVelocity = other.mInitialVelocity;
	mRotates = other.mRotates;
	mDelay = other.mDelay;
	mBulletId = other.mBulletId;
	CopyBehaviors(other.mBulletBehaviors);

	return *this;
}

void WeaponAction::CopyBehaviors(const std::vector<std::shared_ptr<BulletBehavior>>& bulletBehaviors)
{
	mBulletBehaviors.clear();
	for(const auto& behavior : bulletBehaviors)
		mBulletBehaviors.push_back(behavior->Copy());
}

// if index is < 0, we just add it to the end
void WeaponAction::AddBulletBehavior(std::shared_ptr<BulletBehavior> behavior, int index)
{
	//TODO: Handle errors properly
	if(index < 0)
		mBulletBehaviors.push_back(behavior->Copy());
	else
		mBulletBehaviors.insert(mBulletBehaviors.begin() + index, behavior);
}

void WeaponAction::RemoveBulletBehavior(int index)
{
	//TODO: Handle errors properly
	mBulletBehaviors.erase(mBulletBehaviors.begin() + index);
}