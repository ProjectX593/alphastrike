#include "TurretGroupConfig.h"
#include "Weapon.h"


TurretGroupConfig::TurretGroupConfig(): 
	mMaxHealth(1), 
	mFacesPlayer(true), 
	mInvulnerable(false), 
	mVisible(true)
{
	mWeapon = std::make_shared<Weapon>();
}


TurretGroupConfig::~TurretGroupConfig()
{
}
