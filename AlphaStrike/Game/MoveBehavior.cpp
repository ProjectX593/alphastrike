/*
MoveBehavior

A behavior for moving a ship. It gets disabled once the ship gets where it's going, but can be reset.
*/

#include <algorithm>
#include "EnemyShip.h"
#include "MathUtil.h"
#include "MoveBehavior.h"

using std::make_shared;
using std::shared_ptr;

CEREAL_REGISTER_TYPE(MoveBehavior);
CEREAL_REGISTER_DYNAMIC_INIT(moveB);

MoveBehavior::MoveBehavior():
	mLastProgress(0.0f),
	mLerpSinStart(0.0f),
	mLerpSinEnd(1.0f)
{
	mBlocking = true;
}

MoveBehavior::MoveBehavior(const Vector2& movement, float duration, float lerpSinStart, float lerpSinEnd):
	TimedBehavior(duration),
	mMovement(movement),
	mLastProgress(0.0f),
	mLerpSinStart(lerpSinStart),
	mLerpSinEnd(lerpSinEnd)
{
	mBlocking = true;
}

MoveBehavior::~MoveBehavior()
{
}

shared_ptr<EnemyBehavior> MoveBehavior::Copy()
{
	return make_shared<MoveBehavior>(*this);
}

void MoveBehavior::Run(float timestepSeconds)
{
	TimedBehavior::Run(timestepSeconds);

	if(!mBlocking)
		return;

	float lastLerpPos = MathUtil::Lerp(mLastProgress, mLerpSinStart, mLerpSinEnd);
	float progress = std::min(mTimer.GetElapsedTime() / mTimer.GetDuration(), 1.0f);
	float lerpPos = MathUtil::Lerp(progress, mLerpSinStart, mLerpSinEnd);
	mLastProgress = progress;

	Vector2 moveVector = mMovement;
	moveVector.Normalize();
	mEnemyShip->MoveBy(moveVector * ((lerpPos - lastLerpPos) * mMovement.Length()));

	if(mLastProgress >= 1.0f)
		mBlocking = false;
}

void MoveBehavior::Reset()
{
	TimedBehavior::Reset();
	mLastProgress = 0.0f;
	mBlocking = true;
}