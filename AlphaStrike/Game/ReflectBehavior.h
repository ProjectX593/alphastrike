#pragma once

#include "TimedBehavior.h"

class ReflectBehavior : public TimedBehavior
{
public:
	ReflectBehavior(float duration = 0.0f);
	~ReflectBehavior();

	virtual std::shared_ptr<EnemyBehavior> Copy();
	virtual void Run(float timestepSeconds);
	virtual void Reset();
	virtual std::string GetDisplayString() { return "Reflect - " + std::to_string(mTimer.GetDuration()); }

	template <class Archive>
	void serialize(Archive& archive, const std::uint32_t version)
	{
		archive(cereal::base_class<TimedBehavior>(this));
	}

private:
	bool mReflecting;
};

CEREAL_CLASS_VERSION(ReflectBehavior, 1);
CEREAL_FORCE_DYNAMIC_INIT(reflectB);