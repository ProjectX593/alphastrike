/*
InterceptBehavior

A behavior that causes bullets to intercept targets. The behavior may optionally switch to the closest target each frame.
*/

#include "Bullet.h"
#include "EnemyShip.h"
#include "InterceptBehavior.h"
#include "PlayerShip.h"
#include "World.h"

#include "cereal\archives\binary.hpp"
#include "cereal\archives\json.hpp"
CEREAL_REGISTER_TYPE(InterceptBehavior);
CEREAL_REGISTER_DYNAMIC_INIT(intBB);

InterceptBehavior::InterceptBehavior():
	mSpeed(0.0f),
	mTurnRate(0.0f),
	mSwitchesTargets(false),
	mMaxSeekers(0)
{
}

InterceptBehavior::InterceptBehavior(float speed, float turnRate, bool switchesTargets, int maxSeekers, float duration):
	BulletBehavior(duration),
	mSpeed(speed),
	mTurnRate(turnRate),
	mSwitchesTargets(switchesTargets),
	mMaxSeekers(maxSeekers)
{
}

InterceptBehavior::~InterceptBehavior()
{
}

std::shared_ptr<BulletBehavior> InterceptBehavior::Copy()
{
	return std::make_shared<InterceptBehavior>(mSpeed, mTurnRate, mSwitchesTargets, mMaxSeekers, mDuration);
}

void InterceptBehavior::Run(float timestepSeconds)
{
	BulletBehavior::Run(timestepSeconds);

	if(mSwitchesTargets)
		SetTarget(FindNearestShip());
	if(mTarget.lock() != nullptr)
		TurnTowardsTarget(GetVelocityToShip(mTarget.lock(), mSpeed), mTurnRate, timestepSeconds);
}

void InterceptBehavior::Initialize(std::shared_ptr<Bullet> bullet)
{
	BulletBehavior::Initialize(bullet);
	Reset();
}

void InterceptBehavior::Reset()
{
	SetTarget(FindNearestShip());
}

void InterceptBehavior::SetTarget(std::weak_ptr<Ship> target)
{
	if(target.lock() == mTarget.lock())
		return;

	if(mTarget.lock() != nullptr)
	{
		auto es = std::dynamic_pointer_cast<EnemyShip>(mTarget.lock());
		if(es != nullptr)
			es->RemoveSeeker();
	}

	mTarget = target;

	if(target.lock() == nullptr)
		return;

	auto es2 = std::dynamic_pointer_cast<EnemyShip>(target.lock());
	if(es2 != nullptr)
		es2->AddSeeker();
}

std::weak_ptr<Ship> InterceptBehavior::FindNearestShip()
{
	if(mBullet->IsPlayerBullet())
	{
		int fewestSeekers = 10000;
		float shortestDistance = 100000.0f;
		std::shared_ptr<EnemyShip> closestShip = nullptr;
		std::vector<std::shared_ptr<EnemyShip>>& enemyShips = mBullet->GetWorld()->GetEnemyShips();
		Vector2 centerPos = mBullet->GetSprite()->GetCenter();

		for(auto& enemyShip : enemyShips)
		{
			float distanceToShip = (enemyShip->GetSprite()->GetCenter() - centerPos).Length();
			int seekers = enemyShip->GetSeekerCount();
			if(enemyShip == mTarget.lock())
				seekers--;
			if((distanceToShip < shortestDistance && mMaxSeekers <= 0) ||	// if ignoring max seekers, just be closer
				(distanceToShip < shortestDistance && seekers < mMaxSeekers) || // if closer and not enough seekers, ALWAYS target
				(distanceToShip < shortestDistance && fewestSeekers >= mMaxSeekers && seekers <= fewestSeekers) || // if closer and has same or less seekers than current target
				(fewestSeekers >= mMaxSeekers && seekers < fewestSeekers)) // if current target has enough seekers and new target has less than current
			{
				closestShip = enemyShip;
				shortestDistance = distanceToShip;
				fewestSeekers = seekers;
			}
		}

		return closestShip;
	}
	else
	{
		return mBullet->GetWorld()->GetPlayerShip();
	}
}

Vector2 InterceptBehavior::GetVelocityToShip(std::shared_ptr<Ship> ship, float speed)
{
	Vector2 dest;
	bool destSet = false;	// (0,0) could be a valid destination, have to account for that
	std::shared_ptr<EnemyShip> enemy = std::dynamic_pointer_cast<EnemyShip>(ship);
	if(enemy != nullptr && enemy->GetTurrets().size() > 0)
	{
		auto& turrets = enemy->GetTurrets();
		float shortestDistance = 10000.0f;
		for(auto& turret : turrets)
		{
			if(turret.IsInvulnerable())
				continue;
			float distToTurret = (mBullet->GetSprite()->GetAbsoluteCenter() - turret.GetSprite()->GetAbsoluteCenter()).Length();
			if(distToTurret < shortestDistance)
			{
				shortestDistance = distToTurret;
				dest = turret.GetSprite()->GetAbsoluteCenter();
				destSet = true;
			}
		}
	}
	if(!destSet)
	{
		dest = ship->GetSprite()->GetAbsoluteCenter();
	}

	Vector2 velocity = dest - mBullet->GetSprite()->GetAbsoluteCenter();
	velocity.Normalize();
	velocity *= speed;
	return velocity;
}

std::string InterceptBehavior::GetDisplayString()
{
	return "Intercept";
}

void InterceptBehavior::Reflect()
{
	SetTarget(FindNearestShip());	// do this regardless of whether or not this behavior usually changes targets
}