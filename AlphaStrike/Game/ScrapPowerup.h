#pragma once

#include "Powerup.h"

class ScrapPowerup : public Powerup
{
public:
	ScrapPowerup(const Vector2& initialCenterPos, int scrapDrop, AnimationRegister& animationRegister, std::shared_ptr<Layer> worldLayer);
	virtual ~ScrapPowerup();

	virtual void OnDie(ProgressionState& progressionState);

private:
	int mScrapDrop;

	std::string GetAnimationNameForDrop(int scrapDrop);
};

