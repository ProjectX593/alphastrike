#pragma once

#include "EnemyBehavior.h"
#include "ManualTimer.h"
#include <vector>

#include "cereal/types/vector.hpp"

// This struct represents the information for a single turret in the volley behavior
// The default constructor will cause the volley behavior to ignore the turret
struct TurretVolleyInfo
{
	TurretVolleyInfo() {}
	TurretVolleyInfo(int turretIndex, float startRotationRadians, float endRotationRadians, float rotationSpeed, bool clockwise, float fireDelay = 0.0f): mStartRotationRadians(startRotationRadians), mEndRotationRadians(endRotationRadians), mRotationSpeed(rotationSpeed), mClockwise(clockwise), mFireDelay(fireDelay) {}
	int mTurretIndex;
	float mStartRotationRadians;
	float mEndRotationRadians;
	float mRotationSpeed;	// measured in radians per second
	float mFireDelay;		// The time until the turret can start firing from when the behavior starts
	bool mClockwise;	// if false, it is counterclockwise

	template <class Archive>
	void serialize(Archive& archive, const std::uint32_t version)
	{
		archive(mTurretIndex, mStartRotationRadians, mEndRotationRadians, mRotationSpeed, mFireDelay, mClockwise);
	}
};

CEREAL_CLASS_VERSION(TurretVolleyInfo, 2);

enum class TurretVolleyState
{
	rotatingIntoPlace,
	firing,
	discharging
};

class TurretVolleyBehavior : public EnemyBehavior
{
public:
	TurretVolleyBehavior();
	TurretVolleyBehavior(const std::vector<TurretVolleyInfo>& volleyInfos, float duration = 0.0f);
	~TurretVolleyBehavior();

	virtual std::shared_ptr<EnemyBehavior> Copy();
	virtual void Run(float timestepSeconds);
	virtual void Reset();
	virtual std::string GetDisplayString() { return "Turret Volley"; }

	std::vector<TurretVolleyInfo>& GetTurretVolleyInfos() { return mVolleyInfos; }
	float GetDuration(){ return mTimer.GetDuration(); }
	void SetDuration(float duration){ mTimer.SetDuration(duration); }

	template <class Archive>
	void serialize(Archive& archive, const std::uint32_t version)
	{
		archive(mVolleyInfos, mTimer, cereal::base_class<EnemyBehavior>(this));
	}

private:
	std::vector<TurretVolleyInfo> mVolleyInfos;
	bool mStarted;
	TurretVolleyState mState;
	ManualTimer mTimer;

	void TryStartTurretsFiring();
	void TimerDone();
};

CEREAL_CLASS_VERSION(TurretVolleyBehavior, 1);
CEREAL_FORCE_DYNAMIC_INIT(turretVolleyB);