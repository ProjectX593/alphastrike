#pragma once

#include "EnemyBehavior.h"

class DriftBehavior : public EnemyBehavior
{
public:
	DriftBehavior();
	DriftBehavior(float minSpeed, float maxSpeed);
	~DriftBehavior();
	
	float mMinSpeed;
	float mMaxSpeed;

	virtual std::shared_ptr<EnemyBehavior> Copy();
	virtual void Run(float timestepSeconds);
	virtual void Reset();
	virtual std::string GetDisplayString() { return "Drift"; }

	template <class Archive>
	void serialize(Archive& archive, const std::uint32_t version)
	{
		archive(mMinSpeed, mMaxSpeed, cereal::base_class<EnemyBehavior>(this));
	}

private:
	float mSpeed;
};

CEREAL_CLASS_VERSION(DriftBehavior, 1);
CEREAL_FORCE_DYNAMIC_INIT(driftB);