#pragma once

#include "View.h"

class HomeBase;

class ReprocessingView : public View
{
public:
	ReprocessingView(HomeBase& homeBase, AnimationRegister& animationRegister);
	~ReprocessingView();

	void Initialize();
	void RefreshBalances();

private:
	HomeBase& mHomeBase;

	void ConvertAllScrap();
	void ConvertScrap(int scrap);
};

