#pragma once

#include "AnimationRegister.h"
#include "Background.h"
#include <functional>
#include "Item.h"
#include "LiveSpawnGroup.h"
#include <vector>

class Bullet;
class Crate;
class EnemyPool;
class EnemyShip;
class EnemySpawnGroup;
class HudView;
class ItemManager;
class InputSystem;
class Layer;
class PlayerShip;
class Powerup;
class ProgressionState;
class RandomGenerator;

class World
{
public:
	World(ProgressionState& progressionState, std::shared_ptr<Layer> frontendLayer, std::shared_ptr<Layer> worldLayer, std::shared_ptr<Layer> backgroundLayer, ItemManager& itemManager, InputSystem& inputSystem, RandomGenerator& randomGenerator, const Config* config);
	~World();

	void Simulate(float timestepSeconds);
	void Reset();
	void Cleanup();
	bool IsNodeOutsideBounds(std::shared_ptr<Node> node);
	void SetMissionCompleteCallback(std::function<void(bool)> missionCompleteCallback);
	std::shared_ptr<PlayerShip> GetPlayerShip(){ return mPlayerShip; }
	void SpawnBullet(std::shared_ptr<Bullet> bullet);
	void SpawnCrate(const Vector2& pos, const std::string& dropTableId);
	void SpawnItemPowerup(const Vector2& pos, const std::string& itemId, ItemType type);
	void SpawnScrapPowerup(const Vector2& pos, int scrapDrop);
	void OnPlayerDamaged();
	void OnPlayerMoved(float playerXPos);
	void StartEditorMode();

	std::vector<std::shared_ptr<EnemyShip>>& GetEnemyShips() { return mEnemyShips; }
	AnimationRegister& GetAnimationRegister() { return mAnimationRegister; }
	AnimationRegister& GetPersistantAnimationRegister() { return mPersistantAnimationRegister; }
	RandomGenerator& GetRandomGenerator() { return mRandomGenerator; }
	void SpawnEnemyGroup(const EnemySpawnGroup& spawnGroup);
	std::shared_ptr<EnemyShip> SpawnEnemy(const std::string& enemyId, const std::string& initialBehaviorSetId, const Vector2& pos);
	const Config* GetConfig() { return mConfig; }
	ProgressionState& GetProgressionState() { return mProgressionState; }
	std::shared_ptr<Layer> GetWorldLayer() { return mWorldLayer; }
	void SetEnemyPool(const EnemyPool* enemyPool) { mEnemyPool = enemyPool; }

private:
	const Config* mConfig;
	RandomGenerator& mRandomGenerator;
	ProgressionState& mProgressionState;
	AnimationRegister mAnimationRegister;
	AnimationRegister mPersistantAnimationRegister;	// animation register to be used with art assets that are always needed such as the hud or the player ship.
	InputSystem& mInputSystem;
	ItemManager& mItemManager;
	std::shared_ptr<Layer> mWorldLayer;
	std::shared_ptr<Layer> mBackgroundLayer;
	std::shared_ptr<HudView> mHudView;
	Background mBackground;
	std::shared_ptr<PlayerShip> mPlayerShip;
	std::vector<std::shared_ptr<Bullet>> mBullets;
	std::vector<std::shared_ptr<EnemyShip>> mEnemyShips;
	std::vector<LiveSpawnGroup> mLiveSpawnGroups;
	std::vector<std::unique_ptr<Crate>> mCrates;
	std::vector<std::unique_ptr<Powerup>> mPowerups;
	int mCleanupCounter;
	int mSpawnsLeft;
	bool mLoaded;
	bool mBossSpawned;
	bool mSpawnsDisabled;
	std::function<void(bool)> mMissionCompleteCallback;
	float mSpawnDelay;
	const EnemyPool* mEnemyPool;

	void SimulateLiveSpawnGroups(float timestepSeconds);
	bool SpawnEnemies(float timestepSeconds);
	void SimulateComplete();
	void CleanupOffscreenObjects();
	void OnMissionComplete();
	void OnPlayerDie();
	void SetLoaded(bool loaded);
};
