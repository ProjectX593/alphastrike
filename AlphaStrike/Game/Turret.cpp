/*
Turret

A turret is a piece of an EnemyShip that can be individually shot at. Turrets may optionally always face
towards the player ship. Turrets don't shoot on their own, they must be commanded by their owning EnemyShip
to fire.
*/

#include <algorithm>
#include "AnimatedSprite.h"
#include "AnimationRegister.h"
#include "CircleCollider.h"
#include "Config.h"
#include "GameGlobals.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include "PlayerShip.h"
#include "Turret.h"

using std::shared_ptr;

Turret::Turret(const TurretGroupConfig& config, int posIndex, AnimationRegister& animationRegister, shared_ptr<Layer> worldLayer, shared_ptr<PlayerShip> playerShip):
	mPlayerShip(playerShip),
	mWeapon(config.mWeapon->Clone()),
	mHealth(config.mMaxHealth),
	mRotationSpeed((float)M_PI_4),
	mFacesPlayer(config.mFacesPlayer),
	mIdleAnimation(config.mIdleAnimation),
	mChargeAnimation(config.mChargeAnimation),
	mFireAnimation(config.mFireAnimation),
	mDischargeAnimation(config.mDischargeAnimation),
	mState(TurretState::idle),
	mInvulnerable(config.mInvulnerable),
	mShouldFire(false),
	mVolleyQueued(false)
{
	Vector2 pixelPos = config.mPositionsInPixels.at(posIndex);
	Vector2 pos = Vector2((float)pixelPos.x / PIXELS_IN_UNIT, (float)pixelPos.y / PIXELS_IN_UNIT);
	mSprite = std::make_shared<AnimatedSprite>(Vector2());
	mSprite->LoadAndPlayAnimation(animationRegister.RegisterAnimation(config.mAnimationsName), config.mIdleAnimation, true, PIXELS_IN_UNIT);
	mSprite->SetCenterPos(pos);

	// for now we just have one collider, later we will have the option of many
	mColliders.push_back(CircleCollider::AddToCenterOfNode(mSprite));

	if(mFacesPlayer)
		mSprite->SetRotation((float)M_PI);
}

Turret::~Turret()
{
}

// returns true if the turret reached it's destination
bool Turret::RotateTowards(float direction, float timestepSeconds)
{
	float turretRotation = mSprite->GetRotation();
	float rotationDiff = direction - turretRotation;
	while(rotationDiff < 0)
		rotationDiff += (float)M_PI * 2.0f;
	float rotation = std::min(mRotationSpeed * timestepSeconds, rotationDiff);
	if(rotationDiff < M_PI)
		mSprite->SetRotation(turretRotation + rotation);
	else
		mSprite->SetRotation(turretRotation - rotation);

	return rotationDiff < mRotationSpeed * timestepSeconds;
}

void Turret::Simulate(float timestepSeconds, World* world)
{
	if(mHealth <= 0 || !mSprite->IsVisible())
		return;

	if(mState == TurretState::preparingVolley)
	{
		if(RotateTowards(mVolleyInfo.mStartRotationRadians, timestepSeconds))
		{
			mState = TurretState::volleyReady;
			if(mVolleyInfo.mClockwise)
				mVolleyPastZero = mVolleyInfo.mStartRotationRadians < mVolleyInfo.mEndRotationRadians;
			else
				mVolleyPastZero = mVolleyInfo.mStartRotationRadians > mVolleyInfo.mEndRotationRadians;
		}
	}
	if(mState == TurretState::firingVolley)
	{
		if(abs(mVolleyInfo.mStartRotationRadians - mVolleyInfo.mEndRotationRadians) < 0.0001f)
		{
			mState = TurretState::volleyDone;
		}
		else if(mVolleyInfo.mClockwise)
		{
			mSprite->SetRotation(mSprite->GetRotation() + timestepSeconds * mVolleyInfo.mRotationSpeed);
			if(mVolleyPastZero && mSprite->GetRotation() > mVolleyInfo.mEndRotationRadians)
				mState = TurretState::volleyDone;
			else if(!mVolleyPastZero && mSprite->GetRotation() < mVolleyInfo.mEndRotationRadians)
				mVolleyPastZero = true;
		}
		else
		{
			mSprite->SetRotation(mSprite->GetRotation() - timestepSeconds * mVolleyInfo.mRotationSpeed);
			if(mVolleyPastZero && mSprite->GetRotation() < mVolleyInfo.mEndRotationRadians)
				mState = TurretState::volleyDone;
			else if(!mVolleyPastZero && mSprite->GetRotation() > mVolleyInfo.mEndRotationRadians)
				mVolleyPastZero = true;
		}
	}
	
	if(mFacesPlayer && !IsVolleyFiring())
	{
		// default position is facing upwards
		Vector2 direction = mPlayerShip->GetSprite()->GetAbsoluteCenter() - mSprite->GetAbsoluteCenter();
		RotateTowards(direction.GetAngleFromUpVector(), timestepSeconds);
	}

	mWeapon->Simulate(timestepSeconds, mState == TurretState::firing || mState == TurretState::firingVolley || mState == TurretState::volleyDone, mSprite, world, mSprite->GetParent().lock()->GetPos());
}

void Turret::FireOnce()
{
	SetFiring(true);
	SetFiring(false);
}

// If turret is already firing, it will complete it's animation - it does not jump to idle
void Turret::SetFiring(bool firing)
{
	mShouldFire = firing;

	if(mState == TurretState::idle && firing)
	{
		if(mSprite->HasAnimation(mChargeAnimation))
		{
			mSprite->PlayAnimation(mChargeAnimation);
			mSprite->SetAnimationCompleteCallback(std::bind(&Turret::DoneCharging, this));
			mState = TurretState::charging;
		}
		else
		{
			DoneCharging();
		}
	}
}

void Turret::DoneCharging()
{
	mState = TurretState::firing;
	mSprite->PlayAnimation(mFireAnimation);
	mSprite->SetAnimationCompleteCallback(std::bind(&Turret::DoneFiring, this));
}

void Turret::DoneFiring()
{
	if(mShouldFire)
	{
		mSprite->PlayAnimation(mFireAnimation);
		return;
	}

	if(mDischargeAnimation != "")
	{
		mState = TurretState::discharging;
		mSprite->PlayAnimation(mDischargeAnimation);
		mSprite->SetAnimationCompleteCallback(std::bind(&Turret::DoneDischarging, this));
	}
	else
	{
		DoneDischarging();
	}
}

void Turret::DoneDischarging()
{
	mSprite->PlayAnimation(mIdleAnimation);
	if(mVolleyQueued)
	{
		mState = TurretState::preparingVolley;
		mVolleyQueued = false;
	}
	else
	{
		mState = TurretState::idle;
	}
}

// returns true if the bullet hit, false if it did not
bool Turret::HandleCollision(std::shared_ptr<Bullet> bullet, AnimationRegister& animationRegister, std::shared_ptr<Layer> worldLayer)
{
	if(mInvulnerable)
		return false;

	for(const auto& collider : mColliders)
	{
		if(collider->IsCollidingWith(bullet->GetCollider()))
		{
			mHealth -= bullet->GetDamage();
			bullet->Hit(animationRegister, worldLayer);

			if(mHealth <= 0)
			{
				mInvulnerable = true;
				mSprite->SetVisible(false);	// this is possibly temporary - may have a death animation for turrets later, that would be nice
			}

			return true;
		}
	}

	return false;
}

void Turret::SetColliders(const std::vector<ColliderConfig>& colliders)
{
	for(auto& collider : mColliders)
		collider->RemoveFromParentAndCleanup();
	mColliders.clear();

	for(auto& colliderConfig : colliders)
	{
		mColliders.push_back(colliderConfig.GenerateCollider());
		mSprite->AddChild(mColliders[mColliders.size() - 1]);
	}
}

void Turret::PrepareVolleyFire(const TurretVolleyInfo& volleyInfo)
{
	mVolleyInfo = volleyInfo;
	if(mState == TurretState::idle)
		mState = TurretState::preparingVolley;
	else
		mVolleyQueued = true;
}

void Turret::StartVolleyFire()
{
	if(mState != TurretState::volleyReady)
		return;
	
	mState = TurretState::chargingVolley;
	mSprite->SetAnimationCompleteCallback(std::bind(&Turret::OnVolleyCharged, this));
	mSprite->PlayAnimation(mChargeAnimation);
}

void Turret::OnVolleyCharged()
{
	mState = TurretState::firingVolley;
	mSprite->PlayAnimation(mFireAnimation);
}

void Turret::StopVolleyFire()
{
	if(!IsVolleyFiring())
		return;
	mSprite->SetAnimationCompleteCallback(std::bind(&Turret::OnVolleyFireAnimDone, this));
}

void Turret::OnVolleyFireAnimDone()
{
	mSprite->SetAnimationCompleteCallback(std::bind(&Turret::OnVolleyDischarged, this));
	if(mDischargeAnimation != "")
	{
		mSprite->PlayAnimation(mDischargeAnimation);
		mState = TurretState::dischargingVolley;
	}
	else
	{
		OnVolleyDischarged();
	}
}

void Turret::OnVolleyDischarged()
{
	mState = TurretState::idle;
	mSprite->PlayAnimation(mIdleAnimation);
}

bool Turret::IsVolleyFiring()
{
	return mState == TurretState::preparingVolley || mState == TurretState::volleyReady || mState == TurretState::firingVolley || mState == TurretState::volleyDone || mState == TurretState::chargingVolley || mState == TurretState::dischargingVolley;
}