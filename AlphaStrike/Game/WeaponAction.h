#pragma once

#include <memory>
#include <vector>
#include "Vector2.h"

#include "cereal\archives\binary.hpp"
#include "cereal\archives\json.hpp"
#include "cereal\types\polymorphic.hpp"
#include "cereal\types\vector.hpp"

class BulletBehavior;

class WeaponAction
{
public:
	WeaponAction();
	WeaponAction(int damage, const Vector2& centerPosInPixels, const Vector2& initialVelocity, bool rotates, float delay, const std::vector<std::shared_ptr<BulletBehavior>>& bulletBehaviors, const std::string& bulletId);
	WeaponAction(const WeaponAction& other);
	WeaponAction& operator=(const WeaponAction& other);

	int mDamage;
	Vector2 mCenterPosInPixels;
	Vector2 mInitialVelocity;
	bool mRotates;
	float mDelay;
	std::string mBulletId;

	void AddBulletBehavior(std::shared_ptr<BulletBehavior> behavior, int index = -1);
	void RemoveBulletBehavior(int index);

	const std::vector<std::shared_ptr<BulletBehavior>>& GetBulletBehaviors() const { return mBulletBehaviors; }

	template <class Archive>
	void serialize(Archive& archive, const std::uint32_t version)
	{
		archive(mDamage, mCenterPosInPixels, mInitialVelocity, mRotates, mDelay, mBulletBehaviors, mBulletId);
	}

private:
	std::vector<std::shared_ptr<BulletBehavior>> mBulletBehaviors;

	void CopyBehaviors(const std::vector<std::shared_ptr<BulletBehavior>>& bulletBehaviors);
};

CEREAL_CLASS_VERSION(WeaponAction, 2);