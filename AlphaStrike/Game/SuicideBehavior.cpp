/*
SuicideBehavior

Destroys the enemy ship and deals damage to the player when the enemy comes within the specified distance of the player.
*/

#include "EnemyShip.h"
#include "PlayerShip.h"
#include "SuicideBehavior.h"
#include "World.h"

#include "cereal\archives\binary.hpp"
#include "cereal\archives\json.hpp"
CEREAL_REGISTER_TYPE(SuicideBehavior);
CEREAL_REGISTER_DYNAMIC_INIT(suicideB);

SuicideBehavior::SuicideBehavior():
	mSuicideDistance(0.0f),
	mDamage(0)
{

}

SuicideBehavior::SuicideBehavior(float suicideDistance, int damage, const std::string& explosionAnimationName):
	mSuicideDistance(suicideDistance),
	mDamage(damage),
	mExplosionAnimationName(explosionAnimationName)
{
}


SuicideBehavior::~SuicideBehavior()
{
}

std::shared_ptr<EnemyBehavior> SuicideBehavior::Copy()
{
	return std::make_shared<SuicideBehavior>(mSuicideDistance, mDamage, mExplosionAnimationName);
}

void SuicideBehavior::Run(float timestepSeconds)
{
	float dist = mEnemyShip->GetWorld()->GetPlayerShip()->GetSprite()->GetAbsoluteCenter().DistanceTo(mEnemyShip->GetSprite()->GetAbsoluteCenter());
	if(dist < mSuicideDistance)
	{
		mEnemyShip->SetExplosionAnimation(mExplosionAnimationName);
		mEnemyShip->TakeDamage(99999);	// OVERKILL
		mEnemyShip->GetWorld()->GetPlayerShip()->TakeDamage(mDamage);
	}
}