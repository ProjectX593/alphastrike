/*
HudView

The heads up display view that contains the player's health and ammo bars,
as well as an icon for their current secondary weapon.
*/

#include "AnimationRegister.h"
#include "DamageBorderView.h"
#include "GameGlobals.h"
#include "HudView.h"
#include "TextureInfo.h"

using std::make_shared;
using std::shared_ptr;
using std::string;

HudView::HudView(AnimationRegister& animationRegister):
	View(animationRegister)
{
}

HudView::~HudView()
{
}

// Hard coded initialization to avoid json or other files players can manipulate.
// Insufficient resources to produce a UI tool.
void HudView::Initialize(float screenWidthUnits)
{
	//Sprite(const Vector2& pos, float depth, float pixelsPerUnit, const TextureInfo& texInfo);
	shared_ptr<Sprite> statusBackground = make_shared<Sprite>(Vector2(-0.15f, 0.9f));
	statusBackground->SetTexture(mAnimationRegister.RegisterTexture("hudStatusBackground.png"), true, PIXELS_IN_UNIT);

	mSecondaryIcon = make_shared<Sprite>(Vector2(0.041667f, 0.05f));
	mSecondaryIcon->SetTexture(make_shared<TextureInfo>());

	mHealthBar = make_shared<FillBar>(Vector2(0.05f, 0.020833f));
	mHealthBar->SetTexture(mAnimationRegister.RegisterTexture("healthBar.png"), true, PIXELS_IN_UNIT);

	mAmmoBar = make_shared<FillBar>(Vector2(0.154167f, 0.070833f));
	mAmmoBar->SetTexture(mAnimationRegister.RegisterTexture("ammoBar.png"), true, PIXELS_IN_UNIT);

	statusBackground->AddChild(mSecondaryIcon);
	statusBackground->AddChild(mHealthBar);
	statusBackground->AddChild(mAmmoBar);
	AddChild(statusBackground);

	mDamageBorderView = std::make_shared<DamageBorderView>(mAnimationRegister);
	mDamageBorderView->Initialize(screenWidthUnits);
	AddChild(mDamageBorderView);
}

void HudView::OnResizeScreen(float screenWidthUnits)
{
	mDamageBorderView->OnResizeScreen(screenWidthUnits);
}

void HudView::UpdatePlayerStats(float healthPercent, float ammoPercent)
{
	mHealthBar->SetFillPercent(healthPercent);
	mAmmoBar->SetFillPercent(ammoPercent);
}

void HudView::SetWeaponIcon(const string& itemThumbnail)
{
	mSecondaryIcon->SetVisible(itemThumbnail != "");
	if(itemThumbnail != "")
		mSecondaryIcon->SetTexture(mAnimationRegister.RegisterTexture(itemThumbnail), true, PIXELS_IN_UNIT);
}

void HudView::OnPlayerDamaged()
{
	mDamageBorderView->OnPlayerDamaged();
}