/*
ReprocessingView

The part of the players home where they reprocess salvage into energy when they need it right away instead of waiting for a shop.
*/

#include "GameGlobals.h"
#include "HomeBase.h"
#include "ProgressionState.h"
#include "ReprocessingView.h"
#include "TextField.h"

const int ENERGY_CONVERT_0 = 1;
const int ENERGY_CONVERT_1 = 5;
const int ENERGY_CONVERT_2 = 25;

ReprocessingView::ReprocessingView(HomeBase& homeBase, AnimationRegister& animationRegister):
	View(animationRegister),
	mHomeBase(homeBase)
{
}

ReprocessingView::~ReprocessingView()
{
}

void ReprocessingView::Initialize()
{
	LoadFromJson("assets\\screens\\reprocessing.json", PIXELS_IN_UNIT);

	GetChild<Button>("turboliftButton")->SetMouseReleaseCallback(std::bind(&HomeBase::ShowTurbolift, &mHomeBase));

	auto& currencyManager = mHomeBase.GetProgressionState().GetCurrencyManager();
	int ratio = currencyManager.GetScrapPerEnergy();
	GetChild<TextField>("convert0ScrapLabel")->SetText(std::to_string(ENERGY_CONVERT_0 * ratio));
	GetChild<TextField>("convert0EnergyLabel")->SetText(std::to_string(ENERGY_CONVERT_0));
	GetChild<TextField>("convert1ScrapLabel")->SetText(std::to_string(ENERGY_CONVERT_1 * ratio));
	GetChild<TextField>("convert1EnergyLabel")->SetText(std::to_string(ENERGY_CONVERT_1));
	GetChild<TextField>("convert2ScrapLabel")->SetText(std::to_string(ENERGY_CONVERT_2 * ratio));
	GetChild<TextField>("convert2EnergyLabel")->SetText(std::to_string(ENERGY_CONVERT_2));

	GetChild<Button>("convertButton0")->SetMouseReleaseCallback(std::bind(&ReprocessingView::ConvertScrap, this, ENERGY_CONVERT_0 * ratio));
	GetChild<Button>("convertButton1")->SetMouseReleaseCallback(std::bind(&ReprocessingView::ConvertScrap, this, ENERGY_CONVERT_1 * ratio));
	GetChild<Button>("convertButton2")->SetMouseReleaseCallback(std::bind(&ReprocessingView::ConvertScrap, this, ENERGY_CONVERT_2 * ratio));
	GetChild<Button>("convertAllButton")->SetMouseReleaseCallback(std::bind(&ReprocessingView::ConvertAllScrap, this));

	RefreshBalances();
}

void ReprocessingView::RefreshBalances()
{
	auto& currencyManager = mHomeBase.GetProgressionState().GetCurrencyManager();
	int scrap = currencyManager.GetScrap();
	GetChild<TextField>("scrapLabel")->SetText(std::to_string(scrap));
	GetChild<TextField>("energyLabel")->SetText(std::to_string(currencyManager.GetEnergy()));

	int ratio = currencyManager.GetScrapPerEnergy();
	GetChild<TextField>("convertAllScrapLabel")->SetText(std::to_string(scrap / ratio * ratio));
	GetChild<TextField>("convertAllEnergyLabel")->SetText(std::to_string(scrap / ratio));

	GetChild<Button>("convertButton0")->SetDisabled(scrap / ratio < ENERGY_CONVERT_0);
	GetChild<Button>("convertButton1")->SetDisabled(scrap / ratio < ENERGY_CONVERT_1);
	GetChild<Button>("convertButton2")->SetDisabled(scrap / ratio < ENERGY_CONVERT_2);
	GetChild<Button>("convertAllButton")->SetDisabled(scrap < ratio);
}

void ReprocessingView::ConvertAllScrap()
{
	ConvertScrap(mHomeBase.GetProgressionState().GetCurrencyManager().GetScrap());
}

void ReprocessingView::ConvertScrap(int scrap)
{
	if(mHomeBase.GetProgressionState().GetCurrencyManager().GetScrap() < scrap)
		return;

	mHomeBase.GetProgressionState().GetCurrencyManager().ConvertScrapToEnergy(scrap);
	RefreshBalances();
}