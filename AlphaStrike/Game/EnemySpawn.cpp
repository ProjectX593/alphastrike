/*
EnemySpawn

This file contains several classes:
SpawnAnchor - A point on the X axis that appears between two given values, used to spawn enemies about a specified position.
EnemySpawn - An enemy ID to be spawned at a given position.
EnemyRangeSpawn - An enemy ID to be spawned somewhere between two X coordinates.
EnemySpawnGroup - NOT IMPLEMENTED (at the time of writing this comment)
*/

#include "EnemySpawn.h"

#include "cereal\archives\binary.hpp"
#include "cereal\archives\json.hpp"
CEREAL_REGISTER_TYPE(EnemyRangeSpawn);
CEREAL_REGISTER_DYNAMIC_INIT(rangeSpawn);

using std::shared_ptr;
using std::string;
using std::unique_ptr;
using std::vector;

// ---------------- EnemySpawn ---------------- //

EnemySpawn::EnemySpawn():
	mXPos(0.0f),
	mYPos(0.0f),
	mDelay(0.0f)
{
}

EnemySpawn::EnemySpawn(const EnemySpawnConfig& spawnConfig, float xPos, float yPos, float delay):
	mSpawnConfig(spawnConfig),
	mXPos(xPos), 
	mYPos(yPos),
	mDelay(delay)
{
}

const std::string& EnemySpawn::GetEnemyId() const
{
	return mSpawnConfig.mEnemyId;
}

const std::string& EnemySpawn::GetBehaviorSetId() const
{
	return mSpawnConfig.mBehaviorSetId;
}

// ---------------- EnemyRangeSpawn ---------------- //
EnemyRangeSpawn::EnemyRangeSpawn():
	mXMin(0.0f),
	mXMax(0.0f)
{
}

EnemyRangeSpawn::EnemyRangeSpawn(const EnemySpawnConfig& spawnConfig, float xMin, float xMax, float yPos, float delay):
	EnemySpawn(spawnConfig, 0.0f, yPos, delay),
	mXMin(xMin),
	mXMax(xMax)
{
}

float EnemyRangeSpawn::GetXPos() const
{
	//TODO: replace rand()
	return ((float)rand() / (float)RAND_MAX) * (mXMax - mXMin) + mXMin;
}

// ---------------- EnemySpawnGroup ---------------- //
EnemySpawnGroup::EnemySpawnGroup():
	mMinAnchorX(0.0f),
	mMaxAnchorX(0.0f),
	mMinLeaveTime(0.0f),
	mMaxLeaveTime(0.0f),
	mMinDifficulty(LevelDifficulty::easy),
	mMaxDifficulty(LevelDifficulty::easy),
	mLeaderIndex(-1),
	mName("unnamed")
{
}

EnemySpawnGroup::EnemySpawnGroup(LevelDifficulty minDifficulty, LevelDifficulty maxDifficulty, float minAnchorX, float maxAnchorX, float minLeaveTime, float maxLeaveTime, int leaderIndex):
	mMinAnchorX(minAnchorX),
	mMaxAnchorX(maxAnchorX),
	mMinLeaveTime(minLeaveTime),
	mMaxLeaveTime(maxLeaveTime),
	mMinDifficulty(minDifficulty),
	mMaxDifficulty(maxDifficulty),
	mLeaderIndex(leaderIndex),
	mName("unnamed")
{
}

EnemySpawnGroup::EnemySpawnGroup(const EnemySpawnGroup& other):
	mMinAnchorX(other.mMinAnchorX),
	mMaxAnchorX(other.mMaxAnchorX),
	mMinLeaveTime(other.mMinLeaveTime),
	mMaxLeaveTime(other.mMaxLeaveTime),
	mMinDifficulty(other.mMinDifficulty),
	mMaxDifficulty(other.mMaxDifficulty),
	mLeaderIndex(other.mLeaderIndex),
	mName(other.mName),
	mLeaveConfigs(other.mLeaveConfigs)
{
	TakeSpawns(other.mSpawns);
}

EnemySpawnGroup& EnemySpawnGroup::operator=(const EnemySpawnGroup& other)
{
	mMinAnchorX = other.mMinAnchorX;
	mMaxAnchorX = other.mMaxAnchorX;
	mMinLeaveTime = other.mMinLeaveTime;
	mMaxLeaveTime = other.mMaxLeaveTime;
	mMinDifficulty = other.mMinDifficulty;
	mMaxDifficulty = other.mMaxDifficulty;
	mLeaderIndex = other.mLeaderIndex;
	mName = other.mName;
	mLeaveConfigs = other.mLeaveConfigs;
	TakeSpawns(other.mSpawns);
	return *this;
}

void EnemySpawnGroup::TakeSpawns(const std::vector<std::unique_ptr<EnemySpawn>>& spawns)
{
	mSpawns.clear();
	for(const auto& spawn : spawns)
	{
		mSpawns.push_back(spawn->Copy());
	}
}