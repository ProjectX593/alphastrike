#pragma once

#include "View.h"

class AnimationRegister;

class MainMenu : public View
{
public:
	MainMenu(AnimationRegister& animationRegister, std::function<void(const std::string&)> newGameCallback, std::function<void()> quitCallback);

	void Initialize();

private:
	std::function<void(const std::string&)> mNewGameCallback;
	std::function<void()> mQuitCallback;

	void OnNewGameButton();
	void OnContinueButton();
	void OnOptionsButton();
};

