/*
ItemManager

Contains one of each item in the game, to be copied when needed.
This is similar to how Config handles drop tables and spawns, but there is enough
logic to justify this being it's own class.
*/

#include "Config.h"
#include <functional>
#include "ItemManager.h"

using namespace std::placeholders;
using std::shared_ptr;
using std::string;
using std::map;

ItemManager::ItemManager(AnimationRegister& animationRegister, const Config* config):
	mConfig(config)
{
}

ItemManager::~ItemManager()
{
}

const Item* ItemManager::GetItem(const std::string& itemId, ItemType type)
{
	if(type == ItemType::primaryWeapon)
	{
		if(mConfig->mPrimaryWeapons.find(itemId) != mConfig->mPrimaryWeapons.end())
			return &mConfig->mPrimaryWeapons.at(itemId);
	}
	else if(type == ItemType::secondaryWeapon)
	{
		if(mConfig->mSecondaryWeapons.find(itemId) != mConfig->mSecondaryWeapons.end())
			return &mConfig->mSecondaryWeapons.at(itemId);
	}
	else if(type == ItemType::armor)
	{
		if(mConfig->mArmors.find(itemId) != mConfig->mArmors.end())
			return &mConfig->mArmors.at(itemId);
	}

	return nullptr;
}

shared_ptr<Item> ItemManager::CreateItemFromId(const std::string& itemId, ItemType type)
{
	if(type == ItemType::primaryWeapon || type == ItemType::secondaryWeapon)
		return std::make_shared<Weapon>(*dynamic_cast<const Weapon*>(GetItem(itemId, type)));
	else if(type == ItemType::armor)
		return std::make_shared<Armor>(*dynamic_cast<const Armor*>(GetItem(itemId, type)));
	return nullptr;
}