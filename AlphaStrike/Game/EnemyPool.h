#pragma once

#include "EnemyConfig.h"
#include "EnemySpawn.h"

#include "cereal\types\map.hpp"
#include "cereal\types\vector.hpp"
#include "cereal\types\memory.hpp"

class EnemyPool
{
public:
	EnemyPool();
	~EnemyPool();

	void SetEnemyConfigs(const std::map<std::string, EnemyConfig>& enemyConfigs) { mEnemyConfigs = enemyConfigs; }
	std::map<std::string, EnemyConfig>& GetEditableEnemyConfigs() { return mEnemyConfigs; }	// returns a non const list to the caller, should only be used by the editor
	bool EnemyConfigExists(const std::string& enemyId) const { return mEnemyConfigs.find(enemyId) != mEnemyConfigs.end(); }
	const EnemyConfig& GetEnemyConfig(const std::string& enemyId) const { return mEnemyConfigs.at(enemyId); }
	void AddEnemyConfig(const std::string& enemyId, const EnemyConfig& enemyConfig);
	void DeleteEnemyConfig(const std::string& enemyId);
	const std::vector<EnemySpawnGroup>& GetEnemySpawnGroups() const { return mEnemySpawns; }
	std::vector<EnemySpawnGroup>& GetEditableEnemySpawnGroups() { return mEnemySpawns; }

	template<class Archive>
	void serialize(Archive& archive, const std::uint32_t version)
	{
		archive(mEnemyConfigs, mEnemySpawns);
	}

private:
	std::map<std::string, EnemyConfig> mEnemyConfigs;
	std::vector<EnemySpawnGroup> mEnemySpawns;
};

CEREAL_CLASS_VERSION(EnemyPool, 1);