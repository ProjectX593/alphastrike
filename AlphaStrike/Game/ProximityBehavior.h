#pragma once

#include "EnemyBehavior.h"

class ProximityBehavior : public EnemyBehavior
{
public:
	ProximityBehavior();
	ProximityBehavior(float detectionDistance, const std::string& behaviorSetId, float detectionArcStart = 0.0f, float detectionArcEnd = 0.0f);
	~ProximityBehavior();

	float mDetectionDistance;
	float mDetectionArcStart;
	float mDetectionArcEnd;
	std::string mBehaviorSetId;

	virtual std::shared_ptr<EnemyBehavior> Copy();
	virtual void Run(float timestepSeconds);
	virtual std::string GetDisplayString() { return "Proximity"; }

	template <class Archive>
	void serialize(Archive& archive, const std::uint32_t version)
	{
		archive(mDetectionDistance, mDetectionArcStart, mDetectionArcEnd, mBehaviorSetId, cereal::base_class<EnemyBehavior>(this));
	}

private:

};

CEREAL_CLASS_VERSION(ProximityBehavior, 1);
CEREAL_FORCE_DYNAMIC_INIT(proximityB);
