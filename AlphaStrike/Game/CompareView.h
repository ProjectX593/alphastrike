#pragma once

#include "View.h"

class HomeBase;
class Item;
class Weapon;

class CompareView : public View
{
public:
	CompareView(AnimationRegister& animationRegister, HomeBase& homeBase);
	~CompareView();

	void Initialize();
	void SetItems(std::shared_ptr<Item> equippedItem, std::shared_ptr<Item> newItem, const std::string& equippedItemPrefix = "", const std::string& newItemPrefix = "");

private:
	HomeBase& mHomeBase;
	std::shared_ptr<Item> mEquippedItem;
	float mLargestArmorSpeed;
	int mLargestArmorHP;
	float mLargestPrimaryDPS;
	float mLargestSecondaryDPS;

	void UpdateItemNode(std::shared_ptr<Node> itemNode, std::shared_ptr<Item> newItem, std::shared_ptr<Item> oldItem, const std::string& prefix);
	void UpdateBarNode(std::shared_ptr<Node> barNode, const std::string& statName, float oldPercent, float newPercent);
	float CalculateWeaponDamagePerSecond(std::shared_ptr<Weapon> weapon);
	void CalculateLargestStats();
};

