/*
ShootBehavior

A simple behavior that tells an enemy to shoot a specific weapon.
The shot count is the number of shots before the behavior deactivates.
If left out it shoots forever.
*/

#include "EnemyShip.h"
#include "ShootBehavior.h"

#include "cereal\archives\binary.hpp"
#include "cereal\archives\json.hpp"
CEREAL_REGISTER_TYPE(ShootBehavior);
CEREAL_REGISTER_DYNAMIC_INIT(shootB);

using std::shared_ptr;

ShootBehavior::ShootBehavior():
	mWeaponIndex(-1)
{
}

ShootBehavior::ShootBehavior(int weaponIndex, float duration):
	TimedBehavior(duration),
	mWeaponIndex(weaponIndex)
{
}

ShootBehavior::~ShootBehavior()
{
}

shared_ptr<EnemyBehavior> ShootBehavior::Copy()
{
	return std::make_shared<ShootBehavior>(mWeaponIndex, mTimer.GetDuration());
}

void ShootBehavior::Run(float timestepSeconds)
{
	TimedBehavior::Run(timestepSeconds);
	mEnemyShip->FireWeapon(mWeaponIndex);
}

void ShootBehavior::Reset()
{
	TimedBehavior::Reset();
}
