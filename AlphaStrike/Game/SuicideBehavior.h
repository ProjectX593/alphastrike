#pragma once

#include "EnemyBehavior.h"

class SuicideBehavior : public EnemyBehavior
{
public:
	SuicideBehavior();
	SuicideBehavior(float suicideDistance, int damage, const std::string& explosionAnimationName = "");
	~SuicideBehavior();

	virtual std::shared_ptr<EnemyBehavior> Copy();
	virtual void Run(float timestepSeconds);
	virtual std::string GetDisplayString() { return "Suicide"; }

	float GetSuicideDistance(){ return mSuicideDistance; }
	int GetDamage(){ return mDamage; }
	const std::string& GetExplosionAnimationName(){ return mExplosionAnimationName; }
	void SetSuicideDistance(float suicideDistance){ mSuicideDistance = suicideDistance; }
	void SetDamage(int damage){ mDamage = damage; }
	void SetExplosionAnimationName(const std::string& explosionAnimationName){ mExplosionAnimationName = explosionAnimationName; }

	template <class Archive>
	void serialize(Archive& archive, const std::uint32_t version)
	{
		archive(mSuicideDistance, mDamage, mExplosionAnimationName, cereal::base_class<EnemyBehavior>(this));
	}

private:
	float mSuicideDistance;
	int mDamage;
	std::string mExplosionAnimationName;
};

CEREAL_CLASS_VERSION(SuicideBehavior, 1);
CEREAL_FORCE_DYNAMIC_INIT(suicideB);