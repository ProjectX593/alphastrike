/*
EnemyShip

EnemyShips are AI controlled ships that attack the player. They currently have a very simplistic AI
that follows a set of steps repeatedly, and does not respond to the player - bullets they shoot may 
aim at the player however. This will be improved in the future as it doesn't allow the creation of
interesting enemies.
*/

#include <algorithm>
#include "AnimatedSprite.h"
#include "AnimationRegister.h"
#include "Bullet.h"
#include "CircleCollider.h"
#include "Config.h"
#include "EnemyBehavior.h"
#include "EnemyConfig.h"
#include "EnemyShip.h"
#include "EventManager.h"
#include "ExplodeBehavior.h"
#include "GameGlobals.h"
#include "Layer.h"
#include "PlatformUtil.h"
#include "PlayerShip.h"
#include "RandomGenerator.h"
#include "Sprite.h"
#include "TextureInfo.h"
#include "World.h"

using std::function;
using std::make_shared;
using std::shared_ptr;
using std::string;
using std::vector;

EnemyShip::EnemyShip(const Vector2& initialPos, const EnemyConfig& enemyConfig, const std::string& behaviorSetId, World* world):
	Ship(enemyConfig.mMaxHealth, make_shared<AnimatedSprite>(initialPos, D2DSize(0.0f, 0.0f, 11.0f)), world, enemyConfig.mMaxVelocity, enemyConfig.mAcceleration),
	mPos(initialPos),
	mDropsCrates(enemyConfig.mDropTableId != ""),
	mDropTableId(enemyConfig.mDropTableId),
	mDropChance(enemyConfig.mDropChance),
	mScrapDrop(enemyConfig.mScrapDrop),
	mDifficulty(enemyConfig.mDifficulty),
	mSeekerCount(0),
	mAssignLeaderOnDeath(false),
	mExploding(false),
	mLeaves(enemyConfig.mLeaves),
	mLeaving(false),
	mHaltDuration(0.0f),
	mHasEnteredScreenSpace(false)
{
	mShipSprite->LoadAndPlayAnimation(world->GetAnimationRegister().RegisterAnimation(enemyConfig.mAnimationName), enemyConfig.mInitialAnimation, true, PIXELS_IN_UNIT);
	world->GetWorldLayer()->AddChild(mShipSprite);
	mWeapons = enemyConfig.mWeapons;
	mWeaponTriggers.insert(mWeaponTriggers.begin(), mWeapons.size(), false);
	mShipSprite->SetPos(mPos);

	for(auto& weapon : mWeapons)
		weapon.StopActions();

	for(const auto& turretConfig : enemyConfig.mTurretGroupConfigs)
		AddTurretGroup(turretConfig);

	// need to make our own copy of the behaviors
	const auto& behaviorSets = enemyConfig.GetBehaviorSets();
	for(const auto& behaviorSetPair : behaviorSets)
	{
		BehaviorSet newSet;
		for(const auto& behavior : behaviorSetPair.second)
		{
			shared_ptr<EnemyBehavior> cpy = behavior->Copy();
			cpy->Initialize(this);
			newSet.push_back(cpy);
		}
		mBehaviorSets[behaviorSetPair.first] = newSet;
	}

	SetActiveBehaviorSetId(behaviorSetId);

	if(enemyConfig.mReflectAnimationName != "")
		SetReflectTexture(enemyConfig.mReflectAnimationName, DEFAULT_ANIMATION);

	if((int)enemyConfig.mColliderConfigs.size() > 0)
	{
		for(auto colliderConfig : enemyConfig.mColliderConfigs)
		{
			auto newCollider = colliderConfig.GenerateCollider();
			mColliders.push_back(newCollider);
			mShipSprite->AddChild(newCollider);
		}
	}
	else
	{
		mColliders.push_back(CircleCollider::AddToCenterOfNode(mShipSprite));
	}
}

EnemyShip::~EnemyShip()
{
}

void EnemyShip::AddTurretGroup(const TurretGroupConfig& turretGroup)
{
	for(int i = 0; i < (int)turretGroup.mPositionsInPixels.size(); i++)
	{
		mTurrets.push_back(Turret(turretGroup, i, mWorld->GetAnimationRegister(), mWorld->GetWorldLayer(), mWorld->GetPlayerShip()));
		if(turretGroup.mColliderConfigs.size() > 0)
			mTurrets[mTurrets.size() - 1].SetColliders(turretGroup.mColliderConfigs);
		mTurrets[mTurrets.size() - 1].GetSprite()->SetVisible(turretGroup.mVisible);
		mShipSprite->AddChild(mTurrets.at(mTurrets.size() - 1).GetSprite());
	}
}

void EnemyShip::FireWeapon(int weaponIndex)
{
	//TODO: Assert upon trying to fire invalid weapon
	mWeaponTriggers[weaponIndex] = true;
}

void EnemyShip::Simulate(float timestepSeconds)
{
	Ship::Simulate(timestepSeconds);

	if(mHaltDuration > 0.0f)
	{
		float time = timestepSeconds;
		timestepSeconds -= mHaltDuration;	// we do this so the EnemyShip only executes non-halted time
		mHaltDuration -= time;
		if(timestepSeconds < 0.0f)
			return;
	}

	if(mHealth <= 0 && !mExploding)
	{
		// just clear the active behavior set and explode, better than adding a new set with some reserved word since the ship is about to be deleted
		mExploding = true;
		auto& activeBS = mBehaviorSets.at(mActiveBehaviorSetId);
		activeBS.clear();
		shared_ptr<ExplodeBehavior> expl = make_shared<ExplodeBehavior>(0.0f, mExplosionAnimationName);
		expl->Initialize(this);
		activeBS.push_back(expl);
	}

	if(!mExploding)
	{
		for(auto& turret : mTurrets)
			turret.Simulate(timestepSeconds, mWorld);

		for(unsigned int i = 0; i < mWeapons.size(); i++)
		{
			mWeapons[i].Simulate(timestepSeconds, mWeaponTriggers[i], mShipSprite, mWorld);
			mWeaponTriggers[i] = false;
		}
	}

	// do this after we do our actions so the move actions take effect
	mShipSprite->SetPos(mPos);

	RunBehaviors(timestepSeconds);
}

void EnemyShip::RunBehaviors(float timestepSeconds)
{
	if(mBehaviorSets.find(mActiveBehaviorSetId) == mBehaviorSets.end())
	{
		//TODO: Assert
		return;
	}

	auto& activeBS = mBehaviorSets.at(mActiveBehaviorSetId);
	for(const auto& behavior : activeBS)
	{
		behavior->Run(timestepSeconds);
	}
}

void EnemyShip::MoveBy(const Vector2& vector)
{
	mPos += vector;
}

void EnemyShip::SetPos(const Vector2& pos)
{
	mPos = pos;
}

void EnemyShip::SetCenterPos(const Vector2& centerPos)
{
	SetPos(centerPos);
	MoveBy(Vector2(-mShipSprite->GetWidth() / 2.0f, -mShipSprite->GetHeight() / 2.0f));
}

void EnemyShip::OnDie(World* world)
{
	if(mDropsCrates && mWorld->GetRandomGenerator().RandomInRange(0.0f, 1.0f) < mDropChance)
	{
		world->SpawnCrate(mShipSprite->GetCenter(), mDropTableId);
	}
	else if(mScrapDrop > 0)
	{
		world->SpawnScrapPowerup(mShipSprite->GetCenter(), (int)(mScrapDrop * mWorld->GetRandomGenerator().RandomInRange(0.7f, 1.3f) + 1.0f));	// randomize the scrap drop a little
	}

	if(mAssignLeaderOnDeath && mFollowers.size() > 0)
	{
		mFollowers[0]->SetLeader(std::weak_ptr<EnemyShip>());
		for(size_t i = 1; i < mFollowers.size(); i++)
			mFollowers[i]->SetLeader(mFollowers[0]);
	}
	else
	{
		for(const auto& follower : mFollowers)
			follower->SetLeader(std::weak_ptr<EnemyShip>());
	}

	Ship::OnDie();
}

void EnemyShip::SetLeader(std::weak_ptr<EnemyShip> leader)
{
	if(mLeaving || mLeader.lock() == leader.lock())
		return;

	mLeader = leader;
	std::shared_ptr<EnemyShip> l = mLeader.lock();
	if(l != nullptr)
		l->AddFollower(shared_from_this());

	if(l == nullptr)
	{
		auto& activeBS = mBehaviorSets.at(mActiveBehaviorSetId);
		for(const auto& behavior : activeBS)
			behavior->SetLeaderBehavior(std::weak_ptr<EnemyBehavior>());
	}
	else
	{
		mBehaviorSets.clear();
		const auto& leaderBehaviorSets = l->GetBehaviorSets();
		for(const auto& leaderBehaviorSetPair : leaderBehaviorSets)
		{
			mBehaviorSets[leaderBehaviorSetPair.first] = BehaviorSet();
			for(const auto& leaderBehavior : leaderBehaviorSetPair.second)
			{
				shared_ptr<EnemyBehavior> behavior = leaderBehavior->Copy();
				behavior->Initialize(this);
				behavior->SetLeaderBehavior(leaderBehavior);
				mBehaviorSets[leaderBehaviorSetPair.first].push_back(behavior);
			}
		}

		mActiveBehaviorSetId = l->GetActiveBehaviorSetId();
	}
}

void EnemyShip::SetActiveBehaviorSetId(const std::string& behaviorSetId)
{
	if(mLeaving)
		return;
	if(mBehaviorSets.find(behaviorSetId) == mBehaviorSets.end())
	{
		//TODO: Assert
		return;
	}
	mActiveBehaviorSetId = behaviorSetId;
}

void EnemyShip::AddFollower(std::shared_ptr<EnemyShip> follower)
{
	mFollowers.push_back(follower);
}

void EnemyShip::SetAssignLeaderOnDeath(bool assignLeaderOnDeath)
{
	mAssignLeaderOnDeath = assignLeaderOnDeath;
}

bool EnemyShip::HandleCollision(std::shared_ptr<Bullet> bullet)
{
	// if exploding ignore bullets
	if(mExploding)
		return false;

	// can't damage the ship itself until all vulnerable turrets have been shot off
	bool hasVulnerableTurret = false;
	if(mTurrets.size() > 0)
	{
		for(auto& turret : mTurrets)
		{
			if(turret.IsInvulnerable())
				continue;
			if(turret.HandleCollision(bullet, mWorld->GetAnimationRegister(), mWorld->GetWorldLayer()))
				return true;
			hasVulnerableTurret = true;
		}
	}

	if(hasVulnerableTurret)
		return false;
	
	if(IsCollidingWith(bullet->GetCollider()))
	{
		if(mReflective)
		{
			bullet->Reflect();
		}
		else
		{
			TakeDamage(bullet->GetDamage());
			bullet->Hit(mWorld->GetAnimationRegister(), mWorld->GetWorldLayer());
		}
		return true;
	}

	return false;
}

void EnemyShip::Halt(float duration)
{
	mHaltDuration = duration;
}

void EnemyShip::TakeDamage(int damage)
{
	// If there are any live turrets, take no damage - note that we do NOT call the superclass TakeDamage since it causes a visual effect.
	for(auto& turret : mTurrets)
	{
		if(!turret.IsInvulnerable() && !turret.IsDead())
			return;
	}

	if(mHaltDuration > 0.0f)
		return;
	Ship::TakeDamage(damage);
}

bool EnemyShip::IsDead()
{
	if(mExploding)
	{
		// we assume that after all health is gone, only the explode behavior remains
		return !mBehaviorSets.at(mActiveBehaviorSetId).at(0)->IsBlocking();
	}

	return false;
}

void EnemyShip::SetExplosionAnimation(const string& explosionAnimationName)
{
	mExplosionAnimationName = explosionAnimationName;
}

void EnemyShip::AddSeeker()
{
	mSeekerCount++;
}

void EnemyShip::RemoveSeeker()
{
	mSeekerCount = std::max(0, mSeekerCount - 1);
}

int EnemyShip::GetSeekerCount() const
{
	return mSeekerCount;
}

void EnemyShip::Leave(LeaveType type, float acceleration, float xSpawnPos)
{
	if(mLeaving || !mLeaves)
		return;

	SetLeader(std::weak_ptr<EnemyShip>());
	mLeaving = true;

	if(mBehaviorSets.find(mActiveBehaviorSetId) == mBehaviorSets.end())
	{
		//TODO: Assert
		return;
	}

	auto& bs = mBehaviorSets.at(mActiveBehaviorSetId);
	bs.push_back(std::make_shared<LeaveBehavior>(type, acceleration, xSpawnPos));
	bs.at(bs.size() - 1)->Initialize(this);
}