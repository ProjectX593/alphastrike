#pragma once

#include "Item.h"
#include "Powerup.h"

class ItemPowerup : public Powerup
{
public:
	ItemPowerup(const Vector2& initialCenterPos, const std::string& itemId, ItemType type, AnimationRegister& animationRegister, std::shared_ptr<Layer> worldLayer);
	virtual ~ItemPowerup();

	virtual void OnDie(ProgressionState& progressionState);

private:
	std::string mItemId;
	ItemType mType;

	std::string AnimationNameFromItemType(ItemType type);
};

