/*
ItemView

A small subview used to display the player's inventory.
*/

#include "AnimationRegister.h"
#include "CompareView.h"
#include "GameGlobals.h"
#include "ItemView.h"
#include "ProgressionState.h"

ItemView::ItemView(AnimationRegister& animationRegister, ProgressionState& progressionState, std::shared_ptr<CompareView> compareView, bool canEquipItems):
	View(animationRegister),
	mProgressionState(progressionState),
	mCompareView(compareView),
	mCanEquipItems(canEquipItems)
{
}

ItemView::~ItemView()
{
}

void ItemView::Initialize()
{
	LoadFromJson("assets\\screens\\components\\ItemView.json", PIXELS_IN_UNIT);

	for(int i = 0; i < mProgressionState.GetMaxItems(); i++)
	{
		auto& button = GetItemButton(i);
		button->SetMouseReleaseCallback(bind(&ItemView::EquipItem, this, i));
		button->SetMouseOverCallback(bind(&ItemView::MouseOverItem, this, i));
		button->SetMouseOutCallback(bind(&ItemView::MouseOutItem, this));
	}

	RefreshItems();
}

void ItemView::RefreshItems()
{
	mItemTypes.clear();
	mItemGUIDs.clear();

	for(int i = 0; i < mProgressionState.GetMaxItems(); i++)
	{
		GetItemImage(i)->SetVisible(false);
		GetItemButton(i)->SetDisabled(true);
	}

	AddItemsOfType(ItemType::primaryWeapon);
	AddItemsOfType(ItemType::secondaryWeapon);
	AddItemsOfType(ItemType::armor);
}

void ItemView::AddItemsOfType(ItemType type)
{
	auto& items = mProgressionState.GetItemsOfType(type);
	for(auto& item : items)
	{
		if(mProgressionState.GetEquippedItemOfType(type) != nullptr && item.second->GetGUID() == mProgressionState.GetEquippedItemOfType(type)->GetGUID())
			continue;

		int index = (int)mItemTypes.size();
		mItemTypes.push_back(type);
		mItemGUIDs.push_back(item.second->GetGUID());

		auto img = GetItemImage(index);
		auto btn = GetItemButton(index);
		img->SetTexture(mAnimationRegister.RegisterTexture(item.second->mThumbnail));
		img->SetVisible(true);
		btn->SetDisabled(!mCanEquipItems);
	}
}

void ItemView::EquipItem(int index)
{
	if(!mCanEquipItems)
		return;

	mProgressionState.SetEquippedItem(mItemGUIDs.at(index), mItemTypes.at(index));

	RefreshItems();

	if(mItemMouseOverCallback != nullptr && index < (int)mItemGUIDs.size())
		mItemMouseOverCallback(mProgressionState.GetItemWithGUID(mItemGUIDs.at(index), mItemTypes.at(index)));
	if(index >= (int)mItemGUIDs.size())
		mItemMouseOutCallback();
	if(mEquipCallback != nullptr)
		mEquipCallback();
}

void ItemView::MouseOverItem(int index)
{
	if(mItemMouseOverCallback != nullptr && index < (int)mItemGUIDs.size())
		mItemMouseOverCallback(mProgressionState.GetItemWithGUID(mItemGUIDs.at(index), mItemTypes.at(index)));
}

void ItemView::MouseOutItem()
{
	if(mItemMouseOutCallback != nullptr)
		mItemMouseOutCallback();
}

std::shared_ptr<Button> ItemView::GetItemButton(int index)
{
	return GetChild<Button>("item" + std::to_string(index));
}

std::shared_ptr<Sprite> ItemView::GetItemImage(int index)
{
	return GetChild<Sprite>("itemImage" + std::to_string(index));
}