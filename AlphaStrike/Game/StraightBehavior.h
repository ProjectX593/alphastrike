#pragma once

#include "BulletBehavior.h"

class StraightBehavior : public BulletBehavior
{
public:
	StraightBehavior();
	StraightBehavior(const Vector2& targetVelocity, float duration = 0.0f);
	StraightBehavior(const Vector2& targetVelocity, float turnRate, float turnDuration = -1.0f, float duration = 0.0f);	// turn rate is in radians per second, duration < 0 means turn forever
	virtual ~StraightBehavior();

	virtual std::shared_ptr<BulletBehavior> Copy();
	virtual void Run(float timestepSeconds);
	virtual void Reset();
	virtual std::string GetDisplayString();
	virtual void Reflect();

	const Vector2& GetTargetVelocity() const { return mTargetVelocity; }
	float GetTurnRate() const { return mTurnRate; }
	float GetTurnDuration() const { return mTurnDuration; }
	void SetTargetVelocity(const Vector2& targetVelocity) { mTargetVelocity = targetVelocity; }
	void SetTurnRate(float turnRate) { mTurnRate = turnRate; }
	void SetTurnDuration(float turnDuration) { mTurnDuration = turnDuration; }

	template <class Archive>
	void serialize(Archive& archive, const std::uint32_t version)
	{
		archive(mTargetVelocity, mTurnRate, mTurnDuration, cereal::base_class<BulletBehavior>(this));
	}

private:
	Vector2 mTargetVelocity;
	float mTurnRate;
	float mTurnDuration;
	float mTurnTimeLeft;
};

CEREAL_CLASS_VERSION(StraightBehavior, 1);
CEREAL_FORCE_DYNAMIC_INIT(straightBB);