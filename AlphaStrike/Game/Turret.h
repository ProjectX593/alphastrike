#pragma once

#include "ManualTimer.h"
#include <memory>
#include "TurretVolleyBehavior.h"
#include "Weapon.h"
#include <vector>

class AnimatedSprite;
class AnimationRegister;
class Collider;
struct ColliderConfig;
class Layer;
class PlayerShip;
struct TurretGroupConfig;

enum class TurretState
{
	idle,
	charging,
	firing,
	discharging,
	preparingVolley,
	volleyReady,
	chargingVolley,
	firingVolley,
	dischargingVolley,
	volleyDone
};

class Turret
{
public:
	Turret(const TurretGroupConfig& config, int posIndex, AnimationRegister& animationRegister, std::shared_ptr<Layer> worldLayer, std::shared_ptr<PlayerShip> playerShip);
	~Turret();

	void Simulate(float timestepSeconds, World* world);
	void FireOnce();
	bool IsVisible() const { return mSprite->IsVisible(); }
	void SetFiring(bool firing);
	std::shared_ptr<AnimatedSprite> GetSprite() { return mSprite; }
	bool HandleCollision(std::shared_ptr<Bullet> bullet, AnimationRegister& animationRegister, std::shared_ptr<Layer> worldLayer);
	void SetColliders(const std::vector<ColliderConfig>& colliders);
	void PrepareVolleyFire(const TurretVolleyInfo& volleyInfo);
	void StartVolleyFire();
	void StopVolleyFire();

	bool IsInvulnerable() const { return mInvulnerable || !IsVisible(); }
	bool IsDead() { return mHealth <= 0; }
	TurretState GetTurretState(){ return mState; }

private:
	std::shared_ptr<Weapon> mWeapon;	//TODO: Try changing to a unique_ptr once VS 2015 is available (causes compiler errors due to no move constructor)
	std::shared_ptr<PlayerShip> mPlayerShip;
	std::shared_ptr<AnimatedSprite> mSprite;
	int mHealth;
	float mRotationSpeed;	// in radians per second
	bool mFacesPlayer;
	bool mInvulnerable;
	std::string mIdleAnimation;
	std::string mChargeAnimation;
	std::string mFireAnimation;
	std::string mDischargeAnimation;
	TurretState mState;
	bool mShouldFire;
	bool mVolleyPastZero;	// Indicates if the turret rotated past 0 degrees, in the case that it's rotation takes it past that point
	bool mVolleyQueued;		// if true, the turret should begin volley firing as soon as it finishes it's current firing sequence
	std::vector<std::shared_ptr<Collider>> mColliders;
	TurretVolleyInfo mVolleyInfo;

	void DoneCharging();
	void DoneFiring();
	void DoneDischarging();
	bool RotateTowards(float direction, float timestepSeconds);
	void OnVolleyCharged();
	void OnVolleyFireAnimDone();
	void OnVolleyDischarged();
	bool IsVolleyFiring();	// checks if the turret is in any of the volley fire states
};

