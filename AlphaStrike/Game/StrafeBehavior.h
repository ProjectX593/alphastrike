#pragma once

#include "EnemyBehavior.h"

class StrafeBehavior : public EnemyBehavior
{
public:
	StrafeBehavior();
	StrafeBehavior(float xRange, float yRange, float speed, bool randomStartDirection = true);
	virtual ~StrafeBehavior();

	virtual std::shared_ptr<EnemyBehavior> Copy();
	virtual void Run(float timestepSeconds);
	virtual void Reset();
	virtual std::string GetDisplayString() { return "Strafe"; }

	float GetXRange(){ return mXRange; }
	float GetYRange(){ return mYRange; }
	float GetSpeed(){ return mSpeed; }
	bool GetIsRandomStartDirection(){ return mRandomStartDirection; }
	void SetXRange(float xRange){ mXRange = xRange; }
	void SetYRange(float yRange){ mYRange = yRange; }
	void SetSpeed(float speed){ mSpeed = speed; }
	void SetIsRandomStartDirection(bool isRandomStartDirection){ mRandomStartDirection = isRandomStartDirection; }

	template <class Archive>
	void serialize(Archive& archive, const std::uint32_t version)
	{
		archive(mXRange, mYRange, mSpeed, mRandomStartDirection, cereal::base_class<EnemyBehavior>(this));
	}

private:
	float mXRange;
	float mYRange;
	float mSpeed;
	bool mMovingLeft;
	bool mMovingUp;
	bool mRandomStartDirection;
	float mXDistanceTravelled;
	float mYDistanceTravelled;
};

CEREAL_CLASS_VERSION(StrafeBehavior, 1);
CEREAL_FORCE_DYNAMIC_INIT(strafeB);