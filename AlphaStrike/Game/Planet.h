#pragma once

#include "Background.h"
#include "EnemySpawn.h"
#include <fstream>
#include <string>
#include <vector>
#include "Vector2.h"

class Config;
class RandomGenerator;

struct PlanetFGImage
{
	PlanetFGImage(){};
	PlanetFGImage(const std::string& smallFGImage, const std::string& largeFGImage): mSmallFGImage(smallFGImage), mLargeFGImage(largeFGImage) {}
	std::string mSmallFGImage;
	std::string mLargeFGImage;
};

class PlanetConfig
{
public:
	PlanetConfig();
	PlanetConfig(const std::string& planetId, const std::string& description, const std::string& bgImage, const std::vector<std::string>& spawnGroupIds, LevelDifficulty minDifficulty, LevelDifficulty maxDifficulty, const std::vector < std::vector < std::string >> &backgroundLayerImages);

	std::string GetPlanetId() const { return mPlanetId; }
	std::string GetDescription() const { return mDescription; }
	LevelDifficulty GetRandomDifficulty(RandomGenerator& randomGenerator) const;
	const std::vector<std::string>& GetSpawnGroupIds() const { return mSpawnGroupIds; }
	const std::string& GetBGImage() const { return mBGImage; }
	const std::vector<std::vector<std::string>>& GetBackgroundLayerImages() const { return mBackgroundLayerImages; }

private:
	std::string mPlanetId;
	std::string mDescription;
	std::string mBGImage;
	std::vector<std::string> mSpawnGroupIds;
	std::vector<std::vector<std::string>> mBackgroundLayerImages;
	LevelDifficulty mMinDifficulty;
	LevelDifficulty mMaxDifficulty;
};

class Planet
{
public:
	Planet();
	Planet(const Config* config);
	Planet(const PlanetConfig& planetConfig, RandomGenerator& randomGenerator, const Config* config);
	~Planet();

	void SetConfig(const Config* config);

	LevelDifficulty GetDifficulty() const { return mDifficulty; }
	const std::string& GetPlanetId() const { return mPlanetId; }
	const std::vector<std::string>* GetSpawns();
	const PlanetConfig& GetPlanetConfig() const;

	template<class Archive>
	void serialize(Archive& archive, const std::uint32_t version)
	{
		archive(mPlanetId, mDifficulty);
	}
	
private:
	const Config* mConfig;
	std::string mPlanetId;
	LevelDifficulty mDifficulty;
};

CEREAL_CLASS_VERSION(Planet, 1);