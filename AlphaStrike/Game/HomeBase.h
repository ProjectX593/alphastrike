#pragma once

#include <memory>

class AnimationRegister;
class Config;
class ItemManager;
class Layer;
class ProgressionState;
class World;
class View;

class HomeBase
{
public:
	HomeBase(ProgressionState& progressionState, ItemManager& itemManager, const Config& config, std::shared_ptr<Layer> frontendLayer, AnimationRegister& animationRegister, World& world);
	~HomeBase();
	HomeBase(const HomeBase& other) = delete;

	void ShowTurbolift();
	void ShowBridge();
	void ShowHangarBay();
	void ShowReprocessing();
	void ShowCargoBay();
	void ShowShop();
	void ShowEngineering();
	void ShowEscapePod();
	void CloseCurrentView();
	void StartMission();

	ProgressionState& GetProgressionState() { return mProgressionState; }
	ItemManager& GetItemManager() { return mItemManager; }
	const Config& GetConfig() { return mConfig; }
	AnimationRegister& GetAnimationRegister() { return mAnimationRegister; }

private:
	std::weak_ptr<View> mCurrentView;
	ProgressionState& mProgressionState;
	ItemManager& mItemManager;
	const Config& mConfig;
	std::shared_ptr<Layer> mFrontendLayer;
	AnimationRegister& mAnimationRegister;
	World& mWorld;

	void LoadView(std::shared_ptr<View> view);
	void MissionComplete(bool success);
};

