#pragma once

#include <string>
#include <memory>
#include "Vector2.h"

class AnimatedSprite;
class AnimationRegister;
class Layer;
class ProgressionState;

class Powerup
{
public:
	Powerup(const Vector2& initialCenterPos, const std::string& animationName, AnimationRegister& animationRegister, std::shared_ptr<Layer> worldLayer);
	virtual ~Powerup();

	void Simulate(float timestepSeconds);
	std::shared_ptr<AnimatedSprite> GetSprite() { return mSprite; }
	virtual void OnDie(ProgressionState& progressionState) = 0;
	void Cleanup();

protected:
	Powerup();

private:
	Vector2 mPos;
	std::shared_ptr<AnimatedSprite> mSprite;
};

