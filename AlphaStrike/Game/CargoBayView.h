#pragma once

#include "View.h"

class CompareView;
class HomeBase;
class Item;
class ItemView;

class CargoBayView : public View
{
public:
	CargoBayView(HomeBase& homeBase, AnimationRegister& animationRegister);
	~CargoBayView();

	void Initialize();

private:
	HomeBase& mHomeBase;
	std::shared_ptr<CompareView> mCompareView;
	std::shared_ptr<ItemView> mItemView;

	void UpdateCompareView();
	std::shared_ptr<Item> GetNextItem();
	void OnItemMousedOver(std::shared_ptr<Item> item);
	void OnItemMousedOut();
	void OnEquipButton();
	void OnStoreButton();
	void OnRecycleButton();
	void OnTurboliftButton();
};
