/*
EnemyPool

Contains configs for a set of enemies. Used with EnemyGroupPool to spawn groups of enemies.
*/

#include "EnemyPool.h"

EnemyPool::EnemyPool()
{
}

EnemyPool::~EnemyPool()
{
}

void EnemyPool::AddEnemyConfig(const std::string& enemyId, const EnemyConfig& enemyConfig)
{
	if(mEnemyConfigs.find(enemyId) == mEnemyConfigs.end())
		mEnemyConfigs[enemyId] = enemyConfig;
	else
		throw new std::runtime_error("ERROR: Tried to add duplicate enemy config to enemy pool.");
}

void EnemyPool::DeleteEnemyConfig(const std::string& enemyId)
{
	if(mEnemyConfigs.find(enemyId) == mEnemyConfigs.end())
		throw new std::runtime_error("ERROR: Tried to delete enemy config that doesn't exist.");
	mEnemyConfigs.erase(mEnemyConfigs.find(enemyId));
}