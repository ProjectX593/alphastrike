#pragma once

#include "ManualTimer.h"
#include "RandomBehavior.h"
#include "Vector2.h"

class WanderBehavior : public RandomBehavior
{
public:
	WanderBehavior();
	WanderBehavior(float velocity, float directionChangeInterval, float directionChangeFactor, float lowerYBoundary = 0.7f);
	~WanderBehavior();

	virtual std::shared_ptr<EnemyBehavior> Copy();
	virtual void Run(float timestepSeconds);
	virtual void Reset();
	virtual void SetLeaderBehavior(std::weak_ptr<EnemyBehavior> behavior);
	virtual std::string GetDisplayString() { return "Wander"; }

	float GetVelocity() const { return mVelocity; }
	float GetDirectionChangeInterval() const { return mDirectionChangeInterval; }
	float GetDirectionChangeFactor() const { return mDirectionChangeFactor; }
	float GetLowerYBoundary() const { return mLowerYBoundary; }
	void SetVelocity(float velocity){ mVelocity = velocity; }
	void SetDirectionChangeInterval(float directionChangeInterval){ mDirectionChangeInterval = directionChangeInterval; }
	void SetDirectionChangeFactor(float directionChangeFactor){ mDirectionChangeFactor = directionChangeFactor; }
	void SetLowerYBoundary(float lowerYBoundary){ mLowerYBoundary = lowerYBoundary; }

	template <class Archive>
	void serialize(Archive& archive, const std::uint32_t version)
	{
		archive(mVelocity, mDirectionChangeInterval, mDirectionChangeFactor, mTimer, mLowerYBoundary, cereal::base_class<RandomBehavior>(this));
	}

private:
	float mVelocity;
	float mDirectionChangeInterval;
	float mDirectionChangeFactor;
	float mLowerYBoundary;
	ManualTimer mTimer;
	Vector2 mCurrentVelocity;
	Vector2 mIntendedVelocity;
	std::uniform_real_distribution<float> mRandomDist;
	std::uniform_real_distribution<float> mSmallOffsetDist;
	bool mFollowingLeader;
	bool mForcedToCenter;
	Vector2 mLeaderOffset;

	bool OutsideBounds();
	void ResetIntendedVelocity(bool forceToCenter = false);
};

CEREAL_CLASS_VERSION(WanderBehavior, 2);
CEREAL_FORCE_DYNAMIC_INIT(wanderB);