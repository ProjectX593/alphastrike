#pragma once

#include <string>

#define BULLET_DEPTH 15.0f
#define PIXELS_IN_UNIT 240
#define SCREEN_LEFT_EDGE -0.5f	// Result of ((240-480)/2) / 480
#define SCREEN_RIGHT_EDGE 1.5f
#define SCREEN_TOP_EDGE 0.0f
#define SCREEN_BOTTOM_EDGE 1.0f
#define FRONTEND_LAYER_DEPTH 2.0f
#define WORLD_LAYER_DEPTH 1.0f
#define BACKGROUND_LAYER_DEPTH 0.0f
