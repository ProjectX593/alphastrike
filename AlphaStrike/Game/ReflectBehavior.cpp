#include "EnemyShip.h"
#include "ReflectBehavior.h"

#include "cereal\archives\binary.hpp"
#include "cereal\archives\json.hpp"
CEREAL_REGISTER_TYPE(ReflectBehavior);
CEREAL_REGISTER_DYNAMIC_INIT(reflectB);

using std::shared_ptr;

ReflectBehavior::ReflectBehavior(float duration):
	TimedBehavior(duration),
	mReflecting(false)

{
}

ReflectBehavior::~ReflectBehavior()
{
}

shared_ptr<EnemyBehavior> ReflectBehavior::Copy()
{
	return std::make_shared<ReflectBehavior>(mTimer.GetDuration());
}

void ReflectBehavior::Run(float timestepSeconds)
{
	TimedBehavior::Run(timestepSeconds);
	if(!mReflecting)
	{
		mEnemyShip->SetReflective(true);
		mReflecting = true;
	}
}

void ReflectBehavior::Reset()
{
	TimedBehavior::Reset();
	if(mReflecting)
		mEnemyShip->SetReflective(false);
	mReflecting = false;
}
