/*
ScrapPowerup

A powerup that grants the player some scrap when they pick it up. These
are dropped from all enemies unless they drop an item.
*/

#include "ProgressionState.h"
#include "ScrapPowerup.h"

using std::string;

ScrapPowerup::ScrapPowerup(const Vector2& initialCenterPos, int scrapDrop, AnimationRegister& animationRegister, std::shared_ptr<Layer> worldLayer):
	Powerup(initialCenterPos, GetAnimationNameForDrop(scrapDrop), animationRegister, worldLayer),
	mScrapDrop(scrapDrop)
{
}


ScrapPowerup::~ScrapPowerup()
{
}

void ScrapPowerup::OnDie(ProgressionState& progressionState)
{
	progressionState.GetCurrencyManager().AddScrap(mScrapDrop);
	Cleanup();
}

string ScrapPowerup::GetAnimationNameForDrop(int scrapDrop)
{
	return "scrapSmall";
}