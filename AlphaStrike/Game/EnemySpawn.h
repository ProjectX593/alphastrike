#pragma once

#include <memory>
#include <string>
#include <vector>

#include "LeaveBehavior.h"

#include "cereal\archives\binary.hpp"
#include "cereal\archives\json.hpp"
#include "cereal\types\polymorphic.hpp"

enum class LevelDifficulty : int
{
	easy,
	medium,
	hard,
	extreme,
	count,
	finalBoss		// shouldn't appear as a random difficulty, so it isn't part of the count
};

struct SpawnLeaveConfig
{
	SpawnLeaveConfig(): mLeaveType(LeaveType::up), mAcceleration(1.0f), mWeight(10) {}
	SpawnLeaveConfig(LeaveType type, float acceleration, int weight): mLeaveType(type), mAcceleration(acceleration), mWeight(weight) {}
	LeaveType mLeaveType;
	float mAcceleration;
	int mWeight;

	template<class Archive>
	void serialize(Archive& archive, const std::uint32_t version)
	{
		if(version == 1)
			archive(mLeaveType, mWeight);
		else
			archive(mLeaveType, mAcceleration, mWeight);
	}
};
CEREAL_CLASS_VERSION(SpawnLeaveConfig, 2);

struct EnemySpawnConfig
{
	EnemySpawnConfig() {}
	EnemySpawnConfig(const std::string& enemyId, const std::string& behaviorSetId): mEnemyId(enemyId), mBehaviorSetId(behaviorSetId) {}
	std::string mEnemyId;
	std::string mBehaviorSetId;

	template<class Archive>
	void serialize(Archive& archive, const std::uint32_t version)
	{
		archive(mEnemyId, mBehaviorSetId);
	}
};
CEREAL_CLASS_VERSION(EnemySpawnConfig, 1);

class EnemySpawn
{
public:
	EnemySpawn();
	EnemySpawn(const EnemySpawnConfig& spawnConfig, float xPos, float yPos, float delay);

	virtual std::unique_ptr<EnemySpawn> Copy() const { return std::make_unique<EnemySpawn>(mSpawnConfig, mXPos, mYPos, mDelay); }
	virtual std::string GetDisplayString() { return "Normal"; }
	const std::string& GetEnemyId() const;
	const std::string& GetBehaviorSetId() const;

	virtual float GetXPos() const { return mXPos; }
	void SetXPos(float xPos) { mXPos = xPos; }
	float GetYPos() const { return mYPos; }
	void SetYPos(float yPos) { mYPos = yPos; }
	float GetDelay() const { return mDelay; }
	void SetDelay(float delay) { mDelay = delay; }
	EnemySpawnConfig& GetSpawnConfig(){ return mSpawnConfig; }


	template<class Archive>
	void serialize(Archive& archive, const std::uint32_t version)
	{
		archive(mSpawnConfig, mXPos, mYPos, mDelay);
	};

protected:
	EnemySpawnConfig mSpawnConfig;
	float mXPos;
	float mYPos;
	float mDelay;
};
CEREAL_CLASS_VERSION(EnemySpawn, 2);

class EnemyRangeSpawn : public EnemySpawn
{
public:
	EnemyRangeSpawn();
	EnemyRangeSpawn(const EnemySpawnConfig& spawnConfig, float xMin, float xMax, float yPos, float delay);

	virtual std::unique_ptr<EnemySpawn> Copy() const { return std::make_unique<EnemyRangeSpawn>(mSpawnConfig, mXMin, mXMax, mYPos, mDelay); }
	virtual std::string GetDisplayString() { return "Range"; }
	virtual float GetXPos() const;

	float GetXMin() const { return mXMin; }
	void SetXMin(float xMin) { mXMin = xMin; }
	float GetXMax() const { return mXMax; }
	void SetXMax(float xMax) { mXMax = xMax; }

	template<class Archive>
	void serialize(Archive& archive, const std::uint32_t version)
	{
		archive(mXMin, mXMax, cereal::base_class<EnemySpawn>(this));
	};

protected:
	float mXMin;
	float mXMax;
};
CEREAL_CLASS_VERSION(EnemyRangeSpawn, 1);

class EnemySpawnGroup
{
public:
	EnemySpawnGroup();
	EnemySpawnGroup(LevelDifficulty minDifficulty, LevelDifficulty maxDifficulty, float minAnchorX = 0.0f, float maxAnchorX = 0.0f, float minLeaveTime = 0.0f, float maxLeaveTime = 0.0f, int leaderIndex = -1);
	EnemySpawnGroup(const EnemySpawnGroup& other);
	EnemySpawnGroup& operator=(const EnemySpawnGroup& other);

	std::vector<std::unique_ptr<EnemySpawn>> mSpawns;
	std::vector<SpawnLeaveConfig> mLeaveConfigs;
	int mLeaderIndex;
	float mMinAnchorX;
	float mMaxAnchorX;
	float mMinLeaveTime;
	float mMaxLeaveTime;
	LevelDifficulty mMinDifficulty;
	LevelDifficulty mMaxDifficulty;
	std::string mName;	// only used in the editor to make things clearer

	template<class Archive>
	void serialize(Archive& archive, const std::uint32_t version)
	{
		archive(mSpawns, mLeaveConfigs, mMinAnchorX, mMaxAnchorX, mMinLeaveTime, mMaxLeaveTime, mLeaderIndex, mMinDifficulty, mMaxDifficulty, mName);
	}

private:

	void TakeSpawns(const std::vector<std::unique_ptr<EnemySpawn>>& spawns);
};
CEREAL_CLASS_VERSION(EnemySpawnGroup, 4);

CEREAL_FORCE_DYNAMIC_INIT(rangeSpawn);