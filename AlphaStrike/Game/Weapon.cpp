/*
Weapon

A weapon is far more complex than most items. When fired by a player, or an AI that owns it,
it follows a sequence of actions that either fire bullets with specific instructions, or simply
delays between firing bullets for dramatic effect.
*/

#include <algorithm>
#include "AnimatedSprite.h"
#include "Bullet.h"
#include "Config.h"
#include "FileUtil.h"
#include <fstream>
#include "GameGlobals.h"
#include "ItemManager.h"
#include "Weapon.h"
#include "World.h"

using std::shared_ptr;
using std::string;
using std::vector;

CEREAL_REGISTER_TYPE(Weapon);
CEREAL_REGISTER_DYNAMIC_INIT(wep);

Weapon::Weapon():
	mIsPrimaryWeapon(false),
	mFireReleased(true),
	mIsPlayerWeapon(0.0f),
	mTimeUntilFire(0.0f),
	mCooldown(0.0f),
	mExtraDamage(0),
	mCurrentActionIndex(0),
	mActionTimeElapsed(0.0f),
	mAmmoRequired(0)
{
}

// This constructor is used only for constructing weapons to be used by enemies
Weapon::Weapon(float cooldown, const std::vector<WeaponAction>& actions):
	Item(-1, "", "", "", 0),
	mIsPrimaryWeapon(true),
	mFireReleased(true),
	mIsPlayerWeapon(false),
	mTimeUntilFire(0.0f),
	mCooldown(cooldown),
	mExtraDamage(0),
	mActions(actions),
	mCurrentActionIndex(actions.size()),
	mActionTimeElapsed(0.0f),
	mAmmoRequired(0)
{
}

Weapon::Weapon(int guid, 
			   const string& name,
			   const string& description, 
			   const string& thumbnail, 
			   int recycleValue,
			   bool isPrimaryWeapon, 
			   bool isPlayerWeapon, 
			   float cooldown,
			   const vector<WeaponAction>& actions,
			   int ammoRequired,
			   int extraDamage):
	Item(guid, name, description, thumbnail, recycleValue),
	mIsPrimaryWeapon(isPrimaryWeapon),
	mFireReleased(true),
	mIsPlayerWeapon(isPlayerWeapon),
	mTimeUntilFire(0.0f),
	mCooldown(cooldown),
	mExtraDamage(extraDamage),
	mActions(actions),
	mCurrentActionIndex(actions.size()),
	mActionTimeElapsed(0.0f),
	mAmmoRequired(ammoRequired)
{
}

Weapon::~Weapon()
{
}

std::shared_ptr<Weapon> Weapon::Clone() const
{
	return std::make_shared<Weapon>(mGUID, mName, mDescription, mThumbnail, mRecycleValue, mIsPrimaryWeapon, mIsPlayerWeapon, mCooldown, mActions, mAmmoRequired, mExtraDamage);
}

void Weapon::Simulate(float timestepSeconds, bool firePressed, std::shared_ptr<AnimatedSprite> shipSprite, World* world, const Vector2& offset)
{
	mTimeUntilFire -= timestepSeconds;
	mActionTimeElapsed += timestepSeconds;
	mFireReleased = mFireReleased || !firePressed;

	if(firePressed && CanFire())
	{
		mTimeUntilFire = mFireReleased ? mCooldown : (mTimeUntilFire + mCooldown);
		mCurrentActionIndex = 0;
		mActionTimeElapsed = 0;
		mFireReleased = false;
	}

	while(mCurrentActionIndex < (int)mActions.size() && mActionTimeElapsed >= mActions[mCurrentActionIndex].mDelay)
	{
		ExecuteAction(mCurrentActionIndex, shipSprite, world, offset);
		mActionTimeElapsed = mActionTimeElapsed - mActions[mCurrentActionIndex].mDelay;
		mCurrentActionIndex++;
	}
}

void Weapon::ExecuteAction(int actionIndex, std::shared_ptr<AnimatedSprite> shipSprite, World* world, const Vector2& offset)
{
	WeaponAction& action = mActions[actionIndex];

	Vector2 centerPos(action.mCenterPosInPixels / PIXELS_IN_UNIT);

	if(shipSprite->GetRotation() != 0.0f)
	{
		centerPos -= Vector2(shipSprite->GetWidth() * 0.5f, shipSprite->GetHeight() * 0.5f);
		centerPos.Rotate(shipSprite->GetRotation());
		centerPos += Vector2(shipSprite->GetWidth() * 0.5f, shipSprite->GetHeight() * 0.5f);
	}

	centerPos += shipSprite->GetPos() + offset;
	Vector2 initialVelocity = action.mInitialVelocity;
	initialVelocity.Rotate(shipSprite->GetRotation());

	world->SpawnBullet(Bullet::Make(centerPos, initialVelocity, action.mDamage + mExtraDamage, action.GetBulletBehaviors(), mIsPlayerWeapon, action.mRotates, action.mBulletId, world));
}

bool Weapon::CanFire()
{
	return mTimeUntilFire <= 0.0f;
}

void Weapon::StopActions()
{
	mCurrentActionIndex = mActions.size();
}

void Weapon::TakeWeaponConfigValues(const Weapon* weapon)
{
	mExtraDamage = weapon->mExtraDamage;
	mActions = weapon->mActions;
	mCurrentActionIndex = mActions.size();
	mCooldown = weapon->mCooldown;
	mAmmoRequired = weapon->mAmmoRequired;
	TakeItemConfigValues(weapon);
}

ItemType Weapon::GetType()
{
	if(mIsPrimaryWeapon)
		return ItemType::primaryWeapon;
	else
		return ItemType::secondaryWeapon;
}