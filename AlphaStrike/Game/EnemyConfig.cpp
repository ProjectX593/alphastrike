/*
EnemyConfig

Contains all of the information needed to create a single enemy. This class is serializable so it may be used
with the editor.
*/

#include "Collider.h"
#include "EnemyBehavior.h"
#include "EnemyConfig.h"

using std::shared_ptr;
using std::string;
using std::vector;

EnemyConfig::EnemyConfig():
	mMaxHealth(0),
	mScrapDrop(0),
	mDifficulty(0),
	mMaxVelocity(0.0f),
	mAcceleration(0.0f),
	mIsBoss(false),
	mDropChance(0.0f),
	mLeaves(true)
{

}

EnemyConfig::EnemyConfig(int maxHealth, int difficulty, const string& animationName, const string& initialAnimation, int scrapDrop, float maxVelocity, float acceleration, bool isBoss):
	mMaxHealth(maxHealth), 
	mDifficulty(difficulty),
	mScrapDrop(scrapDrop),
	mMaxVelocity(maxVelocity),
	mAcceleration(acceleration),
	mIsBoss(isBoss),
	mAnimationName(animationName), 
	mInitialAnimation(initialAnimation),
	mDropChance(0.0f),
	mLeaves(true)
{
}

EnemyConfig::EnemyConfig(const EnemyConfig& other):
	mMaxHealth(other.mMaxHealth),
	mDifficulty(other.mDifficulty),
	mScrapDrop(other.mScrapDrop),
	mMaxVelocity(other.mMaxVelocity),
	mAcceleration(other.mAcceleration),
	mAnimationName(other.mAnimationName),
	mIsBoss(other.mIsBoss),
	mInitialAnimation(other.mInitialAnimation),
	mDropTableId(other.mDropTableId),
	mDropChance(other.mDropChance),
	mReflectAnimationName(other.mReflectAnimationName),
	mWeapons(other.mWeapons),
	mTurretGroupConfigs(other.mTurretGroupConfigs),
	mColliderConfigs(other.mColliderConfigs),
	mLeaves(other.mLeaves)
{
	SetBehaviors(other.GetBehaviorSets());
}

EnemyConfig& EnemyConfig::operator=(const EnemyConfig& other)
{
	mMaxHealth = other.mMaxHealth;
	mDifficulty = other.mDifficulty;
	mScrapDrop = other.mScrapDrop;
	mMaxVelocity = other.mMaxVelocity;
	mAcceleration = other.mAcceleration;
	mIsBoss = other.mIsBoss;
	mAnimationName = other.mAnimationName;
	mInitialAnimation = other.mInitialAnimation;
	mDropTableId = other.mDropTableId;
	mDropChance = other.mDropChance;
	mReflectAnimationName = other.mReflectAnimationName;
	mWeapons = other.mWeapons;
	mTurretGroupConfigs = other.mTurretGroupConfigs;
	mColliderConfigs = other.mColliderConfigs;
	mLeaves = other.mLeaves;

	SetBehaviors(other.GetBehaviorSets());

	return *this;
}

EnemyConfig::~EnemyConfig()
{
}

void EnemyConfig::SetBehaviors(const std::map<std::string, BehaviorSet>& behaviors)
{
	mBehaviorSets.clear();
	for(const auto& behaviorSet : behaviors)
	{
		BehaviorSet newBehaviorSet;

		for(const auto& behavior : behaviorSet.second)
			newBehaviorSet.push_back(behavior->Copy());

		mBehaviorSets[behaviorSet.first] = newBehaviorSet;
	}
}

void EnemyConfig::AddWeapon(const Weapon& weapon)
{
	mWeapons.push_back(weapon);
}

void EnemyConfig::AddBehaviorSet(const BehaviorSet& behaviorSet, const string& setId)
{
	if(mBehaviorSets.find(setId) != mBehaviorSets.end())
		throw new std::runtime_error("ERROR: Tried to add behavior set with id that is already in use.");
	mBehaviorSets[setId] = BehaviorSet();
	for(auto& behavior : behaviorSet)
		mBehaviorSets[setId].push_back(behavior->Copy());
}

const BehaviorSet& EnemyConfig::GetBehaviorSet(const string& setId) const
{
	return mBehaviorSets.at(setId); 
}

bool EnemyConfig::HasBehaviorSet(const string& setId) const
{
	return mBehaviorSets.find(setId) != mBehaviorSets.end();
}