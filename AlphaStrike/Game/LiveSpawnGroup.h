#pragma once

#include "LeaveBehavior.h"
#include "ManualTimer.h"
#include <memory>
#include <vector>

class EnemyShip;

class LiveSpawnGroup
{
public:
	LiveSpawnGroup(const std::vector<std::shared_ptr<EnemyShip>> enemyShips, LeaveType leaveType, float leaveAcceleration, float timeUntilLeave, float anchorX);
	LiveSpawnGroup(const LiveSpawnGroup& other);
	~LiveSpawnGroup();

	LiveSpawnGroup& operator=(const LiveSpawnGroup& other);

	void Initialize();
	void Simulate(float timestepSeconds);
	void Leave();
	int GetInitialDifficulty() const;
	int GetCurrentDifficulty() const;
	bool AllShipsDead() const;

private:
	int mInitialDifficulty;
	LeaveType mLeaveType;
	float mLeaveAcceleration;
	float mTimeUntilLeave;
	float mAnchorX;
	ManualTimer mLeaveTimer;
	std::vector<std::weak_ptr<EnemyShip>> mEnemyShips;
};

