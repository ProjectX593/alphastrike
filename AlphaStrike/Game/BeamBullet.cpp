/*
BeamBullet

Beam bullets are a type of bullet that stay attached to their parent sprite - either a turret or a ship.
The animation name must be the default. It is assumed that the bullet sprite is drawn vertically so custom
collider widths will act on the animation's X dimension.

Beams are a subclass of bullet so the world can treat them almost the same as normal bullets. Despite the
fact that this causes more memory overhead it is worthwhile to reduce code complexity - there will never
be very many beams on screen at once in any case, so the overhead is not significant.
*/

#include "AnimatedSprite.h"
#include "BeamBullet.h"
#include "GameGlobals.h"
#include "Sprite.h"
#include "SquareCollider.h"
#include "World.h"

using std::shared_ptr;

BeamBullet::BeamBullet(const Vector2& pos, float rotationRadians, shared_ptr<AnimatedSprite> parent, int damage, bool isPlayerBullet, const std::string& animationId, World* world, float colliderWidthPercent):
	Bullet(world)
{
	mDamage = damage;
	mIsPlayerBullet = isPlayerBullet;
	mSprite = std::make_shared<AnimatedSprite>(pos);
	mSprite->LoadAndPlayAnimation(world->GetAnimationRegister().RegisterAnimation(animationId), DEFAULT_ANIMATION, true, PIXELS_IN_UNIT);
	mSprite->SetRotation(rotationRadians);
	mSprite->SetDepth(BULLET_DEPTH);

	// this equation will move the pos to the top left of the collision volume's area by using the sprite's width, and the percentage of that taken up by the collider
	mCollider = std::make_shared<SquareCollider>(Vector2(mSprite->GetWidth() * (1.0f - colliderWidthPercent) * 0.5f, 0.0f), D2DSize(mSprite->GetWidth() * colliderWidthPercent, mSprite->GetHeight()));

	parent->AddChild(mSprite);
	mSprite->AddChild(mCollider);
}

BeamBullet::~BeamBullet()
{
}


std::shared_ptr<BeamBullet> BeamBullet::Make(const Vector2& pos, float rotationRadians, std::shared_ptr<AnimatedSprite> parent, int damage, bool isPlayerBullet, const std::string& animationId, World* world, float colliderWidthPercent)
{
	auto bullet = std::shared_ptr<BeamBullet>(new BeamBullet(pos, rotationRadians, parent, damage, isPlayerBullet, animationId, world, colliderWidthPercent));
	bullet->Initialize();
	return bullet;
}

void BeamBullet::Simulate(float timestepSeconds)
{
	// does nothing but animate and stay stuck to it's parent
}

void BeamBullet::Hit(AnimationRegister& animationRegister, std::shared_ptr<Layer> worldLayer)
{
	// Again, do nothing because beam bullets are (at least for now) uneffected by hitting stuff
}