/*
DriftBehavior

This behavior causes an enemy ship to slowly drift downwards. If the min and max speed are left at 0, the speed will
be set to an appropriate default value.
*/

#include "DriftBehavior.h"
#include "EnemyShip.h"
#include "RandomGenerator.h"
#include "World.h"

const float DEFAULT_SPEED = 0.125f;

CEREAL_REGISTER_TYPE(DriftBehavior);
CEREAL_REGISTER_DYNAMIC_INIT(driftB);

DriftBehavior::DriftBehavior():
	mMinSpeed(0.0f),
	mMaxSpeed(0.0f),
	mSpeed(0.0f)
{
}

DriftBehavior::DriftBehavior(float minSpeed, float maxSpeed):
	mMinSpeed(minSpeed),
	mMaxSpeed(maxSpeed),
	mSpeed(0.0f)
{

}

DriftBehavior::~DriftBehavior()
{
}

std::shared_ptr<EnemyBehavior> DriftBehavior::Copy()
{
	return std::make_shared<DriftBehavior>(mMinSpeed, mMaxSpeed);
}

void DriftBehavior::Run(float timestepSeconds)
{
	mEnemyShip->MoveBy(Vector2(0.0f, mSpeed * timestepSeconds));
}

void DriftBehavior::Reset()
{
	if(mMinSpeed < 0.0001 && mMaxSpeed < 0.0001)
		mSpeed = DEFAULT_SPEED;
	else if(abs(mMinSpeed - mMaxSpeed) < 0.0001)
		mSpeed = mMinSpeed;
	else
		mSpeed = mEnemyShip->GetWorld()->GetRandomGenerator().RandomInRange(mMinSpeed, mMaxSpeed);
}