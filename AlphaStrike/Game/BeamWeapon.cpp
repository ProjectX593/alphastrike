/*
BeamWeapon

A sub type of weapon that maintains a beam shaped bullet in front of it's parent sprite while the fire button is pressed.
*/

#include "BeamBullet.h"
#include "BeamWeapon.h"
#include "GameGlobals.h"
#include "World.h"

using std::make_shared;

CEREAL_REGISTER_TYPE(BeamWeapon);
CEREAL_REGISTER_DYNAMIC_INIT(beamWep);

const float DAMAGE_INTERVAL = 0.1f;	// max of 10 ticks of damage per second

BeamWeapon::BeamWeapon():
	mDamage(0),
	mBeamRotationRadians(0.0f),
	mCollisionWidthPercent(1.0f),
	mTimeUntilDamage(0.0f)
{
}

BeamWeapon::BeamWeapon(const std::string& bulletAnimationId, int damage, const Vector2& beamPixelPos, float beamRotationRadians, float collisionWidthPercent):
	mDamage(damage),
	mBeamPixelPos(beamPixelPos),
	mBeamRotationRadians(beamRotationRadians),
	mCollisionWidthPercent(collisionWidthPercent),
	mBulletAnimationId(bulletAnimationId),
	mTimeUntilDamage(0.0f)
{
	mIsPlayerWeapon = false;	// players can't currently use beams because they're OP
}

BeamWeapon::~BeamWeapon()
{
}

std::shared_ptr<Weapon> BeamWeapon::Clone() const
{
	return make_shared<BeamWeapon>(mBulletAnimationId, mDamage, mBeamPixelPos, mBeamRotationRadians, mCollisionWidthPercent);
}

// the shipSprite can be from either a turret or a ship
void BeamWeapon::Simulate(float timestepSeconds, bool firePressed, std::shared_ptr<AnimatedSprite> shipSprite, World* world, const Vector2& offset)
{
	if(!firePressed && mBeam != nullptr)
	{
		// move it outside the screen to cause the world to clean it up
		mBeam->SetPos(Vector2(1000.0f, 1000.0f));
		mBeam = nullptr;
	}

	if(firePressed && mBeam == nullptr)
	{
		Vector2 bulletPos(mBeamPixelPos.x / PIXELS_IN_UNIT, mBeamPixelPos.y / PIXELS_IN_UNIT);
		mBeam = BeamBullet::Make(bulletPos, mBeamRotationRadians, shipSprite, mDamage, mIsPlayerWeapon, mBulletAnimationId, world, mCollisionWidthPercent);
		world->SpawnBullet(mBeam);
	}
}

// returns true if the beam can damage things. It can only damage things once every DAMAGE_INTERVAL seconds.
bool BeamWeapon::CanDealDamage()
{
	return mTimeUntilDamage <= 0.0f;
}

// Resets the damage interval so the beam doesn't constantly deal damage.
void BeamWeapon::ResetDamageInterval()
{
	mTimeUntilDamage += DAMAGE_INTERVAL;
}