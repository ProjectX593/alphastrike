/*
DamageBorderView

A view that contains the damage border pieces, which light up when the player is hit.
*/

#include <algorithm>
#include "DamageBorderView.h"
#include "GameGlobals.h"

const float EFFECT_DURATION = 0.75f;

DamageBorderView::DamageBorderView(AnimationRegister& animationRegister):
	View(animationRegister)
{
}

DamageBorderView::~DamageBorderView()
{
}

void DamageBorderView::Initialize(float screenWidthUnits)
{
	LoadFromJson("assets\\screens\\components\\damageBorder.json", PIXELS_IN_UNIT);

	mBorderSprites.push_back(GetChild<AnimatedSprite>("topBorder"));
	mBorderSprites.push_back(GetChild<AnimatedSprite>("bottomBorder"));
	mBorderSprites.push_back(GetChild<AnimatedSprite>("leftBorder"));
	mBorderSprites.push_back(GetChild<AnimatedSprite>("rightBorder"));

	for(auto& borderSprite : mBorderSprites)
		borderSprite->SetAlpha(0.0f);

	OnResizeScreen(screenWidthUnits);
}

void DamageBorderView::OnResizeScreen(float screenWidthUnits)
{
	auto lSprite = GetChild<AnimatedSprite>("leftBorder");
	lSprite->SetCenterPos(Vector2((1.0f - screenWidthUnits) / 2.0f + lSprite->GetWidth() / 2.0f, 0.5f));

	auto rSprite = GetChild<AnimatedSprite>("rightBorder");
	rSprite->SetCenterPos(Vector2(1.0f + (screenWidthUnits - 1.0f) / 2.0f - rSprite->GetWidth() / 2.0f, 0.5f));
}

void DamageBorderView::OnPlayerDamaged()
{
	for(auto& borderSprite : mBorderSprites)
		borderSprite->SetAlpha(1.0f);
}

void DamageBorderView::Run(float timestepSeconds)
{
	View::Run(timestepSeconds);
	for(auto& borderSprite : mBorderSprites)
		borderSprite->SetAlpha(std::max(0.0f, borderSprite->GetAlpha() - timestepSeconds / EFFECT_DURATION));
}