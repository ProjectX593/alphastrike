/*
WanderBehavior

A behavior that causes an enemy to move smoothly in random directions.
*/

#include "AnimatedSprite.h"
#include "EnemyShip.h"
#include <exception>
#include "GameGlobals.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include "WanderBehavior.h"

#include "cereal\archives\binary.hpp"
#include "cereal\archives\json.hpp"
CEREAL_REGISTER_TYPE(WanderBehavior);
CEREAL_REGISTER_DYNAMIC_INIT(wanderB);

using std::shared_ptr;

const float BOUNDARY_WIDTH = 0.15f;

WanderBehavior::WanderBehavior():
	mVelocity(0.0f),
	mDirectionChangeInterval(0.0f),
	mDirectionChangeFactor(0.0f),
	mLowerYBoundary(0.7f),
	mTimer(0.0f, std::bind(&WanderBehavior::ResetIntendedVelocity, this, false)),
	mRandomDist(0.0f, 2.0f * (float)M_PI),
	mSmallOffsetDist((float)-M_PI_4, (float)M_PI_4),
	mFollowingLeader(false),
	mForcedToCenter(false)
{
}

WanderBehavior::WanderBehavior(float velocity, float directionChangeInterval, float directionChangeFactor, float lowerYBoundary):
	mVelocity(velocity),
	mDirectionChangeInterval(directionChangeInterval),
	mDirectionChangeFactor(directionChangeFactor),
	mLowerYBoundary(lowerYBoundary),
	mTimer(directionChangeInterval, std::bind(&WanderBehavior::ResetIntendedVelocity, this, false)),
	mRandomDist(0.0f, 2.0f * (float)M_PI),
	mSmallOffsetDist((float)-M_PI_4, (float)M_PI_4),
	mFollowingLeader(false),
	mForcedToCenter(false)
{
}

WanderBehavior::~WanderBehavior()
{
}

std::shared_ptr<EnemyBehavior> WanderBehavior::Copy()
{
	return std::make_shared<WanderBehavior>(mVelocity, mDirectionChangeInterval, mDirectionChangeFactor);
}

void WanderBehavior::Run(float timestepSeconds)
{
	if(mFollowingLeader && mEnemyShip->GetLeader().lock() != nullptr)
	{
		mEnemyShip->SetPos(mEnemyShip->GetLeader().lock()->GetSprite()->GetPos() + mLeaderOffset);
		return;
	}

	mTimer.Run(timestepSeconds);

	float factor = mDirectionChangeFactor * timestepSeconds;
	mCurrentVelocity = Vector2(mIntendedVelocity.x * factor + mCurrentVelocity.x * (1.0f - factor), mIntendedVelocity.y * factor + mCurrentVelocity.y * (1.0f - factor));
	mEnemyShip->MoveBy(mCurrentVelocity * timestepSeconds);

	if(OutsideBounds())
	{
		if(!mForcedToCenter)
		{
			ResetIntendedVelocity(true);
			mForcedToCenter = true;
		}
	}
	else
	{
		mForcedToCenter = false;
	}
}

void WanderBehavior::Reset()
{
	RandomBehavior::Reset();
	ResetIntendedVelocity();
	mForcedToCenter = false;
}

bool WanderBehavior::OutsideBounds()
{
	auto sprite = mEnemyShip->GetSprite();
	Vector2 pos = sprite->GetCenter();

	bool result = pos.x < SCREEN_LEFT_EDGE + BOUNDARY_WIDTH ||
		pos.x + sprite->GetWidth() > SCREEN_RIGHT_EDGE - BOUNDARY_WIDTH ||
		pos.y < SCREEN_TOP_EDGE + BOUNDARY_WIDTH ||
		pos.y + sprite->GetHeight() > mLowerYBoundary;

	return result;
}

void WanderBehavior::ResetIntendedVelocity(bool forceToCenter)
{
	Vector2 pos = mEnemyShip->GetSprite()->GetCenter();
	if(forceToCenter || OutsideBounds())
	{
		mIntendedVelocity = Vector2(0.5f, (mLowerYBoundary - SCREEN_TOP_EDGE) / 2.0f) - pos;
		mIntendedVelocity.Rotate(mSmallOffsetDist(mGenerator));
	}
	else
	{
		mIntendedVelocity = Vector2(0.0f, mVelocity);
		mIntendedVelocity.Rotate(mRandomDist(mGenerator));
	}
	mTimer.Reset();
}

void WanderBehavior::SetLeaderBehavior(std::weak_ptr<EnemyBehavior> behavior)
{
	RandomBehavior::SetLeaderBehavior(behavior);

	std::shared_ptr<EnemyBehavior> b = behavior.lock();
	if(b == nullptr || mEnemyShip->GetLeader().lock() == nullptr)
	{
		mFollowingLeader = false;
		return;
	}

	shared_ptr<WanderBehavior> wanderBehavior = std::dynamic_pointer_cast<WanderBehavior>(b);
	if(wanderBehavior == nullptr)
		throw new std::runtime_error("ERROR: Wander behavior tried to set a different type of behavior to it's leader.");

	mFollowingLeader = true;
	mLeaderOffset = mEnemyShip->GetSprite()->GetPos() - mEnemyShip->GetLeader().lock()->GetSprite()->GetPos();
}