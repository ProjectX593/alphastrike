#pragma once

#include "EnemyBehavior.h"
#include "ManualTimer.h"

class TimedBehavior : public EnemyBehavior
{
public:
	TimedBehavior(float duration = 0.0f);
	virtual ~TimedBehavior();
	TimedBehavior(const TimedBehavior& other);

	void SetDuration(float duration);	// sets the duration, note that this resets the TimedBehavior's time, but does not cause it to start blocking again
	virtual std::shared_ptr<EnemyBehavior> Copy();
	virtual void Run(float timestepSeconds);
	virtual void Reset();
	virtual void Done();
	virtual std::string GetDisplayString() { return "Timed Behavior"; }

	float GetDuration(){ return mTimer.GetDuration(); }

	template<class Archive>
	void serialize(Archive& archive, const std::uint32_t version)
	{
		archive(mTimer, cereal::base_class<EnemyBehavior>(this));
	}


protected:
	ManualTimer mTimer;
};

CEREAL_CLASS_VERSION(TimedBehavior, 1);
CEREAL_FORCE_DYNAMIC_INIT(timedB);