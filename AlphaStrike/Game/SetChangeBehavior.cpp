/*
SetChangeBehavior

One of the most interesting behaviors - when certain conditions are met, the enemyShip this behavior belongs to
will change to an entirely new set of behaviors, immediately stopping all current behaviors.
*/

#include "EnemyShip.h"
#include "SetChangeBehavior.h"
#include "Turret.h"

#include "cereal\archives\binary.hpp"
#include "cereal\archives\json.hpp"
CEREAL_REGISTER_TYPE(SetChangeBehavior);
CEREAL_REGISTER_DYNAMIC_INIT(setChangeB);

SetChangeBehavior::SetChangeBehavior():
	mHealthThreshold(0),
	mChangeWhenTurretsDead(false)
{
}

SetChangeBehavior::SetChangeBehavior(const std::string& newBehaviorSetId, int healthThreshold, bool changeWhenTurretsDead):
	mNewBehaviorSetId(newBehaviorSetId),
	mHealthThreshold(healthThreshold),
	mChangeWhenTurretsDead(changeWhenTurretsDead)
{
}

SetChangeBehavior::~SetChangeBehavior()
{
}

std::shared_ptr<EnemyBehavior> SetChangeBehavior::Copy()
{
	return std::make_shared<SetChangeBehavior>(mNewBehaviorSetId, mHealthThreshold, mChangeWhenTurretsDead);
}

void SetChangeBehavior::Run(float timestepSeconds)
{
	if(mChangeWhenTurretsDead)
	{
		bool allTurretsDead = true;
		auto& turrets = mEnemyShip->GetTurrets();
		for(auto& turret : turrets)
		{
			if(!turret.IsDead())
			{
				allTurretsDead = false;
				break;
			}
		}

		if(allTurretsDead)
		{
			mEnemyShip->SetActiveBehaviorSetId(mNewBehaviorSetId);
			return;
		}
	}

	if(mEnemyShip->GetHealth() <= mHealthThreshold)
		mEnemyShip->SetActiveBehaviorSetId(mNewBehaviorSetId);
}