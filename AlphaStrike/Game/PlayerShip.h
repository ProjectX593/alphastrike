#pragma once

#include "ManualTimer.h"
#include <memory>
#include "Ship.h"
#include "TextureInfo.h"
#include "Vector2.h"

class AnimatedSprite;
class AnimationRegister;
class Armor;
class Bullet;
class InputSystem;
class Layer;
class ProgressionState;
class Sprite;
class Weapon;
class World;

class PlayerShip : public Ship
{
public:
	PlayerShip(std::shared_ptr<Layer>& worldLayer, InputSystem& inputSystem, World* world);
	~PlayerShip();

	void Simulate(float timestepSeconds);
	void Respawn();
	virtual void TakeDamage(int damage);
	bool IsInvulnerable() const;
	std::shared_ptr<Collider> GetCollider();
	void SetInvulnerable(bool invulnerable) { mInvulnerable = invulnerable; }
	void SetPrimaryWeapon(std::shared_ptr<Weapon> weapon);
	void SetSecondaryWeapon(std::shared_ptr<Weapon> weapon);
	void SetArmor(std::shared_ptr<Armor> armor);

private:
	Vector2 mPos;
	InputSystem& mInputSystem;
	std::shared_ptr<Weapon> mPrimaryWeapon;
	std::shared_ptr<Weapon> mSecondaryWeapon;
	std::function<void(Bullet&)> mAddBulletCallback;
	ManualTimer mInvulnTimer;
	bool mInvulnerable;

	void SimulateMovement(float timestepSeconds);
	void SimulateFiring(float timestepSeconds);
	void OnInvulnDone();
};

