#pragma once

#include "EnemyBehavior.h"

class ArriveBehavior : public EnemyBehavior
{
public:
	ArriveBehavior();
	ArriveBehavior(float travelDistance, float initialSpeed);
	virtual ~ArriveBehavior();

	virtual std::shared_ptr<EnemyBehavior> Copy();
	virtual void Run(float timestepSeconds);
	virtual std::string GetDisplayString() { return "Arrive"; }

	float GetTravelDistance(){ return mTravelDistance; }
	float GetInitialSpeed(){return mInitialSpeed; }
	void SetTravelDistance(float travelDistance){ mTravelDistance = travelDistance; }
	void SetInitialSpeed(float initialSpeed){ mInitialSpeed = initialSpeed; }

	template <class Archive>
	void serialize(Archive& archive, const std::uint32_t version)
	{
		archive(mTravelDistance, mInitialSpeed, cereal::base_class<EnemyBehavior>(this));
	}

private:
	float mTravelDistance;
	float mDistanceTraveled;
	float mInitialSpeed;
};

CEREAL_CLASS_VERSION(ArriveBehavior, 1);
CEREAL_FORCE_DYNAMIC_INIT(arriveB);