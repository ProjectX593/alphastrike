/*
HangarBay

This screen is used by the player to repair and equip items to their ship. It has two views, one from a catwalk overlooking
the ship where the player repairs the ship. The second view is from under the ship where the player
selects items from a rack to equip onto their ship.
*/

#include "AnimationRegister.h"
#include "CompareView.h"
#include "GameGlobals.h"
#include "HangarBay.h"
#include "HomeBase.h"
#include "ItemView.h"
#include "ProgressionState.h"

using std::bind;
using std::make_shared;
using std::shared_ptr;
using namespace std::placeholders;

HangarBay::HangarBay(HomeBase& homeBase, AnimationRegister& animationRegister):
	mHomeBase(homeBase),
	mAnimationRegister(animationRegister),
	View(animationRegister)
{
}

HangarBay::~HangarBay()
{
}

void HangarBay::Initialize()
{
	// Set up the catwalk view
	mCatwalkView = make_shared<View>(mAnimationRegister);
	mCatwalkView->LoadFromJson("assets\\screens\\HangarBay.json", PIXELS_IN_UNIT);
	AddChild(mCatwalkView);

	// Set up the underside view
	mCatwalkView->GetChild<Button>("shipButton")->SetMouseReleaseCallback(bind(&HangarBay::SetCatwalkViewVisible, this, false));
	mCatwalkView->GetChild<Button>("turboliftButton")->SetMouseReleaseCallback(bind(&HomeBase::ShowTurbolift, &mHomeBase));

	mUndersideView = make_shared<View>(mAnimationRegister);
	mUndersideView->LoadFromJson("assets\\screens\\HangarBayUnderside.json", PIXELS_IN_UNIT);
	AddChild(mUndersideView);

	//mUndersideView->GetChild<Button>("catwalkView")->SetCallback(bind(&HangarBay::SetCatwalkViewVisible, this, true));
	mUndersideView->GetChild<Button>("turboliftButton")->SetMouseReleaseCallback(bind(&HomeBase::ShowTurbolift, &mHomeBase));

	auto ep = mUndersideView->GetChild<Button>("equippedPrimary");
	auto es = mUndersideView->GetChild<Button>("equippedSecondary");
	auto ea = mUndersideView->GetChild<Button>("equippedArmor");

	ep->SetMouseReleaseCallback(std::bind(&HangarBay::UnequipItem, this, ItemType::primaryWeapon));
	es->SetMouseReleaseCallback(std::bind(&HangarBay::UnequipItem, this, ItemType::secondaryWeapon));
	ea->SetMouseReleaseCallback(std::bind(&HangarBay::UnequipItem, this, ItemType::armor));

	ep->SetMouseOverCallback(std::bind(&HangarBay::OnEquippedItemMousedOver, this, ItemType::primaryWeapon));
	es->SetMouseOverCallback(std::bind(&HangarBay::OnEquippedItemMousedOver, this, ItemType::secondaryWeapon));
	ea->SetMouseOverCallback(std::bind(&HangarBay::OnEquippedItemMousedOver, this, ItemType::armor));

	ep->SetMouseOutCallback(std::bind(&HangarBay::OnItemMouseOut, this));
	es->SetMouseOutCallback(std::bind(&HangarBay::OnItemMouseOut, this));
	ea->SetMouseOutCallback(std::bind(&HangarBay::OnItemMouseOut, this));

	// Set up compare view
	mCompareView = std::make_shared<CompareView>(mAnimationRegister, mHomeBase);
	mCompareView->Initialize();
	mUndersideView->GetChild<Sprite>("compareConsole")->GetChild<Node>("compareConsoleNode")->AddChild(mCompareView);

	mItemView = std::make_shared<ItemView>(mAnimationRegister, mHomeBase.GetProgressionState(), mCompareView);
	mItemView->Initialize();
	mItemView->SetItemMouseOverCallback(std::bind(&HangarBay::OnItemMousedOver, this, _1));
	mItemView->SetItemMouseOutCallback(std::bind(&HangarBay::OnItemMouseOut, this));
	mItemView->SetEquipCallback(std::bind(&HangarBay::RefreshItems, this));
	mUndersideView->GetChild<Node>("itemViewNode")->AddChild(mItemView);

	SetCatwalkViewVisible(true);
	RefreshItems();
}

void HangarBay::RefreshItems()
{
	auto& ps = mHomeBase.GetProgressionState();
	auto updateEquippedItemImage = [this, &ps](ItemType type, const std::string& spritePrefix){
		std::shared_ptr<Item> item = ps.GetEquippedItemOfType(type);
		
		auto sprite = mUndersideView->GetChild<Sprite>(spritePrefix + "Image");
		sprite->SetVisible(item != nullptr);
		if(item != nullptr)
			sprite->SetTexture(mAnimationRegister.RegisterTexture(item->mThumbnail));
		
		mUndersideView->GetChild<Button>(spritePrefix)->SetDisabled(item == nullptr);
	};

	updateEquippedItemImage(ItemType::primaryWeapon, "equippedPrimary");
	updateEquippedItemImage(ItemType::secondaryWeapon, "equippedSecondary");
	updateEquippedItemImage(ItemType::armor, "equippedArmor");

	mItemView->RefreshItems();
}

void HangarBay::UnequipItem(ItemType type)
{
	mHomeBase.GetProgressionState().SetEquippedItem(-1, type);
	RefreshItems();
}

void HangarBay::SetCatwalkViewVisible(bool visible)
{
	mCatwalkView->SetVisible(visible);
	mUndersideView->SetVisible(!visible);
}

void HangarBay::OnItemMousedOver(std::shared_ptr<Item> item)
{
	auto& ps = mHomeBase.GetProgressionState();
	ItemType type = item->GetType();
	mCompareView->SetItems(ps.GetEquippedItemOfType(type), item, "EQUIPPED");

	HighlightTypeIcon(type);
}

void HangarBay::OnEquippedItemMousedOver(ItemType type)
{
	mCompareView->SetItems(mHomeBase.GetProgressionState().GetEquippedItemOfType(type), nullptr, "EQUIPPED");
	HighlightTypeIcon(type);
}

void HangarBay::HighlightTypeIcon(ItemType type)
{
	mUndersideView->GetChild<AnimatedSprite>("primaryIcon")->PlayAnimation(type == ItemType::primaryWeapon ? "highlighted" : DEFAULT_ANIMATION);
	mUndersideView->GetChild<AnimatedSprite>("secondaryIcon")->PlayAnimation(type == ItemType::secondaryWeapon ? "highlighted" : DEFAULT_ANIMATION);
	mUndersideView->GetChild<AnimatedSprite>("armorIcon")->PlayAnimation(type == ItemType::armor ? "highlighted" : DEFAULT_ANIMATION);
}

void HangarBay::OnItemMouseOut()
{
	mCompareView->SetItems(nullptr, nullptr);

	mUndersideView->GetChild<AnimatedSprite>("primaryIcon")->PlayAnimation(DEFAULT_ANIMATION);
	mUndersideView->GetChild<AnimatedSprite>("secondaryIcon")->PlayAnimation(DEFAULT_ANIMATION);
	mUndersideView->GetChild<AnimatedSprite>("armorIcon")->PlayAnimation(DEFAULT_ANIMATION);
}