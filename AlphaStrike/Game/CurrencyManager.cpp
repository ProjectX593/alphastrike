/*
CurrencyManager

Manages player currencies (scrap and energy) - their conversion ratio, and adding or removing currency.
*/

#include <algorithm>
#include "CurrencyManager.h"

const int ENERGY_PER_AMMO = 1;
const int SCRAP_PER_ENERGY = 5;
const int MAX_AMMO = 10;

CurrencyManager::CurrencyManager():
	mScrap(0),
	mEnergy(0),
	mAmmo(MAX_AMMO)
{
}

CurrencyManager::~CurrencyManager()
{
}

void CurrencyManager::AddScrap(int scrap)
{
	mScrap += scrap;
}

void CurrencyManager::AddEnergy(int energy)
{
	mEnergy += energy;
}

void CurrencyManager::ConvertScrapToEnergy(int scrap)
{
	Convert(scrap, mScrap, mEnergy, SCRAP_PER_ENERGY);
}

void CurrencyManager::ConvertEnergyToAmmo(int energy)
{
	energy = std::max(energy * SCRAP_PER_ENERGY, MAX_AMMO - mAmmo);
	if(energy > 0)
		Convert(energy, mEnergy, mAmmo, ENERGY_PER_AMMO);
}

void CurrencyManager::Convert(int amount, int& fromCurrency, int& toCurrency, int ratio)
{
	amount = std::max(0, std::min(amount, fromCurrency));

	int currencyToAdd = amount / ratio;
	fromCurrency -= currencyToAdd * ratio;
	toCurrency += currencyToAdd;
}

bool CurrencyManager::TrySpendAmmo(int ammo)
{
	if(ammo <= mAmmo)
	{
		mAmmo -= ammo;
		return true;
	}
	return false;
}

float CurrencyManager::GetAmmoPercent() const
{
	return static_cast<float>(mAmmo) / static_cast<float>(MAX_AMMO);
}

int CurrencyManager::GetScrapPerEnergy() const
{
	return SCRAP_PER_ENERGY;
}