#pragma once

#include "Item.h"
#include "View.h"

class CompareView;
class ProgressionState;

class ItemView : public View
{
public:
	ItemView(AnimationRegister& animationRegister, ProgressionState& progressionState, std::shared_ptr<CompareView> compareView, bool canEquipItems = true);
	~ItemView();

	void SetItemMouseOverCallback(std::function<void(std::shared_ptr<Item>)> mouseOverCallback) { mItemMouseOverCallback = mouseOverCallback; }
	void SetItemMouseOutCallback(std::function<void()> mouseOutCallback) { mItemMouseOutCallback = mouseOutCallback; }
	void SetEquipCallback(std::function<void()> equipCallback) { mEquipCallback = equipCallback; }

	void Initialize();
	void RefreshItems();

private:
	ProgressionState& mProgressionState;
	std::vector<int> mItemGUIDs;
	std::vector<ItemType> mItemTypes;
	std::shared_ptr<CompareView> mCompareView;
	std::function<void(std::shared_ptr<Item>)> mItemMouseOverCallback;
	std::function<void()> mItemMouseOutCallback;
	std::function<void()> mEquipCallback;
	bool mCanEquipItems;

	void AddItemsOfType(ItemType type);
	void EquipItem(int index);
	void MouseOverItem(int index);
	void MouseOutItem();
	std::shared_ptr<Button> GetItemButton(int index);
	std::shared_ptr<Sprite> GetItemImage(int index);
};

