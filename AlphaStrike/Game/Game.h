#pragma once

#include "AnimationRegister.h"
#include "HomeBase.h"
#include "ItemManager.h"
#include <memory>
#include "ProgressionState.h"
#include "RandomGenerator.h"
#include "Windows.h"
#include "World.h"

class InputSystem;
class Layer;
class MainMenu;
class Scene;
class TextField;

class Game
{
public:
	Game(InputSystem& inputSystem);
	~Game();

	bool Run(float timestepSeconds);

private:
	InputSystem& mInputSystem;
	std::shared_ptr<Scene> mScene;
	std::shared_ptr<Layer> mFrontendLayer;
	std::shared_ptr<Layer> mWorldLayer;
	std::shared_ptr<Layer> mBackgroundLayer;
	bool mShouldQuit;
	bool mRunning;
	Config mConfig;
	World mWorld;
	AnimationRegister mFrontendAnimationRegister;
	ItemManager mItemManager;
	ProgressionState mProgressionState;
	RandomGenerator mRandomGenerator;
	HomeBase mHomeBase;

	std::deque<float> mTimeSamples;
	std::shared_ptr<TextField> mFPSCounter;

	void ShowMainMenu();
	void NewGame(const std::string& fullSaveFilename);
	void Quit(void* data);
	void OnMouseDown(const Vector2& pos);
	void OnMouseUp();
	void OnMouseMove(const Vector2& pos);
	void UpdateFPSCounter(float timestepSeconds);
};

