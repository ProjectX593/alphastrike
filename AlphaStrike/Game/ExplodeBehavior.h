#pragma once

#include "EnemyBehavior.h"

class AnimatedSprite;
class AnimationRegister;
class Node;

class ExplodeBehavior : public EnemyBehavior
{
public:
	ExplodeBehavior(float radius = 0.0f, const std::string& explosionAnimationName = "");
	~ExplodeBehavior();

	virtual std::shared_ptr<EnemyBehavior> Copy();
	virtual void Run(float timestepSeconds);
	virtual void Reset();
	virtual std::string GetDisplayString() { return "Explode"; }

	float GetRadius(){ return mRadius; }
	const std::string& GetExplosionAnimationName(){ return mExplosionAnimationName; }
	void SetRadius(float radius){ mRadius = radius; }
	void SetExplosionAnimationName(const std::string& explosionAnimationName){ mExplosionAnimationName = explosionAnimationName; }

	template <class Archive>
	void serialize(Archive& archive, std::uint32_t version)
	{
		archive(mRadius, mExplosionAnimationName, cereal::base_class<EnemyBehavior>(this));
	}

private:
	float mRadius;
	float mTimeUntilNewSprite;
	float mCurrentRotation;
	float mCurrentDistance;
	int mExplosionCount;
	std::string mExplosionAnimationName;
	bool mStarted;

	void OnAnimationExpired(std::shared_ptr<AnimatedSprite> explosion);
};

CEREAL_CLASS_VERSION(ExplodeBehavior, 1);
CEREAL_FORCE_DYNAMIC_INIT(explodeB);