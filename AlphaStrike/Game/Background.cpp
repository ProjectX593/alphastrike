/*
Background

This class handles the scrolling parallax background in the game.
*/

#include "AnimationRegister.h"
#include "Background.h"
#include "EventManager.h"
#include "GameGlobals.h"
#include "Layer.h"
#include "RendererLocator.h"
#include "Sprite.h"

using std::make_shared;
using namespace std::placeholders;
using std::shared_ptr;
using std::string;
using std::vector;

const int ACTIVE_CHUNK_COUNT = 3;
const float TEXTURE_X_EXTENT = 480.0f / 512.0f;
const float TEXTURE_Y_EXTENT = 240.0f / 256.0f;
const float SPEED = 0.13f;

Background::Background(std::shared_ptr<Layer> backgroundLayer, AnimationRegister& animationRegister):
	mBackgroundLayer(backgroundLayer),
	mAnimationRegister(animationRegister)
{
	for(int i = 0; i < BACKGROUND_LAYER_COUNT; i++)
	{
		mSprites.push_back(vector< shared_ptr<Sprite> >());
		mLastTextureIndices.push_back(-1);
	}
}

Background::~Background()
{
	EventManager::Get().UnsubscribeFromAllEvents(this);
}

void Background::LoadChunkTextures(const vector<vector<string>>& chunkTextures)
{
	mChunkTextures.clear();
	for(const auto& textureSet : chunkTextures)
	{
		vector<shared_ptr<TextureInfo>> textures;
		for(const auto& texture : textureSet)
			textures.push_back(mAnimationRegister.RegisterTexture(texture));
		mChunkTextures.push_back(textures);
	}

	for(size_t i = 0; i < chunkTextures.size(); i++)
	{
		for(int j = 0; j < ACTIVE_CHUNK_COUNT; j++)
		{
			shared_ptr<Sprite> chunkSprite = std::make_shared<Sprite>(Vector2(SCREEN_LEFT_EDGE, static_cast<float>(-j)), D2DSize(SCREEN_RIGHT_EDGE - SCREEN_LEFT_EDGE, 1.0f, 0));
			chunkSprite->SetTexture(i == 0 || j != 0 ? RandomTextureForLayer(i) : make_shared<TextureInfo>(), true, PIXELS_IN_UNIT);
			mBackgroundLayer->AddChild(chunkSprite);
			mSprites[i].push_back(chunkSprite);

			// make it so we only do one for the static layer 0
			if(i == 0)
				break;
		}
	}
}

shared_ptr<TextureInfo> Background::RandomTextureForLayer(int layer)
{
	if(mChunkTextures[layer].size() == 1)
		return mChunkTextures[layer][0];

	int count = 0;
	int index = mLastTextureIndices[layer];

	while(count < 100 && index == mLastTextureIndices[layer])
	{
		//TODO: Replace with <random>
		index = rand() % mChunkTextures[layer].size();
		count++;
	}

	mLastTextureIndices[layer] = index;
	return mChunkTextures[layer][index];
}

void Background::Simulate(float timestepSeconds)
{
	for(size_t i = 1; i < mSprites.size(); i++)
	{
		for(int j = 0; j < (int)mSprites[i].size(); j++)
		{
			shared_ptr<Sprite> chunk = mSprites[i][j];
			Vector2 pos = chunk->GetPos();
			chunk->SetPos(Vector2(pos.x, pos.y + (timestepSeconds * (SPEED / pow(2.0f, static_cast<int>(mSprites.size() - 1 - i))))));

			if(chunk->GetPos().y > 1.0f)
			{
				shared_ptr<Sprite> nextChunk = mSprites[i][(j-1) >= 0 ? j-1 : mSprites[i].size() - 1];
				chunk->SetPos(nextChunk->GetPos() + Vector2(0.0f, -1.0f));
				chunk->SetTexture(RandomTextureForLayer(i));
			}
		}
	}
}

void Background::Cleanup()
{
	for(vector< vector< shared_ptr<Sprite> > >::iterator layerIter = mSprites.begin(); layerIter != mSprites.end(); layerIter++)
	{
		for(vector< shared_ptr<Sprite> >::iterator spriteIter = layerIter->begin(); spriteIter != layerIter->end(); spriteIter++)
			(*spriteIter)->RemoveFromParentAndCleanup();
		layerIter->clear();
	}

	mChunkTextures.clear();
}

void Background::OnPlayerMoved(float playerXPos)
{
	Vector2 screenSize = RendererLocator::GetRenderer().GetScreenSize();
	float rangeOfMotion = (2.0f - (screenSize.x / screenSize.y)) / 2.0f;	// the art is 2.0 across, 1.0 tall. The screen is 1.0 tall, so calculate what is on either side if it is centered.
	playerXPos -= 0.5f; // centers this about the middle of the screen, it now ranges from -1 to 1, since the screen is 2 wide

	for(int i = 0; i < BACKGROUND_LAYER_COUNT; i++)
	{
		vector< shared_ptr<Sprite> >& layerSprites = mSprites[i];
		for(vector< shared_ptr<Sprite> >::iterator iter = layerSprites.begin(); iter != layerSprites.end(); iter++)
		{
			// move the lower layers more to counter the movement of the entire scene more, making it move less overall
			(*iter)->SetPos(Vector2(SCREEN_LEFT_EDGE + (playerXPos * rangeOfMotion) * (1 / pow(2.0f, i + 1)), (*iter)->GetPos().y));
		}
	}
}