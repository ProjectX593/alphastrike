/*
ActivateTurretBehavior

A behavior that allows an EnemyShip to activate any number of it's turrets simultaneously.
It will cause each turret to play it's charge animation, then fire until the behavior runs out of time.
The charge time is included in the duration. The charge animation does not play between shots (this could change later but shouldn't be necessary).
Turrets are referred to by their index in the EnemyShip's list.
*/

#include "ActivateTurretBehavior.h"
#include "EnemyShip.h"

#include "cereal\archives\binary.hpp"
#include "cereal\archives\json.hpp"
CEREAL_REGISTER_TYPE(ActivateTurretBehavior);
CEREAL_REGISTER_DYNAMIC_INIT(aTurretB);

using std::vector;

ActivateTurretBehavior::ActivateTurretBehavior():
	mStarted(false),
	mInfinite(false)
{

}

ActivateTurretBehavior::ActivateTurretBehavior(float duration, const vector<int>& turretsToActivate, bool infinite):
	TimedBehavior(infinite ? 0.0f : duration),
	mTurretsToActivate(turretsToActivate),
	mStarted(false),
	mInfinite(infinite)
{
	if(mInfinite)
		mBlocking = true;
}

ActivateTurretBehavior::~ActivateTurretBehavior()
{
}

std::shared_ptr<EnemyBehavior> ActivateTurretBehavior::Copy()
{
	return std::make_shared<ActivateTurretBehavior>(mTimer.GetDuration(), mTurretsToActivate);
}

void ActivateTurretBehavior::Run(float timestepSeconds)
{
	TimedBehavior::Run(timestepSeconds);
	if(!mStarted)
	{
		mStarted = true;
		auto& enemyTurrets = mEnemyShip->GetTurrets();

		for(auto& turret : mTurretsToActivate)
			enemyTurrets[turret].SetFiring(true);
	}
	else if(mInfinite)
	{
		auto& enemyTurrets = mEnemyShip->GetTurrets();
		bool activeTurret = false;

		for(auto& turret : enemyTurrets)
			activeTurret = activeTurret || turret.GetTurretState() != TurretState::idle;

		if(!activeTurret)
			Reset();
	}
}

void ActivateTurretBehavior::mTurretCharged(int turret)
{
	mShootingTurrets.insert(turret);
}

void ActivateTurretBehavior::Reset()
{
	TimedBehavior::Reset();
	mShootingTurrets.clear();
	mStarted = false;

	auto& enemyTurrets = mEnemyShip->GetTurrets();

	for(auto& turret : mTurretsToActivate)
	{
		enemyTurrets[turret].SetFiring(false);
	}
}
