/*
Config

The config class only contains hard coded data. Ordinarily this would be present as text or binary
files configured outside the code. This will work for a single developer, however.
These classes are not generally meant to be used by the game itself, mostly due to the lack of 
an animation register not being available at all times.
The reason this class exists is merely to save time and prevent users from accessing config files.
This system is currently intended to be used with Alpha Strike only.
*/

#include "CircleCollider.h"
#include "Config.h"
#include "EnemyConfig.h"
#include <exception>
#include "InterceptBehavior.h"
#include "GameGlobals.h"
#include "StringUtil.h"
#include "SquareCollider.h"
#include "Turret.h"
#include "UtilityServiceLocator.h"
#include "WanderBehavior.h"
#include "WinPlatformUtil.h"

using std::make_shared;
using std::map;
using std::shared_ptr;
using std::string;
using std::vector;

Config::Config()
{
	InitDropTables();
	InitPlanets();
	InitStagePlanets();
	InitBulletConfigs();
	InitEnemyPools();
}

Config::~Config()
{
}

// Shouldn't be necessary to call this - there was a time when I added Item IDs into items, and that
// necessitated that I do this once to add them in to the items. Returns true if invalid IDs were found, which indicates the config sould be saved.
bool Config::VerifyItemIds()
{
	bool badIDFound = false;

	// didn't do a templated function because I hate having to clean/rebuild each time I make a change... lazy I know
	for(auto& primWep : mPrimaryWeapons)
	{
		if(primWep.first != primWep.second.mItemId)
		{
			badIDFound = true;
			primWep.second.mItemId = primWep.first;
		}
	}
	for(auto& secWep : mSecondaryWeapons)
	{
		if(secWep.first != secWep.second.mItemId)
		{
			badIDFound = true;
			secWep.second.mItemId = secWep.first;
		}
	}
	for(auto& armor : mArmors)
	{
		if(armor.first != armor.second.mItemId)
		{
			badIDFound = true;
			armor.second.mItemId = armor.first;
		}
	}

	return badIDFound;
}

void Config::InitDropTables()
{
	mDropTables["debugDropTable"] = {
		Drop("debugBasicWeapon", ItemType::primaryWeapon, 0.2f),
		Drop("debugVolleyWeapon", ItemType::primaryWeapon, 0.05f),
		Drop("debugInterceptLaser", ItemType::secondaryWeapon, 0.1f),
		Drop("debugSeekers", ItemType::secondaryWeapon, 0.1f),
		Drop("debugHoming", ItemType::secondaryWeapon, 0.05f),
		Drop("debugArmor0", ItemType::armor, 0.2f),
		Drop("debugArmor1", ItemType::armor, 0.1f)};

	mDropTables["T1Military"] = {
		Drop("T1HeavyCannon", ItemType::primaryWeapon, 0.175f),
		Drop("T1LightCannon", ItemType::primaryWeapon, 0.175f),
		Drop("T1CannonBarrage", ItemType::secondaryWeapon, 0.05f),
		Drop("T1CannonRing", ItemType::secondaryWeapon, 0.05f),
		Drop("T1HomingSalvo", ItemType::secondaryWeapon, 0.05f),
		Drop("T1HomingSwarm", ItemType::secondaryWeapon, 0.05f),
		Drop("T1SeekerSalvo", ItemType::secondaryWeapon, 0.05f),
		Drop("T1SeekerSwarm", ItemType::secondaryWeapon, 0.05f),
		Drop("T1AblativeArmor", ItemType::armor, 0.116667f),
		Drop("T1HeavyArmor", ItemType::armor, 0.116667f),
		Drop("T1LightArmor", ItemType::armor, 0.116667f)
	};
}

// Notes on planets:
// Planet images 
void Config::InitPlanets()
{
	vector<vector<string>> testPlanet0BGImages = {
		{"l1l0.png"},
		{"bg_brokenship.png", "bg_shipyard.png", ""},
		{"l1l2c1.png", "l1l2c2.png", ""},
		{"l1l3c1.png", "l1l3c2.png", "l1l3c3.png", ""}
	};

	PlanetConfig testPlanet0("testPlanet0",
		"Test Planet 0",
		{"debug_big_planet.png"},
		{"mil_t1f"},
		LevelDifficulty::easy, LevelDifficulty::easy,
		testPlanet0BGImages);
	mPlanets[testPlanet0.GetPlanetId()] = testPlanet0;

	PlanetConfig testPlanet1("testPlanet1",
		"Test Planet 1",
		{"debug_big_planet.png"},
		{"mil_t1f"},
		LevelDifficulty::easy, LevelDifficulty::medium,
		testPlanet0BGImages);
	mPlanets[testPlanet1.GetPlanetId()] = testPlanet1;

	PlanetConfig testPlanet2("testPlanet2",
		"Test Planet 2",
		{"debug_big_planet.png"},
		{"mil_t1f"},
		LevelDifficulty::medium, LevelDifficulty::hard,
		testPlanet0BGImages);
	mPlanets[testPlanet2.GetPlanetId()] = testPlanet2;

	PlanetConfig testPlanet3("testPlanet3",
		"Test Planet 3",
		{"debug_big_planet.png"},
		{"mil_t1f"},
		LevelDifficulty::hard, LevelDifficulty::extreme,
		testPlanet0BGImages);
	mPlanets[testPlanet3.GetPlanetId()] = testPlanet3;

	/*PlanetConfig testPlanet4("testPlanet4",
		"Test Planet 4",
		{"debug_big_planet.png"},
		{"mil_t1f", "mil_t1f2", "mil_t1GB", "mil_t1fod", "mil_t1fod2"},
		LevelDifficulty::extreme, LevelDifficulty::extreme,
		testPlanet0BGImages);
	mPlanets[testPlanet4.GetPlanetId()] = testPlanet4;*/
}

void Config::InitStagePlanets()
{
	mStagePlanets = {
		{"testPlanet0", "testPlanet1", "testPlanet2", "testPlanet3"/*, "testPlanet4"*/},
		{"testPlanet0"},
		{"testPlanet0"},
		{"testPlanet0"},
		{"testPlanet0"}
	};
}

const std::vector<std::string> Config::GetPlanetIdsForStage(int stage) const
{
	return mStagePlanets[stage];
}

const PlanetConfig& Config::GetPlanetConfig(const std::string& planetId) const
{
	return mPlanets.at(planetId);
}

DifficultyConfig Config::GetDifficultyConfig(LevelDifficulty difficulty) const
{
	if(difficulty == LevelDifficulty::easy)
		return DifficultyConfig(5.0f, 5);
	else if(difficulty == LevelDifficulty::medium)
		return DifficultyConfig(5.0f, 10);
	else if(difficulty == LevelDifficulty::hard)
		return DifficultyConfig(4.5f, 15);
	else if(difficulty == LevelDifficulty::extreme)
		return DifficultyConfig(4.0f, 20);
	else if(difficulty == LevelDifficulty::finalBoss)
		return DifficultyConfig(0.0f, 100);
	throw std::runtime_error("Invalid difficulty value = %i" + std::to_string(static_cast<int>(difficulty)));
}

void Config::InitEnemyPools()
{
	std::vector<FileInfo> files = UtilityServiceLocator::GetPlatformUtil().GetFilesInDirectory("assets\\enemyPools\\*");

	for(auto& file : files)
	{
		if(file.mIsDirectory)
			continue;

		string fullFilename = "assets\\enemyPools\\" + file.mFilename;
		std::ifstream stream;
		stream.open(fullFilename, std::ios::in | std::ios::binary);
		cereal::BinaryInputArchive archive(stream);

		string key = StringUtil::SplitString(file.mFilename, '.')[0];
		mEnemyPools[key] = EnemyPool();
		archive(mEnemyPools[key]);

		stream.close();
	}
}

void Config::InitBulletConfigs()
{
	mBulletConfigs["bullet1"] = BulletConfig("bullet1Animation", "bullet1Impact", 2);
	mBulletConfigs["bullet2"] = BulletConfig("bullet2Animation", "bullet2Impact", 2);
	mBulletConfigs["enemyBullet0"] = BulletConfig("EnemyBullet0", "EnemyBullet0", 1);
	mBulletConfigs["homingMissile"] = BulletConfig("homingMissile", "homingMissileImpact", 1);
	mBulletConfigs["interceptLaserBullet"] = BulletConfig("interceptLaserBullet", "interceptLaserImpact", 1);
	mBulletConfigs["seekerMissile"] = BulletConfig("seekerMissile", "seekermissileImpact", 1);

	// Player Tier 1
	mBulletConfigs["T1LightBullet"] = BulletConfig("T1LightBullet", "bullet2Impact", 2);
	mBulletConfigs["T1MediumBullet"] = BulletConfig("T1MediumBullet", "bullet2Impact", 2);
	mBulletConfigs["T1HeavyBullet"] = BulletConfig("T1HeavyBullet", "bullet2Impact", 2);
	
	// Player Tier 2
	mBulletConfigs["T2GatlingBullet"] = BulletConfig("T2GatlingBullet", "T2GatlingImpact", 2);

	// military
	mBulletConfigs["militaryBullet0"] = BulletConfig("militaryBullet0", "militaryBullet0Impact", 1);
	mBulletConfigs["mfSideGunBullet"] = BulletConfig("mfSideGunBullet", "mfSideGunBulletImpact", 1);
}

const BulletConfig& Config::GetBulletConfig(const string& bulletId) const
{
	return mBulletConfigs.at(bulletId);
}

const EnemyPool* Config::GetEnemyPool(const std::string& enemyId) const
{
	return &mEnemyPools.at(enemyId);
}

const Weapon* Config::GetPrimaryWeapon(const std::string& weaponId) const
{
	if(mPrimaryWeapons.find(weaponId) == mPrimaryWeapons.end())
		return nullptr;
	return &mPrimaryWeapons.at(weaponId);
}

const Weapon* Config::GetSecondaryWeapon(const std::string& weaponId) const
{
	if(mSecondaryWeapons.find(weaponId) == mSecondaryWeapons.end())
		return nullptr;
	return &mSecondaryWeapons.at(weaponId);
}

const Armor* Config::GetArmor(const std::string& armorId) const
{
	if(mArmors.find(armorId) == mArmors.end())
		return nullptr;
	return &mArmors.at(armorId);
}

// ---------------------- ColliderConfig -------------------------------
shared_ptr<Collider> ColliderConfig::GenerateCollider() const
{
	shared_ptr<Collider> collider;
	Vector2 pos(mPosInPixels.x / PIXELS_IN_UNIT, mPosInPixels.y / PIXELS_IN_UNIT);
	if(mType == ColliderType::circleCollider)
		return std::make_shared<CircleCollider>(pos, mRadiusInPixels / PIXELS_IN_UNIT);
	else if(mType == ColliderType::squareCollider)
		return std::make_shared<SquareCollider>(pos, D2DSize(mSizeInPixels.x / PIXELS_IN_UNIT, mSizeInPixels.y / PIXELS_IN_UNIT));
	throw new std::runtime_error("ERROR: Config had unknown collider type.");
}