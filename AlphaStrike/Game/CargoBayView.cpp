/*
CargoBayView

The view where the player reviews loot after a mission. They are shown dropped items one at a time and given
the option to store or recycle each one.

DELETE THESE NOTES LATER: The compare view should show the current new item on top, and the equipped item compared against
it by default. If an inventory item is moused over, it'll replace the equipped item. There should be text to clearly show to
the user which items are which.
*/

#include "CargoBayView.h"
#include "CompareView.h"
#include "GameGlobals.h"
#include "HomeBase.h"
#include "ItemView.h"
#include "ProgressionState.h"

using namespace std::placeholders;

CargoBayView::CargoBayView(HomeBase& homeBase, AnimationRegister& animationRegister):
	View(animationRegister),
	mHomeBase(homeBase)
{
}

CargoBayView::~CargoBayView()
{
}

void CargoBayView::Initialize()
{
	LoadFromJson("assets\\screens\\cargoBay.json", PIXELS_IN_UNIT);

	GetChild<Button>("turboliftButton")->SetMouseReleaseCallback(std::bind(&CargoBayView::OnTurboliftButton, this));

	mCompareView = std::make_shared<CompareView>(mAnimationRegister, mHomeBase);
	mCompareView->Initialize();
	GetChild<Node>("compareViewNode")->AddChild(mCompareView);

	mItemView = std::make_shared<ItemView>(mAnimationRegister, mHomeBase.GetProgressionState(), mCompareView, false);
	mItemView->Initialize();
	mItemView->SetItemMouseOverCallback(std::bind(&CargoBayView::OnItemMousedOver, this, _1));
	mItemView->SetItemMouseOutCallback(std::bind(&CargoBayView::OnItemMousedOut, this));
	GetChild<Node>("itemViewNode")->AddChild(mItemView);

	GetChild<Button>("equipButton")->SetMouseReleaseCallback(std::bind(&CargoBayView::OnEquipButton, this));
	GetChild<Button>("storeButton")->SetMouseReleaseCallback(std::bind(&CargoBayView::OnStoreButton, this));
	GetChild<Button>("recycleButton")->SetMouseReleaseCallback(std::bind(&CargoBayView::OnRecycleButton, this));

	UpdateCompareView();
}

void CargoBayView::UpdateCompareView()
{
	std::shared_ptr<Item> nextItem = GetNextItem();

	if(nextItem == nullptr)
		mCompareView->SetItems(nullptr, nullptr);
	else
		mCompareView->SetItems(mHomeBase.GetProgressionState().GetEquippedItemOfType(nextItem->GetType()), nextItem, "EQUIPPED", "NEW!");
}

std::shared_ptr<Item> CargoBayView::GetNextItem()
{
	auto& ps = mHomeBase.GetProgressionState();
	if(ps.GetMissionItemsOfType(ItemType::primaryWeapon).size() > 0)
		return ps.GetMissionItemsOfType(ItemType::primaryWeapon).begin()->second;
	if(ps.GetMissionItemsOfType(ItemType::secondaryWeapon).size() > 0)
		return ps.GetMissionItemsOfType(ItemType::secondaryWeapon).begin()->second;
	if(ps.GetMissionItemsOfType(ItemType::armor).size() > 0)
		return ps.GetMissionItemsOfType(ItemType::armor).begin()->second;
	return nullptr;
}

void CargoBayView::OnItemMousedOver(std::shared_ptr<Item> item)
{
	std::shared_ptr<Item> nextItem = GetNextItem();

	if(nextItem == nullptr)
		mCompareView->SetItems(nullptr, nullptr);
	else
		mCompareView->SetItems(item, nextItem, "", "NEW!");
}

void CargoBayView::OnItemMousedOut()
{
	UpdateCompareView();
}

void CargoBayView::OnEquipButton()
{
	if(GetNextItem() == nullptr)
		return;

	mHomeBase.GetProgressionState().AcceptMissionItem(GetNextItem()->GetGUID(), true);
	UpdateCompareView();
	mItemView->RefreshItems();
}

void CargoBayView::OnStoreButton()
{
	if(GetNextItem() == nullptr)
		return;

	mHomeBase.GetProgressionState().AcceptMissionItem(GetNextItem()->GetGUID(), false);
	UpdateCompareView();
	mItemView->RefreshItems();
}

void CargoBayView::OnRecycleButton()
{
	if(GetNextItem() == nullptr)
		return;

	mHomeBase.GetProgressionState().RecycleItem(GetNextItem()->GetGUID(), true);
	UpdateCompareView();
	mItemView->RefreshItems();
}

void CargoBayView::OnTurboliftButton()
{
	if(mHomeBase.GetProgressionState().GetMissionItemCount() == 0)
		mHomeBase.ShowTurbolift();
}