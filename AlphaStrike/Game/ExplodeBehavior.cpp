/*
ExplodeBehavior

A behavior that gives the appearance of an enemy exploding. All explosions by default tell the enemyShip to
play the 'explode' aniamtion.
*/

#include <algorithm>
#include "AnimatedSprite.h"
#include "AnimationRegister.h"
#include "EnemyShip.h"
#include "ExplodeBehavior.h"
#include "GameGlobals.h"
#include "Layer.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include "RandomGenerator.h"
#include "World.h"

#include "cereal\archives\binary.hpp"
#include "cereal\archives\json.hpp"
CEREAL_REGISTER_TYPE(ExplodeBehavior);
CEREAL_REGISTER_DYNAMIC_INIT(explodeB);

using std::make_shared;
using std::shared_ptr;
using std::string;

// for now many of these parameters will just be constant, but this may change if more variation is needed
const float EXPLOSION_INTERVAL = 0.1f;
const float MIN_ROTATION = (float)M_PI_2;
const float MAX_ROTATION = (float)M_PI;
const float DISTANCE_CHANGE = 0.01f;

// Constructor with a specific radius
ExplodeBehavior::ExplodeBehavior(float radius, const string& explosionAnimationName):
	mRadius(radius),
	mTimeUntilNewSprite(0.0f),
	mCurrentRotation(0.0f),
	mCurrentDistance(0.0f),
	mExplosionCount(0),
	mExplosionAnimationName(explosionAnimationName),
	mStarted(false)
{
	mBlocking = true;
}


ExplodeBehavior::~ExplodeBehavior()
{
}

std::shared_ptr<EnemyBehavior> ExplodeBehavior::Copy()
{
	return std::make_shared<ExplodeBehavior>();
}

void ExplodeBehavior::Run(float timestepSeconds)
{
	if(!mBlocking)
		return;

	auto& ar = mEnemyShip->GetWorld()->GetAnimationRegister();

	if(mExplosionAnimationName != "")
	{
		if(mExplosionCount <= 0)
		{
			if(!mStarted)
			{
				shared_ptr<AnimatedSprite> explosion = make_shared<AnimatedSprite>(Vector2(), D2DSize(0.0f, 0.0f, 1.0f));
				explosion->LoadAndPlayAnimation(ar.RegisterAnimation(mExplosionAnimationName), DEFAULT_ANIMATION, true, PIXELS_IN_UNIT);
				explosion->SetCenterPos(mEnemyShip->GetSprite()->GetCenter());
				explosion->SetRotation(mEnemyShip->GetSprite()->GetRotation());
				explosion->SetAnimationCompleteCallback(std::bind(&ExplodeBehavior::OnAnimationExpired, this, explosion));
				explosion->SetDepth(9999.0f);
				mEnemyShip->GetWorld()->GetWorldLayer()->AddChild(explosion);
				mEnemyShip->GetSprite()->SetVisible(false);
				mExplosionCount = 1;
				mStarted = true;
			}
			else
			{
				mBlocking = false;
			}
		}

		return;
	}

	if(mCurrentDistance > mRadius)
	{
		if(mExplosionCount <= 0)
		{
			mBlocking = false;
		}

		return;
	}
		

	mTimeUntilNewSprite -= timestepSeconds;

	if(mTimeUntilNewSprite <= 0.0f)
	{
		Vector2 vec(0.0f, mCurrentDistance);
		mCurrentDistance += DISTANCE_CHANGE;
		vec.Rotate(mCurrentRotation);
		vec += Vector2(mEnemyShip->GetSprite()->GetWidth() / 2.0f, mEnemyShip->GetSprite()->GetHeight() / 2.0f);
		mCurrentRotation += mEnemyShip->GetWorld()->GetRandomGenerator().RandomInRange(MIN_ROTATION, MAX_ROTATION);

		shared_ptr<AnimatedSprite> explosion = make_shared<AnimatedSprite>(Vector2(), D2DSize(0.0f, 0.0f, 1.0f));
		explosion->LoadAndPlayAnimation(ar.RegisterAnimation("explosion1"), DEFAULT_ANIMATION, true, PIXELS_IN_UNIT);
		explosion->SetCenterPos(vec);
		explosion->SetAnimationCompleteCallback(std::bind(&ExplodeBehavior::OnAnimationExpired, this, explosion));
		mEnemyShip->GetSprite()->AddChild(explosion);
		mExplosionCount++;
		mTimeUntilNewSprite += EXPLOSION_INTERVAL;
	}

	// run continuously until 40% of the way through so it doesn't start too tiny
	if(mCurrentDistance < mRadius * 0.4f)
		Run(EXPLOSION_INTERVAL);
}

void ExplodeBehavior::OnAnimationExpired(shared_ptr<AnimatedSprite> explosion)
{
	explosion->RemoveFromParentAndCleanup();
	mExplosionCount--;
}

void ExplodeBehavior::Reset()
{
	if(mRadius < 0.0001f)
		mRadius = std::max(mEnemyShip->GetSprite()->GetWidth() / 2.0f, mEnemyShip->GetSprite()->GetHeight() / 2.0f);	// very simple but should be sufficient

	mCurrentRotation = mEnemyShip->GetWorld()->GetRandomGenerator().RandomInRange(0.0f, (float)M_2_PI);

	mStarted = false;
	mBlocking = true;
}