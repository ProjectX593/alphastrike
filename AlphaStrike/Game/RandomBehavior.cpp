/*
RandomBehavior

Used for behaviors that are random in nature. DO NOT use world's RandomGenerator for these (except to seed), since RandomBehaviors
will need to sync up for LeaderBehaviors and must share a seed.
Derived classes need to create their own uniform_XXXX_distribution so this class doesn't constrain them to a specific random generator.
*/

#include <exception>
#include "EnemyShip.h"
#include "RandomBehavior.h"
#include "RandomGenerator.h"
#include "World.h"

#include "cereal\archives\binary.hpp"
#include "cereal\archives\json.hpp"
CEREAL_REGISTER_TYPE(RandomBehavior);
CEREAL_REGISTER_DYNAMIC_INIT(randomB);

using std::shared_ptr;

RandomBehavior::RandomBehavior()
{
}

// when copying, do NOT take the seed
RandomBehavior::RandomBehavior(const RandomBehavior& other)
{
}

RandomBehavior::~RandomBehavior()
{
}

void RandomBehavior::Reset()
{
	mGenerator.seed(mEnemyShip->GetWorld()->GetRandomGenerator().RandomInRange(0, RAND_MAX));
}

std::shared_ptr<EnemyBehavior> RandomBehavior::Copy()
{
	return std::make_shared<RandomBehavior>(*this);
}

void RandomBehavior::SetLeaderBehavior(std::weak_ptr<EnemyBehavior> behavior)
{
	if(behavior.lock() == nullptr)
	{
		mGenerator.seed(mEnemyShip->GetWorld()->GetRandomGenerator().RandomInRange(0, RAND_MAX));
		return;
	}

	shared_ptr<RandomBehavior> randBehavior = std::dynamic_pointer_cast<RandomBehavior>(behavior.lock());
	if(randBehavior == nullptr)
		throw new std::runtime_error("ERROR: A random behavior tried to set a different type of behavior to it's leader.");

	mGenerator = randBehavior->GetGenerator();
}