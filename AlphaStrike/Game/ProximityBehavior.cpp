/*
ProximityBehavior

A ProximityBehavior will cause an EnemyShip to change it's active BehaviorSet if the player is within a specified
distance of the EnemyShip, and optionally within a certain arc relative to the direction the EnemyShip is facing.
*/

#include "EnemyShip.h"
#include "PlayerShip.h"
#include "ProximityBehavior.h"
#include "World.h"

#include "cereal\archives\binary.hpp"
#include "cereal\archives\json.hpp"
CEREAL_REGISTER_TYPE(ProximityBehavior);
CEREAL_REGISTER_DYNAMIC_INIT(proximityB);

ProximityBehavior::ProximityBehavior():
	mDetectionDistance(0.0f),
	mDetectionArcStart(0.0f),
	mDetectionArcEnd(0.0f)
{
}

ProximityBehavior::ProximityBehavior(float detectionDistance, const std::string& behaviorSetId, float detectionArcStart, float detectionArcEnd):
	mDetectionDistance(detectionDistance),
	mDetectionArcStart(detectionArcStart),
	mDetectionArcEnd(detectionArcEnd),
	mBehaviorSetId(behaviorSetId)
{
}

ProximityBehavior::~ProximityBehavior()
{
}

std::shared_ptr<EnemyBehavior> ProximityBehavior::Copy()
{
	return std::make_shared<ProximityBehavior>(mDetectionDistance, mBehaviorSetId, mDetectionArcStart, mDetectionArcEnd);
}

void ProximityBehavior::Run(float timestepSeconds)
{
	float dist = mEnemyShip->GetSprite()->GetCenter().DistanceTo(mEnemyShip->GetWorld()->GetPlayerShip()->GetSprite()->GetCenter());

	if(dist < mDetectionDistance)
	{
		if(abs(abs(mDetectionArcStart) - abs(mDetectionArcEnd)) > 0.001f)
		{
			auto toPlayer = mEnemyShip->GetWorld()->GetPlayerShip()->GetSprite()->GetAbsoluteCenter() - mEnemyShip->GetSprite()->GetAbsoluteCenter();
			float angle = toPlayer.GetAngleFromUpVector();
			if(angle < mDetectionArcStart || angle > mDetectionArcEnd)
				return;
		}
		mEnemyShip->SetActiveBehaviorSetId(mBehaviorSetId);
	}
}