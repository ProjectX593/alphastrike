/*
World

The game world contains the current game state. It tracks ships and bullets, is responsible for simulating them,
and determining when relevant entities have collided.
*/

#include <algorithm>
#include "AnimatedSprite.h"
#include "BeamBullet.h"
#include "Bullet.h"
#include "Config.h"
#include "Crate.h"
#include "EnemyConfig.h"
#include "EnemyPool.h"
#include "EnemyShip.h"
#include "EnemySpawn.h"
#include "EventManager.h"
#include "GameGlobals.h"
#include "HudView.h"
#include "ItemManager.h"
#include "ItemPowerup.h"
#include "Layer.h"
#include "Powerup.h"
#include "PlayerShip.h"
#include "ProgressionState.h"
#include "RandomGenerator.h"
#include "RendererLocator.h"
#include "ScrapPowerup.h"
#include "Sprite.h"
#include <time.h>
#include "UtilityServiceLocator.h"
#include "World.h"

using std::make_shared;
using std::make_unique;
using std::shared_ptr;
using std::string;
using std::unique_ptr;
using std::vector;
using namespace std::placeholders;

const int CLEANUP_INTERVAL = 30;
const int MIN_SPAWNS = 10;
const int MAX_SPAWNS = 12;
const float FIRST_SPAWN_DELAY = 2.0f;

World::World(ProgressionState& progressionState, shared_ptr<Layer> frontendLayer, shared_ptr<Layer> worldLayer, shared_ptr<Layer> backgroundLayer, ItemManager& itemManager, InputSystem& inputSystem, RandomGenerator& randomGenerator, const Config* config):
	mConfig(config),
	mProgressionState(progressionState),
	mAnimationRegister(UtilityServiceLocator::GetPlatformUtil().GetWorkingDirectory() + "assets\\textures\\", UtilityServiceLocator::GetPlatformUtil().GetWorkingDirectory() + "assets\\animations\\"),
	mPersistantAnimationRegister(UtilityServiceLocator::GetPlatformUtil().GetWorkingDirectory() + "assets\\textures\\", UtilityServiceLocator::GetPlatformUtil().GetWorkingDirectory() + "assets\\animations\\"),
	mItemManager(itemManager),
	mInputSystem(inputSystem),
	mWorldLayer(worldLayer),
	mBackgroundLayer(backgroundLayer),
	mHudView(make_shared<HudView>(mPersistantAnimationRegister)),
	mBackground(mBackgroundLayer, mAnimationRegister),
	mPlayerShip(make_shared<PlayerShip>(mWorldLayer, inputSystem, this)),
	mCleanupCounter(0),
	mSpawnsLeft(0),
	mLoaded(false),
	mSpawnDelay(0.0f),
	mBossSpawned(false),
	mRandomGenerator(randomGenerator),
	mSpawnsDisabled(false)
{
	SetLoaded(false);
	srand(static_cast<unsigned int>(time(NULL)));
	//TODO: replace rand()
	rand();	// the first one was always the same, but will replace later so it doesn't matter...
	Vector2 screenSize = RendererLocator::GetRenderer().GetScreenSize();
	mHudView->Initialize(screenSize.x / screenSize.y);
	frontendLayer->AddChild(mHudView);
}

World::~World()
{
	mPlayerShip->Cleanup();
	EventManager::Get().UnsubscribeFromAllEvents(this);
}

void World::Simulate(float timestepSeconds)
{
	if(!mLoaded)
		return;

	mBackground.Simulate(timestepSeconds);
	mPlayerShip->Simulate(timestepSeconds);

	for(auto& bullet : mBullets)
		bullet->Simulate(timestepSeconds);

	SimulateLiveSpawnGroups(timestepSeconds);
	if(mSpawnsLeft > 0 && SpawnEnemies(timestepSeconds))	// stops enemies spawning once the limit is reached
		mSpawnsLeft--;

	if(mSpawnsLeft <= 0 && mEnemyShips.size() == 0 && !mBossSpawned)
	{
		mBossSpawned = true;	//TODO: Reimplement bosses with the editor
	}

	// we need to copy the enemy ships vector since ships can spawn new ships, ruining the iterator in the process
	auto enemyShipsCopy = mEnemyShips;
	for(auto& enemy : enemyShipsCopy)
		enemy->Simulate(timestepSeconds);
	for(auto& crate : mCrates)
		crate->Simulate(timestepSeconds);
	for(auto& powerup : mPowerups)
		powerup->Simulate(timestepSeconds);

	auto iter = mBullets.begin();
	bool playerInvulnerable = mPlayerShip->IsInvulnerable();
	while(iter != mBullets.end())
	{
		shared_ptr<Bullet> bullet = *iter;
		if(bullet->GetState() == BulletState::done)
		{
			bullet->Cleanup();
			iter = mBullets.erase(iter);
			continue;
		}
		else if(bullet->GetState() == BulletState::exploding)
		{
			iter++;
			continue;
		}

		bool collided = false;
		if(!bullet->IsPlayerBullet() && mPlayerShip->IsCollidingWith(bullet->GetCollider()))
		{
			if(mPlayerShip->IsReflective())
				bullet->Reflect();
			else
			{
				mPlayerShip->TakeDamage(bullet->GetDamage());
				bullet->Hit(mAnimationRegister, mWorldLayer);
				collided = true;
			}
		}
		else if(bullet->IsPlayerBullet())
		{
			for(auto& enemyShip : mEnemyShips)
			{
				collided = enemyShip->HandleCollision(bullet);
				if(collided)
					break;
			}

			if(!collided)
			{
				for(auto& crate : mCrates)
				{
					if(bullet->GetSprite()->IsCollidingWith(crate->GetSprite()))
					{
						crate->TakeDamage(bullet->GetDamage());
						bullet->Hit(mAnimationRegister, mWorldLayer);
						collided = true;
						break;
					}
				}
			}
		}
		iter++;
	}

	for(auto& enemyShip : mEnemyShips)
	{
		if(!enemyShip->IsDead() && !mPlayerShip->IsInvulnerable() && enemyShip->IsCollidingWith(mPlayerShip->GetCollider()))
		{
			// Collision damage is 10% of the larger ships max health, or 20% of max health, whichever is smaller
			int collisionDamage = std::max(mPlayerShip->GetMaxHealth(), enemyShip->GetMaxHealth()) / 10;
			mPlayerShip->TakeDamage(std::min(collisionDamage, mPlayerShip->GetMaxHealth() / 5));
			enemyShip->TakeDamage(std::min(collisionDamage, enemyShip->GetMaxHealth() / 5));
		}
	}

	auto powerupIter = mPowerups.begin();
	while(powerupIter != mPowerups.end())
	{
		if((*powerupIter)->GetSprite()->IsCollidingWith(mPlayerShip->GetSprite()))
		{
			(*powerupIter)->OnDie(mProgressionState);
			powerupIter = mPowerups.erase(powerupIter);
		}
		else
			powerupIter++;
	}

	SimulateComplete();
	if(mCleanupCounter++ > CLEANUP_INTERVAL)
	{
		CleanupOffscreenObjects();
		mCleanupCounter = 0;
	}

	if(mPlayerShip->GetHealth() <= 0)
		OnPlayerDie();

	if(mBossSpawned && mEnemyShips.empty())
		OnMissionComplete();
}

void World::SimulateLiveSpawnGroups(float timestepSeconds)
{
	auto iter = mLiveSpawnGroups.begin();
	while(iter != mLiveSpawnGroups.end())
	{
		if(iter->AllShipsDead())
		{
			iter = mLiveSpawnGroups.erase(iter);
		}
		else
		{
			iter->Simulate(timestepSeconds);
			iter++;
		}
	}
}

// returns true if it spawned an enemy, false if not
bool World::SpawnEnemies(float timestepSeconds)
{
	mSpawnDelay = std::max(mSpawnDelay - timestepSeconds, 0.0f);
	if(mSpawnDelay > 0.0f || mSpawnsDisabled)
		return false;

	// perform max difficulty rating check
	int totalDifficulty = 0;
	for(const auto& liveSpawnGroup : mLiveSpawnGroups)
	{
		if(mProgressionState.GetPlanet().GetDifficulty() == LevelDifficulty::extreme)
			totalDifficulty += liveSpawnGroup.GetCurrentDifficulty();
		else
			totalDifficulty += liveSpawnGroup.GetInitialDifficulty();
	}

	// This does mean we will regularly exceed the limit. Values will be tuned to accommodate this.
	if(totalDifficulty >= mProgressionState.GetDifficultyConfig().mMaxDifficultyPoints)
		return false;

	mSpawnDelay = mProgressionState.GetDifficultyConfig().mSpawnDelay;
	mSpawnDelay = mRandomGenerator.RandomInRange(mSpawnDelay - (mSpawnDelay * 0.15f), mSpawnDelay + (mSpawnDelay * 0.15f));	// make it a little more interesting

	const auto& spawns = mEnemyPool->GetEnemySpawnGroups();

	SpawnEnemyGroup(spawns.at(mRandomGenerator.RandomInRange(0, (int)spawns.size()-1)));

	return true;
}

void World::SpawnEnemyGroup(const EnemySpawnGroup& spawnGroup)
{
	float anchorX = mRandomGenerator.RandomInRange(spawnGroup.mMinAnchorX, spawnGroup.mMaxAnchorX);

	vector<shared_ptr<EnemyShip>> spawnGroupShips;
	for(auto& spawn : spawnGroup.mSpawns)
	{
		shared_ptr<EnemyShip> newEnemy = SpawnEnemy(spawn->GetEnemyId(), spawn->GetBehaviorSetId(), Vector2(spawn->GetXPos() + anchorX, spawn->GetYPos()));
		newEnemy->SetCenterPos(newEnemy->GetPos());
		spawnGroupShips.push_back(newEnemy);

		if(spawn->GetDelay() > 0.0f)
			newEnemy->Halt(spawn->GetDelay());
	}

	int leaderIndex = spawnGroup.mLeaderIndex;
	if(leaderIndex >= 0)
	{
		for(int i = 0; i < (int)spawnGroupShips.size(); i++)
		{
			if(i != leaderIndex)
				spawnGroupShips[i]->SetLeader(spawnGroupShips[leaderIndex]);
		}
	}

	// the leave weights don't add up to 100 or anything, select one of them based on any number of weights
	SpawnLeaveConfig conf = SpawnLeaveConfig(LeaveType::none, 1.0f, 0);
	if(spawnGroup.mLeaveConfigs.size() > 0)
	{
		int totalWeight = 0;
		for(auto& leaveConfig : spawnGroup.mLeaveConfigs)
			totalWeight += leaveConfig.mWeight;
		totalWeight = mRandomGenerator.RandomInRange(0, totalWeight - 1);	// -1 so we dont skew the odds because of the 0 we generate
		for(auto& leaveConfig : spawnGroup.mLeaveConfigs)
		{
			conf = leaveConfig;
			totalWeight -= leaveConfig.mWeight;
			if(totalWeight <= 0)
				break;
		}
	}

	mLiveSpawnGroups.push_back(LiveSpawnGroup(spawnGroupShips, conf.mLeaveType, conf.mAcceleration, mRandomGenerator.RandomInRange(spawnGroup.mMinLeaveTime, spawnGroup.mMaxLeaveTime), anchorX));
}

shared_ptr<EnemyShip> World::SpawnEnemy(const string& enemyId, const string& initialBehaviorSetId, const Vector2& pos)
{
	const EnemyConfig& conf = mEnemyPool->GetEnemyConfig(enemyId);

	if(conf.mAnimationName == "" || conf.mMaxHealth <= 0)
		throw new std::runtime_error("ERROR: Invalid enemy config.");

	shared_ptr<EnemyShip> newEnemy = std::make_shared<EnemyShip>(pos, conf, initialBehaviorSetId, this);
	mEnemyShips.push_back(newEnemy);
	return newEnemy;
}

void World::SimulateComplete()
{
	auto enemyEnd = std::remove_if(mEnemyShips.begin(), mEnemyShips.end(), [this](shared_ptr<EnemyShip> enemy)
		{
			bool dies = enemy->IsDead();
			if(dies)
				enemy->OnDie(this);
			return dies;
		});
	mEnemyShips.erase(enemyEnd, mEnemyShips.end());

	auto cratesEnd = std::remove_if(mCrates.begin(), mCrates.end(), [this](const unique_ptr<Crate>& crate)
		{
			bool dies = crate->IsDead();
			if(dies)
				crate->OnDie(this);
			return dies;
		});
	mCrates.erase(cratesEnd, mCrates.end());

	mHudView->UpdatePlayerStats(mPlayerShip->GetHealthPercent(), mProgressionState.GetCurrencyManager().GetAmmoPercent());
}

template <class T>
void CleanupOffscreenObjectsFromVector(vector<T>& vec, World* world)
{
	auto iter = vec.begin();
	while(iter != vec.end())
	{
		if(world->IsNodeOutsideBounds((*iter)->GetSprite()))
		{
			(*iter)->Cleanup();
			iter = vec.erase(iter);
		}
		else
			iter++;
	}
}

void World::CleanupOffscreenObjects()
{
	for(int i = 0; i < (int)mEnemyShips.size(); i++)
	{	
		auto ship = mEnemyShips.at(i);
		if(ship->mHasEnteredScreenSpace && IsNodeOutsideBounds(ship->GetSprite()))
		{
			ship->Cleanup();
			mEnemyShips.erase(mEnemyShips.begin() + i);
			i--;
		}
		else if(!ship->mHasEnteredScreenSpace && !IsNodeOutsideBounds(ship->GetSprite()))
		{
			ship->mHasEnteredScreenSpace = true;
		}
	}

	CleanupOffscreenObjectsFromVector<shared_ptr<Bullet>>(mBullets, this);
	CleanupOffscreenObjectsFromVector<unique_ptr<Crate>>(mCrates, this);
	CleanupOffscreenObjectsFromVector<unique_ptr<Powerup>>(mPowerups, this);
}                              

bool World::IsNodeOutsideBounds(shared_ptr<Node> node)
{
	if(node->GetPos().x + node->GetWidth() < SCREEN_LEFT_EDGE)
		return true;
	if(node->GetPos().x > SCREEN_RIGHT_EDGE)
		return true;
	if(node->GetPos().y + node->GetHeight() < SCREEN_TOP_EDGE)
		return true;
	if(node->GetPos().y > SCREEN_BOTTOM_EDGE)
		return true;
	return false;
}

void World::OnPlayerMoved(float playerXPos)
{
	Vector2 screenSize = RendererLocator::GetRenderer().GetScreenSize();
	float rangeOfMotion = (2.0f - (screenSize.x / screenSize.y)) / 2.0f;	// the art is 2.0 across, 1.0 tall. The screen is 1.0 tall, so calculate what is on either side if it is centered.
	playerXPos -= 0.5f; // centers this about the middle of the screen, it now ranges from -1 to 1, since the screen is 2 wide
	mWorldLayer->SetPos(Vector2(-playerXPos * rangeOfMotion, 0.0f));
	mBackgroundLayer->SetPos(Vector2(-playerXPos * rangeOfMotion, 0.0f));

	mBackground.OnPlayerMoved(playerXPos);
}

void World::OnMissionComplete()
{
	Cleanup();
	mMissionCompleteCallback(true);
}

void World::OnPlayerDamaged()
{
	mHudView->OnPlayerDamaged();
}

void World::OnPlayerDie()
{
	Cleanup();
	mMissionCompleteCallback(false);
}

void World::Reset()
{
	Cleanup();
	mPlayerShip->Respawn();

	mSpawnsLeft = mRandomGenerator.RandomInRange(MIN_SPAWNS, MAX_SPAWNS);
	mBackground.LoadChunkTextures(mProgressionState.GetPlanet().GetPlanetConfig().GetBackgroundLayerImages());
	mEnemyPool = mConfig->GetEnemyPool("TestPlanet");
	SetLoaded(true);

	shared_ptr<Item> secondary = mProgressionState.GetEquippedItemOfType(ItemType::secondaryWeapon);
	mHudView->SetWeaponIcon(secondary == nullptr ? "" : secondary->mThumbnail);
	mSpawnDelay = FIRST_SPAWN_DELAY;
}

void World::Cleanup()
{
	SetLoaded(false);

	for(auto& bullet : mBullets)
		bullet->Cleanup();
	for(auto& enemy : mEnemyShips)
		enemy->Cleanup();
	for(auto& crate : mCrates)
		crate->Cleanup();
	for(auto& powerup : mPowerups)
		powerup->Cleanup();
	
	mBullets.clear();
	mEnemyShips.clear();
	mCrates.clear();
	mPowerups.clear();
	mBackground.Cleanup();
	mLiveSpawnGroups.clear();

	mAnimationRegister.ReleaseAllTextures();
	mBossSpawned = false;
}

void World::SetLoaded(bool loaded)
{
	mWorldLayer->SetVisible(loaded);
	mBackgroundLayer->SetVisible(loaded);
	mLoaded = loaded;
}

void World::SetMissionCompleteCallback(std::function<void(bool)> missionCompleteCallback)
{
	mMissionCompleteCallback = missionCompleteCallback;
}

void World::SpawnBullet(shared_ptr<Bullet> bullet)
{
	mBullets.push_back(bullet);
	if(std::dynamic_pointer_cast<BeamBullet>(bullet) == nullptr)
		mWorldLayer->AddChild(bullet->GetSprite());
}

void World::SpawnCrate(const Vector2& pos, const string& dropTableId)
{
	mCrates.push_back(make_unique<Crate>(pos, dropTableId, this));
}

void World::SpawnItemPowerup(const Vector2& pos, const string& itemId, ItemType type)
{
	mPowerups.push_back(make_unique<ItemPowerup>(pos, itemId, type, mAnimationRegister, mWorldLayer));
}

void World::SpawnScrapPowerup(const Vector2& pos, int scrapDrop)
{
	mPowerups.push_back(make_unique<ScrapPowerup>(pos, scrapDrop, mAnimationRegister, mWorldLayer));
}

void World::StartEditorMode()
{
	mHudView->SetVisible(false);
	mSpawnsDisabled = true;
}