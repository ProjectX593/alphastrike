/*
Ship -
Common base class for PlayerShip and EnemyShip.
*/

#include <algorithm>
#include "AnimatedSprite.h"
#include "AnimationRegister.h"
#include "Collider.h"
#include "Config.h"
#include "GameGlobals.h"
#include "Ship.h"
#include "Sprite.h"
#include "World.h"

using std::make_shared;
using std::shared_ptr;
using std::vector;

const float DEFAULT_HITBOX_SCALE = 0.8f;
const float FULL_DAMAGE_TINT = 2.0f;
const float DAMAGE_TINT_DURATION = 0.3f;

Ship::Ship(int health, shared_ptr<AnimatedSprite> shipSprite, World* world, float maxVelocity, float acceleration, const vector<ColliderConfig>& colliders):
	mWorld(world),
	mHealth(health),
	mMaxHealth(health),
	mMaxVelocity(maxVelocity),
	mAcceleration(acceleration),
	mShipSprite(shipSprite),
	mReflective(false),
	mDamageTint(0.0f)
{
	SetColliders(colliders);
}

Ship::~Ship()
{
}

void Ship::Simulate(float timestepSeconds)
{
	mDamageTint -= (timestepSeconds / DAMAGE_TINT_DURATION) * FULL_DAMAGE_TINT;
	mDamageTint = std::max(0.0f, mDamageTint);

	mShipSprite->SetColor(Vector3(1.0f + mDamageTint, 1.0f - (mDamageTint / 2.0f), 1.0f - (mDamageTint / 2.0f)));

	mVelocity += (mIntendedVelocity - mVelocity) * mAcceleration * timestepSeconds;
	if(mVelocity.Length() > mMaxVelocity)
	{
		mVelocity.Normalize();
		mVelocity *= mMaxVelocity;
	}
	mIntendedVelocity = Vector2();

	mShipSprite->SetPos(mShipSprite->GetPos() + mVelocity * timestepSeconds);

}

void Ship::TakeDamage(int damage)
{
	damage = std::max(0, damage);

	// always 50% chance to cause at least 1 damage
	//TODO: replace rand()
	if(damage <= 0)
		damage = static_cast<float>(rand()) / RAND_MAX < 0.5f ? 0 : 1;

	mHealth -= damage;
	mDamageTint = FULL_DAMAGE_TINT;
}

void Ship::Kill()
{
	mHealth = 0;
}

int Ship::GetHealth()
{
	return mHealth;
}

float Ship::GetHealthPercent()
{
	return static_cast<float>(mHealth) / static_cast<float>(mMaxHealth);
}

int Ship::GetMaxHealth()
{
	return mMaxHealth;
}

shared_ptr<AnimatedSprite> Ship::GetSprite()
{
	return mShipSprite;
}

void Ship::SetReflective(bool reflective)
{
	mReflective = reflective;
	
	if(mReflectSprite != nullptr)
		mReflectSprite->SetVisible(reflective);
}

bool Ship::IsReflective()
{
	return mReflective;
}

void Ship::OnDie()
{
	Cleanup();
}

void Ship::Cleanup()
{
	mShipSprite->RemoveFromParentAndCleanup();
}

void Ship::SetReflectTexture(const std::string& reflectAnimationName, const std::string& reflectAnimation)
{
	if(mReflectSprite != nullptr)
		mShipSprite->RemoveChild(mReflectSprite->GetId());

	mReflectSprite = make_shared<AnimatedSprite>(Vector2(), D2DSize(0.0f, 0.0f, 3.0f));
	mReflectSprite->LoadAndPlayAnimation(mWorld->GetAnimationRegister().RegisterAnimation(reflectAnimationName), reflectAnimation, true, PIXELS_IN_UNIT);
	mShipSprite->AddChild(mReflectSprite);
	mReflectSprite->SetCenterPos(Vector2(mShipSprite->GetWidth() / 2.0f, mShipSprite->GetHeight() / 2.0f));
	mReflectSprite->SetVisible(mReflective);
}

bool Ship::IsCollidingWith(shared_ptr<Collider> otherCollider)
{
	for(const auto& collider : mColliders)
	{
		if(collider->IsCollidingWith(otherCollider))
			return true;
	}

	return false;
}

// the default behavior, not used for enemy ships however
bool Ship::IsDead()
{
	return mHealth <= 0;
}

void Ship::SetColliders(const vector<ColliderConfig>& colliderConfigs)
{
	for(auto& collider : mColliders)
		collider->RemoveFromParentAndCleanup();
	mColliders.clear();

	for(auto& colliderConfig : colliderConfigs)
	{
		mColliders.push_back(colliderConfig.GenerateCollider());
		mShipSprite->AddChild(mColliders[mColliders.size() - 1]);
	}
}