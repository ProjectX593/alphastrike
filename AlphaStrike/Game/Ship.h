#pragma once

#include <memory>
#include <vector>

class AnimatedSprite;
class Collider;
struct ColliderConfig;
class Sprite;
class World;

class Ship
{
public:
	Ship(int health, std::shared_ptr<AnimatedSprite> shipSprite, World* world, float maxVelocity, float acceleration, const std::vector<ColliderConfig>& colliders = std::vector<ColliderConfig>());
	~Ship();

	virtual void Simulate(float timestepSeconds);
	virtual void TakeDamage(int damage);
	void Kill();
	int GetHealth();
	float GetHealthPercent();
	int GetMaxHealth();
	std::shared_ptr<AnimatedSprite> GetSprite();
	void SetReflective(bool reflective);
	bool IsReflective();
	virtual void OnDie();
	void Cleanup();
	bool IsCollidingWith(std::shared_ptr<Collider> otherCollider);
	virtual bool IsDead();
	void SetColliders(const std::vector<ColliderConfig>& colliderConfigs);
	World* GetWorld() { return mWorld; }

protected:
	World* mWorld;
	int mHealth;
	int mMaxHealth;
	float mDamageTint;
	Vector2 mVelocity;
	Vector2 mIntendedVelocity;	// Reset after each simulation step - the velocity the ship tries to reach
	float mMaxVelocity;
	float mAcceleration;
	std::shared_ptr<AnimatedSprite> mShipSprite;
	bool mReflective;
	std::shared_ptr<AnimatedSprite> mReflectSprite;
	std::vector<std::shared_ptr<Collider>> mColliders;

	void SetReflectTexture(const std::string& reflectAnimationName, const std::string& reflectAnimation);
	
};
