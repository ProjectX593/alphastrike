/*
Powerup

A powerup is anything the player collects by running into it with their ship.
This base class contains minimal functionality and should be extended by specific powerup types
which implement the game logic.
*/

#include "AnimatedSprite.h"
#include "AnimationRegister.h"
#include "GameGlobals.h"
#include "Layer.h"
#include "Powerup.h"

using std::string;

const float MOVE_SPEED = 0.1f;

Powerup::Powerup(const Vector2& initialCenterPos, const string& animationName, AnimationRegister& animationRegister, std::shared_ptr<Layer> worldLayer)
{
	mSprite = std::make_shared<AnimatedSprite>(Vector2(), D2DSize(0.0f, 0.0f, 10.0f));
	mSprite->LoadAndPlayAnimation(animationRegister.RegisterAnimation(animationName), DEFAULT_ANIMATION, true, PIXELS_IN_UNIT);
	mSprite->SetCenterPos(initialCenterPos);
	worldLayer->AddChild(mSprite);
	mPos = mSprite->GetPos();
}

Powerup::~Powerup()
{
}

void Powerup::Simulate(float timestepSeconds)
{
	mPos.y += MOVE_SPEED * timestepSeconds;
	mSprite->SetPos(mPos);
}

void Powerup::Cleanup()
{
	mSprite->RemoveFromParentAndCleanup();
}
