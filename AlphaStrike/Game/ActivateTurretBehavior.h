#pragma once

#include <set>
#include "TimedBehavior.h"
#include <vector>

#include "cereal/types/vector.hpp"

class ActivateTurretBehavior : public TimedBehavior
{
public:
	ActivateTurretBehavior();
	ActivateTurretBehavior(float duration, const std::vector<int>& turretsToActivate, bool infinite = false);
	~ActivateTurretBehavior();

	virtual std::shared_ptr<EnemyBehavior> Copy();
	virtual void Run(float timestepSeconds);
	virtual void Reset();
	virtual std::string GetDisplayString() { return "Activate Turret"; }

	const std::vector<int>& GetTurretsToActivate(){ return mTurretsToActivate; }
	bool GetInfinite() { return mInfinite; }
	void SetTurretsToActivate(const std::vector<int> turretsToActivate){ mTurretsToActivate = turretsToActivate; }
	void SetInfinite(bool infinite){ mInfinite = infinite; }

	template<class Archive>
	void serialize(Archive& archive, const std::uint32_t version)
	{
		archive(mTurretsToActivate, mInfinite, cereal::base_class<TimedBehavior>(this));
	}

private:
	std::vector<int> mTurretsToActivate;
	std::set<int>  mShootingTurrets;
	bool mStarted;
	bool mInfinite;

	void mTurretCharged(int turret);
};

CEREAL_CLASS_VERSION(ActivateTurretBehavior, 1);
CEREAL_FORCE_DYNAMIC_INIT(aTurretB);