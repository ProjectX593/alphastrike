#pragma once

#include "BulletBehavior.h"
#include <memory>
#include "TextureInfo.h"
#include <vector>
#include "Vector2.h"

#include "cereal/archives/binary.hpp"
#include "cereal/archives/json.hpp"

#define EVENT_CREATE_BULLET "createBullet"

class AnimatedSprite;
class AnimationRegister;
class BulletBehavior;
class Collider;
class Config;
class Layer;
class Ship;
class RandomGenerator;
class World;

enum class BulletState
{
	flying,
	exploding,
	done
};

class Bullet : public std::enable_shared_from_this<Bullet>
{
public:
	static std::shared_ptr<Bullet> Make(World* world);
	static std::shared_ptr<Bullet> Make(const Vector2& centerPos, const Vector2& initialVelocity, int damage, const std::vector<std::shared_ptr<BulletBehavior>>& behaviors, bool isPlayerBullet, bool rotates, const std::string& bulletId, World* world, std::shared_ptr<Collider> collider = nullptr);

	Bullet& operator=(const Bullet& other) = delete;
	~Bullet();

	virtual void Simulate(float timestepSeconds);
	void Reflect();

	std::shared_ptr<AnimatedSprite> GetSprite();
	bool IsPlayerBullet();
	virtual void Hit(AnimationRegister& animationRegister, std::shared_ptr<Layer> worldLayer);
	void Cleanup();
	void SetPos(const Vector2& pos);

	int GetDamage() const { return mDamage; }
	std::shared_ptr<Collider> GetCollider() { return mCollider; }
	const BulletState& GetState() const { return mState; }
	const Vector2& GetVelocity() const { return mVelocity; }
	void SetVelocity(const Vector2& velocity);
	World* GetWorld() { return mWorld; }

protected:
	Bullet(World* world);
	Bullet(const Vector2& centerPos, const Vector2& initialVelocity, int damage, const std::vector<std::shared_ptr<BulletBehavior>>& behaviors, bool isPlayerBullet, bool rotates, const std::string& bulletId, World* world, std::shared_ptr<Collider> collider = nullptr);
	Bullet(const Bullet& other) = delete; // To prevent crashes from callback functions going to the wrong place

	void Initialize();
	std::shared_ptr<AnimatedSprite> mSprite;
	std::shared_ptr<Collider> mCollider;
	int mDamage;
	bool mIsPlayerBullet;

private:
	Vector2 mCenterPos;	// we store this since the sprite would have to recalculate it every frame otherwise
	Vector2 mVelocity;
	std::vector<std::shared_ptr<BulletBehavior>> mBehaviors;
	int mBehaviorIndex;
	bool mRotates;
	World* mWorld;
	std::string mBulletId;
	BulletState mState;

	void TrySetDefaultVelocity();
	void DoneExploding();
};

