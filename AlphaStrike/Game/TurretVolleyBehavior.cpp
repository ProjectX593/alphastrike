/*
TurretVolleyBehavior

An enemy ship firing a turret volley will have it's turrets rotate to a specific position, then fire them constantly while rotating
at a given speed in radians per second until they reach a specified rotation.
The turrets will rotate at their given speed until they all reach their start position. It isn't until all turrets have finished
rotating from their start to their end position that the behavior finishes.
Note that the turret volley behavior always blocks until it is finished.
Can be passed a duration  to force the turrets to shoot for a specified amount of time.

NOTE: The index of each TurretVolleyInfo in the vector this behavior is initialized with will correspond to the turret that gets activated.
Obviously, there must be one volley info for each turret.
*/

#include "EnemyShip.h"
#include "Turret.h"
#include "TurretVolleyBehavior.h"

#include "cereal\archives\binary.hpp"
#include "cereal\archives\json.hpp"
CEREAL_REGISTER_TYPE(TurretVolleyBehavior);
CEREAL_REGISTER_DYNAMIC_INIT(turretVolleyB);

using std::shared_ptr;
using std::vector;

TurretVolleyBehavior::TurretVolleyBehavior():
	mStarted(false),
	mState(TurretVolleyState::rotatingIntoPlace)
{
}

TurretVolleyBehavior::TurretVolleyBehavior(const vector<TurretVolleyInfo>& volleyInfos, float duration):
	mVolleyInfos(volleyInfos),
	mStarted(false),
	mState(TurretVolleyState::rotatingIntoPlace),
	mTimer(duration)
{
	mBlocking = true;
}


TurretVolleyBehavior::~TurretVolleyBehavior()
{
}


shared_ptr<EnemyBehavior> TurretVolleyBehavior::Copy()
{
	return std::make_shared<TurretVolleyBehavior>(mVolleyInfos, mTimer.GetDuration());
}

void TurretVolleyBehavior::Run(float timestepSeconds)
{
	auto& turrets = mEnemyShip->GetTurrets();
	bool turretsFiring = false;

	if(!mBlocking)
		return;

	mTimer.Run(timestepSeconds);
	if(!mStarted)
	{
		mStarted = true;
		auto& turrets = mEnemyShip->GetTurrets();
		for(auto& volleyInfo : mVolleyInfos)
			turrets.at(volleyInfo.mTurretIndex).PrepareVolleyFire(volleyInfo);
	}

	// wait for all turrets to be in place before letting any of them fire
	if(mState == TurretVolleyState::rotatingIntoPlace)
	{
		bool turretsInPlace = true;
		for(auto& volleyInfo : mVolleyInfos)
			turretsInPlace = turretsInPlace && (turrets.at(volleyInfo.mTurretIndex).GetTurretState() == TurretState::volleyReady || turrets.at(volleyInfo.mTurretIndex).IsDead());

		if(turretsInPlace)
		{
			mState = TurretVolleyState::firing;
			TryStartTurretsFiring();
		}
	}
	else if(mState == TurretVolleyState::firing && !mTimer.IsExpired())
	{
		TryStartTurretsFiring();
	}
	// wait for all turrets to stop firing, then go inactive
	else if(mState == TurretVolleyState::firing && mTimer.IsExpired())
	{
		TryStartTurretsFiring();

		bool turretsDone = true;
		for(auto& volleyInfo : mVolleyInfos)
			turretsDone = turretsDone && (turrets.at(volleyInfo.mTurretIndex).GetTurretState() == TurretState::volleyDone || turrets.at(volleyInfo.mTurretIndex).IsDead());

		if(turretsDone)
		{
			for(auto& turret : turrets)
				turret.StopVolleyFire();
			mState = TurretVolleyState::discharging;
		}
	}
	else if(mState == TurretVolleyState::discharging)
	{
		bool turretsDone = true;
		for(auto& volleyInfo : mVolleyInfos)
			turretsDone = turretsDone && (turrets.at(volleyInfo.mTurretIndex).GetTurretState() == TurretState::idle || turrets.at(volleyInfo.mTurretIndex).IsDead());
		if(turretsDone)
			mBlocking = false;
	}
}

void TurretVolleyBehavior::TryStartTurretsFiring()
{
	auto& turrets = mEnemyShip->GetTurrets();
	for(auto& volleyInfo : mVolleyInfos)
	{
		if(turrets.at(volleyInfo.mTurretIndex).IsDead())
			continue;
		if(volleyInfo.mFireDelay <= mTimer.GetElapsedTime())
			turrets.at(volleyInfo.mTurretIndex).StartVolleyFire();
	}
}

void TurretVolleyBehavior::Reset()
{
	if(mEnemyShip->GetTurrets().size() > mVolleyInfos.size())
		throw new std::runtime_error("ERROR: Did not get enough volley infos for turret volley.");

	mStarted = false;
	mBlocking = true;
	mState = TurretVolleyState::rotatingIntoPlace;
	mTimer.Reset();
}