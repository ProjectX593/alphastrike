/*
SequenceBehavior

Runs groups of behaviors repeatedly. Unlike normal behaviors, once the behaviors go inactive, they are no longer run
until their group is run again. The active group doesn't change until all blocking behaviors complete.

SequenceBehaviors may be nested, in which case the run count is used to determine how many times the sequence of actions
should be run before the sequence is no longer active and blocking.

The behaviors in the sequence run from first to last by default but may be randomized by passing a random generator in. 
Note that randomized behaviors will never stop blocking.
*/

#include "EnemyShip.h"
#include <exception>
#include "RandomGenerator.h"
#include "SequenceBehavior.h"
#include "World.h"

#include "cereal\archives\binary.hpp"
#include "cereal\archives\json.hpp"
CEREAL_REGISTER_TYPE(SequenceBehavior);
CEREAL_REGISTER_DYNAMIC_INIT(sequenceB);

using std::make_shared;
using std::shared_ptr;
using std::weak_ptr;
using std::vector;

SequenceBehavior::SequenceBehavior():
	mCurrentGroup(0),
	mRunCount(0),
	mRunsLeft(0),
	mIsRandom(false)
{

}

SequenceBehavior::SequenceBehavior(const vector<vector<shared_ptr<EnemyBehavior>>>& behaviors, bool isRandom, int runCount):
	mBehaviors(behaviors),
	mCurrentGroup(0),
	mRunCount(runCount),
	mRunsLeft(runCount),
	mIsRandom(isRandom)
{
	mBlocking = true;
}

SequenceBehavior::~SequenceBehavior()
{
}

shared_ptr<EnemyBehavior> SequenceBehavior::Copy()
{
	vector<vector<shared_ptr<EnemyBehavior>>> behaviors;
	for(const auto& behaviorGroup : mBehaviors)
	{
		vector<shared_ptr<EnemyBehavior>> newGroup;
		for(const auto& behavior : behaviorGroup)
			newGroup.push_back(behavior->Copy());
		behaviors.push_back(newGroup);
	}

	return make_shared<SequenceBehavior>(behaviors, mIsRandom, mRunCount);
}

bool SequenceBehavior::RunBehaviors(vector<shared_ptr<EnemyBehavior>>& behaviors, float timestepSeconds)
{
	bool blocking = false;

	for(auto& behavior : behaviors)
	{
		behavior->Run(timestepSeconds);
		if(behavior->IsBlocking())
			blocking = true;
	}

	return blocking;
}

void SequenceBehavior::Run(float timestepSeconds)
{
	if(!mBlocking || mBehaviors.size() <= 0)
		return;

	auto& behaviors = mBehaviors[mCurrentGroup];
	bool blocking = false;
	blocking = RunBehaviors(behaviors, timestepSeconds);

	if(!blocking)
	{
		for(auto& behavior : behaviors)
			behavior->Reset();

		if(mIsRandom)
		{
			mCurrentGroup = mEnemyShip->GetWorld()->GetRandomGenerator().RandomInRange(0, (int)mBehaviors.size() - 1);
			mRunsLeft--;
		}
		else
		{
			mCurrentGroup += 1;
			if(mCurrentGroup == mBehaviors.size())
			{
				mRunsLeft--;
				mCurrentGroup = 0;
			}
		}

		if(mRunCount > 0 && mRunsLeft <= 0)
			mBlocking = false;

		// We run the behaviors again - running two sets in the same frame. This prevents the same behavior set from 'leaking' to the next frame
		// in some cases where precise timing is required. We do this only once though, to prevent an infinite loop.
		RunBehaviors(mBehaviors[mCurrentGroup], timestepSeconds);
	}
}

void SequenceBehavior::Initialize(EnemyShip* enemyShip)
{
	for(auto& behaviorGroup : mBehaviors)
	{
		for(auto& behavior : behaviorGroup)
			behavior->Initialize(enemyShip);
	}
}

void SequenceBehavior::Reset()
{
	mBlocking = true;
	if(mIsRandom)
		mCurrentGroup = mEnemyShip->GetWorld()->GetRandomGenerator().RandomInRange(0, (int)mBehaviors.size() - 1);
	else
		mCurrentGroup = 0;
	mRunsLeft = mRunCount;

	for(auto& behaviorGroup : mBehaviors)
	{
		for(auto& behavior : behaviorGroup)
			behavior->Reset();
	}
}

// This function makes the potentially dangerous assumption that the contents of the leader sequence
// are the same as this sequence. This is to avoid wastefully copying the contents a second time.
void SequenceBehavior::SetLeaderBehavior(weak_ptr<EnemyBehavior> leaderBehavior)
{
	shared_ptr<EnemyBehavior> behavior = leaderBehavior.lock();
	// to similar double nested for loops to avoid an if/ternary inside of a single one - should be more efficient
	if(behavior == nullptr)
	{
		for(const auto& behaviorGroup : mBehaviors)
		{
			for(const auto& behavior : behaviorGroup)
				behavior->SetLeaderBehavior(weak_ptr<EnemyBehavior>());
		}
		return;
	}

	shared_ptr<SequenceBehavior> leaderSequence = std::dynamic_pointer_cast<SequenceBehavior>(behavior);
	if(leaderSequence == nullptr)
		throw new std::runtime_error("ERROR: Sequence tried to set non sequence behavior as leader.");

	const auto& leaderBehaviorGroups = leaderSequence->GetBehaviors();
	for(size_t i = 0; i < mBehaviors.size(); i++)
	{
		for(size_t j = 0; j < mBehaviors[i].size(); j++)
			mBehaviors[i][j]->SetLeaderBehavior(leaderBehaviorGroups[i][j]);
	}
}
