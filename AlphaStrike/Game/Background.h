#pragma once

#include <memory>
#include "TextureInfo.h"
#include <vector>

class Layer;
class Sprite;
class AnimationRegister;

const int BACKGROUND_LAYER_COUNT = 4;

class Background
{
public:
	Background(std::shared_ptr<Layer> backgroundLayer, AnimationRegister& animationRegister);
	~Background();

	void LoadChunkTextures(const std::vector<std::vector<std::string>>& chunkTextures);
	std::shared_ptr<TextureInfo> RandomTextureForLayer(int layer);
	void Simulate(float timestepSeconds);
	void OnPlayerMoved(float playerXPos);
	void Cleanup();

private:
	AnimationRegister& mAnimationRegister;
	std::vector<std::vector<std::shared_ptr<TextureInfo>>> mChunkTextures;
	std::vector<int> mLastTextureIndices;
	std::vector<std::vector<std::shared_ptr<Sprite>>> mSprites;
	std::shared_ptr<Layer> mBackgroundLayer;
};

