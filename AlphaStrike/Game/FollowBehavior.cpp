/*
FollowBehavior

Causes an enemy to follow the player ship. Can be set to follow only in the x direction, or directly follow the player.
*/

#include "AnimatedSprite.h"
#include "EnemyShip.h"
#include "FollowBehavior.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include "PlayerShip.h"
#include "World.h"

#include "cereal\archives\binary.hpp"
#include "cereal\archives\json.hpp"
CEREAL_REGISTER_TYPE(FollowBehavior);
CEREAL_REGISTER_DYNAMIC_INIT(followB);

FollowBehavior::FollowBehavior():
	mMaxDistance(0.0f),
	mMaxVelocity(0.0f),
	mAcceleration(0.0f),
	mXOnly(false),
	mBackwards(false)
{
	
}

FollowBehavior::FollowBehavior(float maxDistance, float maxVelocity, float acceleration, bool xOnly, bool backwards):
	mMaxDistance(maxDistance),
	mMaxVelocity(maxVelocity),
	mAcceleration(acceleration),
	mXOnly(xOnly),
	mBackwards(backwards)
{
}


FollowBehavior::~FollowBehavior()
{
}

std::shared_ptr<EnemyBehavior> FollowBehavior::Copy()
{
	return std::make_shared<FollowBehavior>(mMaxDistance, mMaxVelocity, mAcceleration, mXOnly, mBackwards);
}

void FollowBehavior::Run(float timestepSeconds)
{
	if(mXOnly)
	{
		float dist = mEnemyShip->GetWorld()->GetPlayerShip()->GetSprite()->GetAbsoluteCenter().x - mEnemyShip->GetSprite()->GetAbsoluteCenter().x;
		if(abs(dist) > mMaxDistance && dist != 0.0f)
		{
			mVelocity.x += (abs(dist) / dist) * mAcceleration * timestepSeconds;
			if(mVelocity.x > mMaxVelocity)
				mVelocity.x = mMaxVelocity;
			if(mVelocity.x < -mMaxVelocity)
				mVelocity.x = -mMaxVelocity;
		}
		else if(dist != 0.0f)
		{
			float change = mAcceleration * timestepSeconds;
			if(change > abs(mVelocity.x))
				mVelocity.x = 0;
			else
				mVelocity.x -= (abs(mVelocity.x) / mVelocity.x) * change;
		}
	}
	else
	{
		Vector2 toPlayer = mEnemyShip->GetWorld()->GetPlayerShip()->GetSprite()->GetAbsoluteCenter() - mEnemyShip->GetSprite()->GetAbsoluteCenter();
		float dist = toPlayer.Length();

		if(dist > mMaxDistance )
		{
			mVelocity += toPlayer.GetNormalized() * mAcceleration * timestepSeconds;
			if(mVelocity.Length() > mMaxVelocity)
			{
				mVelocity.Normalize();
				mVelocity *= mMaxVelocity;
			}
			mEnemyShip->GetSprite()->SetRotation(mVelocity.GetAngleFromUpVector() + (mBackwards ? (float)M_PI : 0.0f));
		}
	}

	mEnemyShip->MoveBy(mVelocity * timestepSeconds);
}

void FollowBehavior::Reset()
{
	EnemyBehavior::Reset();
	mVelocity = Vector2();
}