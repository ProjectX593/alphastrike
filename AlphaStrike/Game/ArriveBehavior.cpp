/*
ArriveBehavior

Causes an enemy to move smoothly down into the screen from above.
Blocks by default so it can be used in sequences, but cannot be reset.
Note that the arrive behavior doesn't use the EnemyShip acceleration - it moves the ship
independantly of that value.
*/

#include "ArriveBehavior.h"
#include "EnemyShip.h"

#include "cereal\archives\binary.hpp"
#include "cereal\archives\json.hpp"
CEREAL_REGISTER_TYPE(ArriveBehavior);
CEREAL_REGISTER_DYNAMIC_INIT(arriveB);

ArriveBehavior::ArriveBehavior():
	mTravelDistance(0.0f),
	mDistanceTraveled(0.0f),
	mInitialSpeed(0.0f)
{
	mBlocking = true;
}

ArriveBehavior::ArriveBehavior(float travelDistance, float initialSpeed):
	mTravelDistance(travelDistance),
	mDistanceTraveled(0.0f),
	mInitialSpeed(initialSpeed)
{
	mBlocking = true;
}

ArriveBehavior::~ArriveBehavior()
{
}

std::shared_ptr<EnemyBehavior> ArriveBehavior::Copy()
{
	return std::make_shared<ArriveBehavior>(*this);
}

void ArriveBehavior::Run(float timestepSeconds)
{
	if(!mBlocking)
		return;

	float dist = (mInitialSpeed - (0.9f * mInitialSpeed * mDistanceTraveled / mTravelDistance)) * timestepSeconds;
	mEnemyShip->MoveBy(Vector2(0.0f, dist));
	mDistanceTraveled += dist;

	if(mDistanceTraveled >= mTravelDistance)
		mBlocking = false;
}
