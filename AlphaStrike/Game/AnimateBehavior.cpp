/*
AnimateBehavior

A behavior that plays an animation once until it stops. This behavior blocks until the animation is complete.
May add a mode in the future that plays the animation repeatedly but doesn't block.
*/

#include "AnimateBehavior.h"
#include "AnimatedSprite.h"
#include "EnemyShip.h"

#include "cereal\archives\binary.hpp"
#include "cereal\archives\json.hpp"
CEREAL_REGISTER_TYPE(AnimateBehavior);
CEREAL_REGISTER_DYNAMIC_INIT(animateB);

using std::shared_ptr;
using std::string;

// Default constructor used by cereal
AnimateBehavior::AnimateBehavior():
	mPlaying(false),
	mBlockUntilComplete(true)
{

}

// This constructor starts an animation looping repeatedly and immediately stops
AnimateBehavior::AnimateBehavior(const string& animation):
	TimedBehavior(0.0f),
	mAnimation(animation),
	mFinishAnimation(""),
	mPlaying(false),
	mBlockUntilComplete(true)
{
}

// This constructor plays the first animation once, then the finishAnimation repeatedly
AnimateBehavior::AnimateBehavior(const string& animation, const string& finishAnimation, bool blockUntilComplete):
	TimedBehavior(0.0f), // we have to start with 0 since we can't determine the length of the animation in the initializer list
	mAnimation(animation),
	mFinishAnimation(finishAnimation),
	mPlaying(false),
	mBlockUntilComplete(blockUntilComplete)
{
}

AnimateBehavior::~AnimateBehavior()
{
}

shared_ptr<EnemyBehavior> AnimateBehavior::Copy()
{
	return std::make_shared<AnimateBehavior>(*this);
}

void AnimateBehavior::Run(float timestepSeconds)
{
	if(!mPlaying)
	{
		mEnemyShip->GetSprite()->PlayAnimation(mAnimation);
		mPlaying = true;
	}

	TimedBehavior::Run(timestepSeconds);
}

void AnimateBehavior::Reset()
{
	auto animations = mEnemyShip->GetSprite()->GetAnimations();
	auto animation = animations.find(mAnimation);
	if(animation == animations.end())
		throw std::runtime_error("ERROR: Animation not found: " + mAnimation);

	if(mBlockUntilComplete)
		SetDuration((float)animation->second.mFrameCount / animation->second.mFramesPerSecond);

	TimedBehavior::Reset();

	mPlaying = false;
}

void AnimateBehavior::Done()
{
	mBlocking = false;

	if(mFinishAnimation != "")
		mEnemyShip->GetSprite()->PlayAnimation(mFinishAnimation);
}