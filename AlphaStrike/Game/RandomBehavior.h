#pragma once

#include "EnemyBehavior.h"
#include <random>

class RandomBehavior : public EnemyBehavior
{
public:
	RandomBehavior();
	RandomBehavior(const RandomBehavior& other);
	virtual ~RandomBehavior();

	virtual std::shared_ptr<EnemyBehavior> Copy();
	virtual void Reset();
	virtual void SetLeaderBehavior(std::weak_ptr<EnemyBehavior> behavior);
	const std::default_random_engine& GetGenerator() { return mGenerator; }
	virtual std::string GetDisplayString() { return "RANDOM BASE"; }

	template <class Archive>
	void serialize(Archive& archive, const std::uint32_t version)
	{
		archive(cereal::base_class<EnemyBehavior>(this));
	}

protected:
	std::default_random_engine mGenerator;
};

CEREAL_CLASS_VERSION(RandomBehavior, 1);
CEREAL_FORCE_DYNAMIC_INIT(randomB);