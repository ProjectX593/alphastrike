#pragma once

#include "EnemyConfig.h"
#include <functional>
#include "LeaveBehavior.h"
#include "Ship.h"
#include <string>
#include "Turret.h"
#include "Weapon.h"
#include "Vector2.h"

class EnemyBehavior;
class EnemyConfig;
class Sprite;

class EnemyShip;

class EnemyShip : public Ship, public std::enable_shared_from_this<EnemyShip>
{
public:
	EnemyShip(const Vector2& initialPos, const EnemyConfig& enemyConfig, const std::string& behaviorSetId, World* world);
	~EnemyShip();

	bool mHasEnteredScreenSpace;

	virtual void Simulate(float timestepSeconds);
	void MoveBy(const Vector2& vector);
	void SetPos(const Vector2& pos);
	void SetCenterPos(const Vector2& centerPos);
	virtual void OnDie(World* world);
	void FireWeapon(int weaponIndex);
	void SetLeader(std::weak_ptr<EnemyShip> leader);
	void SetActiveBehaviorSetId(const std::string& behaviorSetId);
	void AddFollower(std::shared_ptr<EnemyShip> follower);
	void SetAssignLeaderOnDeath(bool assignLeaderOnDeath);
	bool HandleCollision(std::shared_ptr<Bullet> bullet);
	void Halt(float duration);
	virtual void TakeDamage(int damage);
	virtual bool IsDead();
	void AddSeeker();
	void RemoveSeeker();
	int GetSeekerCount() const;
	void Leave(LeaveType type, float acceleration, float xSpawnPos);

	const Vector2& GetPos() { return mPos; }
	int GetDifficulty() { return mDifficulty; }
	std::weak_ptr<EnemyShip> GetLeader() { return mLeader; }
	const std::map<std::string, BehaviorSet>& GetBehaviorSets() { return mBehaviorSets; }
	const BehaviorSet& GetBehaviorSet(const std::string& behaviorSetId) { return mBehaviorSets.at(behaviorSetId); }
	const BehaviorSet& GetActiveBehaviorSet() { return mBehaviorSets.at(mActiveBehaviorSetId); }
	const std::string& GetActiveBehaviorSetId() { return mActiveBehaviorSetId; }
	std::vector<Turret>& GetTurrets() { return mTurrets; }
	void SetExplosionAnimation(const std::string& explosionAnimationName);

protected:
	Vector2 mPos;
	std::map<std::string, BehaviorSet> mBehaviorSets;
	std::string mActiveBehaviorSetId;
	std::vector<Turret> mTurrets;

	void AddTurretGroup(const TurretGroupConfig& turretGroup);

private:
	std::vector<Weapon> mWeapons;
	std::vector<bool> mWeaponTriggers;
	bool mDropsCrates;
	bool mAssignLeaderOnDeath;
	bool mExploding;
	bool mLeaves;
	bool mLeaving;
	std::string mDropTableId;
	int mScrapDrop;
	int mDifficulty;
	int mSeekerCount;
	float mHaltDuration;
	float mDropChance;
	std::weak_ptr<EnemyShip> mLeader;
	std::vector<std::shared_ptr<EnemyShip>> mFollowers;
	std::string mExplosionAnimationName;	// a special explosion animation to be played in place of a standard explosion

	void RunBehaviors(float timestepSeconds);
};