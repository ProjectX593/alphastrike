#pragma once

#include "TimedBehavior.h"
#include "Vector2.h"

class BulletCircleBehavior : public TimedBehavior
{
public:
	BulletCircleBehavior();
	BulletCircleBehavior(const Vector2& centerPos, float velocity, int bulletCount, int damage, const std::string& bulletId, float duration = 0.1f, float cooldown = 1.0f, float offsetRadians = 0.0f);
	~BulletCircleBehavior();

	virtual std::shared_ptr<EnemyBehavior> Copy();
	virtual void Run(float timestepSeconds);
	virtual void Reset();
	virtual std::string GetDisplayString() { return "Bullet Circle"; }

	const Vector2& GetCenterPos() const { return mCenterPos; }
	void SetCenterPos(const Vector2& centerPos){ mCenterPos = centerPos; }
	float GetVelocity() const { return mVelocity; }
	void SetVelocity(float velocity) { mVelocity = velocity; }
	int GetBulletCount() const { return mBulletCount; }
	void SetBulletCount(int bulletCount) { mBulletCount = bulletCount; }
	int GetDamage() const { return mDamage; }
	void SetDamage(int damage){ mDamage = damage; }
	const std::string& GetBulletId() const { return mBulletId; }
	void SetBulletId(const std::string& bulletId){ mBulletId = bulletId; }
	float GetCooldown() const { return mCooldown; }
	void SetCooldown(float cooldown){ mCooldown = cooldown; }
	float GetOffsetRadians() const { return mOffsetRadians; }
	void SetOffsetRadians(float offsetRadians) { mOffsetRadians = offsetRadians; }

	template <class Archive>
	void serialize(Archive& archive, const std::uint32_t version)
	{
		archive(mCenterPos, mVelocity, mBulletCount, mDamage, mBulletId, mCooldown, mOffsetRadians, cereal::base_class<TimedBehavior>(this));
	}

private:
	Vector2 mCenterPos;
	float mVelocity;
	int mBulletCount;
	int mDamage;
	std::string mBulletId;
	float mCooldown;
	float mTimeUntilFire;
	float mOffsetRadians;
};

CEREAL_CLASS_VERSION(BulletCircleBehavior, 2);
CEREAL_FORCE_DYNAMIC_INIT(bulletCB);