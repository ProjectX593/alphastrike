#pragma once

#include <functional>
#include "Item.h"
#include "Ship.h"

class AnimationRegister;
class Layer;

class Crate : public Ship
{
public:
	Crate(const Vector2& initialPos, const std::string& dropTableId, World* world);
	~Crate();

	virtual void OnDie(World* world);
	virtual void Simulate(float timestepSeconds);

private:
	Vector2 mPos;
	std::string mDropTableId;
};
