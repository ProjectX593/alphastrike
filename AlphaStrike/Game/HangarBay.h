#pragma once

#include "Item.h"
#include "View.h"

class HomeBase;
class ItemView;
class CompareView;

class HangarBay : public View
{
public:
	HangarBay(HomeBase& homeBase, AnimationRegister& animationRegister);
	~HangarBay();

	void Initialize();

private:
	HomeBase& mHomeBase;
	AnimationRegister& mAnimationRegister;
	std::shared_ptr<View> mCatwalkView;
	std::shared_ptr<View> mUndersideView;
	std::shared_ptr<CompareView> mCompareView;
	std::shared_ptr<ItemView> mItemView;

	void RefreshItems();
	void SetCatwalkViewVisible(bool visible);
	void UnequipItem(ItemType type);
	void OnItemMousedOver(std::shared_ptr<Item> item);
	void OnEquippedItemMousedOver(ItemType type);
	void HighlightTypeIcon(ItemType type);
	void OnItemMouseOut();
};
