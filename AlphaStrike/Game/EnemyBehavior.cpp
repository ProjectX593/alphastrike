/*
EnemyBehavior

The base class for all enemy behaviors which contains all the shared functions needed for the behavior system.
The EnemyShip is not in the constructor so these objects may be used in config and copied.

Unless there is a large memory usage or cpu overhead from copying these structures and all their data,
they will be copied instead of pointed to a central config structure to keep the code simpler.

mBlocking - This behavior is in the process of running, and will stop sequence behaviors from proceeding.
*/

#include "EnemyBehavior.h"
#include "EnemyShip.h"

using std::function;
using std::shared_ptr;

EnemyBehavior::EnemyBehavior():
	mBlocking(false),
	mEnemyShip(nullptr)
{
}

EnemyBehavior::~EnemyBehavior()
{
}

// Needed so we can make deep copies of shared_ptrs without a gross if else ladder
shared_ptr<EnemyBehavior> EnemyBehavior::Copy()
{
	return std::make_shared<EnemyBehavior>(*this);
}

void EnemyBehavior::Run(float timestepSeconds)
{
}

void EnemyBehavior::Initialize(EnemyShip* enemyShip)
{
	mEnemyShip = enemyShip;
	Reset();
}

bool EnemyBehavior::IsBlocking()
{
	return mBlocking;
}

// This function does nothing by default but is intended to cause an object
// that is no longer blocking for whatever reason to reset it's status and block again
// Typically will occur in a SequenceBehavior.
void EnemyBehavior::Reset()
{
}

// Some behaviors do something different when they belong to a follower ship,
// this function is overwritten to provide that functionality.
void EnemyBehavior::SetLeaderBehavior(std::weak_ptr<EnemyBehavior> behavior)
{

}