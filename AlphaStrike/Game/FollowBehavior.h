#pragma once

#include "EnemyBehavior.h"

class FollowBehavior : public EnemyBehavior
{
public:
	FollowBehavior();
	FollowBehavior(float maxDistance, float maxVelocity, float acceleration, bool xOnly, bool backwards);
	~FollowBehavior();

	float mMaxDistance;
	float mMaxVelocity;
	float mAcceleration;
	bool mXOnly;
	bool mBackwards;

	virtual std::shared_ptr<EnemyBehavior> Copy();
	virtual void Run(float timestepSeconds);
	virtual void Reset();
	virtual std::string GetDisplayString() { return "Follow"; }

	template <class Archive>
	void serialize(Archive& archive, const std::uint32_t version)
	{
		archive(mMaxDistance, mMaxVelocity, mAcceleration, mXOnly, mBackwards, cereal::base_class<EnemyBehavior>(this));
	}

private:
	Vector2 mVelocity;
};

CEREAL_CLASS_VERSION(FollowBehavior, 2);
CEREAL_FORCE_DYNAMIC_INIT(followB);