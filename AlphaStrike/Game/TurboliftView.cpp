/*
TurboliftView

The turbolift screen - it allows the player to navigate between the various home base screens.
*/

#include "Button.h"
#include "GameGlobals.h"
#include "HomeBase.h"
#include "TurboliftView.h"


TurboliftView::TurboliftView(HomeBase& homeBase, AnimationRegister& animationRegister):
	View(animationRegister),
	mHomeBase(homeBase)
{
}

TurboliftView::~TurboliftView()
{
}

void TurboliftView::Initialize()
{
	LoadFromJson("assets\\screens\\turbolift.json", PIXELS_IN_UNIT);

	GetChild<Button>("bridgeButton")->SetMouseReleaseCallback(std::bind(&HomeBase::ShowBridge, &mHomeBase));
	GetChild<Button>("hangarBayButton")->SetMouseReleaseCallback(std::bind(&HomeBase::ShowHangarBay, &mHomeBase));
	GetChild<Button>("reprocessingButton")->SetMouseReleaseCallback(std::bind(&HomeBase::ShowReprocessing, &mHomeBase));
	GetChild<Button>("cargoBayButton")->SetMouseReleaseCallback(std::bind(&HomeBase::ShowCargoBay, &mHomeBase));
	GetChild<Button>("shopButton")->SetMouseReleaseCallback(std::bind(&HomeBase::ShowShop, &mHomeBase));
	GetChild<Button>("engineeringButton")->SetMouseReleaseCallback(std::bind(&HomeBase::ShowEngineering, &mHomeBase));
	//GetChild<Button>("escapePodButton")->SetMouseReleaseCallback(std::bind(&HomeBase::ShowEscapePod, &mHomeBase));
	GetChild<Button>("escapePodButton")->SetMouseReleaseCallback(std::bind(&TurboliftView::HackSave, this));
}


//HACKS TO TEST CEREAL
#include "ProgressionState.h"
#include "UtilityServiceLocator.h"
void TurboliftView::HackSave()
{
	std::string dir = UtilityServiceLocator::GetPlatformUtil().GetWorkingDirectory() + "saves";
	UtilityServiceLocator::GetPlatformUtil().CreateDir(dir);
	mHomeBase.GetProgressionState().SaveToFile(dir + "\\default.sav");
}