/*
PlayerShip

The player's ship moves between 0 and 1 in it's coordinate space (actually 0.05 and 0.95 or so depending on it's width...)
*/

#include <algorithm>
#include "AnimatedSprite.h"
#include "AnimationRegister.h"
#include "Bullet.h"
#include "CircleCollider.h"
#include "EventManager.h"
#include "GameGlobals.h"
#include "InputSystem.h"
#include "Item.h"
#include "Layer.h"
#include <math.h>
#include "PlatformUtil.h"
#include "PlayerShip.h"
#include "ProgressionState.h"
#include <string>
#include "Sprite.h"
#include "Weapon.h"
#include "World.h"

using std::make_shared;
using std::shared_ptr;
using std::string;

const float MAX_VELOCITY = 1.0f;
const float ACCELERATION = 15.0f;
const float BULLET_VELOCITY = 0.7f;
const float PLAYER_SHIP_HITBOX_SCALE = 0.8f;
const float INVULNERABILITY_DURATION = 0.3f;
const int INITIAL_HEALTH = 5;
const Vector2 START_POS = Vector2(0.5f, 0.7f);

PlayerShip::PlayerShip(shared_ptr<Layer>& worldLayer, InputSystem& inputSystem, World* world):
	Ship(INITIAL_HEALTH, make_shared<AnimatedSprite>(START_POS, D2DSize(0.0f, 0.0f, 100.0f)), world, MAX_VELOCITY, ACCELERATION),
	mInputSystem(inputSystem),
	mInvulnTimer(INVULNERABILITY_DURATION, std::bind(&PlayerShip::OnInvulnDone, this), true),
	mInvulnerable(false)
{
	mShipSprite->LoadAndPlayAnimation(world->GetPersistantAnimationRegister().RegisterAnimation("playerShipAnim"), DEFAULT_ANIMATION, true, PIXELS_IN_UNIT);

	worldLayer->AddChild(mShipSprite);

	mColliders.push_back(CircleCollider::AddToCenterOfNode(mShipSprite, PLAYER_SHIP_HITBOX_SCALE));
}


PlayerShip::~PlayerShip()
{
}

void PlayerShip::Simulate(float timestepSeconds)
{
	SimulateMovement(timestepSeconds);

	Ship::Simulate(timestepSeconds);

	mShipSprite->SetPos(Vector2(std::max(SCREEN_LEFT_EDGE, std::min(mShipSprite->GetPos().x, SCREEN_RIGHT_EDGE - mShipSprite->GetWidth())),
		std::max(0.0f, std::min(mShipSprite->GetPos().y, 1.0f - mShipSprite->GetHeight()))));
	mWorld->OnPlayerMoved(mShipSprite->GetCenter().x);

	SimulateFiring(timestepSeconds);
	mInvulnTimer.Run(timestepSeconds);

	if(IsInvulnerable())
	{
		float alpha = (int)(mInvulnTimer.GetTimeLeft() * 15) % 2 == 0 ? 0.5f : 0.75f;
		mShipSprite->SetAlpha(alpha);
	}
}

void PlayerShip::SimulateMovement(float timestepSeconds)
{
	if(mInputSystem.GetKeyStatus(ButtonType::gameUp))
		mIntendedVelocity.y -= 1.0f;
	if(mInputSystem.GetKeyStatus(ButtonType::gameDown))
		mIntendedVelocity.y += 1.0f;
	if(mInputSystem.GetKeyStatus(ButtonType::gameLeft))
		mIntendedVelocity.x -= 1.0f;
	if(mInputSystem.GetKeyStatus(ButtonType::gameRight))
		mIntendedVelocity.x += 1.0f;

	mIntendedVelocity.Normalize();
	mIntendedVelocity *= mMaxVelocity;
}

void PlayerShip::SimulateFiring(float timestepSeconds)
{
	if(mPrimaryWeapon != nullptr)
		mPrimaryWeapon->Simulate(timestepSeconds, mInputSystem.GetKeyStatus(ButtonType::gameAttack1) > 0.0f, mShipSprite, mWorld);
	if(mSecondaryWeapon != nullptr)
	{
		bool fire = mInputSystem.GetKeyStatus(ButtonType::gameAttack2) > 0.0f && mSecondaryWeapon->CanFire() && mWorld->GetProgressionState().GetCurrencyManager().TrySpendAmmo(mSecondaryWeapon->mAmmoRequired);
		mSecondaryWeapon->Simulate(timestepSeconds, fire, mShipSprite, mWorld);
	}
}

void PlayerShip::Respawn()
{
	mShipSprite->SetPos(START_POS);

	ProgressionState& ps = mWorld->GetProgressionState();
	SetPrimaryWeapon(std::dynamic_pointer_cast<Weapon>(ps.GetEquippedItemOfType(ItemType::primaryWeapon)));
	SetSecondaryWeapon(std::dynamic_pointer_cast<Weapon>(ps.GetEquippedItemOfType(ItemType::secondaryWeapon)));
	SetArmor(std::dynamic_pointer_cast<Armor>(ps.GetEquippedItemOfType(ItemType::armor)));

	mVelocity = Vector2();
}

bool PlayerShip::IsInvulnerable() const
{
	return !mInvulnTimer.IsExpired() || mInvulnerable;
}

void PlayerShip::TakeDamage(int damage)
{
	if(IsInvulnerable() || damage <= 0)
		return;
	mInvulnTimer.Reset();
	Ship::TakeDamage(damage);

	if(mHealth > 0)
		mWorld->OnPlayerDamaged();
}

void PlayerShip::OnInvulnDone()
{
	mShipSprite->SetAlpha(1.0f);
}

shared_ptr<Collider> PlayerShip::GetCollider()
{
	return mColliders[0];
}

void PlayerShip::SetPrimaryWeapon(std::shared_ptr<Weapon> weapon)
{
	mPrimaryWeapon = weapon;
}

void PlayerShip::SetSecondaryWeapon(std::shared_ptr<Weapon> weapon)
{
	mSecondaryWeapon = weapon;
}

void PlayerShip::SetArmor(std::shared_ptr<Armor> armor)
{
	mMaxHealth = INITIAL_HEALTH;
	if(armor != nullptr)
	{
		mMaxHealth += armor->mHP;
		float speedFactor = (armor == nullptr ? 1.0f : armor->mSpeedFactor);
		mMaxVelocity = speedFactor * MAX_VELOCITY;
		mAcceleration = speedFactor * ACCELERATION;
	}
	mHealth = mMaxHealth;
}