#pragma once

#include "ManualTimer.h"
#include "TimedBehavior.h"

class AnimateBehavior : public TimedBehavior
{
public:
	AnimateBehavior();
	AnimateBehavior(const std::string& animation);
	AnimateBehavior(const std::string& animation, const std::string& finishAnimation, bool blockUntilComplete = true);
	~AnimateBehavior();

	std::string mAnimation;
	std::string mFinishAnimation;
	bool mBlockUntilComplete;

	virtual std::shared_ptr<EnemyBehavior> Copy();
	virtual void Run(float timestepSeconds);
	virtual void Reset();
	virtual void Done();
	virtual std::string GetDisplayString() { return "Animate - " + mAnimation; }

	template<class Archive>
	void serialize(Archive& archive, const std::uint32_t version)
	{
		if(version == 1)
			archive(mAnimation, mFinishAnimation, cereal::base_class<TimedBehavior>(this));
		else
			archive(mAnimation, mFinishAnimation, mBlockUntilComplete, cereal::base_class<TimedBehavior>(this));
	}

private:
	bool mPlaying;
};

CEREAL_CLASS_VERSION(AnimateBehavior, 2);
CEREAL_FORCE_DYNAMIC_INIT(animateB);