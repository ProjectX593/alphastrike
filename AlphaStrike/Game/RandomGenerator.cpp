/*
RandomGenerator

Wraps std random number generators. Previously a singleton class, it's usefulness as a class is now debatable...
*/

#include "RandomGenerator.h"
#include <time.h>


RandomGenerator::RandomGenerator()
{
	mGenerator.seed(static_cast<int>(time(NULL)));
}


RandomGenerator::~RandomGenerator()
{
}

int RandomGenerator::RandomInRange(int min, int max)
{
	std::uniform_int_distribution<int> dist(min, max);
	return dist(mGenerator);

}

float RandomGenerator::RandomInRange(float min, float max)
{
	std::uniform_real_distribution <float>dist(min, max);
	return dist(mGenerator);
}