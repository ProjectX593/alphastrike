/*
HomeBase

This class contains all the logic for displaying the various screens in game between missions. It is
essentially a UI manager/helper of sorts.
*/

#include "BridgeView.h"
#include "CargoBayView.h"
#include "HangarBay.h"
#include "HomeBase.h"
#include "Layer.h"
#include "ProgressionState.h"
#include "ReprocessingView.h"
#include "TurboliftView.h"
#include "World.h"
#include "View.h"

using std::function;
using std::make_shared;
using std::shared_ptr;
using namespace std::placeholders;

HomeBase::HomeBase(ProgressionState& progressionState, ItemManager& itemManager, const Config& config, std::shared_ptr<Layer> frontendLayer, AnimationRegister& animationRegister, World& world):
	mProgressionState(progressionState),
	mItemManager(itemManager),
	mConfig(config),
	mFrontendLayer(frontendLayer),
	mAnimationRegister(animationRegister),
	mWorld(world)
{
	mWorld.SetMissionCompleteCallback(std::bind(&HomeBase::MissionComplete, this, _1));
}

HomeBase::~HomeBase()
{
}

void HomeBase::ShowTurbolift()
{
	auto tl = make_shared<TurboliftView>(*this, mAnimationRegister);
	tl->Initialize();
	LoadView(tl);
}

void HomeBase::ShowBridge()
{
	auto b = make_shared<BridgeView>(*this, mAnimationRegister);
	b->Initialize();
	LoadView(b);
}

void HomeBase::ShowHangarBay()
{
	auto hb = make_shared<HangarBay>(*this, mAnimationRegister);
	hb->Initialize();
	LoadView(hb);
}

void HomeBase::ShowReprocessing()
{
	auto rp = make_shared<ReprocessingView>(*this, mAnimationRegister);
	rp->Initialize();
	LoadView(rp);
}

void HomeBase::ShowCargoBay()
{
	auto cb = make_shared<CargoBayView>(*this, mAnimationRegister);
	cb->Initialize();
	LoadView(cb);
}

void HomeBase::ShowShop()
{
}

void HomeBase::ShowEngineering()
{
}

void HomeBase::ShowEscapePod()
{
}

void HomeBase::CloseCurrentView()
{
	if(mCurrentView.lock() != nullptr)
		mCurrentView.lock()->Close();
}

void HomeBase::LoadView(shared_ptr<View> view)
{
	CloseCurrentView();
	mCurrentView = view;
	mFrontendLayer->AddChild(view);
}

void HomeBase::StartMission()
{
	CloseCurrentView();
	mWorld.Reset();
}

void HomeBase::MissionComplete(bool success)
{
	if(success)
	{
		mProgressionState.MissionComplete();
		ShowCargoBay();
	}
	else
	{
		ShowBridge();
	}
}