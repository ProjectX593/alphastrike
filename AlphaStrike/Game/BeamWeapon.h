#pragma once

#include "Weapon.h"

class BeamBullet;

class BeamWeapon : public Weapon
{
public:
	BeamWeapon();
	BeamWeapon(const std::string& bulletAnimationId, int damage, const Vector2& beamPixelPos, float beamRotationRadians, float collisionWidthPercent = 1.0f);
	~BeamWeapon();

	virtual std::shared_ptr<Weapon> Clone() const;
	virtual void Simulate(float timestepSeconds, bool firePressed, std::shared_ptr<AnimatedSprite> shipSprite, World* world, const Vector2& offset = Vector2());
	bool CanDealDamage();
	void ResetDamageInterval();

	const std::string& GetBulletAnimationId() const { return mBulletAnimationId; }
	void SetBulletAnimationId(const std::string& bulletAnimationId){ mBulletAnimationId = bulletAnimationId; }
	int GetDamage() const { return mDamage; }
	void SetDamage(int damage) { mDamage = damage; }
	float GetCollisionWidthPercent() const { return mCollisionWidthPercent; }
	void SetCollisionWidthPercent(float collisionWidthPercent) { mCollisionWidthPercent = collisionWidthPercent; }
	float GetBeamRotationRadians() const { return mBeamRotationRadians; }
	void SetBeamRotationRadians(float beamRotationRadians){ mBeamRotationRadians = beamRotationRadians; }
	const Vector2& GetBeamPixelPos() const { return mBeamPixelPos; }
	void SetBeamPixelPos(const Vector2& beamPixelPos) { mBeamPixelPos = beamPixelPos; }

	template<class Archive>
	void serialize(Archive& archive, const std::uint32_t version)
	{
		archive(mBulletAnimationId, mDamage, mBeamPixelPos, mBeamRotationRadians, mCollisionWidthPercent, cereal::base_class<Weapon>(this));
	}

private:
	std::shared_ptr<BeamBullet> mBeam;
	Vector2 mBeamPixelPos;
	float mBeamRotationRadians;
	float mCollisionWidthPercent;
	std::string mBulletAnimationId;
	int mDamage;
	float mTimeUntilDamage;
};

CEREAL_CLASS_VERSION(BeamWeapon, 1);
CEREAL_FORCE_DYNAMIC_INIT(beamWep);