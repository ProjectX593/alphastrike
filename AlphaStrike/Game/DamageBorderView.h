#pragma once
#include "View.h"

class DamageBorderView : public View
{
public:
	DamageBorderView(AnimationRegister& animationRegister);
	~DamageBorderView();

	void Initialize(float screenWidthUnits);
	void OnResizeScreen(float screenWidthUnits);
	void OnPlayerDamaged();

private:
	std::vector<std::shared_ptr<AnimatedSprite>> mBorderSprites;

	virtual void Run(float timestepSeconds);
};

