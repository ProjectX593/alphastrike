#pragma once

#include <random>

class RandomGenerator
{
public:
	RandomGenerator();
	~RandomGenerator();

	int RandomInRange(int min, int max);
	float RandomInRange(float min, float max);

private:
	std::default_random_engine mGenerator;
};

