/*
BulletBehavior

The base for all bullet behaviors. Derived classes will control the movements of bullets.
By default has a duration of 0, indicating it runs forever.

BulletBehaviors are stored using shared_ptr instead of unique_ptr mainly because the fact that they are stored in vectors
causes far too much complexity in the code and makes it far less readable. They are always explicitly copied into the behaviors,
so there is no risk of bad behavior there. YES it's not the best code but the clunkiness of unique_ptr + vector is too much to deal with.
*/

#include "Bullet.h"
#include "BulletBehavior.h"

BulletBehavior::BulletBehavior(float duration):
	mDuration(duration),
	mTimeLeft(duration)
{
}

BulletBehavior::~BulletBehavior()
{
}

std::shared_ptr<BulletBehavior> BulletBehavior::Copy()
{
	return std::make_shared<BulletBehavior>();
}

void BulletBehavior::Run(float timestepSeconds)
{
	if(mBullet == nullptr)
		throw new std::runtime_error("ERROR: Tried to run an uninitialized BulletBehavior.");
	mTimeLeft -= timestepSeconds;
}

void BulletBehavior::Initialize(std::shared_ptr<Bullet> bullet)
{
	mBullet = bullet;
}

void BulletBehavior::Reset()
{
	mTimeLeft = mDuration;
}

std::string BulletBehavior::GetDisplayString()
{
	return "Empty Behavior";
}

void BulletBehavior::TurnTowardsTarget(Vector2 targetVelocity, float turnRate, float timestepSeconds)
{
	Vector2 velocity = mBullet->GetVelocity();
	Vector2 newVelocity;
	velocity.Normalize();
	float speed = targetVelocity.Length();
	targetVelocity.Normalize();
	float currentAngle = targetVelocity.CalculateAngle(velocity);

	if(currentAngle < turnRate * timestepSeconds)
	{
		newVelocity = targetVelocity;
	}
	else
	{
		// need to find if angle is positive or negative
		float bulletAngle = velocity.GetAngleFromUpVector();
		targetVelocity.Rotate(-bulletAngle);

		newVelocity = velocity;
		float correction = turnRate * timestepSeconds * (targetVelocity.x > 0 ? 1.0f : -1.0f);
		newVelocity.Rotate(correction);
	}
	newVelocity *= speed;
	mBullet->SetVelocity(newVelocity * targetVelocity.Length());
}

bool BulletBehavior::Done()
{
	return mDuration > 0.0f && mTimeLeft <= 0.0f;
}

void BulletBehavior::Reflect()
{
}