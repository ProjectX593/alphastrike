#pragma once

#include "AnimatedSprite.h"
#include <string>
#include "Weapon.h"

#include "cereal\archives\binary.hpp"
#include "cereal\archives\json.hpp"
#include "Collider.h"	// cpp files in other projects can't serialize things with colliders, not sure why that happens when EnemyBehaviors work but I don't want to spend more time on this for now
						// the fix is to just include this here

class EnemyBehavior;

enum class ColliderType : int
{
	circleCollider,
	squareCollider,
	invalid
};

// This class is a bit clumsy, and not reusable in the case that more types of colliders are ever implemented. 
// However, it will get the job done for the purposes of this game (and as such lives in the game's project).
struct ColliderConfig
{
	ColliderConfig():mType(ColliderType::invalid), mRadiusInPixels(0.0f) {}
	ColliderConfig(ColliderType type, const Vector2& posInPixels, float radiusInPixels): mType(type), mPosInPixels(posInPixels), mRadiusInPixels(radiusInPixels) {}
	ColliderConfig(ColliderType type, const Vector2& posInPixels, const Vector2& sizeInPixels): mType(type), mPosInPixels(posInPixels), mSizeInPixels(sizeInPixels){}

	std::shared_ptr<Collider> GenerateCollider() const;

	ColliderType mType;
	Vector2 mPosInPixels;
	Vector2 mSizeInPixels;
	float mRadiusInPixels;

	std::string GetDisplayName()
	{
		if(mType == ColliderType::circleCollider)
			return "Circle Collider Config";
		else if(mType == ColliderType::squareCollider)
			return "Square Collider Config";
		else
			return "UNKNOWN COLLIDER CONFIG";
	}

	template<class Archive>
	void serialize(Archive& archive, const std::uint32_t version)
	{
		archive(mType, mPosInPixels, mSizeInPixels, mRadiusInPixels);
	}
};

CEREAL_CLASS_VERSION(ColliderConfig, 1);

struct TurretGroupConfig
{
	TurretGroupConfig(): mMaxHealth(1), mFacesPlayer(true), mInvulnerable(false), mVisible(true)
	{
		mWeapon = std::make_shared<Weapon>();
	}

	int mMaxHealth;
	std::vector<Vector2> mPositionsInPixels;	// each pos adds another turret to the group, these positions indicate the CENTER of the turret
	std::string mAnimationsName;
	std::string mIdleAnimation;
	std::string mChargeAnimation;
	std::string mFireAnimation;
	std::string mDischargeAnimation;
	bool mFacesPlayer;
	bool mInvulnerable;
	bool mVisible;
	std::shared_ptr<Weapon> mWeapon;	// this is a shared_ptr since it might be a BeamWeapon
	std::vector<ColliderConfig> mColliderConfigs;		// each turret will have all of these colliders

	template<class Archive>
	void serialize(Archive& archive, const std::uint32_t version)
	{
		archive(mMaxHealth, mPositionsInPixels, mAnimationsName, mIdleAnimation, mChargeAnimation, mFireAnimation, mDischargeAnimation,
			mFacesPlayer, mInvulnerable, mVisible, mWeapon, mColliderConfigs);
	}
};

CEREAL_CLASS_VERSION(TurretGroupConfig, 3);

typedef std::vector<std::shared_ptr<EnemyBehavior>> BehaviorSet;

class EnemyConfig
{
public:
	EnemyConfig();
	EnemyConfig(int maxHealth, int difficulty, const std::string& animationName, const std::string& initialAnimation = DEFAULT_ANIMATION, int scrapDrop = 0, float maxVelocity = 0.3f, float acceleration = 0.5f, bool isBoss = false);
	EnemyConfig(const EnemyConfig& other);
	EnemyConfig& operator=(const EnemyConfig& other);
	~EnemyConfig();

	int mMaxHealth;
	int mDifficulty;
	int mScrapDrop;
	float mMaxVelocity;
	float mAcceleration;
	bool mIsBoss;
	bool mLeaves;
	std::string mAnimationName;
	std::string mInitialAnimation = DEFAULT_ANIMATION;
	std::string mDropTableId;
	float mDropChance;
	std::string mReflectAnimationName;
	std::vector<Weapon> mWeapons;
	std::vector<TurretGroupConfig> mTurretGroupConfigs;
	std::vector<ColliderConfig> mColliderConfigs; // custom colliders for the ship, if empty, a default circle collider is used.

	const BehaviorSet& GetBehaviorSet(const std::string& setId) const;
	bool HasBehaviorSet(const std::string& setId) const;
	const std::map<std::string, BehaviorSet>& GetBehaviorSets() const { return mBehaviorSets; }
	std::map<std::string, BehaviorSet>& GetEditableBehaviorSets() { return mBehaviorSets; } // this is used by the editor, and should not be used by the game

	void SetBehaviors(const std::map<std::string, BehaviorSet>& behaviors);
	void AddBehaviorSet(const BehaviorSet& behaviorSet, const std::string& setId);

	void AddWeapon(const Weapon& weapon);
	void SetReflectAnimationName(const std::string& reflectAnimationName) { mReflectAnimationName = reflectAnimationName; }

	template<class Archive>
	void serialize(Archive& archive, const std::uint32_t version)
	{
		archive(mMaxHealth, mDifficulty, mScrapDrop, mMaxVelocity, mAcceleration, mIsBoss, mLeaves, mAnimationName, mInitialAnimation, mDropTableId, mDropChance,
			mReflectAnimationName, mWeapons, mBehaviorSets, mTurretGroupConfigs, mColliderConfigs);
	}

private:
	std::map<std::string, BehaviorSet> mBehaviorSets;
};

CEREAL_CLASS_VERSION(EnemyConfig, 6);