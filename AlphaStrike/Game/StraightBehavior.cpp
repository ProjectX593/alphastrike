/*
StraightBehavior

A very simple bullet behavior that causes a bullet to turn towards a target velocity at a specified rate.
*/

#include "Bullet.h"
#include "StraightBehavior.h"

#include "cereal\archives\binary.hpp"
#include "cereal\archives\json.hpp"
CEREAL_REGISTER_TYPE(StraightBehavior);
CEREAL_REGISTER_DYNAMIC_INIT(straightBB);

StraightBehavior::StraightBehavior():
	mTurnRate(0.0f),
	mTurnDuration(0.0f),
	mTurnTimeLeft(0.0f)
{	
}

StraightBehavior::StraightBehavior(const Vector2& targetVelocity, float duration):
	BulletBehavior(duration),
	mTargetVelocity(targetVelocity),
	mTurnRate(0.0f),
	mTurnDuration(0.0f),
	mTurnTimeLeft(0.0f)
{
}

StraightBehavior::StraightBehavior(const Vector2& targetVelocity, float turnRate, float turnDuration, float duration):
	BulletBehavior(duration),
	mTargetVelocity(targetVelocity),
	mTurnRate(turnRate),
	mTurnDuration(turnDuration),
	mTurnTimeLeft(turnDuration)
{
}


StraightBehavior::~StraightBehavior()
{
}

std::shared_ptr<BulletBehavior> StraightBehavior::Copy()
{
	return std::make_shared<StraightBehavior>(mTargetVelocity, mTurnRate, mTurnDuration, mDuration);
}

void StraightBehavior::Run(float timestepSeconds)
{
	BulletBehavior::Run(timestepSeconds);

	if(mTurnDuration == 0.0f || mTurnTimeLeft <= 0.0f)
		return;

	TurnTowardsTarget(mTargetVelocity, mTurnRate, timestepSeconds);
	mTurnTimeLeft -= timestepSeconds;
}

void StraightBehavior::Reset()
{
	mTurnTimeLeft = mTurnDuration;
}

std::string StraightBehavior::GetDisplayString()
{
	return "Straight";
}

void StraightBehavior::Reflect()
{
	mTargetVelocity = -mTargetVelocity;
}