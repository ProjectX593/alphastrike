/*
BridgeView

The bridge screen, where players will select mission groups and launch missions.
*/

#include "AnimationRegister.h"
#include "AnimatedSprite.h"
#include "BridgeView.h"
#include "Button.h"
#include "GameGlobals.h"
#include "HomeBase.h"
#include "ProgressionState.h"
#include "RandomGenerator.h"
#include "TextField.h"

using std::shared_ptr;
using std::string;
using std::vector;

const int PLANET_ICON_COUNT = 12;
const int MISSION_ICON_ROW_COUNT = 3;
const int MISSION_ICONS_PER_ROW = 4;

BridgeView::BridgeView(HomeBase& homeBase, AnimationRegister& animationRegister):
	View(animationRegister),
	mHomeBase(homeBase),
	mShopSelected(false),
	mSelectedPlanetIndex(-1)
{
}

BridgeView::~BridgeView()
{
}

void BridgeView::Initialize()
{
	mMainView = std::make_shared<View>(mAnimationRegister);
	mMainView->LoadFromJson("assets\\screens\\bridge.json", PIXELS_IN_UNIT);
	AddChild(mMainView);

	mMainView->GetChild<Button>("turboliftButton")->SetMouseReleaseCallback(std::bind(&HomeBase::ShowTurbolift, &mHomeBase));
	mMainView->GetChild<Button>("consoleButton")->SetMouseReleaseCallback(std::bind(&BridgeView::ShowConsoleView, this));

	mConsoleView = std::make_shared<View>(mAnimationRegister);
	mConsoleView->LoadFromJson("assets\\screens\\bridgeConsole.json", PIXELS_IN_UNIT);
	AddChild(mConsoleView);

	mConsoleView->GetChild<Button>("turboliftButton")->SetMouseReleaseCallback(std::bind(&HomeBase::ShowTurbolift, &mHomeBase));
	mConsoleView->GetChild<Button>("goButton")->SetMouseReleaseCallback(std::bind(&BridgeView::OnGoButton, this));
	mConsoleView->SetVisible(false);

	if(mHomeBase.GetProgressionState().MissionAvailable())
		ShowMissionState();
	else
		ShowPlanetState();
	UpdateGoButton();
}

void BridgeView::ShowConsoleView()
{
	mMainView->SetVisible(false);
	mConsoleView->SetVisible(true);
}

void BridgeView::HideConsoleIcons()
{
	for(int i = 0; i < PLANET_ICON_COUNT; i++)
		mConsoleView->GetChild<Button>("planetButton" + std::to_string(i))->SetVisible(false);
	for(int i = 0; i < MISSION_ICON_ROW_COUNT; i++)
	{
		for(int j = 0; j < MISSION_ICONS_PER_ROW; j++)
			GetMissionIcon(i, j)->SetVisible(false);
	}
}

void BridgeView::ShowPlanetState()
{
	HideConsoleIcons();

	ProgressionState& ps = mHomeBase.GetProgressionState();
	const auto& planets = ps.GetAvailablePlanets();

	for(size_t i = 0; i < planets.size(); i++)
	{
		auto button = GetButtonForPlanet(i);
		button->SetVisible(true);
		button->SetMouseClickCallback(std::bind(&BridgeView::ClickPlanet, this, i));
		button->SetMouseOverCallback(std::bind(&BridgeView::UpdateGoButton, this, i));
		button->SetMouseOutCallback(std::bind(&BridgeView::UpdateGoButton, this, -1));
		button->LoadAndPlayAnimation(mAnimationRegister.RegisterAnimation("BD" + std::to_string(static_cast<int>(planets.at(i).GetDifficulty())) + "Button"), "default");
	}

	/*(auto shopButton = GetShopButton();
	shopButton->SetVisible(true);
	shopButton->SetMouseClickCallback(std::bind(&BridgeView::ClickShop, this));*/
}

void BridgeView::ShowMissionState()
{
	HideConsoleIcons();

	int missionNumber = mHomeBase.GetProgressionState().GetMissionNumber();
	int missions = mHomeBase.GetProgressionState().GetMissionsPerPlanet();
	for(int i = 0; i < missions; i++)
	{
		auto btn = GetMissionIcon(i, mHomeBase.GetProgressionState().GetRandomGenerator().RandomInRange(0, 3));
		btn->SetVisible(true);
		if(i == missionNumber)
			btn->LoadAndPlayAnimation(mAnimationRegister.RegisterAnimation("BD1Button"));
		else if(i < missionNumber)
			btn->LoadAndPlayAnimation(mAnimationRegister.RegisterAnimation("BCDebugDebris"));
		else if(i > missionNumber)
			btn->LoadAndPlayAnimation(mAnimationRegister.RegisterAnimation("BD2Button"));
	}

	mConsoleView->GetChild<TextField>("goButtonLabel")->SetText("LAUNCH");
}

void BridgeView::ClickPlanet(int planetIndex)
{
	//if(mShopSelected)
	//	GetShopButton()->SetSelected(false);
	if(mSelectedPlanetIndex >= 0)
		GetButtonForPlanet(mSelectedPlanetIndex)->SetSelected(false);
	GetButtonForPlanet(planetIndex)->SetSelected(true);

	mShopSelected = false;
	mSelectedPlanetIndex = planetIndex;
	UpdateGoButton();
}

void BridgeView::ClickShop()
{
	/*if(!mShopSelected && mSelectedPlanetIndex >= 0)
		GetButtonForPlanet(mSelectedPlanetIndex)->SetSelected(false);

	GetShopButton()->SetSelected(true);
	mShopSelected = true;
	UpdateLaunchButtonText();*/
}

void BridgeView::OnGoButton()
{
	ProgressionState& ps = mHomeBase.GetProgressionState();
	if(mShopSelected)
	{
		mHomeBase.ShowShop();
	}
	else if(!ps.MissionAvailable())
	{
		ps.SelectPlanet(mSelectedPlanetIndex);
		ShowMissionState();
	}
	else
	{
		mHomeBase.StartMission();
	}
}

void BridgeView::UpdateGoButton(int planetIndexOverride)
{
	string text = "";
	if(mHomeBase.GetProgressionState().MissionAvailable())
		text = "LAUNCH";
	else if(mShopSelected)
		text = "SHOP";
	else if(mSelectedPlanetIndex >= 0)
		text = "GO";
	mConsoleView->GetChild<Button>("goButton")->SetDisabled(text == "");
	mConsoleView->GetChild<TextField>("goButtonLabel")->SetText(text);

	string description = "";
	int index = planetIndexOverride >= 0 ? planetIndexOverride : mSelectedPlanetIndex;
	if(index >= 0)
	{
		LevelDifficulty difficulty = mHomeBase.GetProgressionState().GetAvailablePlanets().at(index).GetDifficulty();
		if(difficulty == LevelDifficulty::easy)
			description = "EASY";
		else if(difficulty == LevelDifficulty::medium)
			description = "MEDIUM";
		else if(difficulty == LevelDifficulty::hard)
			description = "HARD";
		else if(difficulty == LevelDifficulty::extreme)
			description = "EXTREME";
	}
	mConsoleView->GetChild<TextField>("descriptionLabel")->SetText(description);
}

shared_ptr<Button> BridgeView::GetButtonForPlanet(int index)
{
	const auto& planetToDisplayIndMap = mHomeBase.GetProgressionState().GetPlanetToDisplayIndices();
	return mConsoleView->GetChild<Button>("planetButton" + std::to_string(planetToDisplayIndMap.at(index)));
}

shared_ptr<AnimatedSprite> BridgeView::GetMissionIcon(int missionIndex, int columnIndex)
{
	return mConsoleView->GetChild<AnimatedSprite>("missionIcon" + std::to_string(missionIndex) + "-" + std::to_string(columnIndex));
}