#pragma once

#include "BulletBehavior.h"

class Ship;

class InterceptBehavior : public BulletBehavior
{
public:
	InterceptBehavior();
	InterceptBehavior(float mSpeed, float turnRate, bool switchesTargets, int maxSeekers = 0, float duration = 0.0f);
	~InterceptBehavior();

	float mSpeed;
	float mTurnRate;
	bool mSwitchesTargets;
	int mMaxSeekers;

	virtual std::shared_ptr<BulletBehavior> Copy();
	virtual void Run(float timestepSeconds);
	virtual void Initialize(std::shared_ptr<Bullet> bullet);
	virtual void Reset();
	virtual std::string GetDisplayString();
	virtual void Reflect();

	template <class Archive>
	void serialize(Archive& archive, const std::uint32_t version)
	{
		archive(mSpeed, mTurnRate, mSwitchesTargets, mMaxSeekers, cereal::base_class<BulletBehavior>(this));
	}

private:
	std::weak_ptr<Ship> mTarget;

	void SetTarget(std::weak_ptr<Ship> target);
	std::weak_ptr<Ship> InterceptBehavior::FindNearestShip();
	Vector2 GetVelocityToShip(std::shared_ptr<Ship> ship, float speed);
};

CEREAL_CLASS_VERSION(InterceptBehavior, 2);
CEREAL_FORCE_DYNAMIC_INIT(intBB);