#pragma once

class EnemySpawn;

class Mission
{
public:
	Mission();
	~Mission();

	const EnemySpawn& GetRandomSpawn();

};

