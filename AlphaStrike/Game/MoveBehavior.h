#pragma once

#include "TimedBehavior.h"
#include "Vector2.h"

class MoveBehavior : public TimedBehavior
{
public:
	MoveBehavior();
	MoveBehavior(const Vector2& movement, float duration, float lerpSinStart = 0.0f, float lerpSinEnd = 1.0f);
	~MoveBehavior();

	virtual std::shared_ptr<EnemyBehavior> Copy();
	virtual void Run(float timestepSeconds);
	void Reset();
	virtual std::string GetDisplayString() { return "Move"; }

	const Vector2& GetMovement() const { return mMovement; }
	void SetMovement(const Vector2& movement){ mMovement = movement; }
	float GetLerpSinStart() const { return mLerpSinStart; }
	void SetLerpSinStart(float lerpSinStart) { mLerpSinStart = lerpSinStart; }
	float GetLerpSinEnd() const { return mLerpSinEnd; }
	void SetLerpSinEnd(float lerpSinEnd) { mLerpSinEnd = lerpSinEnd; }

	template <class Archive>
	void serialize(Archive& archive, const std::uint32_t version)
	{ 
		archive(mMovement, mLerpSinStart, mLerpSinEnd, cereal::base_class<TimedBehavior>(this));
	}

private:
	Vector2 mMovement;
	float mLastProgress;
	float mLerpSinStart;
	float mLerpSinEnd;
};

CEREAL_CLASS_VERSION(MoveBehavior, 2);
CEREAL_FORCE_DYNAMIC_INIT(moveB);