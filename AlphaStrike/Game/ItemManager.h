#pragma once

#include <map>
#include <memory>
#include <string>
#include <vector>
#include "Weapon.h"

class AnimationRegister;
class Config;

class ItemManager
{
public:
	ItemManager(AnimationRegister& animationRegister, const Config* config);
	~ItemManager();

	const Item* GetItem(const std::string& itemId, ItemType type);
	std::shared_ptr<Item> CreateItemFromId(const std::string& itemId, ItemType type);

private:
	const Config* mConfig;
};

