/*
ProgressionState

The ProgressionState tracks all of the players progress through the game.
It keeps track of their inventory, as well as their current planet and mission, and is
responsible for saving and loading this information to files.
*/

#include <algorithm>
#include "Config.h"
#include "FileUtil.h"
#include <fstream>
#include "GameGlobals.h"
#include "ItemManager.h"
#include <numeric>
#include "PlatformUtil.h"
#include "Planet.h"
#include "ProgressionState.h"
#include "RandomGenerator.h"
#include "UtilityServiceLocator.h"

#include "cereal/archives/json.hpp"

using std::ifstream;
using std::make_shared;
using std::map;
using std::ofstream;
using std::shared_ptr;
using std::string;
using std::vector;

const int MIN_PLANET_CHOICES = 2;
const int MAN_PLANET_CHOICES = 4;
const int PLANET_DISPLAY_COUNT = 12;
const int SHOP_DISPLAY_COUNT = 2;
const int TOTAL_PLANET_COUNT = 4;
const int MISSIONS_PER_PLANET = 3;
const int MAX_ITEMS = 8;

ProgressionState::ProgressionState(ItemManager& itemManager, RandomGenerator& randomGenerator, const Config& config):
	mConfig(config),
	mRandomGenerator(randomGenerator),
	mItemManager(itemManager),
	mNextItemGUID(0),
	mMissionNumber(0),
	mPlanetNumber(-1),
	mPlanet(&config)
{
	// this actually adds item maps for 'count' and 'none'. While strange, it allows us to return an empty map of items for none or 'count', and avoid a crash
	mItemMaps.insert(mItemMaps.begin(), static_cast<int>(ItemType::none) + 1, map<int, shared_ptr<Item>>());
	mMissionItemMaps.insert(mMissionItemMaps.begin(), static_cast<int>(ItemType::none) + 1, map<int, shared_ptr<Item>>());
}

ProgressionState::~ProgressionState()
{
}

// Starts a new game at the first mission, first planet
void ProgressionState::StartNewGame()
{
	for(auto& itemType : mItemMaps)
		itemType.clear();
	mEquippedItemGUIDs.clear();
	mEquippedItemGUIDs.insert(mEquippedItemGUIDs.begin(), static_cast<int>(ItemType::itemTypeCount), -1);
	mPlanetNumber = 0;
	mMissionNumber = 0;
	mNextItemGUID = 0;
	mPlanetToDisplayIndices.clear();
	mShopDisplayIndex = -1;

	GeneratePlanetChoices();
	AddStartingItems();
}

bool ProgressionState::MissionAvailable()
{
	return mAvailablePlanets.size() == 0;
}

void ProgressionState::SelectPlanet(int planetIndex)
{
	mPlanet = mAvailablePlanets.at(planetIndex);
	mAvailablePlanets.clear();
}

void ProgressionState::MissionComplete()
{
	mMissionNumber++;
	if(mMissionNumber >= MISSIONS_PER_PLANET)
	{
		mMissionNumber = 0;
		mPlanetNumber++;
		GeneratePlanetChoices();
	}
}

void ProgressionState::GeneratePlanetChoices()
{
	mAvailablePlanets.clear();
	vector<string> possiblePlanets = mConfig.GetPlanetIdsForStage(mPlanetNumber);

	// return only one planet when we are in the second last mission - the last mission will have only one planet for the boss.
	int planetsToGenerate = mPlanetNumber == TOTAL_PLANET_COUNT - 1 ? 1 : mRandomGenerator.RandomInRange(MIN_PLANET_CHOICES, MAN_PLANET_CHOICES);
	for(int i = 0; i < planetsToGenerate; i++)
	{
		int planetIndex = mRandomGenerator.RandomInRange(0, possiblePlanets.size() - 1);
		const PlanetConfig& planet = mConfig.GetPlanetConfig(possiblePlanets.at(planetIndex));
		mAvailablePlanets.push_back(Planet(planet, mRandomGenerator, &mConfig));
	}

	// generate the display indices
	vector<int> iconIndices = vector<int>(PLANET_DISPLAY_COUNT);
	std::iota(iconIndices.begin(), iconIndices.end(), 0);

	for(size_t i = 0; i < mAvailablePlanets.size(); i++)
	{
		int ind = mRandomGenerator.RandomInRange(0, (int)iconIndices.size() - 1);
		mPlanetToDisplayIndices[i] = iconIndices.at(ind);
		iconIndices.erase(iconIndices.begin() + ind);
	}

	mShopDisplayIndex = mRandomGenerator.RandomInRange(0, SHOP_DISPLAY_COUNT - 1);
}

// For testing purposes, give the player a bunch of items
void ProgressionState::AddStartingItems()
{
	auto wepGUID = AddItem(mItemManager.CreateItemFromId("T1MediumCannon", ItemType::primaryWeapon), false);
	auto armorGUID = AddItem(mItemManager.CreateItemFromId("CivilianArmor", ItemType::armor), false);

	AddItem(mItemManager.CreateItemFromId("T1HeavyCannon", ItemType::primaryWeapon), false);
	AddItem(mItemManager.CreateItemFromId("T1AblativeArmor", ItemType::armor), false);

	SetEquippedItem(wepGUID, ItemType::primaryWeapon);
	SetEquippedItem(armorGUID, ItemType::armor);
}

//TODO: Consider making itemManager a factory class - have it create the shared_ptrs for us instead of this weird copy thing.
int ProgressionState::AddItem(std::shared_ptr<Item> item, bool inMission)
{
	int typeIndex = static_cast<int>(item->GetType());

	if(inMission)
	{
		mMissionItemMaps.at(typeIndex)[mNextItemGUID] = item;
		mMissionItemMaps.at(typeIndex)[mNextItemGUID]->SetGUID(mNextItemGUID);
	}
	else
	{
		mItemMaps.at(typeIndex)[mNextItemGUID] = item;
		mItemMaps.at(typeIndex)[mNextItemGUID]->SetGUID(mNextItemGUID);
	}

	mNextItemGUID++;
	return mNextItemGUID - 1;
}

void ProgressionState::RemoveItem(int itemGUID, ItemType type)
{
	mItemMaps.at(static_cast<int>(type)).erase(itemGUID);
}

const std::map<int, std::shared_ptr<Item>>& ProgressionState::GetItemsOfType(ItemType type)
{
	return mItemMaps.at(static_cast<int>(type));
}

const std::map<int, std::shared_ptr<Item>>& ProgressionState::GetMissionItemsOfType(ItemType type)
{
	return mMissionItemMaps.at(static_cast<int>(type));
}

void ProgressionState::SetEquippedItem(int itemGUID, ItemType type)
{
	if(GetItemWithGUID(itemGUID, type) == nullptr)
		mEquippedItemGUIDs.at((int)type) = -1;
	else
		mEquippedItemGUIDs.at((int)type) = itemGUID;
}

shared_ptr<Item> ProgressionState::GetEquippedItemOfType(ItemType type)
{
	int typeIndex = static_cast<int>(type);
	if(typeIndex >= static_cast<int>(ItemType::itemTypeCount))
		return nullptr;

	if(mItemMaps.at(typeIndex).find(mEquippedItemGUIDs.at(typeIndex)) != mItemMaps.at(typeIndex).end())
		return mItemMaps.at(typeIndex).at(mEquippedItemGUIDs.at(typeIndex));
	else
		return nullptr;
}

shared_ptr<Item> ProgressionState::GetItemWithGUID(int guid, ItemType type)
{
	int typeIndex = static_cast<int>(type);
	if(typeIndex >= static_cast<int>(ItemType::itemTypeCount))
		return nullptr;

	auto& item = mItemMaps.at(typeIndex).find(guid);
	if(item != mItemMaps.at(typeIndex).end())
		return item->second;
	return nullptr;
}

void ProgressionState::RecycleItem(int guid, bool isMissionItem)
{
	auto& map = isMissionItem ? mMissionItemMaps : mItemMaps;
	for(auto& itemMap : map)
	{
		for(auto& item : itemMap)
		{
			if(item.first == guid)
			{
				mCurrencyManager.AddEnergy(item.second->mRecycleValue);
				itemMap.erase(itemMap.find(item.first));
				return;
			}
		}
	}
}

bool ProgressionState::AcceptMissionItem(int guid, bool equip)
{
	if(GetInventoryItemCount() >= MAX_ITEMS)
		return false;

	for(int i = 0; i < (int)mMissionItemMaps.size(); i++)
	{
		for(auto& item : mMissionItemMaps.at(i))
		{
			if(item.first == guid)
			{
				mItemMaps.at(i)[guid] = item.second;

				if(equip)
					SetEquippedItem(guid, item.second->GetType());

				mMissionItemMaps.at(i).erase(mMissionItemMaps.at(i).find(item.first));
				return true;
			}
		}
	}

	return false;
}

int ProgressionState::GetInventoryItemCount()
{
	int count = 0;
	for(auto& itemMap : mItemMaps)
	{
		for(auto& item : itemMap)
		{
			if(GetEquippedItemOfType(item.second->GetType())->GetGUID() != item.second->GetGUID())
				count++;
		}
	}
	return count;
}

int ProgressionState::GetMissionItemCount()
{
	int count = 0;
	for(auto& itemMap : mMissionItemMaps)
	{
		count += itemMap.size();
	}
	return count;
}

const DifficultyConfig& ProgressionState::GetDifficultyConfig() const
{
	return mConfig.GetDifficultyConfig(mPlanet.GetDifficulty());
}

//TODO: Catch exceptions
void ProgressionState::LoadFromFile(const std::string& fullFilename)
{
	mAvailablePlanets.clear();

	ifstream stream;
	stream.open(fullFilename, std::ios::in | std::ios::binary);
	cereal::BinaryInputArchive archive(stream);
	archive(*this);

	// We don't save anything retrievable from config, so initialize that data here
	if(mPlanet.GetPlanetId() != "")
		mPlanet.SetConfig(&mConfig);
	for(auto& planet : mAvailablePlanets)
		planet.SetConfig(&mConfig);

	// If I ever do a balance patch to the game, we can't rely on some of the serialized values being accurate - the player
	// might wind up with old values in their weapons they've already acquired while getting new values in dropped weapons. Too
	// strange to allow!
	for(auto& primWep : mItemMaps.at((int)ItemType::primaryWeapon))
	{
		auto configWep = mConfig.GetPrimaryWeapon(primWep.second->mItemId);
		if(configWep != nullptr)
			std::dynamic_pointer_cast<Weapon>(primWep.second)->TakeWeaponConfigValues(configWep);
	}
	for(auto& secWep : mItemMaps.at((int)ItemType::secondaryWeapon))
	{
		auto configWep = mConfig.GetSecondaryWeapon(secWep.second->mItemId);
		if(configWep != nullptr)
			std::dynamic_pointer_cast<Weapon>(secWep.second)->TakeWeaponConfigValues(configWep);
	}
	for(auto& armor : mItemMaps.at((int)ItemType::armor))
	{
		auto configArmor = mConfig.GetArmor(armor.second->mItemId);
		if(configArmor != nullptr)
			std::dynamic_pointer_cast<Armor>(armor.second)->TakeArmorConfigValues(configArmor);
	}
}

//TODO: Catch exceptions
void ProgressionState::SaveToFile(const string& fullFilename)
{
	ofstream stream;
	stream.open(fullFilename, std::ios::out | std::ios::binary);
	cereal::BinaryOutputArchive archive(stream);
	archive(*this);
}

int ProgressionState::GetMaxItems()
{
	return MAX_ITEMS;
}

int ProgressionState::GetMissionsPerPlanet()
{
	return MISSIONS_PER_PLANET;
}