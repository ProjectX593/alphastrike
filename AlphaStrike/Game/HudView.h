#pragma once

#include "FillBar.h"
#include "View.h"

class DamageBorderView;

class HudView : public View
{
public:
	HudView(AnimationRegister& animationRegister);
	~HudView();

	void Initialize(float screenWidthUnits);
	void OnResizeScreen(float screenWidthUnits);
	void UpdatePlayerStats(float healthPercent, float ammoPercent);
	void SetWeaponIcon(const std::string& itemThumbnail);
	void OnPlayerDamaged();

private:
	std::shared_ptr<Sprite> mSecondaryIcon;
	std::shared_ptr<FillBar> mHealthBar;
	std::shared_ptr<FillBar> mAmmoBar;
	std::shared_ptr<DamageBorderView> mDamageBorderView;
};

