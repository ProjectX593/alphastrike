#pragma once

#include "Config.h"
#include "CurrencyManager.h"
#include "Item.h"
#include "json.h"
#include <memory>
#include "Planet.h"
#include "TextureInfo.h"
#include "Vector2.h"

#include "cereal/archives/binary.hpp"
#include "cereal/archives/json.hpp"
#include "cereal/cereal.hpp"
#include "cereal/types/map.hpp"
#include "cereal/types/memory.hpp"
#include "cereal/types/polymorphic.hpp"
#include "cereal/types/vector.hpp"

class ItemManager;
class RandomGenerator;

class ProgressionState
{
public:
	ProgressionState() = delete;
	ProgressionState(ItemManager& itemManager, RandomGenerator& randomGenerator, const Config& config);
	~ProgressionState();

	void StartNewGame();
	bool MissionAvailable();
	void SelectPlanet(int planetIndex);
	void MissionComplete();

	int AddItem(std::shared_ptr<Item> item, bool inMission);
	void RemoveItem(int itemGUID, ItemType type);
	const std::map<int, std::shared_ptr<Item>>& GetItemsOfType(ItemType type);
	const std::map<int, std::shared_ptr<Item>>& GetMissionItemsOfType(ItemType type);
	void SetEquippedItem(int itemGUID, ItemType type);
	std::shared_ptr<Item> GetEquippedItemOfType(ItemType type);
	std::shared_ptr<Item> GetItemWithGUID(int guid, ItemType type);
	void RecycleItem(int guid, bool isMissionItem);
	bool AcceptMissionItem(int guid, bool equip);
	int GetInventoryItemCount();
	int GetMissionItemCount();
	const DifficultyConfig& GetDifficultyConfig() const;

	CurrencyManager& GetCurrencyManager() { return mCurrencyManager; }
	const Planet& GetPlanet() const { return mPlanet; }
	const std::vector<Planet>& GetAvailablePlanets() const { return mAvailablePlanets; }
	int GetMissionNumber() const { return mMissionNumber; }
	void SetMissionNumber(int mission) { mMissionNumber = mission; }
	RandomGenerator& GetRandomGenerator() { return mRandomGenerator; }
	ItemManager& GetItemManager() { return mItemManager; }
	const std::map<int, int>& GetPlanetToDisplayIndices() const { return mPlanetToDisplayIndices; }
	int GetShopDisplayIndex() const { return mShopDisplayIndex; }
	int GetPlanetNumber() const { return mPlanetNumber; }

	int GetMaxItems();
	int GetMissionsPerPlanet();

	void LoadFromFile(const std::string& fullFilename);
	void SaveToFile(const std::string& fullFilename);

	template<class Archive> 
	void serialize(Archive& archive, const std::uint32_t version)
	{
		archive(
			mPlanet,
			mAvailablePlanets,
			mMissionNumber,
			mPlanetNumber,
			mNextItemGUID,
			mItemMaps,
			mMissionItemMaps,
			mEquippedItemGUIDs,
			mPlanetToDisplayIndices,
			mShopDisplayIndex,
			mCurrencyManager);
	};

private:
	const Config& mConfig;
	RandomGenerator& mRandomGenerator;
	std::vector<Planet> mAvailablePlanets;
	std::map<int, int> mPlanetToDisplayIndices;
	int mShopDisplayIndex;
	int mNextItemGUID;
	Planet mPlanet;
	std::string mPlanetId;
	int mMissionNumber;
	int mPlanetNumber;
	ItemManager& mItemManager;
	std::vector<std::map<int, std::shared_ptr<Item>>> mItemMaps;
	std::vector<int> mEquippedItemGUIDs;
	CurrencyManager mCurrencyManager;

	// items acquired by the player during the mission, not yet in their possession
	int mMissionScrap;
	std::vector<std::map<int, std::shared_ptr<Item>>> mMissionItemMaps;

	void AddStartingItems();
	void GeneratePlanetChoices();
};

CEREAL_CLASS_VERSION(ProgressionState, 2);