#pragma once

#include "Bullet.h"

class SquareCollider;

class BeamBullet : public Bullet
{
public:
	~BeamBullet();

	static std::shared_ptr<BeamBullet> Make(const Vector2& pos, float rotationRadians, std::shared_ptr<AnimatedSprite> parent, int damage, bool isPlayerBullet, const std::string& animationId, World* world, float colliderWidthPercent = 1.0f);

	virtual void Simulate(float timestepSeconds);
	virtual void Hit(AnimationRegister& animationRegister, std::shared_ptr<Layer> worldLayer);

private:
	BeamBullet(const Vector2& pos, float rotationRadians, std::shared_ptr<AnimatedSprite> parent, int damage, bool isPlayerBullet, const std::string& animationId, World* world, float colliderWidthPercent = 1.0f);
};

