#pragma once

#include "AnimatedSprite.h"
#include "EnemyPool.h"
#include "EnemyShip.h"
#include "Item.h"
#include <map>
#include "Planet.h"
#include <string>
#include "TextureInfo.h"
#include <vector>
#include "Weapon.h"

#include "cereal/types/map.hpp"
#include "cereal/types/string.hpp"
#include "cereal/types/vector.hpp"

class AnimationRegister;
class Boss;
class EnemyBehavior;

struct Drop
{
	Drop(const std::string& itemId, ItemType type, float probability):mItemId(itemId), mType(type), mProbability(probability) {}
	std::string mItemId;
	ItemType mType;
	float mProbability;
};

struct DifficultyConfig
{
	DifficultyConfig(): mSpawnDelay(0.0f), mMaxDifficultyPoints(0) {}
	DifficultyConfig(float spawnDelay, int maxDifficultyPoints): mSpawnDelay(spawnDelay), mMaxDifficultyPoints(maxDifficultyPoints) {}
	float mSpawnDelay;
	int mMaxDifficultyPoints;
};

struct BulletConfig
{
	BulletConfig(): mImpactAnimationCount(0) {}
	BulletConfig(const std::string& animation, const std::string& impactAnimation, int impactAnimationCount): mAnimation(animation), mImpactAnimation(impactAnimation), mImpactAnimationCount(impactAnimationCount) {}
	std::string mAnimation;
	std::string mImpactAnimation;
	int mImpactAnimationCount;
};

class Config
{
public:
	Config();
	~Config();

	std::map<std::string, Weapon> mPrimaryWeapons;
	std::map<std::string, Weapon> mSecondaryWeapons;
	std::map<std::string, Armor> mArmors;

	bool VerifyItemIds();
	const std::vector<std::string> GetPlanetIdsForStage(int stage) const;
	const PlanetConfig& GetPlanetConfig(const std::string& planetId) const;
	DifficultyConfig GetDifficultyConfig(LevelDifficulty difficulty) const;
	const BulletConfig& GetBulletConfig(const std::string& bulletId) const;
	const EnemyPool* GetEnemyPool(const std::string& enemyPoolId) const;
	const Weapon* GetPrimaryWeapon(const std::string& weaponId) const;
	const Weapon* GetSecondaryWeapon(const std::string& weaponId) const;
	const Armor* GetArmor(const std::string& armorId) const;

	const std::map<std::string, std::vector<Drop>>& GetDropTables() const { return mDropTables; }

	template<class Archive>
	void serialize(Archive& archive, const std::uint32_t version)
	{
		archive(mPrimaryWeapons, mSecondaryWeapons, mArmors);
	}

private:
	std::map<std::string, std::vector<Drop>> mDropTables;
	std::map<std::string, PlanetConfig> mPlanets;
	std::vector<std::vector<std::string>> mStagePlanets;
	std::map<std::string, BulletConfig> mBulletConfigs;
	std::map<std::string, EnemyPool> mEnemyPools;

	void InitDropTables();
	void InitPlanets();
	void InitStagePlanets();
	void InitBulletConfigs();
	void InitEnemyPools();
};

CEREAL_CLASS_VERSION(Config, 1);