/*
ItemPowerup

A type of powerup that awards the player with an item when they collect it.
*/

#include "ItemManager.h"
#include "ItemPowerup.h"
#include "ProgressionState.h"

using std::string;

ItemPowerup::ItemPowerup(const Vector2& initialCenterPos, const string& itemId, ItemType type, AnimationRegister& animationRegister, std::shared_ptr<Layer> worldLayer):
	Powerup(initialCenterPos, AnimationNameFromItemType(type), animationRegister, worldLayer),
	mItemId(itemId),
	mType(type)
{
}

ItemPowerup::~ItemPowerup()
{
}

void ItemPowerup::OnDie(ProgressionState& progressionState)
{
	ItemManager& im = progressionState.GetItemManager();
	if(mType == ItemType::primaryWeapon)
		progressionState.AddItem(im.CreateItemFromId(mItemId, mType), true);
	else if(mType == ItemType::secondaryWeapon)
		progressionState.AddItem(im.CreateItemFromId(mItemId, mType), true);
	else if(mType == ItemType::armor)
		progressionState.AddItem(im.CreateItemFromId(mItemId, mType), true);

	Cleanup();
}

string ItemPowerup::AnimationNameFromItemType(ItemType type)
{
	string animationName = "";
	if(type == ItemType::primaryWeapon)
		animationName = "primaryPowerup";
	else if(type == ItemType::secondaryWeapon)
		animationName = "secondaryPowerup";
	else if(type == ItemType::armor)
		animationName = "armorPowerup";
	return animationName;
}