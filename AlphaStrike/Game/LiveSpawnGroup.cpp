/*
LiveSpawnGroup

LiveSpawnGroup represents a grouping of enemy ships that spawned together that are currently present in the world.
The difficulty of the spawn is the combined difficulty of all of the ships, either at the time the group was spawned,
or the ships that remain depending on the difficulty.
*/

#include "EnemyShip.h"
#include "LiveSpawnGroup.h"

using std::shared_ptr;
using std::vector;
using std::weak_ptr;

LiveSpawnGroup::LiveSpawnGroup(const vector<shared_ptr<EnemyShip>> enemyShips, LeaveType leaveType, float leaveAcceleration, float timeUntilLeave, float anchorX):
	mLeaveType(leaveType),
	mTimeUntilLeave(timeUntilLeave),
	mLeaveAcceleration(leaveAcceleration),
	mLeaveTimer(timeUntilLeave, std::bind(&LiveSpawnGroup::Leave, this)),
	mAnchorX(anchorX)
{
	mInitialDifficulty = 0;
	for(const auto& enemyShip : enemyShips)
	{
		mInitialDifficulty += enemyShip->GetDifficulty();
		mEnemyShips.push_back(weak_ptr<EnemyShip>(enemyShip));
	}
}

LiveSpawnGroup::~LiveSpawnGroup()
{
}

LiveSpawnGroup::LiveSpawnGroup(const LiveSpawnGroup& other)
{
	mLeaveType = other.mLeaveType;
	mTimeUntilLeave = other.mTimeUntilLeave;
	mLeaveAcceleration = other.mLeaveAcceleration;
	mLeaveTimer = ManualTimer(mTimeUntilLeave, std::bind(&LiveSpawnGroup::Leave, this));
	mEnemyShips = other.mEnemyShips;
	mInitialDifficulty = other.mInitialDifficulty;
	mAnchorX = other.mAnchorX;
}

LiveSpawnGroup& LiveSpawnGroup::operator=(const LiveSpawnGroup& other)
{
	mLeaveType = other.mLeaveType;
	mTimeUntilLeave = other.mTimeUntilLeave;
	mLeaveAcceleration = other.mLeaveAcceleration;
	mLeaveTimer = ManualTimer(mTimeUntilLeave, std::bind(&LiveSpawnGroup::Leave, this));
	mEnemyShips = other.mEnemyShips;
	mInitialDifficulty = other.mInitialDifficulty;
	mAnchorX = other.mAnchorX;

	return *this;
}

void LiveSpawnGroup::Simulate(float timestepSeconds)
{
	mLeaveTimer.Run(timestepSeconds);
}

void LiveSpawnGroup::Leave()
{
	for(auto& enemyShip : mEnemyShips)
	{
		auto es = enemyShip.lock();
		if(es != nullptr)
			es->Leave(mLeaveType, mLeaveAcceleration, mAnchorX);
	}
}

int LiveSpawnGroup::GetInitialDifficulty() const
{
	return mInitialDifficulty;
}

int LiveSpawnGroup::GetCurrentDifficulty() const
{
	int difficulty = 0;
	for(const auto& enemyShip : mEnemyShips)
	{
		auto ptr = enemyShip.lock();
		if(ptr != nullptr)
			difficulty += ptr->GetDifficulty();
	}
	return difficulty;
}

bool LiveSpawnGroup::AllShipsDead() const
{
	for(const auto& enemyShip : mEnemyShips)
	{
		if(enemyShip.lock() != nullptr)
			return false;
	}

	return true;
}
