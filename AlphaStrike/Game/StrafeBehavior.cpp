/*
StrafeBehavior

A behavior that causes an enemy to strafe up and down and/or left to right. If x and y are non zero it moves diagonally.
*/

#include "EnemyShip.h"
#include "RandomGenerator.h"
#include "StrafeBehavior.h"
#include "World.h"

#include "cereal\archives\binary.hpp"
#include "cereal\archives\json.hpp"
CEREAL_REGISTER_TYPE(StrafeBehavior);
CEREAL_REGISTER_DYNAMIC_INIT(strafeB);

using std::shared_ptr;

StrafeBehavior::StrafeBehavior():
	mXRange(0.0f),
	mYRange(0.0f),
	mSpeed(0.0f),
	mXDistanceTravelled(0.0f),
	mYDistanceTravelled(0.0f),
	mRandomStartDirection(true)
{
}

StrafeBehavior::StrafeBehavior(float xRange, float yRange, float speed, bool randomStartDirection):
	mXRange(xRange),
	mYRange(yRange),
	mSpeed(speed),
	mXDistanceTravelled(0.0f),
	mYDistanceTravelled(0.0f),
	mRandomStartDirection(randomStartDirection)
{
}

StrafeBehavior::~StrafeBehavior()
{
}

shared_ptr<EnemyBehavior> StrafeBehavior::Copy()
{
	return std::make_shared<StrafeBehavior>(mXRange, mYRange, mSpeed, mRandomStartDirection);
}

void StrafeBehavior::Run(float timestepSeconds)
{
	float xVel = mXRange == 0.0f ? 0.0f : (mSpeed - (mSpeed * 0.8f * mXDistanceTravelled / mXRange)) * timestepSeconds * (mMovingLeft ? -1.0f : 1.0f);
	float yVel = mYRange == 0.0f ? 0.0f : (mSpeed - (mSpeed * 0.8f * mYDistanceTravelled / mYRange)) * timestepSeconds * (mMovingUp ? -1.0f : 1.0f);
	mXDistanceTravelled += abs(xVel);
	mYDistanceTravelled += abs(yVel);

	if(mXDistanceTravelled >= mXRange)
	{
		mXDistanceTravelled = 0.0f;
		mMovingLeft = !mMovingLeft;
	}
	if(mYDistanceTravelled >= mYRange)
	{
		mYDistanceTravelled = 0.0f;
		mMovingUp = !mMovingUp;
	}

	mEnemyShip->MoveBy(Vector2(xVel, yVel));
}

void StrafeBehavior::Reset()
{
	auto rng = mEnemyShip->GetWorld()->GetRandomGenerator();
	mMovingLeft = !mRandomStartDirection || rng.RandomInRange(0, 9) <= 4;
	mMovingUp = !mRandomStartDirection || rng.RandomInRange(0, 9) <= 4;
}