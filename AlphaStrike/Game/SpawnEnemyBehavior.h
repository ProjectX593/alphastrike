#pragma once

#include "EnemyBehavior.h"
#include "Vector2.h"

class SpawnEnemyBehavior : public EnemyBehavior
{
public:
	SpawnEnemyBehavior();
	SpawnEnemyBehavior(const std::string& enemyId, const std::string& initialBehaviorSetId, const Vector2& relativeCenterPos, int requiredTurretIndex = -1);
	~SpawnEnemyBehavior();

	virtual std::shared_ptr<EnemyBehavior> Copy();
	virtual void Run(float timestepSeconds);
	virtual void Reset();
	virtual std::string GetDisplayString() { return "Spawn - " + mEnemyId; }

	const std::string& GetEnemyId(){ return mEnemyId; }
	const std::string& GetInitialBehaviorSetId(){ return mInitialBehaviorSetId; }
	const Vector2& GetRelativePos(){ return mRelativePos; }
	int GetRequiredTurretIndex(){ return mRequiredTurretIndex; }
	void SetEnemyId(const std::string& enemyId){ mEnemyId = enemyId; }
	void SetInitialBehaviorSetId(const std::string& initialBehaviorSetId){ mInitialBehaviorSetId = initialBehaviorSetId; }
	void SetRelativePos(const Vector2& relativePos){ mRelativePos = relativePos; }
	void SetRequiredTurretIndex(int requiredTurretIndex){ mRequiredTurretIndex = requiredTurretIndex; }

	template <class Archive>
	void serialize(Archive& archive, const std::uint32_t version)
	{
		archive(mEnemyId, mInitialBehaviorSetId, mRelativePos, mRequiredTurretIndex, cereal::base_class<EnemyBehavior>(this));
	}

private:
	std::string mEnemyId;
	std::string mInitialBehaviorSetId;
	Vector2 mRelativePos;
	int mRequiredTurretIndex;
};

CEREAL_CLASS_VERSION(SpawnEnemyBehavior, 1);
CEREAL_FORCE_DYNAMIC_INIT(spawnEB);