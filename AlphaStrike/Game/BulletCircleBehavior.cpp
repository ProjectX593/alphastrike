/*
BulletCircleBehavior

Causes the enemy ship to repeatedly emit circles of bullets. The cooldown is the time between each circle being fired, offsetRadians
is the offset in radians of the first bullet from the up vector. All bullets are evenly spaced.
*/

#include "BulletCircleBehavior.h"
#include "EnemyShip.h"
#include "GameGlobals.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include "World.h"

#include "cereal\archives\binary.hpp"
#include "cereal\archives\json.hpp"
CEREAL_REGISTER_TYPE(BulletCircleBehavior);
CEREAL_REGISTER_DYNAMIC_INIT(bulletCB);

using std::string;

BulletCircleBehavior::BulletCircleBehavior():
	TimedBehavior(0.1f),
	mVelocity(0.0f),
	mBulletCount(0),
	mDamage(0),
	mCooldown(1.0f),
	mTimeUntilFire(0.0f),
	mOffsetRadians(0.0f)
{
}

BulletCircleBehavior::BulletCircleBehavior(const Vector2& centerPos, float velocity, int bulletCount, int damage, const string& bulletId, float duration, float cooldown, float offsetRadians):
	TimedBehavior(duration),
	mCenterPos(centerPos),
	mVelocity(velocity),
	mBulletCount(bulletCount),
	mDamage(damage),
	mBulletId(bulletId),
	mCooldown(cooldown),
	mTimeUntilFire(0.0f),
	mOffsetRadians(offsetRadians)
{
}

BulletCircleBehavior::~BulletCircleBehavior()
{
}

std::shared_ptr<EnemyBehavior> BulletCircleBehavior::Copy()
{
	return std::make_shared<BulletCircleBehavior>(mCenterPos, mVelocity, mBulletCount, mDamage, mBulletId, GetDuration(), mCooldown, mOffsetRadians);
}

void BulletCircleBehavior::Run(float timestepSeconds)
{
	TimedBehavior::Run(timestepSeconds);
	mTimeUntilFire -= timestepSeconds;

	if(mTimeUntilFire > 0)
		return;

	Vector2 velocity = Vector2(0.0f, -mVelocity);
	Vector2 centerPos = mCenterPos / PIXELS_IN_UNIT;
	velocity.Rotate(mOffsetRadians);
	for(int i = 0; i < mBulletCount; i++)
	{
		mEnemyShip->GetWorld()->SpawnBullet(Bullet::Make(mEnemyShip->GetSprite()->GetPos() + centerPos, velocity, mDamage, {}, false, true, mBulletId, mEnemyShip->GetWorld()));
		velocity.Rotate(((float)M_PI * 2.0f) / mBulletCount);
	}

	mTimeUntilFire += mCooldown;
}

void BulletCircleBehavior::Reset()
{
	TimedBehavior::Reset();
	mTimeUntilFire = 0.0f;
}
