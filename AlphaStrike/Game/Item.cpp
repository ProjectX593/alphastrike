/*
Item

An Item class that represents all of the items in the game. There are four types:
Weapon - Used for both primary and secondary weapons, they shoot sequences of Bullets.
Armor - Gives the player additional health and damage resistance.
*/

#include "Config.h"
#include "FileUtil.h"
#include "Item.h"
#include "ItemManager.h"
#include <memory>

using std::shared_ptr;
using std::string;

// ------------------ Item ------------------ //

Item::Item():
	mRecycleValue(0)
{

}

Item::Item(int guid, const string& name, const string& description, const string& thumbnail, int recycleValue):
	mGUID(guid),
	mName(name),
	mDescription(description),
	mThumbnail(thumbnail),
	mRecycleValue(recycleValue)
{
}

Item::~Item()
{
}

void Item::SetGUID(int guid)
{
	mGUID = guid;
}

void Item::TakeItemConfigValues(const Item* item)
{
	mName = item->mName;
	mDescription = item->mDescription; 
	mThumbnail = item->mThumbnail;
	mRecycleValue = item->mRecycleValue;
}

// ------------------ Armor ------------------ //

Armor::Armor():
	mHP(0),
	mSpeedFactor(1.0f)
{
}

Armor::~Armor()
{
}

void Armor::TakeArmorConfigValues(const Armor* armor)
{
	mSpeedFactor = armor->mSpeedFactor;
	mHP = armor->mHP;
	TakeItemConfigValues(armor);
}