#pragma once

#include <memory>
#include <vector>
#include "Vector2.h"

#include "cereal\archives\binary.hpp"
#include "cereal\archives\json.hpp"
#include "cereal\types\polymorphic.hpp"
#include "cereal\types\vector.hpp"

class Collider;
class Weapon;

class TurretGroupConfig
{
	TurretGroupConfig();
	~TurretGroupConfig();

	int mMaxHealth;
	std::vector<Vector2> mPositionsInPixels;	// each pos adds another turret to the group, these positions indicate the CENTER of the turret
	std::string mAnimationsName;
	std::string mIdleAnimation;
	std::string mChargeAnimation;
	std::string mFireAnimation;
	std::string mDischargeAnimation;
	bool mFacesPlayer;
	bool mInvulnerable;
	bool mVisible;
	std::shared_ptr<Weapon> mWeapon;	// this is a shared_ptr since it might be a BeamWeapon
	std::vector<std::shared_ptr<Collider>> mColliders;		// each turret will have all of these colliders

	template<class Archive>
	void serialize(Archive& archive, const std::uint32_t version)
	{
		archive(mMaxHealth, mPositionsInPixels, mAnimationsName, mIdleAnimation, mChargeAnimation, mFireAnimation, mDischargeAnimation,
			mFacesPlayer, mInvulnerable, mVisible, mWeapon, mColliders);
	}
};

CEREAL_CLASS_VERSION(TurretGroupConfig, 2);