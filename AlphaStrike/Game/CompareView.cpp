/*
CompareView

A small view intended to be used as part of a larger view, it is used for comparing the stats of two items.
Two items of any type may be compared in the view, regardless of whether or not that actually makes
any sense to the player.
*/

#include <algorithm>
#include "CompareView.h"
#include "FillBar.h"
#include "GameGlobals.h"
#include "HomeBase.h"
#include "Item.h"
#include "ProgressionState.h"
#include "TextField.h"
#include "Weapon.h"

using std::dynamic_pointer_cast;
using std::shared_ptr;
using std::string;

CompareView::CompareView(AnimationRegister& animationRegister, HomeBase& homeBase):
	View(animationRegister),
	mHomeBase(homeBase),
	mLargestArmorHP(0),
	mLargestArmorSpeed(0.0f),
	mLargestPrimaryDPS(0.0f),
	mLargestSecondaryDPS(0.0f)
{
}

CompareView::~CompareView()
{
}

void CompareView::Initialize()
{
	LoadFromJson("assets\\screens\\CompareView.json", PIXELS_IN_UNIT);
	SetItems(nullptr, nullptr);
	CalculateLargestStats();
}

void CompareView::SetItems(shared_ptr<Item> equippedItem, shared_ptr<Item> newItem, const string& equippedItemPrefix, const string& newItemPrefix)
{
	mEquippedItem = equippedItem;
	UpdateItemNode(GetChild<Node>("equippedItem"), equippedItem, nullptr, equippedItemPrefix);
	UpdateItemNode(GetChild<Node>("newItem"), newItem, mEquippedItem, newItemPrefix);
}

// Note:: If you pass in two different types of items, this function will behave as though you didn't pass an old item
void CompareView::UpdateItemNode(shared_ptr<Node> itemView, shared_ptr<Item> newItem, shared_ptr<Item> oldItem, const string& prefix)
{
	if(newItem == nullptr)
	{
		itemView->GetChild<TextField>("nameLabel")->SetText("");
		itemView->GetChild<Node>("statBar0")->SetVisible(false);
		itemView->GetChild<Node>("statBar1")->SetVisible(false);
		itemView->GetChild<Node>("statBar2")->SetVisible(false);
		return;
	}

	itemView->GetChild<TextField>("nameLabel")->SetText((prefix == "" ? "" : prefix + " - ") + newItem->mName);

	shared_ptr<Weapon> wep = dynamic_pointer_cast<Weapon>(newItem);
	if(wep != nullptr)
	{
		shared_ptr<Weapon> oldWep = dynamic_pointer_cast<Weapon>(oldItem);
		float highestDPS = wep->IsPrimaryWeapon() ? mLargestPrimaryDPS : mLargestSecondaryDPS;
		float newDPSPercent = CalculateWeaponDamagePerSecond(wep) / highestDPS;

		UpdateBarNode(itemView->GetChild<Node>("statBar0"), "DPS", newDPSPercent, oldWep == nullptr ? newDPSPercent : (CalculateWeaponDamagePerSecond(oldWep) / highestDPS));
		itemView->GetChild<Node>("statBar1")->SetVisible(false);
		itemView->GetChild<Node>("statBar2")->SetVisible(false);
		return;
	}

	shared_ptr<Armor> armor = dynamic_pointer_cast<Armor>(newItem);
	if(armor != nullptr)
	{
		shared_ptr<Armor> oldArmor = dynamic_pointer_cast<Armor>(oldItem);
		int oldHP = oldArmor == nullptr ? armor->mHP : oldArmor->mHP;
		UpdateBarNode(itemView->GetChild<Node>("statBar0"), "HP", (float)armor->mHP / mLargestArmorHP, (float)oldHP / mLargestArmorHP);
		UpdateBarNode(itemView->GetChild<Node>("statBar1"), "SPD", armor->mSpeedFactor / mLargestArmorSpeed, (oldArmor == nullptr ? armor->mSpeedFactor : oldArmor->mSpeedFactor) / mLargestArmorSpeed);
		itemView->GetChild<Node>("statBar2")->SetVisible(false);
		return;
	}

	//TODO: power cores
}

void CompareView::UpdateBarNode(shared_ptr<Node> barNode, const std::string& statName, float oldPercent, float newPercent)
{
	barNode->SetVisible(true);
	barNode->GetChild<TextField>("statLabel")->SetText(statName);
	barNode->GetChild<FillBar>("blueBar")->SetFillPercent(std::min(oldPercent, newPercent));
	barNode->GetChild<FillBar>("redBar")->SetFillPercent(newPercent > oldPercent ? newPercent : 0.0f);
	barNode->GetChild<FillBar>("greenBar")->SetFillPercent(newPercent < oldPercent ? oldPercent : 0.0f);
}

float CompareView::CalculateWeaponDamagePerSecond(std::shared_ptr<Weapon> weapon)
{
	const auto& actions = weapon->GetActions();
	float totalTime = weapon->mCooldown;
	float totalDamage = 0.0f;
	for(const auto& action : actions)
	{
		totalTime += action.mDelay;
		totalDamage += action.mDamage;
	}

	if(totalTime == 0.0f)
		throw new std::runtime_error("ERROR: Weapon actions had no delay in CompareView.");

	return totalDamage / totalTime;
}

void CompareView::CalculateLargestStats()
{
	const auto& armors = mHomeBase.GetConfig().mArmors;
	for(const auto& armor : armors)
	{
		mLargestArmorHP = std::max(mLargestArmorHP, armor.second.mHP);
		mLargestArmorSpeed = std::max(mLargestArmorSpeed, armor.second.mSpeedFactor);
	}

	const auto& primaryWeapons = mHomeBase.GetConfig().mPrimaryWeapons;
	for(const auto& primaryWep : primaryWeapons)
	{
		mLargestPrimaryDPS = std::max(mLargestPrimaryDPS, CalculateWeaponDamagePerSecond(primaryWep.second.Clone()));
	}

	const auto& secondaryWeapons = mHomeBase.GetConfig().mSecondaryWeapons;
	for(const auto& secondaryWep : secondaryWeapons)
	{
		mLargestSecondaryDPS = std::max(mLargestSecondaryDPS, CalculateWeaponDamagePerSecond(secondaryWep.second.Clone()));
	}
}