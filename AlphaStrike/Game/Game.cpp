/*
Game
Main game class for Alpha Strike.
*/

#include "EventManager.h"
#include "Game.h"
#include "InputSystem.h"
#include "Layer.h"
#include "MainMenu.h"
#include "RendererLocator.h"
#include "Scene.h"
#include "TextField.h"
#include "UtilityServiceLocator.h"

using std::make_shared;
using std::shared_ptr;
using namespace std::placeholders;

Game::Game(InputSystem& inputSystem):
	mInputSystem(inputSystem),
	mShouldQuit(false),
	mRunning(false),
	mFrontendAnimationRegister(UtilityServiceLocator::GetPlatformUtil().GetWorkingDirectory() + "assets\\textures\\", UtilityServiceLocator::GetPlatformUtil().GetWorkingDirectory() + "assets\\animations\\"),
	mItemManager(mFrontendAnimationRegister, &mConfig),
	mProgressionState(mItemManager, mRandomGenerator, mConfig),
	mScene(make_shared<Scene>()),
	mFrontendLayer(make_shared<Layer>(2.0f)),
	mWorldLayer(make_shared<Layer>(1.0f)),
	mBackgroundLayer(make_shared<Layer>(0.0f)),
	mWorld(mProgressionState, mFrontendLayer, mWorldLayer, mBackgroundLayer, mItemManager, mInputSystem, mRandomGenerator, &mConfig),
	mHomeBase(mProgressionState, mItemManager, mConfig, mFrontendLayer, mFrontendAnimationRegister, mWorld)
{
	RendererLocator::GetRenderer().SetSampleMode(SampleMode::smNearestNeighbor);

	std::ifstream stream;
	stream.open(UtilityServiceLocator::GetPlatformUtil().GetWorkingDirectory() + "assets\\alphaStrike.cfg", std::ios::out | std::ios::binary);
	cereal::BinaryInputArchive archive(stream);
	archive(mConfig);
	stream.close();
	
	mScene->AddChild(mWorldLayer);
	mScene->AddChild(mBackgroundLayer);
	mScene->AddChild(mFrontendLayer);
	mInputSystem.SetDownCallbackForKey(ButtonType::frontendBack, std::bind(&Game::Quit, this, _1), this);

	ShowMainMenu();

	//NOTE: The fact that we register this callback just so it can call the function without doing anything else at all is not useful, and should probably be removed
	mInputSystem.SetMouseDownCallback(std::bind(&Game::OnMouseDown, this, _1));
	mInputSystem.SetMouseUpCallback(std::bind(&Game::OnMouseUp, this));
	mInputSystem.SetMouseMoveCallback(std::bind(&Game::OnMouseMove, this, _1));

	mFPSCounter = make_shared<TextField>(12, "arial", Vector2(0.0f, 0.1f), D2DSize(1.0f, 1.0f, 1000.0f));
	mFrontendLayer->AddChild(mFPSCounter);
}

Game::~Game(void)
{
	EventManager::Get().UnsubscribeFromAllEvents(this);
}

bool Game::Run(float timestepSeconds)
{
	UpdateFPSCounter(timestepSeconds);

	mScene->Run(timestepSeconds);
	mWorld.Simulate(timestepSeconds);

	Renderer& renderer = RendererLocator::GetRenderer();
	renderer.BeginFrame();
	mScene->Render();
	renderer.EndFrame();

	return mShouldQuit;
}

void Game::ShowMainMenu()
{
	shared_ptr<MainMenu> mainMenu(new MainMenu(mFrontendAnimationRegister, std::bind(&Game::NewGame, this, _1), std::bind(&Game::Quit, this, nullptr)));
	mainMenu->Initialize();
	mFrontendLayer->AddChild(mainMenu);
}

void Game::NewGame(const std::string& fullSaveFilename)
{
	mProgressionState.StartNewGame();
	if(fullSaveFilename != "")
		mProgressionState.LoadFromFile(fullSaveFilename);
	mHomeBase.ShowTurbolift();
}

void Game::Quit(void* data)
{
	mShouldQuit = true;
}

void Game::OnMouseDown(const Vector2& pos)
{
	Vector2 screenCoords = RendererLocator::GetRenderer().PixelCoordsToScreenCoords(pos);
	mScene->OnMouseDown(screenCoords);
}

void Game::OnMouseUp()
{
	mScene->OnMouseUp();
}

void Game::OnMouseMove(const Vector2& pos)
{
	Vector2 screenCoords = RendererLocator::GetRenderer().PixelCoordsToScreenCoords(pos);
	mScene->OnMouseMove(screenCoords);
}

void Game::UpdateFPSCounter(float timestepSeconds)
{
	mTimeSamples.push_front(timestepSeconds);
	if(mTimeSamples.size() > 1000)
		mTimeSamples.pop_back();

	float fps = 0.0f;
	for(float& sample : mTimeSamples)
		fps += sample;
	fps /= (float)mTimeSamples.size();
	fps = 1.0f / fps;

	mFPSCounter->SetText(std::to_string(fps));
}