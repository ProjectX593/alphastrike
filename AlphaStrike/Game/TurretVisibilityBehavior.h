#pragma once

#include "EnemyBehavior.h"

class TurretVisibilityBehavior : public EnemyBehavior
{
public:
	TurretVisibilityBehavior();
	TurretVisibilityBehavior(const std::vector<int> turretsToChange, bool visible);
	~TurretVisibilityBehavior();

	virtual std::shared_ptr<EnemyBehavior> Copy();
	virtual void Run(float timestepSeconds);
	virtual void Reset();
	virtual std::string GetDisplayString() { return "Turret Visibility Change"; }

	const std::vector<int>& GetTurretsToChange() const { return mTurretsToChange; }
	void SetTurretsToChange(const std::vector<int>& turretsToChange){ mTurretsToChange = turretsToChange; }
	bool GetVisible() const { return mVisible; }
	void SetVisible(bool visible) { mVisible = visible; }

	template <class Archive>
	void serialize(Archive& archive, std::uint32_t version)
	{
		archive(mTurretsToChange, mVisible, cereal::base_class<EnemyBehavior>(this));
	}

private:
	std::vector<int> mTurretsToChange;
	bool mVisible;
	bool mActive;
};

CEREAL_CLASS_VERSION(TurretVisibilityBehavior, 1);
CEREAL_FORCE_DYNAMIC_INIT(turretVisibilityB);