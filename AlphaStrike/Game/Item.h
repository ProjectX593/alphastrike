#pragma once

#include <memory>
#include <string>
#include "TextureInfo.h"

#include "cereal\archives\binary.hpp"
#include "cereal\archives\json.hpp"
#include "cereal\types\polymorphic.hpp"
#include "cereal\types\string.hpp"

class ItemManager;

enum class ItemType : int
{
	primaryWeapon,
	secondaryWeapon,
	armor,
	itemTypeCount,
	none
};

class Item
{
public:
	Item();
	Item(int guid, const std::string& name, const std::string& description, const std::string& thumbnail, int recycleValue);
	virtual ~Item();

	std::string mItemId;
	std::string mName;
	std::string mDescription;
	std::string mThumbnail;
	int mRecycleValue;

	const std::string& GetDescription() const { return mDescription; }
	void SetGUID(int guid);
	int GetGUID() const { return mGUID; }
	void TakeItemConfigValues(const Item* item);
	virtual ItemType GetType() = 0;

	template<class Archive> 
	void serialize(Archive& archive, const std::uint32_t version)
	{
		archive(mItemId, mGUID, mName, mDescription, mThumbnail, mRecycleValue);
	}

protected:
	int mGUID;
};

CEREAL_CLASS_VERSION(Item, 4);

class Armor : public Item
{
public:
	Armor();
	Armor(int guid, const std::string& name, const std::string& description, const std::string& thumbnail, int recycleValue, int hp, float speedFactor):
		Item(guid, name, description, thumbnail, recycleValue), mHP(hp), mSpeedFactor(speedFactor) {}
	virtual ~Armor();

	int mHP;
	float mSpeedFactor;

	virtual void TakeArmorConfigValues(const Armor* armor);
	virtual ItemType GetType() { return ItemType::armor; }

	template<class Archive> 
	void serialize(Archive& archive, const std::uint32_t version)
	{
		archive(mHP, mSpeedFactor, cereal::base_class<Item>(this));
	}

private:
};

CEREAL_CLASS_VERSION(Armor, 1);
CEREAL_REGISTER_TYPE(Armor);