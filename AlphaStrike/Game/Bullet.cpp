/*
Bullet

This class represents a single bullet. Bullets can be very simple and fly in straight lines,
but they can also be complex - tracking targets and performing different actions such as flying in
one direction, then intercepting the nearest target.

Note that the bullet's position is really it's center. This makes it easier to place it in config.
*/

#include <algorithm>
#include "AnimatedSprite.h"
#include "AnimationRegister.h"
#include "Bullet.h"
#include "CircleCollider.h"
#include "Config.h"
#include "GameGlobals.h"
#include "Layer.h"
#include "PlatformUtil.h"
#include "RandomGenerator.h"
#include <string>
#include "World.h"

using std::shared_ptr;
using std::string;
using std::weak_ptr;
using std::vector;

const float DEFAULT_SPEED = 0.7f;	// used when no target is found for certain bullets

Bullet::Bullet(World* world):
	mWorld(world),
	mIsPlayerBullet(false),
	mBehaviorIndex(0),
	mDamage(0),
	mRotates(false),
	mState(BulletState::flying)
{
}

Bullet::Bullet(const Vector2& centerPos, const Vector2& initialVelocity, int damage, const std::vector<std::shared_ptr<BulletBehavior>>& behaviors, bool isPlayerBullet, bool rotates, const string& bulletId, World* world, shared_ptr<Collider> collider):
	mCenterPos(centerPos),
	mIsPlayerBullet(isPlayerBullet),
	mBehaviorIndex(0),
	mDamage(damage),
	mRotates(rotates),
	mWorld(world),
	mCollider(collider),
	mBulletId(bulletId),
	mState(BulletState::flying)
{
	mSprite = std::make_shared<AnimatedSprite>(Vector2(), D2DSize(0.0f, 0.0f, BULLET_DEPTH));
	mSprite->LoadAndPlayAnimation(world->GetAnimationRegister().RegisterAnimation(world->GetConfig()->GetBulletConfig(mBulletId).mAnimation), DEFAULT_ANIMATION, true, PIXELS_IN_UNIT);
	mSprite->SetCenterPos(mCenterPos);
	//ActivateAction(0);

	if(mCollider == nullptr)
		mCollider = CircleCollider::AddToCenterOfNode(mSprite);

	for(auto& behavior : behaviors)
	{
		mBehaviors.push_back(behavior->Copy());
	}

	SetVelocity(initialVelocity);
}

Bullet::~Bullet()
{
}

std::shared_ptr<Bullet> Bullet::Make(World* world)
{
	return std::shared_ptr<Bullet>(new Bullet(world));
}

std::shared_ptr<Bullet> Bullet::Make(const Vector2& centerPos, const Vector2& initialVelocity, int damage, const std::vector<std::shared_ptr<BulletBehavior>>& behaviors, bool isPlayerBullet, bool rotates, const std::string& bulletId, World* world, std::shared_ptr<Collider> collider)
{
	auto bullet = std::shared_ptr<Bullet>(new Bullet(centerPos, initialVelocity, damage, behaviors, isPlayerBullet, rotates, bulletId, world, collider));
	bullet->Initialize();
	return bullet;
}

void Bullet::Initialize()
{
	for(auto& behavior : mBehaviors)
		behavior->Initialize(shared_from_this());
}

void Bullet::Simulate(float timestepSeconds)
{
	if(mState != BulletState::flying)
		return;

	if(mBehaviors.size() > 0)
	{
		mBehaviors.at(mBehaviorIndex)->Run(timestepSeconds);
		if(mBehaviors.at(mBehaviorIndex)->Done())
		{
			mBehaviorIndex = mBehaviorIndex == mBehaviors.size() - 1 ? 0 : mBehaviorIndex + 1;
			mBehaviors.at(mBehaviorIndex)->Reset();
		}
	}

	mCenterPos += mVelocity * timestepSeconds;
	mSprite->SetCenterPos(mCenterPos);
}

void Bullet::SetVelocity(const Vector2& velocity)
{
	mVelocity = velocity;
	if(mRotates)
	{
		Vector2 normalizedVelocity = velocity;
		normalizedVelocity.Normalize();
		float angle = Vector2(0.0f, -1.0f).CalculateAngle(normalizedVelocity);
		mSprite->SetRotation(normalizedVelocity.x > 0 ? angle : -angle);
	}
}

void Bullet::TrySetDefaultVelocity()
{
	if(mVelocity.Length() == 0)
		SetVelocity(Vector2(0.0f, DEFAULT_SPEED * (mIsPlayerBullet ? -1.0f : 1.0f)));
}

void Bullet::Reflect()
{
	Vector2 newVelocity = mVelocity * -1.0f;
	SetVelocity(newVelocity);
	mIsPlayerBullet = !mIsPlayerBullet;

	for(auto& behavior : mBehaviors)
		behavior->Reflect();
}

shared_ptr<AnimatedSprite> Bullet::GetSprite()
{
	return mSprite;
}

bool Bullet::IsPlayerBullet()
{
	return mIsPlayerBullet;
}

#include "BeamBullet.h"
void Bullet::Hit(AnimationRegister& animationRegister, shared_ptr<Layer> worldLayer)
{
	mState = BulletState::exploding;
	const BulletConfig& bulletConfig = mWorld->GetConfig()->GetBulletConfig(mBulletId);
	mSprite->RemoveFromParentAndCleanup();
	int animationNumber = mWorld->GetRandomGenerator().RandomInRange(0, bulletConfig.mImpactAnimationCount - 1);
	string animationName = "impact" + std::to_string(animationNumber);
	Vector2 center = mSprite->GetCenter();
	mSprite = std::make_shared<AnimatedSprite>(Vector2(), D2DSize(0.0f, 0.0f, mSprite->GetDepth()));
	mSprite->LoadAndPlayAnimation(animationRegister.RegisterAnimation(bulletConfig.mImpactAnimation), animationName, true, PIXELS_IN_UNIT);
	mSprite->SetCenterPos(center);
	mSprite->SetAnimationCompleteCallback(std::bind(&Bullet::DoneExploding, this));

	worldLayer->AddChild(mSprite);
}

void Bullet::DoneExploding()
{
	mState = BulletState::done;
	mSprite->SetVisible(false);
	
}

void Bullet::Cleanup()
{
	mSprite->RemoveFromParentAndCleanup();
	mSprite = nullptr;
}

void Bullet::SetPos(const Vector2& pos)
{
	mSprite->SetPos(pos);
}