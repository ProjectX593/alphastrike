/*
If these files are not included anywhere in the program, the serialization functionality is not compiled
and will cause crashes that don't occur in the editor (since the editor by it's nature must use all of these classes.)
This file is an unfortunate hack that I'd prefer to remove as soon as possible...
*/

#include "ActivateTurretBehavior.h"
#include "AnimateBehavior.h"
#include "ArriveBehavior.h"
#include "BulletCircleBehavior.h"
#include "DriftBehavior.h"
#include "ExplodeBehavior.h"
#include "FollowBehavior.h"
#include "LeaveBehavior.h"
#include "MoveBehavior.h"
#include "ProximityBehavior.h"
#include "ReflectBehavior.h"
#include "SequenceBehavior.h"
#include "SetChangeBehavior.h"
#include "ShootBehavior.h"
#include "SpawnEnemyBehavior.h"
#include "StrafeBehavior.h"
#include "SuicideBehavior.h"
#include "TurretVisibilityBehavior.h"
#include "TurretVolleyBehavior.h"
#include "WanderBehavior.h"

#include "BeamWeapon.h"