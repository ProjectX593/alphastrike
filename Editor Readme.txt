The editor is nearly complete - or at least as complete as I need it to be for my own use. It is entirely possible to provide invalid data for many of the parameters and cause a crash, so save often!

The first thing to do when using it is to open an enemy pool by clicking 'LOAD' in the bottom right corner and selecting 'TestPlanet.epl' from the list that appears. Next, select one of the buttons at the top of the screen to edit something in the pool:

Spawn Groups - These are groups of enemies that appear in the game. Each spawn group has properties on the right for the 'anchor' (the X position enemies will appear around), the min and max difficulty (which currently has no effect) and a leader index. If the leader index indicates an enemy in the SPAWNS list that every other ship will copy behaviors and random number generators from, effectively causing them to all act in unison.
Each spawn has an enemy ID (which must exist in the ENEMY section), a behavior set ID (which also must exist in that enemy), a delay in seconds which will freeze the enemy until it has passed, as well as a position relative to the anchor.
Click RESPAWN at the bottom to spawn the currently selected spawn group while this menu is open.

Enemies - Each of these is an enemy that can appear in a spawn group. They all have health, difficulty (this affects the spawn system, and is probably best left at 1), an amount of scrap to drop, a max velocity and acceleration, a bool indicating if it is a boss, an animation name (animations are found in the 'assets/animations' folder next to the .exe and must exist, each animation has a .png and .json, use the filename with no extension), an initial animation (refer to the .json files for animation names), and a drop table ID (at the moment these are hard coded in the game and may be moved to the editor later).
Note that selecting an enemy will activate all of the rest of the buttons at top of the screen. Those menus will edit pieces of the selected enemy.
Click RESPAWN at the bottom to spawn the currently selected enemy ship. Note that it's positioning may be incorrect, as they are intended to be positioned inside of spawn groups.
The text field next to the RESPAWN button determines which behavior set to spawn the ship with.

Behaviors - Probably the most interesting part of the editor; this is where you edit each enemy's behaviors. The best example is probably militaryFortress. Each enemy has at least one 'set' of behaviors. Each behavior in the active set will be run once per frame. Sequence behaviors are used to run groups of behaviors one after another, or randomly. Creating behavior sets is done by clicking the NEW button in the BEHAVIOR SETS area.
Use the buttons above the list of behaviors to create, copy, delete, or rearrange them. Note that if you have a behavior set group selected, these buttons modify the groups instead. 
There are many different behaviors, but I'll detail the less self explanatory ones:
Turret Volley - This causes any number of turrets attached to the enemy to swivel from the start rotation to the end rotation, firing constantly until all of the turrets have completed their rotation, or they reach the 'duration', whichever comes first.
Turret Visibility - Causes the specified turrets (specify turrets with a comma separated list of indices) to either appear or disappear. This is useful for enemies with multiple phases that have new turrets pop up later on in the fight.
Set Change - Causes an enemy to change to a new behavior set when it hits a health threshold, and optionally when its turrets are all dead. It is currently untested!
Explode - This causes the enemy to create an explosion animation. When an enemy dies, all of it's behaviors stop and are replaced with this. It would most likely be used to make a transition between states more flashy.

Turrets - Each entry in the list on the left is actually a turret group. Each group has various properties (animation info, health etc) that define what a single turret looks like, as well as a list of positions for each instance of the turret to appear at. These positions are in pixels.

Weapons - Weapons attached to either an enemy ship or a turret. You need to click ENEMY, select an enemy, then WEAPON to edit ship weapons, or TURRET, select a turret group, then WEAPON for turret weapons.
Each weapon has a sequence of 'Weapon Actions' which are essentially just positions and times at which bullets spawn out of the parent.
Each weapon action then has bullet behaviors - things which the bullet will do once it's spawned. At the moment, bullets can only fly in a straight line, or intercept a nearby target. Empty behaviors are present for timing purposes. For example, the player's secondary weapon has an empty behavior that takes up a fraction of a second so the bullets spread out in front of it before converging on a nearby target.

Colliders - This screen is currently causing a crash when the game is not attached to the debugger. Enemy ships with no colliders specified will have a default circle collider attached to them with a radius of the smaller of the ships width and height.
