#pragma once

#include "View.h"

class SelectionCell : public View
{
public:
	SelectionCell(AnimationRegister& animationRegister);
	~SelectionCell();

	void Initialize(const std::string& text, std::function<void()> selectCallback, const std::string& backgroundAnimationName = "", const std::string& animationToPlay = DEFAULT_ANIMATION, const std::string& assetFilename = "assets\\screens\\components\\selectionCell.json");
	void SetSelected(bool selected);
	void SetBackgroundAnimation(const std::string& animationName, const std::string& animationToPlay = DEFAULT_ANIMATION);
};

