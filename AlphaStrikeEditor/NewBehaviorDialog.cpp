/*
NewBehaviorDialog

A dialog that lets the user choose from a number of behavior types, then calls a callback with the newly created behavior,
or a second callback if the user closes the dialog.
*/

#include "Button.h"
#include "EditorGlobals.h"
#include "NewBehaviorDialog.h"
#include "TableView.h"
#include "TextField.h"

#include "ActivateTurretBehavior.h"
#include "AnimateBehavior.h"
#include "ArriveBehavior.h"
#include "BulletCircleBehavior.h"
#include "DriftBehavior.h"
#include "ExplodeBehavior.h"
#include "FollowBehavior.h"
#include "MoveBehavior.h"
#include "ProximityBehavior.h"
#include "ReflectBehavior.h"
#include "SetChangeBehavior.h"
#include "SequenceBehavior.h"
#include "ShootBehavior.h"
#include "SpawnEnemyBehavior.h"
#include "StrafeBehavior.h"
#include "SuicideBehavior.h"
#include "TurretVisibilityBehavior.h"
#include "TurretVolleyBehavior.h"
#include "WanderBehavior.h"

NewBehaviorDialog::NewBehaviorDialog(AnimationRegister& animationRegister, std::function<void(std::shared_ptr<EnemyBehavior>)> doneCallback, std::function<void()> closeCallback):
	View(animationRegister),
	mDoneCallback(doneCallback),
	mCloseCallback(closeCallback)
{
}

NewBehaviorDialog::~NewBehaviorDialog()
{
}

std::shared_ptr<View> NewBehaviorDialog::CreateBehaviorTypeCell(const std::string& cellText, std::function<void()> createCallback)
{
	std::shared_ptr<View> cell = std::make_shared<View>(mAnimationRegister);
	cell->LoadFromJson("assets\\screens\\components\\selectionCell.json", PIXELS_IN_UNIT);
	cell->GetChild<TextField>("label")->SetText(cellText);
	cell->Resize(0.0f, cell->GetChild<Button>("btn")->GetHeight());

	cell->GetChild<Button>("btn")->SetMouseReleaseCallback([this, cell, createCallback](){
		if(mSelectedCell.lock() != nullptr)
			mSelectedCell.lock()->GetChild<Button>("btn")->SetSelected(false);
		mSelectedCell = cell;
		cell->GetChild<Button>("btn")->SetSelected(true);
		createCallback();
	});

	return cell;
}

void NewBehaviorDialog::Initialize()
{
	LoadFromJson("assets\\screens\\components\\newBehaviorDialog.json", PIXELS_IN_UNIT);
	
	std::shared_ptr<TableView> tv = GetChild<TableView>("behaviorTypeTable");
	
	tv->AddTableNode(CreateBehaviorTypeCell("Activate Turret", std::bind(&NewBehaviorDialog::CreateActivateTurretBehavior, this)));
	tv->AddTableNode(CreateBehaviorTypeCell("Animate", std::bind(&NewBehaviorDialog::CreateAnimateBehavior, this)));
	tv->AddTableNode(CreateBehaviorTypeCell("Arrive", std::bind(&NewBehaviorDialog::CreateArriveBehavior, this)));
	tv->AddTableNode(CreateBehaviorTypeCell("Bullet Circle", std::bind(&NewBehaviorDialog::CreateBulletCircleBehavior, this)));
	tv->AddTableNode(CreateBehaviorTypeCell("Drift", [this](){mBehaviorToCreate = std::make_shared<DriftBehavior>();}));
	tv->AddTableNode(CreateBehaviorTypeCell("Explode", std::bind(&NewBehaviorDialog::CreateExplodeBehavior, this)));
	tv->AddTableNode(CreateBehaviorTypeCell("Follow", std::bind(&NewBehaviorDialog::CreateFollowBehavior, this)));
	tv->AddTableNode(CreateBehaviorTypeCell("Move", std::bind(&NewBehaviorDialog::CreateMoveBehavior, this)));
	tv->AddTableNode(CreateBehaviorTypeCell("Proximity", [this](){mBehaviorToCreate = std::make_shared<ProximityBehavior>();}));
	tv->AddTableNode(CreateBehaviorTypeCell("Reflect", std::bind(&NewBehaviorDialog::CreateReflectBehavior, this)));
	tv->AddTableNode(CreateBehaviorTypeCell("Set Change", std::bind(&NewBehaviorDialog::CreateSetChangeBehavior, this)));
	tv->AddTableNode(CreateBehaviorTypeCell("Sequence", std::bind(&NewBehaviorDialog::CreateSequenceBehavior, this)));
	tv->AddTableNode(CreateBehaviorTypeCell("Shoot", std::bind(&NewBehaviorDialog::CreateShootBehavior, this)));
	tv->AddTableNode(CreateBehaviorTypeCell("Spawn Enemy", std::bind(&NewBehaviorDialog::CreateSpawnEnemyBehavior, this)));
	tv->AddTableNode(CreateBehaviorTypeCell("Strafe", std::bind(&NewBehaviorDialog::CreateStrafeBehavior, this)));
	tv->AddTableNode(CreateBehaviorTypeCell("Suicide", std::bind(&NewBehaviorDialog::CreateSuicideBehavior, this)));
	tv->AddTableNode(CreateBehaviorTypeCell("Timed", std::bind(&NewBehaviorDialog::CreateTimedBehavior, this)));
	tv->AddTableNode(CreateBehaviorTypeCell("Turret Visibility", std::bind(&NewBehaviorDialog::CreateTurretVisibilityBehavior, this)));
	tv->AddTableNode(CreateBehaviorTypeCell("Turret Volley", std::bind(&NewBehaviorDialog::CreateTurretVolleyBehavior, this)));
	tv->AddTableNode(CreateBehaviorTypeCell("Wander", std::bind(&NewBehaviorDialog::CreateWanderBehavior, this)));

	GetChild<Button>("createButton")->SetMouseReleaseCallback(std::bind(&NewBehaviorDialog::OnCreateButton, this));
	GetChild<Button>("cancelButton")->SetMouseReleaseCallback(std::bind(&NewBehaviorDialog::OnCancelButton, this));
}

void NewBehaviorDialog::OnCreateButton()
{
	if(mBehaviorToCreate == nullptr)
		return;

	RemoveFromParentAndCleanup();
	mDoneCallback(mBehaviorToCreate);
}

void NewBehaviorDialog::OnCancelButton()
{
	RemoveFromParentAndCleanup();
	mCloseCallback();
}

void NewBehaviorDialog::CreateActivateTurretBehavior()
{
	mBehaviorToCreate = std::make_shared<ActivateTurretBehavior>(1.0f, std::vector<int>(), false);
}

void NewBehaviorDialog::CreateAnimateBehavior()
{
	mBehaviorToCreate = std::make_shared<AnimateBehavior>("uninitialized");
}

void NewBehaviorDialog::CreateArriveBehavior()
{
	mBehaviorToCreate = std::make_shared<ArriveBehavior>(1.0f, 2.0f);
}

void NewBehaviorDialog::CreateBulletCircleBehavior()
{
	mBehaviorToCreate = std::make_shared<BulletCircleBehavior>(Vector2(), 0.4f, 8, 1, "UNSPECIFIED", 0.1f, 1.0f, 0.0f);
}

void NewBehaviorDialog::CreateExplodeBehavior()
{
	mBehaviorToCreate = std::make_shared<ExplodeBehavior>();
}

void NewBehaviorDialog::CreateFollowBehavior()
{
	mBehaviorToCreate = std::make_shared<FollowBehavior>(0.1f, 0.3f, 0.15f, true, false);
}

void NewBehaviorDialog::CreateMoveBehavior()
{
	mBehaviorToCreate = std::make_shared<MoveBehavior>(Vector2(), 1.0f);
}

void NewBehaviorDialog::CreateReflectBehavior()
{
	mBehaviorToCreate = std::make_shared<ReflectBehavior>(1.0f);
}

void NewBehaviorDialog::CreateSetChangeBehavior()
{
	mBehaviorToCreate = std::make_shared<SetChangeBehavior>();
}

void NewBehaviorDialog::CreateSequenceBehavior()
{
	mBehaviorToCreate = std::make_shared<SequenceBehavior>();
}

void NewBehaviorDialog::CreateShootBehavior()
{
	mBehaviorToCreate = std::make_shared<ShootBehavior>(0, 0.0f);
}

void NewBehaviorDialog::CreateSpawnEnemyBehavior()
{
	mBehaviorToCreate = std::make_shared<SpawnEnemyBehavior>("", "default", Vector2());
}

void NewBehaviorDialog::CreateStrafeBehavior()
{
	mBehaviorToCreate = std::make_shared<StrafeBehavior>(0.3f, 0.0f, 0.3f, false);
}

void NewBehaviorDialog::CreateSuicideBehavior()
{
	mBehaviorToCreate = std::make_shared<SuicideBehavior>(0.05f, 1);
}

void NewBehaviorDialog::CreateTimedBehavior()
{
	mBehaviorToCreate = std::make_shared<TimedBehavior>(1.0f);
}

void NewBehaviorDialog::CreateTurretVisibilityBehavior()
{
	mBehaviorToCreate = std::make_shared<TurretVisibilityBehavior>();
}

void NewBehaviorDialog::CreateTurretVolleyBehavior()
{
	mBehaviorToCreate = std::make_shared<TurretVolleyBehavior>();
}

void NewBehaviorDialog::CreateWanderBehavior()
{
	mBehaviorToCreate = std::make_shared<WanderBehavior>(0.3f, 1.0f, 0.5f);
}
