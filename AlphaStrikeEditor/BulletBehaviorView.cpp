/*
BulletBehaviorView

The view used for editing bullet behaviors.
*/

#include "BulletBehaviorView.h"
#include "EditorGlobals.h"
#include "NewBulletBehaviorDialog.h"
#include "SelectionCell.h"
#include "TableView.h"
#include "TextField.h"
#include "Weapon.h"

#include "InterceptBehavior.h"
#include "StraightBehavior.h"

using std::dynamic_pointer_cast;
using namespace std::placeholders;

BulletBehaviorView::BulletBehaviorView(AnimationRegister& animationRegister, InputSystem& inputSystem, std::shared_ptr<TableView> propertyTable):
	PropertyEditorView(animationRegister, inputSystem),
	mSelectedBehaviorIndex(-1),
	mWeaponAction(nullptr)
{
	SetPropertyTable(propertyTable);
}

BulletBehaviorView::~BulletBehaviorView()
{
}

void BulletBehaviorView::Initialize()
{
	LoadFromJson("assets\\screens\\components\\mediumEditorView.json", PIXELS_IN_UNIT);

	GetChild<Button>("newButton")->SetMouseReleaseCallback(std::bind(&BulletBehaviorView::OnNewButton, this));
	GetChild<Button>("copyButton")->SetMouseReleaseCallback(std::bind(&BulletBehaviorView::OnCopyButton, this));
	GetChild<Button>("deleteButton")->SetMouseReleaseCallback(std::bind(&BulletBehaviorView::OnDeleteButton, this));
	GetChild<Button>("upButton")->SetMouseReleaseCallback(std::bind(&BulletBehaviorView::OnUpButton, this));
	GetChild<Button>("downButton")->SetMouseReleaseCallback(std::bind(&BulletBehaviorView::OnDownButton, this));
	GetChild<Button>("ringButton")->SetVisible(false);

	mBehaviorTable = GetChild<TableView>("contentTable");
	GetChild<TextField>("titleLabel")->SetText("Bullet Behaviors");

	RefreshButtons();
}

void BulletBehaviorView::SelectWeaponAction(WeaponAction* weaponAction)
{
	mSelectedBehaviorIndex = -1;
	mWeaponAction = weaponAction;
	ClearProperties();

	RefreshBehaviors();
	RefreshButtons();
}

void BulletBehaviorView::SelectBulletBehavior(int index, std::weak_ptr<SelectionCell> cell)
{
	if(mSelectedCell.lock() != nullptr)
		mSelectedCell.lock()->SetSelected(false);
	mSelectedCell = cell;
	mSelectedCell.lock()->SetSelected(true);

	mSelectedBehaviorIndex = index;
	RefreshButtons();
	RefreshProperties();
}

void BulletBehaviorView::RefreshBehaviors()
{
	mBehaviorTable->ClearTableNodes();
	mSelectedCell = std::weak_ptr<SelectionCell>();

	if(mWeaponAction == nullptr)
		return;

	const auto& behaviors = mWeaponAction->GetBulletBehaviors();
	for(int i = 0; i < (int)behaviors.size(); i++)
	{
		auto cell = std::make_shared<SelectionCell>(mAnimationRegister);
		cell->Initialize(behaviors.at(i)->GetDisplayString(), std::bind(&BulletBehaviorView::SelectBulletBehavior, this, i, cell), "", DEFAULT_ANIMATION, "assets\\screens\\components\\mediumSelectionCell.json");
		mBehaviorTable->AddTableNode(cell);

		if(mSelectedBehaviorIndex == i)
		{
			mSelectedCell = cell;
			cell->SetSelected(true);
		}
	}
}



void BulletBehaviorView::RefreshProperties()
{
	ClearProperties();

	if(mSelectedBehaviorIndex < 0)
		return;

	const auto& behavior = mWeaponAction->GetBulletBehaviors().at(mSelectedBehaviorIndex);

	if(dynamic_pointer_cast<InterceptBehavior>(behavior) != nullptr)
		AddInterceptBehaviorProperties(dynamic_pointer_cast<InterceptBehavior>(behavior));
	else if(dynamic_pointer_cast<StraightBehavior>(behavior) != nullptr)
		AddStraightBehaviorProperties(dynamic_pointer_cast<StraightBehavior>(behavior));
	else
		AddEmptyBehaviorProperties(behavior);
}

void BulletBehaviorView::RefreshButtons()
{
	GetChild<Button>("newButton")->SetDisabled(mWeaponAction == nullptr || mNewBehaviorDialog != nullptr);
	bool canCopyDelete = mWeaponAction != nullptr && mSelectedBehaviorIndex >= 0 && mNewBehaviorDialog == nullptr;
	GetChild<Button>("copyButton")->SetDisabled(!canCopyDelete);
	GetChild<Button>("deleteButton")->SetDisabled(!canCopyDelete);
	GetChild<Button>("upButton")->SetDisabled(!canCopyDelete);
	GetChild<Button>("downButton")->SetDisabled(!canCopyDelete);
}

void BulletBehaviorView::OnNewButton()
{
	if(mWeaponAction == nullptr || mNewBehaviorDialog != nullptr)
		return;

	mNewBehaviorDialog = std::make_shared<NewBulletBehaviorDialog>(mAnimationRegister, std::bind(&BulletBehaviorView::OnNewBehaviorDialogDone, this, _1), std::bind(&BulletBehaviorView::OnNewBehaviorDialogClosed, this));
	mNewBehaviorDialog->Initialize();
	mNewBehaviorDialog->SetDepth(10.0f);
	mNewBehaviorDialog->SetPos(Vector2(0.035f, 0.035f));
	AddChild(mNewBehaviorDialog);

	RefreshButtons();
}

void BulletBehaviorView::OnNewBehaviorDialogDone(std::shared_ptr<BulletBehavior> behavior)
{
	mWeaponAction->AddBulletBehavior(behavior, mSelectedBehaviorIndex);
	OnNewBehaviorDialogClosed();

	RefreshBehaviors();
	RefreshButtons();
	RefreshProperties();
}

void BulletBehaviorView::OnNewBehaviorDialogClosed()
{
	mNewBehaviorDialog = nullptr;
	RefreshButtons();
}

void BulletBehaviorView::OnCopyButton()
{
	if(mWeaponAction == nullptr || mSelectedBehaviorIndex < 0)
		return;

	mWeaponAction->AddBulletBehavior(mWeaponAction->GetBulletBehaviors().at(mSelectedBehaviorIndex), mSelectedBehaviorIndex);
	RefreshBehaviors();
	RefreshProperties();
}

void BulletBehaviorView::OnDeleteButton()
{
	if(mWeaponAction == nullptr || mSelectedBehaviorIndex < 0)
		return;

	mWeaponAction->RemoveBulletBehavior(mSelectedBehaviorIndex);
	if(mSelectedBehaviorIndex == mWeaponAction->GetBulletBehaviors().size())
		mSelectedBehaviorIndex--;

	RefreshBehaviors();
	RefreshProperties();
}

void BulletBehaviorView::OnUpButton()
{
	if(mWeaponAction == nullptr || mSelectedBehaviorIndex <= 0)
		return;

	auto behavior = mWeaponAction->GetBulletBehaviors().at(mSelectedBehaviorIndex);
	mWeaponAction->RemoveBulletBehavior(mSelectedBehaviorIndex);
	mSelectedBehaviorIndex--;
	mWeaponAction->AddBulletBehavior(behavior, mSelectedBehaviorIndex);

	RefreshBehaviors();
}

void BulletBehaviorView::OnDownButton()
{
	if(mWeaponAction == nullptr || mSelectedBehaviorIndex < 0 || mSelectedBehaviorIndex >= (int)mWeaponAction->GetBulletBehaviors().size() - 1)
		return;

	auto behavior = mWeaponAction->GetBulletBehaviors().at(mSelectedBehaviorIndex);
	mWeaponAction->RemoveBulletBehavior(mSelectedBehaviorIndex);
	mSelectedBehaviorIndex++;
	mWeaponAction->AddBulletBehavior(behavior, mSelectedBehaviorIndex);

	RefreshBehaviors();
}

void BulletBehaviorView::AddEmptyBehaviorProperties(std::shared_ptr<BulletBehavior> behavior)
{
	AddFloatProperty("duration", "Duration", behavior->GetDuration(), [behavior](float duration){behavior->SetDuration(duration); });
}

void BulletBehaviorView::AddStraightBehaviorProperties(std::shared_ptr<StraightBehavior> behavior)
{
	AddEmptyBehaviorProperties(behavior);
	AddVector2Property("tarVel", "Target Velocity", behavior->GetTargetVelocity(), [behavior](const Vector2& targetVelocity){behavior->SetTargetVelocity(targetVelocity); });
	AddFloatProperty("turnRate", "Turn Rate", behavior->GetTurnRate(), [behavior](float turnRate){behavior->SetTurnRate(turnRate); });
	AddFloatProperty("turnDur", "Turn Duration", behavior->GetTurnDuration(), [behavior](float turnDuration){behavior->SetTurnDuration(turnDuration); });
}

void BulletBehaviorView::AddInterceptBehaviorProperties(std::shared_ptr<InterceptBehavior> behavior)
{
	AddEmptyBehaviorProperties(behavior);
	AddFloatProperty("speed", "Speed", behavior->mSpeed, [behavior](float speed){behavior->mSpeed = speed; });
	AddFloatProperty("turnRate", "Turn Rate", behavior->mTurnRate, [behavior](float turnRate){behavior->mTurnRate = turnRate; });
	AddBoolProperty("swchTar", "Switches Targets", behavior->mSwitchesTargets, [behavior](bool switchesTargets){behavior->mSwitchesTargets = switchesTargets; });
	AddIntProperty("maxSeek", "Max Seekers (0 to disable)", behavior->mMaxSeekers, [behavior](int maxSeekers){behavior->mMaxSeekers = maxSeekers; });
}
