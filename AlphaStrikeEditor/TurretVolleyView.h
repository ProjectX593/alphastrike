#pragma once

#include "PropertyEditorView.h"

class TurretVolleyBehavior;
struct TurretVolleyInfo;

class TurretVolleyView : public PropertyEditorView
{
public:
	TurretVolleyView(std::shared_ptr<TurretVolleyBehavior> behavior, AnimationRegister& animationRegister, InputSystem& inputSystem);
	~TurretVolleyView();

	void Initialize(std::shared_ptr<TableView> propertyTable);

private:
	std::shared_ptr<TurretVolleyBehavior> mBehavior;
	int mSelectedVolleyIndex;
	std::weak_ptr<SelectionCell> mSelectedCell;

	void RefreshVolleyInfos();
	void RefreshButtons();
	void SelectTurretVolley(int index, std::weak_ptr<SelectionCell> cell);
	void RefreshProperties();
	void OnNewButton();
	void OnCopyButton();
	void OnDeleteButton();
	TurretVolleyInfo& GetSelectedVolleyInfo();

	void OnTurretIndexChanged(int turretIndex);
	void OnStartRotationChanged(float startRotation);
	void OnEndRotationChanged(float endRotation);
	void OnRotationSpeedChanged(float rotationSpeed);
	void OnClockwiseChanged(bool clockwise);
	void OnFireDelayChanged(float fireDelay);
};

