#pragma once

#include "PropertyEditorView.h"

class BulletBehaviorView;
class Weapon;

class WeaponActionView : public PropertyEditorView
{
public:
	WeaponActionView(AnimationRegister& animationRegister, InputSystem& inputSystem, std::shared_ptr<BulletBehaviorView> bulletBehaviorView, std::shared_ptr<TableView> propertyTable);
	~WeaponActionView();

	void Initialize();
	void SetWeapon(Weapon* weapon);
	void RefreshActions();
	void RefreshButtons();
	void RefreshProperties();
	void SelectAction(int actionIndex, std::weak_ptr<SelectionCell> cell = std::weak_ptr<SelectionCell>());
	void OnNewButton();
	void OnCopyButton();
	void OnDeleteButton();
	void OnUpButton();
	void OnDownButton();

private:
	std::shared_ptr<BulletBehaviorView> mBulletBehaviorView;
	std::shared_ptr<TableView> mActionTable;
	int mSelectedActionIndex;
	std::weak_ptr<SelectionCell> mSelectedCell;
	Weapon* mWeapon;
	bool mRingDialogOpen;

	void OnRingButton();
	void OnRingDialogClosed();
};

