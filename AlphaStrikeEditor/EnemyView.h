#pragma once

#include "PropertyEditorView.h"

class EnemyConfig;
class EnemyPool;
class NewItemDialog;

class EnemyView : public PropertyEditorView
{
public:
	EnemyView(EnemyPool& enemyPool, AnimationRegister& animationRegister, InputSystem& inputSystem, std::function<void(const std::string&)> selectEnemyIdCallback);
	~EnemyView();

	void Initialize(const std::string& enemyIdToSelect);

private:
	EnemyPool& mEnemyPool;
	std::string mSelectedEnemyConfigId;
	std::shared_ptr<TableView> mEnemyTable;
	std::shared_ptr<NewItemDialog> mNewEnemyView;
	bool mCopyEnemy;
	std::weak_ptr<View> mSelectedCell;
	std::function<void(const std::string&)> mSelectEnemyIdCallback;

	void CreateNewEnemyDialog(bool copyEnemy);
	bool OnNewEnemyViewDone(std::string enemyId);
	void OnNewEnemyViewClosed();
	void OnDeleteButton();
	void SetSelectedEnemyConfigId(const std::string& enemyConfigId);
	void RefreshEnemyTable();
	void SelectEnemyConfig(const std::string& enemyConfigId, std::weak_ptr<View> cell);
	EnemyConfig& GetSelectedEnemy();
	void AddPropertyCells();
	void UpdatePropertyCells();
	void RemovePropertyCells();

	void OnHealthChanged(int health);
	void OnDifficultyChanged(int difficulty);
	void OnScrapDropChanged(int scrapDrop);
	void OnMaxVelocityChanged(float maxVelocity);
	void OnAccelerationChanged(float acceleration);
	void OnIsBossChanged(bool isBoss);
	void OnLeavesChanged(bool leaves);
	void OnAnimationNameChanged(const std::string& animationName);
	void OnInitialAnimationChanged(const std::string& initialAnimation);
	void OnDropTableIdChanged(const std::string& dropTableId);
	void OnDropChanceChanged(float dropChance);
};

