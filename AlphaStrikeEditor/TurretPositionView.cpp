/*
TurretPositionView

A very small view used to edit turret positions using a hybrid of property cells and normal selection cells.
*/

#include <algorithm>
#include "Button.h"
#include "EditorGlobals.h"
#include "TableView.h"
#include "TextField.h"
#include "TurretPositionView.h"

TurretPositionView::TurretPositionView(AnimationRegister& animationRegister, InputSystem& inputSystem):
	PropertyEditorView(animationRegister, inputSystem),
	mTurretPositions(nullptr),
	mSelectedPositionIndex(-1)
{
}

TurretPositionView::~TurretPositionView()
{
}

void TurretPositionView::Initialize()
{
	LoadFromJson("assets\\screens\\components\\behaviorSetView.json", PIXELS_IN_UNIT);
	GetChild<TextField>("titleLabel")->SetText("TURRET POSITIONS");

	GetChild<Button>("newButton")->SetMouseReleaseCallback(std::bind(&TurretPositionView::OnNewButton, this));
	GetChild<Button>("copyButton")->SetMouseReleaseCallback(std::bind(&TurretPositionView::OnCopyButton, this));
	GetChild<Button>("deleteButton")->SetMouseReleaseCallback(std::bind(&TurretPositionView::OnDeleteButton, this));

	SetPropertyTable(GetChild<TableView>("behaviorSetTable"));
	SetCellFilename("assets\\screens\\components\\smallPropertyCell.json");

	SetSelectedTurretPositionIndex(-1);
	RefreshTurretPositions();
}

void TurretPositionView::SetSelectedTurretPositionIndex(int index)
{
	mSelectedPositionIndex = index;
	RefreshButtons();
}

void TurretPositionView::RefreshTurretPositions()
{
	RefreshButtons();
	ClearProperties();

	if(mTurretPositions == nullptr)
		return;

	mSelectedCell = std::weak_ptr<View>();
	for(int i = 0; i < (int)mTurretPositions->size(); i++)
	{
		auto& pos = mTurretPositions->at(i);
		auto cell = AddVector2Property(std::to_string(i), "Pos " + std::to_string(i), pos, [this, i](const Vector2& newPosition){
			mTurretPositions->at(i) = newPosition;
		});
		cell.lock()->GetChild<Button>("button")->SetMouseReleaseCallback(std::bind(&TurretPositionView::OnTurretPositionSelected, this, i, cell));

		if(i == mSelectedPositionIndex)
		{
			cell.lock()->GetChild<Button>("button")->SetSelected(true);
			mSelectedCell = cell;
		}
	}
}

void TurretPositionView::OnTurretPositionSelected(int index, std::weak_ptr<View> cell)
{
	if(index == mSelectedPositionIndex)
		return;

	if(mSelectedCell.lock() != nullptr)
		mSelectedCell.lock()->GetChild<Button>("button")->SetSelected(false);
	mSelectedCell = cell;
	mSelectedCell.lock()->GetChild<Button>("button")->SetSelected(true);
	mSelectedPositionIndex = index;

	RefreshButtons();
}

void TurretPositionView::RefreshButtons()
{
	GetChild<Button>("newButton")->SetDisabled(mTurretPositions == nullptr);
	GetChild<Button>("copyButton")->SetDisabled(mTurretPositions == nullptr || mSelectedPositionIndex < 0);
	GetChild<Button>("deleteButton")->SetDisabled(mTurretPositions == nullptr || mSelectedPositionIndex < 0);
}

void TurretPositionView::OnNewButton()
{
	if(mTurretPositions == nullptr)
		return;

	if(mSelectedPositionIndex >= 0)
		mTurretPositions->insert(mTurretPositions->begin() + mSelectedPositionIndex, Vector2());
	else
		mTurretPositions->push_back(Vector2());

	RefreshTurretPositions();
}

void TurretPositionView::OnCopyButton()
{
	if(mTurretPositions == nullptr || mSelectedPositionIndex < 0)
		return;

	mTurretPositions->insert(mTurretPositions->begin() + mSelectedPositionIndex, mTurretPositions->at(mSelectedPositionIndex));
	RefreshTurretPositions();
}

void TurretPositionView::OnDeleteButton()
{
	if(mTurretPositions == nullptr || mSelectedPositionIndex < 0)
		return;

	mTurretPositions->erase(mTurretPositions->begin() + mSelectedPositionIndex);
	mSelectedPositionIndex = std::min(mSelectedPositionIndex, (int)mTurretPositions->size() - 1);
	RefreshTurretPositions();
}

void TurretPositionView::SetTurretPositions(std::vector<Vector2>* turretPositions)
{
	mTurretPositions = turretPositions;
	RefreshTurretPositions();
}