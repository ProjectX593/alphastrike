/*
SpawnView

A view for editing spawns of enemies.
TODO: There is a lot of duplicated logic for the two sub views here, consider creating new classes for them. For now, I just have to get this prototype done...
*/

#include "Button.h"
#include "EditorGlobals.h"
#include "EnemyPool.h"
#include "EnemySpawn.h"
#include "LeaveConfigView.h"
#include "SelectionCell.h"
#include "SpawnGroupView.h"
#include "SpawnView.h"
#include "TableView.h"
#include "TextField.h"

SpawnGroupView::SpawnGroupView(EnemyPool& enemyPool, AnimationRegister& animationRegister, InputSystem& inputSystem, std::function<void(int)> spawnGroupSelectedCallback):
	PropertyEditorView(animationRegister, inputSystem),
	mEnemyPool(enemyPool),
	mSpawnsVisible(true),
	mSpawnGroupSelectedCallback(spawnGroupSelectedCallback),
	mSelectedSpawnGroupIndex(-1)
{
}

SpawnGroupView::~SpawnGroupView()
{
}

void SpawnGroupView::Initialize(int selectedSpawnGroupIndex)
{
	mSelectedSpawnGroupIndex = selectedSpawnGroupIndex;
	LoadFromJson("assets\\screens\\components\\spawnView.json", PIXELS_IN_UNIT);

	SetPropertyTable(GetChild<TableView>("detailsTable"));
	mSpawnGroupTable = GetChild<TableView>("spawnGroupTable");

	mSpawnView = std::make_shared<SpawnView>(nullptr, mAnimationRegister, mInputSystem);
	mSpawnView->Initialize(mPropertyTable);
	GetChild<Node>("genericListEditor")->AddChild(mSpawnView);

	mLeaveConfigView = std::make_shared<LeaveConfigView>(nullptr, mAnimationRegister, mInputSystem);
	mLeaveConfigView->Initialize(mPropertyTable);
	GetChild<Node>("genericListEditor")->AddChild(mLeaveConfigView);

	GetChild<Button>("newButton")->SetMouseReleaseCallback(std::bind(&SpawnGroupView::NewSpawnGroup, this));
	GetChild<Button>("copyButton")->SetMouseReleaseCallback(std::bind(&SpawnGroupView::CopySpawnGroup, this));
	GetChild<Button>("deleteButton")->SetMouseReleaseCallback(std::bind(&SpawnGroupView::DeleteSpawnGroup, this));

	GetChild<Button>("upButton")->SetMouseReleaseCallback(std::bind(&SpawnGroupView::OnUpButton, this));
	GetChild<Button>("downButton")->SetMouseReleaseCallback(std::bind(&SpawnGroupView::OnDownButton, this));

	GetChild<Button>("spawnsButton")->SetMouseReleaseCallback(std::bind(&SpawnGroupView::OnSpawnsButton, this));
	GetChild<Button>("leaveButton")->SetMouseReleaseCallback(std::bind(&SpawnGroupView::OnLeaveButton, this));

	RefreshButtons();
	RefreshSpawnGroups();
}

void SpawnGroupView::RefreshButtons()
{
	bool spawnGroupSelected = mSelectedSpawnGroupIndex >= 0;
	GetChild<Button>("copyButton")->SetDisabled(!spawnGroupSelected);
	GetChild<Button>("deleteButton")->SetDisabled(!spawnGroupSelected);
	GetChild<Button>("upButton")->SetDisabled(!spawnGroupSelected);
	GetChild<Button>("downButton")->SetDisabled(!spawnGroupSelected);
	GetChild<Button>("spawnsButton")->SetSelected(mSpawnsVisible);
	GetChild<Button>("leaveButton")->SetSelected(!mSpawnsVisible);

	mSpawnView->SetVisible(mSpawnsVisible);
	mLeaveConfigView->SetVisible(!mSpawnsVisible);
}

void SpawnGroupView::OnSpawnsButton()
{
	if(mSpawnsVisible)
		return;

	mSpawnsVisible = true;
	mSpawnView->SetSpawns(GetSpawns());
	RefreshButtons();
}

void SpawnGroupView::OnLeaveButton()
{
	if(!mSpawnsVisible)
		return;

	mSpawnsVisible = false;
	mLeaveConfigView->SetLeaveConfigs(GetLeaveConfigs());
	RefreshButtons();
}

void SpawnGroupView::NewSpawnGroup()
{
	auto& spawnGroups = mEnemyPool.GetEditableEnemySpawnGroups();
	auto sg = EnemySpawnGroup(LevelDifficulty::easy, LevelDifficulty::medium, 0.0f, 0.0f, 10.0f, 15.0f);
	if(mSelectedSpawnGroupIndex < 0)
	{
		spawnGroups.push_back(sg);
		mSelectedSpawnGroupIndex = spawnGroups.size() - 1;
	}
	else
	{
		spawnGroups.insert(spawnGroups.begin() + mSelectedSpawnGroupIndex, sg);
	}

	SelectSpawnGroup(mSelectedSpawnGroupIndex);
}

void SpawnGroupView::CopySpawnGroup()
{
	if(mSelectedSpawnGroupIndex < 0)
		return;

	auto& spawnGroups = mEnemyPool.GetEditableEnemySpawnGroups();
	spawnGroups.insert(spawnGroups.begin() + mSelectedSpawnGroupIndex, spawnGroups.at(mSelectedSpawnGroupIndex));

	SelectSpawnGroup(mSelectedSpawnGroupIndex);
}

void SpawnGroupView::DeleteSpawnGroup()
{
	if(mSelectedSpawnGroupIndex < 0)
		return;

	auto& spawns = mEnemyPool.GetEditableEnemySpawnGroups();
	spawns.erase(spawns.begin() + mSelectedSpawnGroupIndex);
	if(mSelectedSpawnGroupIndex >= (int)spawns.size())
		mSelectedSpawnGroupIndex = (int)spawns.size() - 1;

	SelectSpawnGroup(mSelectedSpawnGroupIndex);
}

void SpawnGroupView::OnUpButton()
{
	if(mSelectedSpawnGroupIndex <= 0)
		return;

	auto& spawns = mEnemyPool.GetEditableEnemySpawnGroups();
	auto spawn = spawns.at(mSelectedSpawnGroupIndex);
	spawns.erase(spawns.begin() + mSelectedSpawnGroupIndex);
	mSelectedSpawnGroupIndex--;
	spawns.insert(spawns.begin() + mSelectedSpawnGroupIndex, spawn);
	SelectSpawnGroup(mSelectedSpawnGroupIndex);
}

void SpawnGroupView::OnDownButton()
{
	auto& spawns = mEnemyPool.GetEditableEnemySpawnGroups();
	if(mSelectedSpawnGroupIndex < 0 || mSelectedSpawnGroupIndex >= (int)spawns.size() - 1)
		return;

	auto spawn = spawns.at(mSelectedSpawnGroupIndex);
	spawns.erase(spawns.begin() + mSelectedSpawnGroupIndex);
	mSelectedSpawnGroupIndex++;
	spawns.insert(spawns.begin() + mSelectedSpawnGroupIndex, spawn);
	SelectSpawnGroup(mSelectedSpawnGroupIndex);
}

void SpawnGroupView::RefreshSpawnGroups()
{
	mSpawnGroupTable->ClearTableNodes();

	auto& spawnGroups = mEnemyPool.GetEditableEnemySpawnGroups();
	for(int i = 0; i < (int)spawnGroups.size(); i++)
	{
		auto& spawnGroup = spawnGroups.at(i);
		auto cell = std::make_shared<SelectionCell>(mAnimationRegister);
		cell->Initialize(spawnGroup.mName, std::bind(&SpawnGroupView::SelectSpawnGroup, this, i));
		mSpawnGroupTable->AddTableNode(cell);

		if(i == mSelectedSpawnGroupIndex)
			cell->SetSelected(true);
	}
}

void SpawnGroupView::AddSpawnGroupProperties()
{
	ClearProperties();

	if(mSelectedSpawnGroupIndex < 0)
		return;

	auto& spawnGroup = mEnemyPool.GetEditableEnemySpawnGroups().at(mSelectedSpawnGroupIndex);
	AddStringProperty("name", "Name", spawnGroup.mName, [&spawnGroup, this](const std::string& name){
		spawnGroup.mName = name;
		RefreshSpawnGroups();
	});
	AddFloatProperty("ancXMin", "Anchor X Min", spawnGroup.mMinAnchorX, [&spawnGroup](float xMin){spawnGroup.mMinAnchorX = xMin; });
	AddFloatProperty("ancXMax", "Anchor X Max", spawnGroup.mMaxAnchorX, [&spawnGroup](float xMax){spawnGroup.mMaxAnchorX = xMax; });
	AddIntProperty("minDiff", "Minimum Difficulty", static_cast<int>(spawnGroup.mMinDifficulty), [&spawnGroup](int minDifficulty){spawnGroup.mMinDifficulty = static_cast<LevelDifficulty>(minDifficulty); });
	AddIntProperty("maxDiff", "Maximum Difficulty", static_cast<int>(spawnGroup.mMaxDifficulty), [&spawnGroup](int maxDifficulty){spawnGroup.mMaxDifficulty = static_cast<LevelDifficulty>(maxDifficulty); });
	AddFloatProperty("minLv", "Min Leave Time", spawnGroup.mMinLeaveTime, [&spawnGroup](float minLeaveTime){spawnGroup.mMinLeaveTime = minLeaveTime; });
	AddFloatProperty("maxLv", "Max Leave Time", spawnGroup.mMaxLeaveTime, [&spawnGroup](float maxLeaveTime){spawnGroup.mMaxLeaveTime = maxLeaveTime; });
	AddIntProperty("leaderInd", "Leader Index", spawnGroup.mLeaderIndex, [&spawnGroup](int leaderIndex){spawnGroup.mLeaderIndex = leaderIndex; });
}

void SpawnGroupView::SelectSpawnGroup(int index)
{
	mSelectedSpawnGroupIndex = index;

	if(mSpawnsVisible)
		mSpawnView->SetSpawns(GetSpawns());
	else
		mLeaveConfigView->SetLeaveConfigs(GetLeaveConfigs());

	RefreshSpawnGroups();
	AddSpawnGroupProperties();
	mSpawnGroupSelectedCallback(mSelectedSpawnGroupIndex);
	RefreshButtons();
}

std::vector<std::unique_ptr<EnemySpawn>>* SpawnGroupView::GetSpawns()
{
	if(mSelectedSpawnGroupIndex < 0)
		return nullptr;
	return &mEnemyPool.GetEditableEnemySpawnGroups().at(mSelectedSpawnGroupIndex).mSpawns;
}

std::vector<SpawnLeaveConfig>* SpawnGroupView::GetLeaveConfigs()
{
	if(mSelectedSpawnGroupIndex < 0)
		return nullptr;
	return &mEnemyPool.GetEditableEnemySpawnGroups().at(mSelectedSpawnGroupIndex).mLeaveConfigs;
}
