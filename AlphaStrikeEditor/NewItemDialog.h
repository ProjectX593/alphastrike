#pragma once

#include "View.h"

class NewItemDialog : public View
{
public:
	NewItemDialog(AnimationRegister& animationRegister, std::function<bool(std::string)> doneCallback, std::function<void()> closeCallback = nullptr);
	~NewItemDialog();
	
	void Initialize(const std::string& title);
	void SetTitle(const std::string& title);
	void SetDoneCallback(std::function<bool(std::string)> doneCallback) { mDoneCallback = doneCallback; }
	void SetCloseCallback(std::function<void()> closeCallback) { mCloseCallback = closeCallback; }

private:
	std::function<bool(std::string)> mDoneCallback;
	std::function<void()> mCloseCallback;

	void OnCreateButton();
	void OnCancelButton();
};

