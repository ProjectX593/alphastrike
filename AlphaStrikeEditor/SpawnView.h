#pragma once

#include "GenericListView.h"

class EnemySpawn;

class SpawnView : public GenericListView
{
public:
	SpawnView(std::vector<std::unique_ptr<EnemySpawn>>* spawns, AnimationRegister& animationRegister, InputSystem& inputSystem);
	~SpawnView();

	virtual void Initialize(std::shared_ptr<TableView> propertyTable);
	void SetSpawns(std::vector<std::unique_ptr<EnemySpawn>>* spawns);

private:
	std::vector<std::unique_ptr<EnemySpawn>>* mSpawns;

	virtual void AddItems();
	virtual void AddProperties();
	virtual void OnNewButton();
	virtual void OnCopyButton();
	virtual void OnDeleteButton();
};

