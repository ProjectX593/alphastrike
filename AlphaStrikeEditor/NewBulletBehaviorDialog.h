#pragma once

#include "View.h"

class BulletBehavior;
class SelectionCell;

class NewBulletBehaviorDialog : public View
{
public:
	NewBulletBehaviorDialog(AnimationRegister& animationRegister, std::function<void(std::shared_ptr<BulletBehavior>)> doneCallback, std::function<void()> closeCallback);
	~NewBulletBehaviorDialog();

	void Initialize();

private:
	std::function<void(std::shared_ptr<BulletBehavior>)> mDoneCallback;
	std::function<void()> mCloseCallback;
	std::shared_ptr<BulletBehavior> mBehaviorToCreate;
	std::weak_ptr<SelectionCell> mSelectedCell;

	std::shared_ptr<View> CreateBehaviorTypeCell(const std::string& cellText, std::function<void()> createCallback);
	void OnCreateButton();
	void OnCancelButton();

	void CreateEmptyBehavior();
	void CreateStraightBehavior();
	void CreateInterceptBehavior();
};

