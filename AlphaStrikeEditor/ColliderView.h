#pragma once

#include "EnemyConfig.h"
#include "PropertyEditorView.h"

class CircleCollider;
class Collider;
class NewColliderDialog;
class SelectionCell;
class SquareCollider;

class ColliderView : public PropertyEditorView
{
public:
	ColliderView(std::vector<ColliderConfig>& colliderConfig, AnimationRegister& animationRegister, InputSystem& inputSystem);
	~ColliderView();

	void Initialize();

private:
	std::vector<ColliderConfig>& mColliderConfigs;
	std::shared_ptr<TableView> mColliderTable;
	int mSelectedColliderIndex;
	std::weak_ptr<SelectionCell> mSelectedCell;
	std::shared_ptr<NewColliderDialog> mNewColliderDialog;

	void RefreshColliders();
	void RefreshButtons();
	void RefreshProperties();
	void SelectTurretGroup(int colliderIndex, std::weak_ptr<SelectionCell> cell = std::weak_ptr<SelectionCell>());
	void OnNewButton();
	void OnNewColliderDialogDone(ColliderConfig newColliderConfig);
	void OnNewColliderDialogClosed();
	void OnCopyButton();
	void OnDeleteButton();
};

