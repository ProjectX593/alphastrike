/*
WeaponView

The view used for editing weapons. It contains 3 panels, one for weapons, one for weapon actions for each weapon, 
and one for bullet behaviors for each weapon action.
Turret mode changes the weapon view to act on a single shared_ptr<Weapon>, for use with turrets.
*/

#include "BeamWeapon.h"
#include "BulletBehaviorView.h"
#include "EditorGlobals.h"
#include "EnemyConfig.h"
#include "SelectionCell.h"
#include "TableView.h"
#include "TextField.h"
#include "WeaponActionView.h"
#include "WeaponView.h"

using std::string;

WeaponView::WeaponView(AnimationRegister& animationRegister, InputSystem& inputSystem, std::vector<Weapon>* weapons):
	PropertyEditorView(animationRegister, inputSystem),
	mWeapons(weapons),
	mSelectedWeaponIndex(-1),
	mTurretMode(false)
{
}

WeaponView::WeaponView(AnimationRegister& animationRegister, InputSystem& inputSystem, std::shared_ptr<Weapon> turretWeapon):
	PropertyEditorView(animationRegister, inputSystem),
	mWeapons(nullptr),
	mTurretWeapon(turretWeapon),
	mSelectedWeaponIndex(-1),
	mTurretMode(true)
{
}

WeaponView::~WeaponView()
{
}

void WeaponView::Initialize()
{
	LoadFromJson("assets\\screens\\components\\weaponView.json", PIXELS_IN_UNIT);

	mWeaponTable = GetChild<TableView>("weaponTable");
	SetPropertyTable(GetChild<TableView>("detailsTable"));

	mBulletBehaviorView = std::make_shared<BulletBehaviorView>(mAnimationRegister, mInputSystem, mPropertyTable);
	mBulletBehaviorView->Initialize();
	GetChild<Node>("bulletBehaviorViewAnchor")->AddChild(mBulletBehaviorView);

	mWeaponActionView = std::make_shared<WeaponActionView>(mAnimationRegister, mInputSystem, mBulletBehaviorView, mPropertyTable);
	mWeaponActionView->Initialize();
	GetChild<Node>("weaponActionViewAnchor")->AddChild(mWeaponActionView);

	GetChild<Button>("newButton")->SetMouseReleaseCallback(std::bind(&WeaponView::OnNewButton, this));
	GetChild<Button>("copyButton")->SetMouseReleaseCallback(std::bind(&WeaponView::OnCopyButton, this));
	GetChild<Button>("deleteButton")->SetMouseReleaseCallback(std::bind(&WeaponView::OnDeleteButton, this));
	GetChild<Button>("equipButton")->SetVisible(false);
	GetChild<TextField>("l7")->SetVisible(false);

	RefreshWeapons();

	BeamWeapon* beam = dynamic_cast<BeamWeapon*>(GetSelectedWeapon());
	if(beam != nullptr)
	{
		mBulletBehaviorView->SetVisible(false);
		mWeaponActionView->SetVisible(false);
	}
	if(mTurretMode)
	{
		mWeaponActionView->SetWeapon(mTurretWeapon.get());
		RefreshProperties();
	}
}

Weapon* WeaponView::GetSelectedWeapon()
{
	if(mTurretMode)
		return mTurretWeapon.get();
	else if(mSelectedWeaponIndex >= 0)
		return &mWeapons->at(mSelectedWeaponIndex);
	else
		return nullptr;
}

void WeaponView::RefreshWeapons()
{
	mWeaponTable->ClearTableNodes();
	RefreshButtons();
	mSelectedCell = std::weak_ptr<SelectionCell>();

	if(mTurretMode)
	{
		AddWeaponCell(0, mTurretWeapon.get());
	}
	else
	{
		for(int i = 0; i < (int)mWeapons->size(); i++)
			AddWeaponCell(i, &mWeapons->at(i));
	}
}

void WeaponView::AddWeaponCell(int index, Weapon* weapon)
{
	std::shared_ptr<SelectionCell> cell = std::make_shared<SelectionCell>(mAnimationRegister);
	cell->Initialize(weapon->mName, std::bind(&WeaponView::SelectWeapon, this, index, cell));
	mWeaponTable->AddTableNode(cell);

	if(index == mSelectedWeaponIndex || mTurretMode)
	{
		mSelectedCell = cell;
		cell->SetSelected(true);
	}
}

void WeaponView::RefreshButtons()
{
	GetChild<Button>("newButton")->SetDisabled(mTurretMode);
	GetChild<Button>("copyButton")->SetDisabled(mTurretMode || mSelectedWeaponIndex < 0);
	GetChild<Button>("deleteButton")->SetDisabled(mTurretMode || mSelectedWeaponIndex < 0);
}

void WeaponView::RefreshProperties()
{
	ClearProperties();
	Weapon* weapon = GetSelectedWeapon();

	if(weapon == nullptr)
		return;

	AddFloatProperty("cooldown", "Cooldown", weapon->mCooldown, [weapon](float cooldown){
		weapon->mCooldown = cooldown; 
	});
	AddStringProperty("name", "Name", weapon->mName, [weapon, this](const string& name){
		weapon->mName = name;
		RefreshWeapons();
	});

	BeamWeapon* beam = dynamic_cast<BeamWeapon*>(weapon);
	if(beam != nullptr)
	{
		AddStringProperty("bulletAnim", "Beam Bullet Animation", beam->GetBulletAnimationId(), [beam](const string& bulletAnimationId){beam->SetBulletAnimationId(bulletAnimationId); });
		AddIntProperty("dam", "Damage", beam->GetDamage(), [beam](int damage){beam->SetDamage(damage); });
		AddVector2Property("pos", "Beam Pixel Position", beam->GetBeamPixelPos(), [beam](const Vector2& beamPixelPos){beam->SetBeamPixelPos(beamPixelPos); });
		AddFloatProperty("rot", "Beam Rotation (Radians)", beam->GetBeamRotationRadians(), [beam](float beamRotationRadians){beam->SetBeamRotationRadians(beamRotationRadians); });
		AddFloatProperty("collWidth", "Collision Width Percentage", beam->GetCollisionWidthPercent(), [beam](float collisionWidthPercent){beam->SetCollisionWidthPercent(collisionWidthPercent); });
	}
}

void WeaponView::SelectWeapon(int weaponIndex, std::weak_ptr<SelectionCell> cell)
{
	if(cell.lock() != nullptr)
	{
		if(mSelectedCell.lock() != nullptr)
			mSelectedCell.lock()->SetSelected(false);
		mSelectedCell = cell;
		mSelectedCell.lock()->SetSelected(true);
	}

	if(!mTurretMode)
	{
		mSelectedWeaponIndex = weaponIndex;
		if(mSelectedWeaponIndex >= 0)
			mWeaponActionView->SetWeapon(&mWeapons->at(mSelectedWeaponIndex));
		else
			mWeaponActionView->SetWeapon(nullptr);
	}
	else
	{
		mWeaponActionView->SetWeapon(mTurretWeapon.get());
	}

	RefreshButtons();
	RefreshProperties();
}

void WeaponView::OnNewButton()
{
	if(mTurretMode)
		return;

	if(mSelectedWeaponIndex < 0)
		mWeapons->push_back(Weapon());
	else
		mWeapons->insert(mWeapons->begin() + mSelectedWeaponIndex, Weapon());

	RefreshWeapons();
	SelectWeapon(mSelectedWeaponIndex);
}

void WeaponView::OnCopyButton()
{
	if(mTurretMode || mSelectedWeaponIndex < 0)
		return;
	
	mWeapons->insert(mWeapons->begin() + mSelectedWeaponIndex + 1, mWeapons->at(mSelectedWeaponIndex));
	mSelectedWeaponIndex++;

	RefreshWeapons();
	SelectWeapon(mSelectedWeaponIndex);
}

void WeaponView::OnDeleteButton()
{
	if(mTurretMode || mSelectedWeaponIndex < 0)
		return;
	
	mWeapons->erase(mWeapons->begin() + mSelectedWeaponIndex);
	
	if(mSelectedWeaponIndex == mWeapons->size())
		mSelectedWeaponIndex--;

	RefreshWeapons();
	SelectWeapon(mSelectedWeaponIndex);
}