#pragma once

#include "WeaponView.h"

class WeaponMapView : public WeaponView
{
public:
	WeaponMapView(AnimationRegister& animationRegister, InputSystem& inputSystem, std::map<std::string, Weapon>& weaponMap, std::shared_ptr<PlayerShip> playerShip, ItemType weaponType);
	~WeaponMapView();

	void Initialize();

protected:
	virtual void RefreshWeapons();
	virtual Weapon* GetSelectedWeapon();
	virtual void RefreshButtons();
	virtual void RefreshProperties();
	void ShowNewWeaponDialog(Weapon* weapon, const std::string& title);
	virtual void OnNewButton();
	virtual void OnCopyButton();
	virtual void OnDeleteButton();

private:
	std::string mSelectedWeaponId;
	std::map<std::string, Weapon>& mWeaponMap;
	bool mNewWeaponDialogVisible;
	std::shared_ptr<PlayerShip> mPlayerShip;
	ItemType mWeaponType;

	void AddMapWeaponCell(const std::string& weaponId, Weapon* weapon);
	void SelectMapWeapon(const std::string& weaponId, std::weak_ptr<SelectionCell> cell = std::weak_ptr<SelectionCell>());
	bool OnNewWeaponDialogDone(const std::string& weaponId, Weapon* weaponToCopy);
	void OnEquipButton();
};

