/*
LeaveConfigView

A small view for configuring a list of leave configs.
*/

#include "EnemySpawn.h"
#include "LeaveConfigView.h"
#include "TextField.h"

#define DEFAULT_LEAVE_ACC 0.002f

LeaveConfigView::LeaveConfigView(std::vector<SpawnLeaveConfig>* leaveConfigs, AnimationRegister& animationRegister, InputSystem& inputSystem):
	GenericListView(animationRegister, inputSystem),
	mLeaveConfigs(leaveConfigs)
{
	mEnabled = leaveConfigs != nullptr;
}


LeaveConfigView::~LeaveConfigView()
{
}

void LeaveConfigView::Initialize(std::shared_ptr<TableView> propertyTable)
{
	GenericListView::Initialize(propertyTable);
	GetChild<TextField>("titleLabel")->SetText("LEAVE BEHAVIORS");
}

void LeaveConfigView::SetLeaveConfigs(std::vector<SpawnLeaveConfig>* leaveConfigs)
{
	mSelectedIndex = -1;
	mLeaveConfigs = leaveConfigs;
	mEnabled = mLeaveConfigs != nullptr;
	Refresh();
}

void LeaveConfigView::AddItems()
{
	if(mLeaveConfigs == nullptr)
		return;

	for(int i = 0; i < (int)mLeaveConfigs->size(); i++)
		AddItem(LeaveBehavior::StrFromLeaveType(mLeaveConfigs->at(i).mLeaveType), i);
}

void LeaveConfigView::AddProperties()
{
	if(mLeaveConfigs == nullptr || mSelectedIndex < 0)
		return;

	auto& lb = mLeaveConfigs->at(mSelectedIndex);
	AddStringProperty("type", "Leave Type", LeaveBehavior::StrFromLeaveType(lb.mLeaveType), 
		[&lb](const std::string& leaveType){
		LeaveType type = LeaveBehavior::LeaveTypeFromStr(leaveType);
		// Only update for valid values
		if(type != LeaveType::none || leaveType == LeaveBehavior::StrFromLeaveType(LeaveType::none))
			lb.mLeaveType = type;
	});
	AddFloatProperty("acc", "Acceleration", lb.mAcceleration, [&lb](float acceleration){lb.mAcceleration = acceleration; });
	AddIntProperty("weight", "Weight", lb.mWeight, [&lb](int weight){lb.mWeight = weight; });
}

void LeaveConfigView::OnNewButton()
{
	if(mLeaveConfigs == nullptr)
		return;

	if(mSelectedIndex < 0)
	{
		mLeaveConfigs->push_back(SpawnLeaveConfig(LeaveType::none, DEFAULT_LEAVE_ACC, 10));
		mSelectedIndex = mLeaveConfigs->size() - 1;
	}
	else
	{
		mLeaveConfigs->insert(mLeaveConfigs->begin() + mSelectedIndex, SpawnLeaveConfig(LeaveType::none, DEFAULT_LEAVE_ACC, 10));
	}

	GenericListView::OnNewButton();
}

void LeaveConfigView::OnCopyButton()
{
	if(mLeaveConfigs == nullptr || mSelectedIndex < 0)
		return;

	mLeaveConfigs->insert(mLeaveConfigs->begin() + mSelectedIndex, mLeaveConfigs->at(mSelectedIndex));
	GenericListView::OnCopyButton();
}

void LeaveConfigView::OnDeleteButton()
{
	if(mLeaveConfigs == nullptr || mSelectedIndex < 0)
		return;

	mLeaveConfigs->erase(mLeaveConfigs->begin() + mSelectedIndex);
	if(mSelectedIndex >= (int)mLeaveConfigs->size())
		mSelectedIndex = mLeaveConfigs->size() - 1;

	GenericListView::OnDeleteButton();
}
