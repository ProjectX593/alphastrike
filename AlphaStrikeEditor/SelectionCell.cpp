/*
SelectionCell

These cells are put into various tableViews in the editor. This small class is used to create those cells.
*/

#include "AnimationRegister.h"
#include "EditorGlobals.h"
#include "SelectionCell.h"
#include "TextField.h"

SelectionCell::SelectionCell(AnimationRegister& animationRegister):
	View(animationRegister)
{
}

SelectionCell::~SelectionCell()
{
}

void SelectionCell::Initialize(const std::string& text, std::function<void()> selectCallback, const std::string& backgroundAnimationName, const std::string& animationToPlay, const std::string& assetFilename)
{
	LoadFromJson(assetFilename, PIXELS_IN_UNIT);
	GetChild<TextField>("label")->SetText(text);
	Resize(0.0f, GetChild<Button>("btn")->GetHeight());
	GetChild<Button>("btn")->SetMouseReleaseCallback(selectCallback);

	if(backgroundAnimationName != "")
		GetChild<Button>("btn")->LoadAndPlayAnimation(mAnimationRegister.RegisterAnimation(backgroundAnimationName), animationToPlay);
}

void SelectionCell::SetSelected(bool selected)
{
	GetChild<Button>("btn")->SetSelected(selected);
}

void SelectionCell::SetBackgroundAnimation(const std::string& animationName, const std::string& animationToPlay)
{
	GetChild<Button>("btn")->LoadAndPlayAnimation(mAnimationRegister.RegisterAnimation(animationName), animationToPlay);
}