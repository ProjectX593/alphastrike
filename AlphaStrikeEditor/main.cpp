/*
main.cpp - Created by Alain Carter April 14, 2013.
Alpha Strike's editor.

Tihs file contains the main function and windows message processing loop.
*/


#include <algorithm>
#include <windows.h>
#include <WindowsX.h>

#include "Editor.h"
#include "InputSystem.h"
#include "RendererLocator.h"
#include <time.h>
#include "UtilityServiceLocator.h"
#include "WindowsKeyboardMapping.h"

using namespace std;

LRESULT CALLBACK WindowProc(HWND, UINT, WPARAM, LPARAM);
void EnableOpenGL(HWND hwnd, HDC*, HGLRC*);
void DisableOpenGL(HWND, HDC, HGLRC);

const int MAX_TIME_STEP_MILLISECONDS = 100;
	
const int DEBUG_SCREEN_WIDTH = 1920;
const int DEBUG_SCREEN_HEIGHT = 1080;
const bool FULLSCREEN = true;

//  GetWndClassEx - Helper function to reduce size of main function.
WNDCLASSEX GetWndClassEx(HINSTANCE hInstance)
{
	// register window class
	WNDCLASSEX wcex;
	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_OWNDC;
	wcex.lpfnWndProc = WindowProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = "Alpha Strike Enemy Editor";
	wcex.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	return wcex;
}

//  WinMain - The one and only main function
int WINAPI WinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR lpCmdLine,
	int nCmdShow)
{
	WNDCLASSEX wcex = GetWndClassEx(hInstance);
	HWND hwnd;

	if(!RegisterClassEx(&wcex))
		return 0;

	// create main window 
	hwnd = CreateWindowEx(NULL,
		"Alpha Strike Enemy Editor",
		"Alpha Strike Enemy Editor",
		FULLSCREEN ? WS_POPUP : WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		DEBUG_SCREEN_WIDTH,
		DEBUG_SCREEN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	SetFocus(hwnd);

	// initialize the game
	UtilityServiceLocator::InitWinPlatformUtil();
	RendererLocator::InitWithGLRenderer();
	RendererLocator::GetRenderer().Initialize(hwnd, DEBUG_SCREEN_WIDTH, DEBUG_SCREEN_HEIGHT);
	RendererLocator::GetRenderer().SetFTFontDirectory("Assets\\fonts\\");
	ShowWindow(hwnd, nCmdShow);
	InputSystem inputSystem = InputSystem();
	WindowsKeyboardMapping wkm = WindowsKeyboardMapping(inputSystem);
	Editor editor = Editor(inputSystem);

	MSG msg;
	bool quit = false;
	int lastTime = clock();

	// program main loop
	while(!quit)
	{
		// check for messages
		if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			// handle or dispatch messages
			if(msg.message == WM_QUIT)
			{
				quit = TRUE;
			}
			else if(msg.message == WM_KEYDOWN)
			{
				wkm.OnKeyDown(msg.wParam);
			}
			else if(msg.message == WM_KEYUP)
			{
				wkm.OnKeyUp(msg.wParam);
			}
			else if(msg.message == WM_CHAR)
			{
				wkm.OnCharInput(msg.wParam);
			}
			else if(msg.message == WM_LBUTTONDOWN)
			{
				editor.OnMouseDown(Vector2(GET_X_LPARAM(msg.lParam), GET_Y_LPARAM(msg.lParam) + (FULLSCREEN ? 0 : 39)));
			}
			else if(msg.message == WM_LBUTTONUP)
			{
				editor.OnMouseUp();
			}
			else if(msg.message == WM_MOUSEMOVE)
			{
				editor.OnMouseMove(Vector2(GET_X_LPARAM(msg.lParam), GET_Y_LPARAM(msg.lParam) + (FULLSCREEN ? 0 : 39)));
			}
			else if(msg.message == WM_MOUSEWHEEL)
			{
				float delta = (float)GET_WHEEL_DELTA_WPARAM(msg.wParam) / WHEEL_DELTA;
				editor.OnMouseWheel(delta);
			}

			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			// don't simulate for more than MAX_TIME_STEP_MILLISECONDS or wierd behavior will result
			int currTime = clock();
			if(currTime != lastTime) // don't do timesteps of 0
				quit = editor.Run(min(currTime - lastTime, MAX_TIME_STEP_MILLISECONDS) / 1000.0f);
			lastTime = currTime;
		}
	}

	/* destroy the window explicitly */
	DestroyWindow(hwnd);

	//return msg.wParam;
	return 0;
}

//  WindowProc - Handles messages from Windows such as shutdown.
LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch(uMsg)
	{
	case WM_CLOSE:
		PostQuitMessage(0);
		break;

	case WM_DESTROY:
		return 0;

	case WM_KEYDOWN:
	{
		switch(wParam)
		{
		case VK_ESCAPE:
			PostQuitMessage(0);
			break;
		}
	}
	break;

	default:
		return DefWindowProc(hwnd, uMsg, wParam, lParam);
	}

	return 0;
}