/*
BehaviorSetView

A small view for editing behavior sets.
*/

#include "AnimationRegister.h"
#include "BehaviorSetView.h"
#include "Button.h"
#include "EditorGlobals.h"
#include "NewItemDialog.h"
#include "SelectionCell.h"
#include "TableView.h"
#include "TextField.h"

using std::string;

using namespace std::placeholders;

BehaviorSetView::BehaviorSetView(EnemyConfig& enemyConfig, std::function<bool(bool)> onPopupCallback, std::function<void()> refreshBehaviorsCallback, AnimationRegister& animationRegister):
	View(animationRegister),
	mEnemyConfig(enemyConfig),
	mOnPopupCallback(onPopupCallback),
	mRefreshBehaviorsCallback(refreshBehaviorsCallback),
	mCopyBehaviorSet(false),
	mButtonsDisabled(false)
{

}

BehaviorSetView::~BehaviorSetView()
{
}

void BehaviorSetView::Initialize()
{
	LoadFromJson("assets\\screens\\components\\behaviorSetView.json", PIXELS_IN_UNIT);
	GetChild<TextField>("titleLabel")->SetText("BEHAVIOR SETS");

	GetChild<Button>("newButton")->SetMouseReleaseCallback(std::bind(&BehaviorSetView::CreateNewBehaviorSetDialog, this, false));
	GetChild<Button>("copyButton")->SetMouseReleaseCallback(std::bind(&BehaviorSetView::CreateNewBehaviorSetDialog, this, true));
	GetChild<Button>("deleteButton")->SetMouseReleaseCallback(std::bind(&BehaviorSetView::OnDeleteBehaviorSetButton, this));

	SetSelectedBehaviorSetId("");
	RefreshBehaviorSets();
}

void BehaviorSetView::CreateNewBehaviorSetDialog(bool copy)
{
	// We could disable the buttons but that makes the code too complex, so just leave them enabled but doing nothing while the other dialog is open
	if(!mOnPopupCallback(true))
		return;

	string title = copy ? "COPY BEHAVIOR SET" : "NEW BEHAVIOR SET";

	if(mNewBehaviorSetDialog != nullptr && mCopyBehaviorSet == copy)
	{
		return;
	}
	else if(mNewBehaviorSetDialog != nullptr)
	{
		mNewBehaviorSetDialog->SetTitle(title);
		mCopyBehaviorSet = copy;
		return;
	}

	mCopyBehaviorSet = copy;

	mNewBehaviorSetDialog = std::make_shared<NewItemDialog>(mAnimationRegister, std::bind(&BehaviorSetView::OnNewBehaviorSetDialogDone, this, _1), std::bind(&BehaviorSetView::OnNewBehaviorSetDialogClosed, this));
	mNewBehaviorSetDialog->Initialize(title);
	GetChild<Node>("newBehaviorSetDialogAnchor")->AddChild(mNewBehaviorSetDialog);
}

bool BehaviorSetView::OnNewBehaviorSetDialogDone(std::string behaviorSetName)
{
	if(behaviorSetName == "")
	{
		return false;
	}
	else if(mEnemyConfig.GetBehaviorSets().find(behaviorSetName) != mEnemyConfig.GetBehaviorSets().end())
	{
		mNewBehaviorSetDialog->SetTitle("ALREADY EXISTS");
		return false;
	}

	if(mCopyBehaviorSet)
		mEnemyConfig.AddBehaviorSet(GetSelectedBehaviorSet(), behaviorSetName);
	else
		mEnemyConfig.AddBehaviorSet({}, behaviorSetName);

	mNewBehaviorSetDialog = nullptr;
	RefreshBehaviorSets();
	mOnPopupCallback(false);
	return true;
}

void BehaviorSetView::OnNewBehaviorSetDialogClosed()
{
	mNewBehaviorSetDialog = nullptr;
	mOnPopupCallback(false);
}

void BehaviorSetView::OnDeleteBehaviorSetButton()
{
	if(!mOnPopupCallback(false))
		return;

	auto& behaviors = mEnemyConfig.GetEditableBehaviorSets();

	behaviors.erase(behaviors.find(mSelectedBehaviorSetId));
	SetSelectedBehaviorSetId("");

	RefreshBehaviorSets();
	mRefreshBehaviorsCallback();
}

void BehaviorSetView::RefreshBehaviorSets()
{
	auto& tv = GetChild<TableView>("behaviorSetTable");
	tv->ClearTableNodes();
	mSelectedBehaviorSetCell = std::weak_ptr<SelectionCell>();

	for(auto& behaviorSet : mEnemyConfig.GetEditableBehaviorSets())
	{
		auto cell = std::make_shared<SelectionCell>(mAnimationRegister);
		std::weak_ptr<SelectionCell> weak = cell;
		cell->Initialize(behaviorSet.first, [&behaviorSet, weak, this](){OnBehaviorSetSelected(behaviorSet.first, weak); });
		tv->AddTableNode(cell);

		if(behaviorSet.first == mSelectedBehaviorSetId)
		{
			cell->SetSelected(true);
			mSelectedBehaviorSetCell = weak;
		}
	}
}

void BehaviorSetView::SetSelectedBehaviorSetId(const std::string& behaviorSetId)
{
	mSelectedBehaviorSetId = behaviorSetId;
	RefreshButtons();
}

void BehaviorSetView::OnBehaviorSetSelected(const std::string& behaviorSetId, std::weak_ptr<SelectionCell> behaviorSetCell)
{
	if(behaviorSetId == mSelectedBehaviorSetId)
		return;

	if(mSelectedBehaviorSetCell.lock() != nullptr)
		mSelectedBehaviorSetCell.lock()->GetChild<Button>("btn")->SetSelected(false);

	behaviorSetCell.lock()->SetSelected(true);
	mSelectedBehaviorSetCell = behaviorSetCell;

	SetSelectedBehaviorSetId(behaviorSetId);
	mRefreshBehaviorsCallback();
}

// Will cause an assert if nothing is selected, the UI ought to prevent that from ever happening
BehaviorSet& BehaviorSetView::GetSelectedBehaviorSet()
{
	return mEnemyConfig.GetEditableBehaviorSets().at(mSelectedBehaviorSetId);
}

bool BehaviorSetView::NewBehaviorSetDialogVisible()
{
	return mNewBehaviorSetDialog != nullptr;
}

void BehaviorSetView::SetButtonsDisabled(bool disable)
{
	mButtonsDisabled = disable;
	RefreshButtons();
}

void BehaviorSetView::RefreshButtons()
{
	GetChild<Button>("newButton")->SetDisabled(mButtonsDisabled);
	GetChild<Button>("copyButton")->SetDisabled(mButtonsDisabled || mSelectedBehaviorSetId == "");
	GetChild<Button>("deleteButton")->SetDisabled(mButtonsDisabled || mSelectedBehaviorSetId == "");
}