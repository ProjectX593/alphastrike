/*
NewBulletBehaviorDialog

A dialog used for creating new bullet behaviors.
Contains some duplicated code from NewBehaviorDialog. While that is somewhat gross, I have no future plans for the editor
so it's probably fine, and could be refactored fairly easily if needed.
*/

#include "EditorGlobals.h"
#include "NewBulletBehaviorDialog.h"
#include "SelectionCell.h"
#include "TableView.h"

#include "InterceptBehavior.h"
#include "StraightBehavior.h"

NewBulletBehaviorDialog::NewBulletBehaviorDialog(AnimationRegister& animationRegister, std::function<void(std::shared_ptr<BulletBehavior>)> doneCallback, std::function<void()> closeCallback):
	View(animationRegister),
	mDoneCallback(doneCallback),
	mCloseCallback(closeCallback)
{
}

NewBulletBehaviorDialog::~NewBulletBehaviorDialog()
{
}

std::shared_ptr<View> NewBulletBehaviorDialog::CreateBehaviorTypeCell(const std::string& cellText, std::function<void()> createCallback)
{
	std::shared_ptr<SelectionCell> cell = std::make_shared<SelectionCell>(mAnimationRegister);
	cell->Initialize(cellText, [this, cell, createCallback](){
		if(mSelectedCell.lock() != nullptr)
			mSelectedCell.lock()->SetSelected(false);
		mSelectedCell = cell;
		cell->SetSelected(true);
		createCallback();
	});

	return cell;
}

void NewBulletBehaviorDialog::Initialize()
{
	LoadFromJson("assets\\screens\\components\\newBehaviorDialog.json", PIXELS_IN_UNIT);

	auto tv = GetChild<TableView>("behaviorTypeTable");
	tv->AddTableNode(CreateBehaviorTypeCell("Empty Behavior", std::bind(&NewBulletBehaviorDialog::CreateEmptyBehavior, this)));
	tv->AddTableNode(CreateBehaviorTypeCell("Straight Behavior", std::bind(&NewBulletBehaviorDialog::CreateStraightBehavior, this)));
	tv->AddTableNode(CreateBehaviorTypeCell("Intercept Behavior", std::bind(&NewBulletBehaviorDialog::CreateInterceptBehavior, this)));

	GetChild<Button>("createButton")->SetMouseReleaseCallback(std::bind(&NewBulletBehaviorDialog::OnCreateButton, this));
	GetChild<Button>("cancelButton")->SetMouseReleaseCallback(std::bind(&NewBulletBehaviorDialog::OnCancelButton, this));
}

void NewBulletBehaviorDialog::OnCreateButton()
{
	if(mBehaviorToCreate == nullptr)
		return;

	RemoveFromParentAndCleanup();
	mDoneCallback(mBehaviorToCreate);
}

void NewBulletBehaviorDialog::OnCancelButton()
{
	RemoveFromParentAndCleanup();
	mCloseCallback();
}

void NewBulletBehaviorDialog::CreateEmptyBehavior()
{
	mBehaviorToCreate = std::make_shared<BulletBehavior>();
}

void NewBulletBehaviorDialog::CreateStraightBehavior()
{
	mBehaviorToCreate = std::make_shared<StraightBehavior>(Vector2(0.0f, -1.0f));
}

void NewBulletBehaviorDialog::CreateInterceptBehavior()
{
	mBehaviorToCreate = std::make_shared<InterceptBehavior>(1.0f, 1.0f, false);
}
