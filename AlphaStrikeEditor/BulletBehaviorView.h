#pragma once

#include "PropertyEditorView.h"

class BulletBehavior;
class NewBulletBehaviorDialog;
class SelectionCell;
class WeaponAction;

class InterceptBehavior;
class StraightBehavior;

class BulletBehaviorView : public PropertyEditorView
{
public:
	BulletBehaviorView(AnimationRegister& animationRegister, InputSystem& inputSystem, std::shared_ptr<TableView> propertyTable);
	~BulletBehaviorView();

	void Initialize();
	void SelectWeaponAction(WeaponAction* weaponAction);

private:
	int mSelectedBehaviorIndex;
	WeaponAction* mWeaponAction;
	std::shared_ptr<TableView> mBehaviorTable;
	std::weak_ptr<SelectionCell> mSelectedCell;
	std::shared_ptr<NewBulletBehaviorDialog> mNewBehaviorDialog;

	void SelectBulletBehavior(int index, std::weak_ptr<SelectionCell> cell);
	void RefreshBehaviors();
	void RefreshProperties();
	void RefreshButtons();
	void OnNewButton();
	void OnNewBehaviorDialogDone(std::shared_ptr<BulletBehavior> behavior);
	void OnNewBehaviorDialogClosed();
	void OnCopyButton();
	void OnDeleteButton();
	void OnUpButton();
	void OnDownButton();

	void AddEmptyBehaviorProperties(std::shared_ptr<BulletBehavior> behavior);
	void AddStraightBehaviorProperties(std::shared_ptr<StraightBehavior> behavior);
	void AddInterceptBehaviorProperties(std::shared_ptr<InterceptBehavior> behavior);
};

