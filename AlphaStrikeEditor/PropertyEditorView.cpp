/*
PropertyEditorView

A base class for a few editor views that have the capacity to edit various properties of objects using a table.
Contains functionality to add property nodes to this table. Note that the property nodes are a fixed size, and should
fit in the provided table. At the moment, supports number and string properties. More properties may be added later.

Note: It's worth noting that multiple PropertyEditorViews can share the same TableView. They can all insert into the same
table, and if one of the PropertyEditorViews erases the contents of the TableView, the other PropertyEditorViews will
behave as expected.
*/

#include <algorithm>
#include "AnimatedSprite.h"
#include "EditorGlobals.h"
#include "InputSystem.h"
#include "PropertyEditorView.h"
#include "TableView.h"
#include "TextField.h"
#include "StringUtil.h"

using std::shared_ptr;
using std::string;
using std::weak_ptr;

PropertyEditorView::PropertyEditorView(AnimationRegister& animationRegister, InputSystem& inputSystem):
	View(animationRegister),
	mInputSystem(inputSystem),
	mEnterDown(false),
	mCellFilename("assets\\screens\\components\\propertyCell.json")
{
}


PropertyEditorView::~PropertyEditorView()
{
}

std::weak_ptr<View> PropertyEditorView::AddIntProperty(const std::string& id, const std::string& displayName, int initialValue, std::function<void(int)> propertyChangedCallback)
{
	std::function<bool(const string&)> genericCallback = [propertyChangedCallback](const string& value){
		propertyChangedCallback(atoi(value.c_str()));
		return true;
	};

	return AddPropertyCell(id, displayName, std::to_string(initialValue), genericCallback);
}

std::weak_ptr<View> PropertyEditorView::AddFloatProperty(const std::string& id, const std::string& displayName, float initialValue, std::function<void(float)> propertyChangedCallback)
{
	std::function<bool(const string&)> genericCallback = [propertyChangedCallback](const string& value){
		propertyChangedCallback((float)atof(value.c_str()));
		return true;
	};

	
	return AddPropertyCell(id, displayName, StringUtil::StripTrailingZeroes(std::to_string(initialValue)), genericCallback);
}

std::weak_ptr<View> PropertyEditorView::AddStringProperty(const std::string& id, const std::string& displayName, const std::string& initialValue, std::function <void(const std::string&)> propertyChangedCallback)
{
	std::function<bool(const string&)> genericCallback  = [propertyChangedCallback](const string& value){
		propertyChangedCallback(value);
		return true;
	};

	return AddPropertyCell(id, displayName, initialValue, genericCallback);
}

std::weak_ptr<View> PropertyEditorView::AddBoolProperty(const std::string& id, const std::string& displayName, bool initialValue, std::function<void(bool)> propertyChangedCallback)
{
	std::function<bool(const string&)> genericCallback = [propertyChangedCallback](const string& value){
		string localValue = value;
		std::transform(localValue.begin(), localValue.end(), localValue.begin(), ::tolower);
		if(localValue == "true")
		{
			propertyChangedCallback(true);
			return true;
		}
		else if(localValue == "false")
		{
			propertyChangedCallback(false);
			return true;
		}
		return false;
	};
	return AddPropertyCell(id, displayName, initialValue ? "true" : "false", genericCallback);
}

std::weak_ptr<View> PropertyEditorView::AddVector2Property(const std::string& propertyId, const std::string& displayName, const Vector2& initialValue, std::function<void(const Vector2&)> propertyChangedCallback)
{
	std::function<bool(const string&)> genericCallback = [propertyChangedCallback](const string& value){
		std::vector<string> parts = StringUtil::SplitString(value, ',');
		if(parts.size() != 2)
			return false;

		propertyChangedCallback(Vector2((float)atof(parts.at(0).c_str()), (float)atof(parts.at(1).c_str())));
		return true;
	};

	std::string x = StringUtil::StripTrailingZeroes(std::to_string(initialValue.x));
	std::string y = StringUtil::StripTrailingZeroes(std::to_string(initialValue.y));
	return AddPropertyCell(propertyId, displayName, x + "," + y, genericCallback);
}

std::weak_ptr<View> PropertyEditorView::AddVectorIntProperty(const std::string& propertyId, const std::string& displayName, const std::vector<int>& initialValue, std::function<void(const std::vector<int>&)> propertyChangedCallback)
{
	std::function<bool(const string&)> genericCallback = [propertyChangedCallback](const string& value){
		std::vector<string> parts = StringUtil::SplitString(value, ',');
		std::vector<int> result;
		for(auto& part : parts)
			result.push_back(atoi(part.c_str()));
		propertyChangedCallback(result);
		return true;
	};

	std::string initialStrValue;
	for(int i = 0; i < (int)initialValue.size() - 1; i++)
		initialStrValue += std::to_string(initialValue.at(i)) + ",";
	if(initialValue.size() > 0)
		initialStrValue += std::to_string(initialValue.at(initialValue.size() - 1));

	return AddPropertyCell(propertyId, displayName, initialStrValue, genericCallback);
}

std::weak_ptr<View> PropertyEditorView::AddPropertyCell(const std::string& id, const std::string& displayName, const std::string& initialValue, std::function<bool(const std::string&)> propertyChangedCallback)
{
	if(mPropertyTable == nullptr)
		return std::weak_ptr<View>();

	shared_ptr<View> cell = std::make_shared<View>(mAnimationRegister);
	cell->LoadFromJson(mCellFilename, PIXELS_IN_UNIT);

	auto valueLabel = cell->GetChild<TextField>("valueLabel");
	cell->GetChild<TextField>("nameLabel")->SetText(displayName);
	valueLabel->SetMouseReleaseCallback([id, this](){
		OnCellSelected(id);
	});
	valueLabel->SetText(initialValue);
	valueLabel->SetTextChangedCallback([cell](){
		cell->GetChild<AnimatedSprite>("background")->PlayAnimation("dirty");
	});

	Property newProperty;
	newProperty.mPropertyCell = cell;
	newProperty.mPropertyChangedCallback = propertyChangedCallback;

	mProperties[id] = newProperty;

	cell->Resize(0.0f, cell->GetChild<AnimatedSprite>("background")->GetHeight());
	mPropertyTable->AddTableNode(cell);
	return cell;
}

void PropertyEditorView::ClearProperties()
{
	if(mPropertyTable != nullptr)
		mPropertyTable->ClearTableNodes();

	mProperties.clear();
	mSelectedCellId = "";
}

void PropertyEditorView::SetPropertyValue(const std::string& propertyId, const std::string& newValue)
{
	if(mProperties.find(propertyId) == mProperties.end())
		return;

	auto cell = mProperties.at(propertyId).mPropertyCell.lock();
	if(cell == nullptr)
		return;
	cell->GetChild<AnimatedSprite>("background")->PlayAnimation("default");
	cell->GetChild<TextField>("valueLabel")->SetText(newValue);
}

void PropertyEditorView::SetPropertyTable(std::shared_ptr<TableView> propertyTable)
{
	mPropertyTable = propertyTable;
}

void PropertyEditorView::OnCellSelected(const string& id)
{
	mSelectedCellId = id;
}

void PropertyEditorView::Run(float timestepSeconds)
{
	Node::Run(timestepSeconds);

	bool keyDown = mInputSystem.GetKeyStatus(ButtonType::frontendConfirm) < 0.0001;
	if(!keyDown || mEnterDown)
	{
		mEnterDown = keyDown;
		return;
	}

	mEnterDown = true;

	if(mSelectedCellId == "")
		return;


	Property& prop = mProperties.at(mSelectedCellId);
	auto cell = prop.mPropertyCell.lock();

	if(cell != nullptr)
	{
		// We have to do this first since the callback might end up erasing the cell
		cell->GetChild<AnimatedSprite>("background")->PlayAnimation("default");
		prop.mPropertyChangedCallback(cell->GetChild<TextField>("valueLabel")->GetText());
	}
		
}