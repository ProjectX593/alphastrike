#pragma once

#include "EnemyConfig.h"
#include "PropertyEditorView.h"

class BehaviorSetView;
class EnemyConfig;
class NewBehaviorDialog;
class SelectionCell;

class ActivateTurretBehavior;
class AnimateBehavior;
class ArriveBehavior;
class BulletCircleBehavior;
class DriftBehavior;
class ExplodeBehavior;
class FollowBehavior;
class MoveBehavior;
class ProximityBehavior;
class ReflectBehavior;
class SetChangeBehavior;
class SequenceBehavior;
class ShootBehavior;
class SpawnEnemyBehavior;
class StrafeBehavior;
class SuicideBehavior;
class TimedBehavior;
class TurretVisibilityBehavior;
class TurretVolleyBehavior;
class WanderBehavior;

// Represents a selection in a tree of sequence behaviors. Contains the sequence behavior selected, as well as the group index inside of it, and the behavior index within that
struct SequenceSelection
{
	SequenceSelection():mGroupIndex(-1), mBehaviorIndex(-1) {}
	std::shared_ptr<SequenceBehavior> mSequence;
	int mGroupIndex;
	int mBehaviorIndex;
};

class BehaviorView : public PropertyEditorView
{
public:
	BehaviorView(AnimationRegister& animationRegister, InputSystem& inputSystem, EnemyConfig& enemyConfig);
	~BehaviorView();

	void Initialize();

private:
	EnemyConfig& mEnemyConfig;

	std::shared_ptr<TableView> mSelectionTable;
	std::shared_ptr<BehaviorSetView> mBehaviorSetView;
	std::shared_ptr<View> mPropertyView;
	std::vector<int> mSelectionIndices;	// the first index is in the enemy config itself, then it alternates between sequence behavior and
	std::shared_ptr<NewBehaviorDialog> mNewBehaviorDialog;
	std::weak_ptr<SelectionCell> mSelectedBehaviorCell;

	void AddSequenceCells(std::shared_ptr<SequenceBehavior> sequence, std::vector<int>& selectionIndices);
	void AddBehaviorCells(const std::vector<std::shared_ptr<EnemyBehavior>>& behaviors, std::vector<int>& selectionIndices);
	bool RefreshBehaviorButtons(bool showDialog);
	void RefreshBehaviors(bool clearBehaviorSelection);
	SequenceSelection GetSelectedSequence();
	void InsertBehavior(std::shared_ptr<EnemyBehavior> behavior);
	void OnNewBehaviorButton();
	void OnNewBehaviorDialogDone(std::shared_ptr<EnemyBehavior> behavior);
	void OnNewBehaviorDialogClosed();
	void OnCopyBehaviorButton();
	void OnDeleteBehaviorButton();
	void MoveSelectedItem(int offset);
	void OnUpButton();
	void OnDownButton();
	void SelectCell(const std::vector<int>& selectionIndices, std::weak_ptr<SelectionCell> cell);
	void SelectSequenceBehaviorGroup(int sequenceBehaviorGroupIndex);
	bool SequenceGroupSelected();
	std::shared_ptr<EnemyBehavior> GetSelectedBehavior();
	void RefreshBehaviorProperties();

	void AddActivateTurretProperties(std::shared_ptr<ActivateTurretBehavior> behavior);
	void AddAnimateProperties(std::shared_ptr<AnimateBehavior> behavior);
	void AddArriveProperties(std::shared_ptr<ArriveBehavior> behavior);
	void AddBulletCircleProperties(std::shared_ptr<BulletCircleBehavior> behavior);
	void AddDriftProperties(std::shared_ptr<DriftBehavior> behavior);
	void AddExplodeProperties(std::shared_ptr<ExplodeBehavior> behavior);
	void AddFollowProperties(std::shared_ptr<FollowBehavior> behavior);
	void AddMoveProperties(std::shared_ptr<MoveBehavior> behavior);
	void AddProximityProperties(std::shared_ptr<ProximityBehavior> behavior);
	void AddReflectProperties(std::shared_ptr<ReflectBehavior> behavior);
	void AddSetChangeProperties(std::shared_ptr<SetChangeBehavior> behavior);
	void AddSequenceProperties(std::shared_ptr<SequenceBehavior> behavior);
	void AddShootProperties(std::shared_ptr<ShootBehavior> behavior);
	void AddSpawnEnemyProperties(std::shared_ptr<SpawnEnemyBehavior> behavior);
	void AddStrafeProperties(std::shared_ptr<StrafeBehavior> behavior);
	void AddSuicideProperties(std::shared_ptr<SuicideBehavior> behavior);
	void AddTurretVolleyProperties(std::shared_ptr<TurretVolleyBehavior> behavior);
	void AddWanderProperties(std::shared_ptr<WanderBehavior> behavior);
	void AddTimedBehaviorProperties(std::shared_ptr<TimedBehavior> behavior);
	void AddTurretVisibilityProperties(std::shared_ptr<TurretVisibilityBehavior> behavior);
};

