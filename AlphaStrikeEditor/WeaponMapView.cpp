/*
WeaponMapView

An extension of WeaponView that can handle maps instead of vectors - this requires changes both in the code
and in the UI.
*/

#include "NewItemDialog.h"
#include "PlayerShip.h"
#include "SelectionCell.h"
#include "TableView.h"
#include "TextField.h"
#include "WeaponActionView.h"
#include "WeaponMapView.h"

using namespace std::placeholders;

WeaponMapView::WeaponMapView(AnimationRegister& animationRegister, InputSystem& inputSystem, std::map<std::string, Weapon>& weaponMap, std::shared_ptr<PlayerShip> playerShip, ItemType weaponType):
	WeaponView(animationRegister, inputSystem, nullptr),
	mWeaponMap(weaponMap),
	mNewWeaponDialogVisible(false),
	mPlayerShip(playerShip),
	mWeaponType(weaponType)
{
}

WeaponMapView::~WeaponMapView()
{
}

void WeaponMapView::Initialize()
{
	WeaponView::Initialize();
	GetChild<Button>("equipButton")->SetVisible(true);
	GetChild<TextField>("l7")->SetVisible(true);
	GetChild<Button>("equipButton")->SetMouseReleaseCallback(std::bind(&WeaponMapView::OnEquipButton, this));
}

void WeaponMapView::RefreshWeapons()
{
	mWeaponTable->ClearTableNodes();
	RefreshButtons();
	mSelectedCell = std::weak_ptr<SelectionCell>();

	for(auto& weapon : mWeaponMap)
		AddMapWeaponCell(weapon.first, &weapon.second);
}

Weapon* WeaponMapView::GetSelectedWeapon()
{
	if(mWeaponMap.find(mSelectedWeaponId) == mWeaponMap.end())
		return nullptr;
	return &mWeaponMap.at(mSelectedWeaponId);
}

void WeaponMapView::RefreshButtons()
{
	bool disabled = mWeaponMap.find(mSelectedWeaponId) == mWeaponMap.end();
	GetChild<Button>("copyButton")->SetDisabled(disabled);
	GetChild<Button>("deleteButton")->SetDisabled(disabled);
}

void WeaponMapView::RefreshProperties()
{
	WeaponView::RefreshProperties();
	Weapon* weapon = GetSelectedWeapon();

	if(weapon == nullptr)
		return;

	AddStringProperty("id", "ID", mSelectedWeaponId, [this](const std::string& id){
		Weapon tmp = *GetSelectedWeapon();
		mWeaponMap.erase(mWeaponMap.find(mSelectedWeaponId));
		mSelectedWeaponId = id;
		mWeaponMap[mSelectedWeaponId] = tmp;
		RefreshWeapons();
	});
	AddIntProperty("ammo", "Ammo Required", weapon->mAmmoRequired, [weapon](int ammoRequired){weapon->mAmmoRequired = ammoRequired; });
	AddIntProperty("recValue", "Recycle Value", weapon->mRecycleValue, [weapon](int recycleValue){weapon->mRecycleValue = recycleValue; });
	AddStringProperty("desc", "Description", weapon->mDescription, [weapon](const std::string& description){weapon->mDescription = description; });
	AddStringProperty("thumb", "Thumbnail Img", weapon->mThumbnail, [weapon](const std::string& thumbnail){weapon->mThumbnail = thumbnail; });
}

void WeaponMapView::AddMapWeaponCell(const std::string& weaponId, Weapon* weapon)
{
	std::shared_ptr<SelectionCell> cell = std::make_shared<SelectionCell>(mAnimationRegister);
	cell->Initialize(weaponId, std::bind(&WeaponMapView::SelectMapWeapon, this, weaponId, cell));
	mWeaponTable->AddTableNode(cell);

	if(weaponId == mSelectedWeaponId)
	{
		mSelectedCell = cell;
		cell->SetSelected(true);
	}
}

void WeaponMapView::SelectMapWeapon(const std::string& weaponId, std::weak_ptr<SelectionCell> cell)
{
	if(cell.lock() != nullptr)
	{
		if(mSelectedCell.lock() != nullptr)
			mSelectedCell.lock()->SetSelected(false);
		mSelectedCell = cell;
		mSelectedCell.lock()->SetSelected(true);
	}

	mSelectedWeaponId = weaponId;
	if(mWeaponMap.find(weaponId) != mWeaponMap.end())
		mWeaponActionView->SetWeapon(&mWeaponMap.at(mSelectedWeaponId));
	else
		mWeaponActionView->SetWeapon(nullptr);

	RefreshButtons();
	RefreshProperties();
}

void WeaponMapView::ShowNewWeaponDialog(Weapon* weapon, const std::string& title)
{
	auto nid = std::make_shared<NewItemDialog>(mAnimationRegister, std::bind(&WeaponMapView::OnNewWeaponDialogDone, this, _1, weapon));
	nid->Initialize(title);
	GetChild<Node>("newWeaponDialogAnchor")->AddChild(nid);
}

void WeaponMapView::OnNewButton()
{
	ShowNewWeaponDialog(nullptr, "NEW WEAPON");
}

void WeaponMapView::OnCopyButton()
{
	ShowNewWeaponDialog(GetSelectedWeapon(), "COPY WEAPON");
}

bool WeaponMapView::OnNewWeaponDialogDone(const std::string& weaponId, Weapon* weaponToCopy)
{
	if(mWeaponMap.find(weaponId) != mWeaponMap.end())
		return false;

	if(weaponToCopy == nullptr)
		mWeaponMap[weaponId] = Weapon(-1, "", "", "", 0, mWeaponType == ItemType::primaryWeapon, true, 0.0f, std::vector<WeaponAction>());
	else
		mWeaponMap[weaponId] = *weaponToCopy;

	RefreshWeapons();
	SelectMapWeapon(mSelectedWeaponId);

	return true;
}

void WeaponMapView::OnDeleteButton()
{
	if(mWeaponMap.find(mSelectedWeaponId) == mWeaponMap.end())
		return;

	mWeaponMap.erase(mSelectedWeaponId);
	mSelectedWeaponId = "";

	RefreshWeapons();
}

void WeaponMapView::OnEquipButton()
{
	Weapon* weapon = GetSelectedWeapon();
	if(weapon == nullptr)
		return;

	if(mWeaponType == ItemType::primaryWeapon)
		mPlayerShip->SetPrimaryWeapon(std::make_shared<Weapon>(*weapon));
	else
		mPlayerShip->SetSecondaryWeapon(std::make_shared<Weapon>(*weapon));
}