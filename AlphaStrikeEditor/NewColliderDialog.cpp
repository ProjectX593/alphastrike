/*
NewColliderDialog

Simple dialog that creates new colliders.
Somewhat duplicated from NewBulletBehaviorDialog.. but this editor just needs to get done...
*/

#include "EditorGlobals.h"
#include "NewColliderDialog.h"
#include "SelectionCell.h"
#include "TableView.h"
#include "TextField.h"

NewColliderDialog::NewColliderDialog(AnimationRegister& animationRegister, std::function<void(ColliderConfig)> doneCallback, std::function<void()> closeCallback):
	View(animationRegister),
	mDoneCallback(doneCallback),
	mCloseCallback(closeCallback)
{
}

NewColliderDialog::~NewColliderDialog()
{
}

std::shared_ptr<View> NewColliderDialog::CreateColliderTypeCell(const std::string& cellText, std::function<void()> createCallback)
{
	std::shared_ptr<SelectionCell> cell = std::make_shared<SelectionCell>(mAnimationRegister);
	cell->Initialize(cellText, [this, cell, createCallback](){
		if(mSelectedCell.lock() != nullptr)
			mSelectedCell.lock()->SetSelected(false);
		mSelectedCell = cell;
		cell->SetSelected(true);
		createCallback();
	});

	return cell;
}

void NewColliderDialog::Initialize()
{
	LoadFromJson("assets\\screens\\components\\newBehaviorDialog.json", PIXELS_IN_UNIT);

	auto tv = GetChild<TableView>("behaviorTypeTable");
	tv->AddTableNode(CreateColliderTypeCell("Circle Collider", std::bind(&NewColliderDialog::CreateCircleCollider, this)));
	tv->AddTableNode(CreateColliderTypeCell("Square Collider", std::bind(&NewColliderDialog::CreateSquareCollider, this)));

	GetChild<Button>("createButton")->SetMouseReleaseCallback(std::bind(&NewColliderDialog::OnCreateButton, this));
	GetChild<Button>("cancelButton")->SetMouseReleaseCallback(std::bind(&NewColliderDialog::OnCancelButton, this));
}

void NewColliderDialog::OnCreateButton()
{
	if(mColliderToCreate.mType == ColliderType::invalid)
		return;

	RemoveFromParentAndCleanup();
	mDoneCallback(mColliderToCreate);
}

void NewColliderDialog::OnCancelButton()
{
	RemoveFromParentAndCleanup();
	mCloseCallback();
}

void NewColliderDialog::CreateCircleCollider()
{
	mColliderToCreate.mType = ColliderType::circleCollider;
}

void NewColliderDialog::CreateSquareCollider()
{
	mColliderToCreate.mType = ColliderType::squareCollider;
}
