/*
TurretVolleyView

A small view for editting turret volley behaviors.
*/

#include "Button.h"
#include "EditorGlobals.h"
#include "SelectionCell.h"
#include "StringUtil.h"
#include "TableView.h"
#include "TextField.h"
#include "TurretVolleyBehavior.h"
#include "TurretVolleyView.h"

using std::string;

const string PROPERTY_TURRET_INDEX = "turretIndex";
const string PROPERTY_START_ROT = "startRot";
const string PROPERTY_END_ROT = "endRot";
const string PROPERTY_ROT_SPEED = "rotSpeed";
const string PROPERTY_CLOCKWISE = "clockwise";
const string PROPERTY_FIRE_DELAY = "fireDelay";

TurretVolleyView::TurretVolleyView(std::shared_ptr<TurretVolleyBehavior> behavior, AnimationRegister& animationRegister, InputSystem& inputSystem):
	PropertyEditorView(animationRegister, inputSystem),
	mBehavior(behavior),
	mSelectedVolleyIndex(-1)
{
}

TurretVolleyView::~TurretVolleyView()
{
	ClearProperties();
}

void TurretVolleyView::Initialize(std::shared_ptr<TableView> propertyTable)
{
	LoadFromJson("assets\\screens\\components\\behaviorSetView.json", PIXELS_IN_UNIT);
	GetChild<TextField>("titleLabel")->SetText("TURRET VOLLEY INFOS");

	GetChild<Button>("newButton")->SetMouseReleaseCallback(std::bind(&TurretVolleyView::OnNewButton, this));
	GetChild<Button>("copyButton")->SetMouseReleaseCallback(std::bind(&TurretVolleyView::OnCopyButton, this));
	GetChild<Button>("deleteButton")->SetMouseReleaseCallback(std::bind(&TurretVolleyView::OnDeleteButton, this));

	SetPropertyTable(propertyTable);
	ClearProperties();

	AddIntProperty(PROPERTY_TURRET_INDEX, "Turret Index", 0, [this](int turretIndex){OnTurretIndexChanged(turretIndex); });
	AddFloatProperty(PROPERTY_START_ROT, "Start Rotation", 0.0, [this](float startRotation){OnStartRotationChanged(startRotation);});
	AddFloatProperty(PROPERTY_END_ROT, "End Rotation", 0.0, [this](float endRotation){OnEndRotationChanged(endRotation);});
	AddFloatProperty(PROPERTY_ROT_SPEED, "Rotation Speed", 0.0, [this](float rotationSpeed){OnRotationSpeedChanged(rotationSpeed);});
	AddBoolProperty(PROPERTY_CLOCKWISE, "Moves Clockwise", 0.0, [this](bool clockwise){OnClockwiseChanged(clockwise);});
	AddFloatProperty(PROPERTY_FIRE_DELAY, "Fire Delay", 0.0, [this](float fireDelay){OnFireDelayChanged(fireDelay);});

	RefreshButtons();
	RefreshVolleyInfos();
}

void TurretVolleyView::RefreshVolleyInfos()
{
	auto tv = GetChild<TableView>("behaviorSetTable");
	tv->ClearTableNodes();

	auto& volleyInfos = mBehavior->GetTurretVolleyInfos();
	for(unsigned int i = 0; i < volleyInfos.size(); i++)
	{
		auto cell = std::make_shared<SelectionCell>(mAnimationRegister);
		std::weak_ptr<SelectionCell> weak = cell;
		cell->Initialize("Volley For Turret " + std::to_string(volleyInfos.at(i).mTurretIndex), std::bind(&TurretVolleyView::SelectTurretVolley, this, i, cell));
		tv->AddTableNode(cell);

		if(i == mSelectedVolleyIndex)
		{
			cell->SetSelected(true);
			mSelectedCell = cell;
		}
	}
}

void TurretVolleyView::SelectTurretVolley(int index, std::weak_ptr<SelectionCell> cell)
{
	if(mSelectedCell.lock() != nullptr)
		mSelectedCell.lock()->SetSelected(false);

	mSelectedCell = cell;
	cell.lock()->SetSelected(true);
	mSelectedVolleyIndex = index;
	RefreshProperties();
	RefreshButtons();
}

void TurretVolleyView::RefreshButtons()
{
	GetChild<Button>("copyButton")->SetDisabled(mSelectedVolleyIndex < 0);
	GetChild<Button>("deleteButton")->SetDisabled(mSelectedVolleyIndex < 0);
}

void TurretVolleyView::RefreshProperties()
{
	TurretVolleyInfo* volleyInfo = mSelectedVolleyIndex < 0 ? nullptr : &mBehavior->GetTurretVolleyInfos().at(mSelectedVolleyIndex);
	SetPropertyValue(PROPERTY_TURRET_INDEX, volleyInfo == nullptr ? "" : std::to_string(volleyInfo->mTurretIndex));
	SetPropertyValue(PROPERTY_START_ROT, volleyInfo == nullptr ? "" : StringUtil::StripTrailingZeroes(std::to_string(volleyInfo->mStartRotationRadians)));
	SetPropertyValue(PROPERTY_END_ROT, volleyInfo == nullptr ? "" : StringUtil::StripTrailingZeroes(std::to_string(volleyInfo->mEndRotationRadians)));
	SetPropertyValue(PROPERTY_ROT_SPEED, volleyInfo == nullptr ? "" : StringUtil::StripTrailingZeroes(std::to_string(volleyInfo->mRotationSpeed)));
	SetPropertyValue(PROPERTY_CLOCKWISE, volleyInfo == nullptr ? "" : (volleyInfo->mClockwise ? "true" : "false"));
	SetPropertyValue(PROPERTY_FIRE_DELAY, volleyInfo == nullptr ? "" : StringUtil::StripTrailingZeroes(std::to_string(volleyInfo->mFireDelay)));
}

void TurretVolleyView::OnNewButton()
{
	auto& volleyInfos = mBehavior->GetTurretVolleyInfos();
	TurretVolleyInfo newVolley = TurretVolleyInfo(0, 0.0f, 0.0f, 1.0f, true);
	if(mSelectedVolleyIndex >= 0)
		volleyInfos.insert(volleyInfos.begin() + mSelectedVolleyIndex, newVolley);
	else
		volleyInfos.push_back(newVolley);
	RefreshVolleyInfos();
}

void TurretVolleyView::OnCopyButton()
{
	if(mSelectedVolleyIndex < 0)
		return;

	auto& volleyInfos = mBehavior->GetTurretVolleyInfos();
	volleyInfos.insert(volleyInfos.begin() + mSelectedVolleyIndex, volleyInfos.at(mSelectedVolleyIndex));
	RefreshVolleyInfos();
}

void TurretVolleyView::OnDeleteButton()
{
	if(mSelectedVolleyIndex < 0)
		return;
	auto& volleyInfos = mBehavior->GetTurretVolleyInfos();
	volleyInfos.erase(volleyInfos.begin() + mSelectedVolleyIndex);

	mSelectedVolleyIndex = -1;
	RefreshVolleyInfos();
}

TurretVolleyInfo& TurretVolleyView::GetSelectedVolleyInfo()
{
	return mBehavior->GetTurretVolleyInfos().at(mSelectedVolleyIndex);
}

void TurretVolleyView::OnTurretIndexChanged(int turretIndex)
{
	if(mSelectedVolleyIndex >= 0)
		GetSelectedVolleyInfo().mTurretIndex = turretIndex;
}

void TurretVolleyView::OnStartRotationChanged(float startRotation)
{
	if(mSelectedVolleyIndex >= 0)
		GetSelectedVolleyInfo().mStartRotationRadians = startRotation;
}

void TurretVolleyView::OnEndRotationChanged(float endRotation)
{
	if(mSelectedVolleyIndex >= 0)
		GetSelectedVolleyInfo().mEndRotationRadians = endRotation;
}

void TurretVolleyView::OnRotationSpeedChanged(float rotationSpeed)
{
	if(mSelectedVolleyIndex >= 0)
		GetSelectedVolleyInfo().mRotationSpeed = rotationSpeed;
}

void TurretVolleyView::OnClockwiseChanged(bool clockwise)
{
	if(mSelectedVolleyIndex >= 0)
		GetSelectedVolleyInfo().mClockwise = clockwise;
}

void TurretVolleyView::OnFireDelayChanged(float fireDelay)
{
	if(mSelectedVolleyIndex >= 0)
		GetSelectedVolleyInfo().mFireDelay = fireDelay;
}