#pragma once

#include "AnimationRegister.h"
#include "Config.h"
#include "EnemyPool.h"
#include "ItemManager.h"
#include <memory>
#include "ProgressionState.h"
#include  "RandomGenerator.h"
#include "World.h"

class EditorView;
class InputSystem;
class Layer;
class Scene;

class Editor
{
public:
	Editor(InputSystem& inputSystem);
	~Editor();

	Config mConfig;

	bool Run(float timestepSeconds);
	void OnMouseDown(const Vector2& pos);
	void OnMouseUp();
	void OnMouseMove(const Vector2& pos);
	void OnMouseWheel(float delta);
	void Quit();
	World& GetWorld() { return mWorld; }
	std::string GetSaveDirectory(const std::string& subDirectory = "");

private:
	EnemyPool mEnemyPool;
	InputSystem& mInputSystem;
	std::shared_ptr<Scene> mScene;
	std::shared_ptr<Layer> mFrontendLayer;
	std::shared_ptr<Layer> mGameplayLayer;
	std::shared_ptr<Layer> mBackgroundLayer;
	AnimationRegister mAnimationRegister;
	RandomGenerator mRandomGenerator;
	ItemManager mItemManager;
	ProgressionState mProgressionState;
	World mWorld;
	std::shared_ptr<EditorView> mEditorView;
	bool mQuit;
};

