#pragma once

#include "View.h"

class InputSystem;

class PropertyEditorView : public View
{
public:
	PropertyEditorView(AnimationRegister& animationRegister, InputSystem& inputSystem);
	~PropertyEditorView();

	virtual void Run(float timestepSeconds);

protected:
	std::weak_ptr<View> AddIntProperty(const std::string& propertyId, const std::string& displayName, int initialValue, std::function<void(int)> propertyChangedCallback);
	std::weak_ptr<View> AddFloatProperty(const std::string& propertyId, const std::string& displayName, float initialValue, std::function<void(float)> propertyChangedCallback);
	std::weak_ptr<View> AddStringProperty(const std::string& propertyId, const std::string& displayName, const std::string& initialValue, std::function <void(const std::string&)> propertyChangedCallback);
	std::weak_ptr<View> AddBoolProperty(const std::string& propertyId, const std::string& displayName, bool initialValue, std::function<void(bool)> propertyChangedCallback);
	std::weak_ptr<View> AddVector2Property(const std::string& propertyId, const std::string& displayName, const Vector2& initialValue, std::function<void(const Vector2&)> propertyChangedCallback);
	std::weak_ptr<View> AddVectorIntProperty(const std::string& propertyId, const std::string& displayName, const std::vector<int>& initialValue, std::function<void(const std::vector<int>&)> propertyChangedCallback);
	void ClearProperties();
	void SetPropertyTable(std::shared_ptr<TableView> propertyTable);
	void SetPropertyValue(const std::string& propertyId, const std::string& newValue);
	int GetPropertyCount() { return mProperties.size(); }
	const std::string& GetCellFilename() { return mCellFilename; }
	void SetCellFilename(const std::string& cellFilename) { mCellFilename = cellFilename; }

	InputSystem& mInputSystem;
	std::shared_ptr<TableView> mPropertyTable;

private:
	// A generic property, needs to be given a function that will convert the string value into the appropriate type
	struct Property
	{
		Property():mDirty(false), mPropertyChangedCallback(nullptr) {}
		std::weak_ptr<View> mPropertyCell;
		bool mDirty;
		std::function<bool(const std::string&)> mPropertyChangedCallback; // Returns true if the string is a valid value, false if not
	};

	std::map<std::string, Property> mProperties;
	std::string mSelectedCellId;
	std::string mCellFilename;
	bool mEnterDown;

	std::weak_ptr<View> AddPropertyCell(const std::string& id, const std::string& displayName, const std::string& initialValue, std::function<bool(const std::string&)> propertyChangedCallback);
	void OnCellSelected(const std::string& id);
};

