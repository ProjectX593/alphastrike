/*
CreateBulletRingDialog

A dialog for creating rings of bullets for use with the Weapon views.
*/

#include "CreateBulletRingDialog.h"
#include "EditorGlobals.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include "TableView.h"
#include "Weapon.h"

CreateBulletRingDialog::CreateBulletRingDialog(AnimationRegister& animationRegister, InputSystem& inputSystem, Weapon* weapon, std::function<void()> closeCallback):
	PropertyEditorView(animationRegister, inputSystem),
	mWeapon(weapon),
	mCloseCallback(closeCallback),
	mDamage(1),
	mBulletCount(1),
	mVelocity(0.6f),
	mRotates(true),
	mStartAngleRadians(0.0f),
	mEndAngleRadians(0.0f)
{
}

CreateBulletRingDialog::~CreateBulletRingDialog()
{
}

void CreateBulletRingDialog::Initialize()
{
	LoadFromJson("assets\\screens\\components\\bulletRingDialog.json", PIXELS_IN_UNIT);

	SetPropertyTable(GetChild<TableView>("propertyTable"));

	GetChild<Button>("createButton")->SetMouseReleaseCallback(std::bind(&CreateBulletRingDialog::OnCreateButton, this));
	GetChild<Button>("cancelButton")->SetMouseReleaseCallback(std::bind(&CreateBulletRingDialog::OnCancelButton, this));

	AddIntProperty("dam", "Damage Per Bullet", mDamage, [this](int damage){mDamage = damage; });
	AddIntProperty("cnt", "Bullet Count", mBulletCount, [this](int bulletCount){mBulletCount = bulletCount; });
	AddVector2Property("Center", "Center (Pixels)", mCenter, [this](const Vector2& center){mCenter = center; });
	AddFloatProperty("vel", "Velocity", mVelocity, [this](float velocity){mVelocity = velocity; });
	AddBoolProperty("rot", "Rotates", mRotates, [this](bool rotates){mRotates = rotates; });
	AddStringProperty("bulId", "Bullet ID", mBulletId, [this](const std::string& bulletId){mBulletId = bulletId; });
	AddFloatProperty("startRad", "Start Angle (Radians)", mStartAngleRadians, [this](float startRadians){mStartAngleRadians = startRadians; });
	AddFloatProperty("endRad", "End Angle (Radians)", mEndAngleRadians, [this](float endRadians){mEndAngleRadians = endRadians; });
}

void CreateBulletRingDialog::OnCreateButton()
{
	auto& actions = mWeapon->GetEditableActions();
	Vector2 currentVector = Vector2(0.0f, -1.0f);
	currentVector.Rotate(mStartAngleRadians);
	
	float end = mEndAngleRadians;
	while(end <= mStartAngleRadians)	// this handles the case where they are equal too
		end += 2.0f * (float)M_PI;
	int extra = end - mStartAngleRadians >= 2.0f * (float)M_PI - 0.0001 ? 0 : 1;	// Circles and arcs have a slightly different step between bullets
	float angleStep = (end - mStartAngleRadians) / (mBulletCount - extra);

	for(int i = 0; i < mBulletCount; i++)
	{
		actions.push_back(WeaponAction(mDamage, mCenter, currentVector * mVelocity, mRotates, 0.0f, std::vector<std::shared_ptr<BulletBehavior>>(), mBulletId));
		currentVector.Rotate(angleStep);
	}

	OnCancelButton();
}

void CreateBulletRingDialog::OnCancelButton()
{
	mCloseCallback();
	RemoveFromParentAndCleanup();
}