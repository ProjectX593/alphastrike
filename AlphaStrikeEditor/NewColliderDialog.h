#pragma once

#include "EnemyConfig.h"
#include "View.h"

class Collider;
class SelectionCell;

class NewColliderDialog : public View
{
public:
	NewColliderDialog(AnimationRegister& animationRegister, std::function<void(ColliderConfig)> doneCallback, std::function<void()> closeCallback);
	~NewColliderDialog();

	void Initialize();

private:
	std::function<void(ColliderConfig)> mDoneCallback;
	std::function<void()> mCloseCallback;
	ColliderConfig mColliderToCreate;
	std::weak_ptr<SelectionCell> mSelectedCell;

	std::shared_ptr<View> CreateColliderTypeCell(const std::string& cellText, std::function<void()> createCallback);
	void OnCreateButton();
	void OnCancelButton();

	void CreateCircleCollider();
	void CreateSquareCollider();
};

