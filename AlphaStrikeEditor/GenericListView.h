#pragma once

#include "PropertyEditorView.h"

class InputSystem;

class GenericListView : public PropertyEditorView
{
public:
	GenericListView(AnimationRegister& animationRegister, InputSystem& inputSystem);
	~GenericListView();

	virtual void Initialize(std::shared_ptr<TableView> propertyTable);
	void SelectItem(int itemIndex);

protected:
	std::shared_ptr<TableView> mTable;
	int mSelectedIndex;
	bool mEnabled;

	void Refresh();
	virtual void AddItems() = 0;
	void AddItem(const std::string& text, int index);
	virtual void AddProperties() = 0;
	virtual void OnNewButton();
	virtual void OnCopyButton();
	virtual void OnDeleteButton();
};

