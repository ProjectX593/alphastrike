#pragma once

#include "View.h"

class EnemyBehavior;

class NewBehaviorDialog : public View
{
public:
	NewBehaviorDialog(AnimationRegister& animationRegister, std::function<void(std::shared_ptr<EnemyBehavior>)> doneCallback, std::function<void()> closeCallback);
	~NewBehaviorDialog();

	void Initialize();

private:
	std::function<void(std::shared_ptr<EnemyBehavior>)> mDoneCallback;
	std::function<void()> mCloseCallback;
	std::shared_ptr<EnemyBehavior> mBehaviorToCreate;
	std::weak_ptr<View> mSelectedCell;

	std::shared_ptr<View> CreateBehaviorTypeCell(const std::string& cellText, std::function<void()> createCallback);
	void OnCreateButton();
	void OnCancelButton();

	void CreateActivateTurretBehavior();
	void CreateAnimateBehavior();
	void CreateArriveBehavior();
	void CreateBulletCircleBehavior();
	void CreateExplodeBehavior();
	void CreateFollowBehavior();
	void CreateMoveBehavior();
	void CreateReflectBehavior();
	void CreateSetChangeBehavior();
	void CreateSequenceBehavior();
	void CreateShootBehavior();
	void CreateSpawnEnemyBehavior();
	void CreateStrafeBehavior();
	void CreateSuicideBehavior();
	void CreateTimedBehavior();
	void CreateTurretVisibilityBehavior();
	void CreateTurretVolleyBehavior();
	void CreateWanderBehavior();
};

