#pragma once

#include "PropertyEditorView.h"
#include "Weapon.h"

class BulletBehaviorView;
class EnemyConfig;
class SelectionCell;
class WeaponActionView;

class WeaponView : public PropertyEditorView
{
public:
	WeaponView(AnimationRegister& animationRegister, InputSystem& inputSystem, std::vector<Weapon>* weapons);
	WeaponView(AnimationRegister& animationRegister, InputSystem& inputSystem, std::shared_ptr<Weapon> turretWeapon);
	~WeaponView();

	void Initialize();

protected:
	std::vector<Weapon>* mWeapons;
	std::shared_ptr<TableView> mWeaponTable;
	std::weak_ptr<SelectionCell> mSelectedCell;
	std::shared_ptr<WeaponActionView> mWeaponActionView;

	virtual void RefreshWeapons();
	virtual void RefreshProperties();
	virtual Weapon* GetSelectedWeapon();
	virtual void RefreshButtons();
	virtual void OnNewButton();
	virtual void OnCopyButton();
	virtual void OnDeleteButton();

private:
	std::shared_ptr<Weapon> mTurretWeapon;
	std::shared_ptr<BulletBehaviorView> mBulletBehaviorView;
	int mSelectedWeaponIndex;
	bool mTurretMode;

	void AddWeaponCell(int index, Weapon* weapon);
	void SelectWeapon(int weaponIndex, std::weak_ptr<SelectionCell> cell = std::weak_ptr<SelectionCell>());
};

