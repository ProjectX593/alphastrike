/*
SpawnView

A view for editing a list of spawns within a spawn group.
*/

#include "EnemySpawn.h"
#include "SelectionCell.h"
#include "SpawnView.h"
#include "TableView.h"
#include "TextField.h"

SpawnView::SpawnView(std::vector<std::unique_ptr<EnemySpawn>>* spawns, AnimationRegister& animationRegister, InputSystem& inputSystem):
	GenericListView(animationRegister, inputSystem),
	mSpawns(spawns)
{
	mEnabled = mSpawns != nullptr;
}


SpawnView::~SpawnView()
{
}

void SpawnView::Initialize(std::shared_ptr<TableView> propertyTable)
{
	GenericListView::Initialize(propertyTable);
	GetChild<TextField>("titleLabel")->SetText("SPAWNS");
}

void SpawnView::SetSpawns(std::vector<std::unique_ptr<EnemySpawn>>* spawns)
{
	mSelectedIndex = -1;
	mSpawns = spawns;
	mEnabled = spawns != nullptr;
	Refresh();
}

void SpawnView::AddItems()
{
	if(mSpawns == nullptr)
		return;

	for(int i = 0; i < (int)mSpawns->size(); i++)
	{
		auto& spawn = mSpawns->at(i);
		AddItem(spawn->GetDisplayString() + " - " + spawn->GetEnemyId(), i);
	}
}

void SpawnView::AddProperties()
{
	if(mSpawns == nullptr || mSelectedIndex < 0)
		return;

	auto& spawn = mSpawns->at(mSelectedIndex);

	AddStringProperty("enemyId", "Enemy ID", spawn->GetSpawnConfig().mEnemyId, [&spawn, this](const std::string& enemyId){
		spawn->GetSpawnConfig().mEnemyId = enemyId;
		Refresh();
	});
	AddStringProperty("behSID", "Behavior Set ID", spawn->GetSpawnConfig().mBehaviorSetId, [&spawn](const std::string& behaviorSetId){ spawn->GetSpawnConfig().mBehaviorSetId = behaviorSetId; });
	AddFloatProperty("delay", "Delay", spawn->GetDelay(), [&spawn](float delay){ spawn->SetDelay(delay); });

	auto rangeSpawn = dynamic_cast<EnemyRangeSpawn*>(spawn.get());
	if(rangeSpawn != nullptr)
	{
		AddFloatProperty("xMin", "Min X Position", rangeSpawn->GetXMin(), [rangeSpawn](float xMin){rangeSpawn->SetXMin(xMin); });
		AddFloatProperty("xMax", "Max X Position", rangeSpawn->GetXMax(), [rangeSpawn](float xMax){rangeSpawn->SetXMax(xMax); });
		AddFloatProperty("yPos", "Y Position", rangeSpawn->GetYPos(), [rangeSpawn](float yPos){rangeSpawn->SetYPos(yPos); });
	}
	else
	{
		AddFloatProperty("xPos", "X Position", spawn->GetXPos(), [&spawn](float xPos){spawn->SetXPos(xPos); });
		AddFloatProperty("yPos", "Y Position", spawn->GetYPos(), [&spawn](float yPos){spawn->SetYPos(yPos); });
	}
}

void SpawnView::OnNewButton()
{
	if(mSpawns == nullptr)
		return;

	if(mSelectedIndex < 0)
	{
		mSpawns->push_back(std::make_unique<EnemySpawn>());
		mSelectedIndex = (int)mSpawns->size() - 1;
	}
	else
	{
		mSpawns->insert(mSpawns->begin() + mSelectedIndex, std::make_unique<EnemySpawn>());
	}

	GenericListView::OnNewButton();
}

void SpawnView::OnCopyButton()
{
	if(mSpawns == nullptr || mSelectedIndex < 0)
		return;

	mSpawns->insert(mSpawns->begin() + mSelectedIndex, mSpawns->at(mSelectedIndex)->Copy());
	GenericListView::OnCopyButton();
}

void SpawnView::OnDeleteButton()
{
	if(mSpawns == nullptr || mSelectedIndex < 0)
		return;

	mSpawns->erase(mSpawns->begin() + mSelectedIndex);

	if(mSelectedIndex >= (int)mSpawns->size())
		mSelectedIndex = (int)mSpawns->size() - 1;

	GenericListView::OnDeleteButton();
}