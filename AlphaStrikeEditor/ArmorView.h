#pragma once

#include "PropertyEditorView.h"

class Armor;
class PlayerShip;
class SelectionCell;

class ArmorView : public PropertyEditorView
{
public:
	ArmorView(AnimationRegister& animationReigster, InputSystem& inputSystem, std::shared_ptr<PlayerShip> playerShip, std::map<std::string, Armor>& armors);
	~ArmorView();

	void Initialize();

private:
	std::map<std::string, Armor>& mArmors;
	std::shared_ptr<TableView> mArmorTable;
	std::weak_ptr<SelectionCell> mSelectedCell;
	std::string mSelectedArmorId;
	std::shared_ptr<PlayerShip> mPlayerShip;

	void RefreshArmors();
	void AddArmorCell(const std::string& armorId);
	void SelectArmor(const std::string& armorId, std::weak_ptr<SelectionCell> cell = std::weak_ptr<SelectionCell>());
	void RefreshProperties();
	Armor* GetSelectedArmor();
	void RefreshButtons();
	void OnEquipButton();
	void ShowNewArmorDialog(Armor* weapon, const std::string& title);
	bool OnNewArmorDialogDone(const std::string& armorId, Armor* armorToCopy);
	void OnNewButton();
	void OnCopyButton();
	void OnDeleteButton();
};

