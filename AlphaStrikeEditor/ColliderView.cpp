/*
ColliderView

The view that is used for editing vectors of colliders. These colliders may currently belong to either EnemyShips or Turrets.
More copy pasted stuff... but this editor just needs to be done. I'll revisit it if I need to make more changes to the editor later.
*/

#include "Collider.h"
#include "ColliderView.h"
#include "EditorGlobals.h"
#include "NewColliderDialog.h"
#include "SelectionCell.h"
#include "TableView.h"

#include "CircleCollider.h"
#include "SquareCollider.h"

using namespace std::placeholders;

ColliderView::ColliderView(std::vector<ColliderConfig>& colliderConfigs, AnimationRegister& animationRegister, InputSystem& inputSystem):
	PropertyEditorView(animationRegister, inputSystem),
	mColliderConfigs(colliderConfigs)
{
}

ColliderView::~ColliderView()
{
}

void ColliderView::Initialize()
{
	LoadFromJson("assets\\screens\\components\\colliderView.json", PIXELS_IN_UNIT);

	mColliderTable = GetChild<TableView>("colliderTable");
	SetPropertyTable(GetChild<TableView>("detailsTable"));

	GetChild<Button>("newButton")->SetMouseReleaseCallback(std::bind(&ColliderView::OnNewButton, this));
	GetChild<Button>("copyButton")->SetMouseReleaseCallback(std::bind(&ColliderView::OnCopyButton, this));
	GetChild<Button>("deleteButton")->SetMouseReleaseCallback(std::bind(&ColliderView::OnDeleteButton, this));

	RefreshColliders();
}

void ColliderView::RefreshColliders()
{
	mColliderTable->ClearTableNodes();
	mSelectedCell = std::weak_ptr<SelectionCell>();

	for(int i = 0; i < (int)mColliderConfigs.size(); i++)
	{
		std::shared_ptr<SelectionCell> cell = std::make_shared<SelectionCell>(mAnimationRegister);
		cell->Initialize(mColliderConfigs.at(i).GetDisplayName(), std::bind(&ColliderView::SelectTurretGroup, this, i, cell));
		mColliderTable->AddTableNode(cell);

		if(i == mSelectedColliderIndex)
		{
			mSelectedCell = cell;
			cell->SetSelected(true);
		}
	}

	RefreshButtons();
	RefreshProperties();
}

void ColliderView::RefreshButtons()
{
	GetChild<Button>("newButton")->SetDisabled(mNewColliderDialog != nullptr);
	GetChild<Button>("copyButton")->SetDisabled(mNewColliderDialog != nullptr || mSelectedColliderIndex < 0);
	GetChild<Button>("deleteButton")->SetDisabled(mNewColliderDialog != nullptr || mSelectedColliderIndex < 0);
}

void ColliderView::RefreshProperties()
{
	ClearProperties();
	if(mSelectedColliderIndex < 0)
		return;

	auto& colliderConfig = mColliderConfigs.at(mSelectedColliderIndex);

	AddVector2Property("pos", "Position (Pixels)", colliderConfig.mPosInPixels, [&colliderConfig](const Vector2& posInPixels){colliderConfig.mPosInPixels = posInPixels; });

	if(colliderConfig.mType == ColliderType::circleCollider)
	{
		AddFloatProperty("rad", "Radius (Pixels)", colliderConfig.mRadiusInPixels, [&colliderConfig](float radius){colliderConfig.mRadiusInPixels = radius; });
	}
	else if(colliderConfig.mType == ColliderType::squareCollider)
	{
		AddFloatProperty("wid", "Width (Pixels)", colliderConfig.mSizeInPixels.x, [&colliderConfig](float widthInPixels){colliderConfig.mSizeInPixels.x = widthInPixels; });
		AddFloatProperty("hei", "Height (Pixels)", colliderConfig.mSizeInPixels.y, [&colliderConfig](float heightInPixels){colliderConfig.mSizeInPixels.y = heightInPixels; });
	}
	else
	{
		throw(new std::runtime_error("ERROR: Invalid collider type."));
	}
}

void ColliderView::SelectTurretGroup(int colliderIndex, std::weak_ptr<SelectionCell> cell)
{
	if(colliderIndex == mSelectedColliderIndex)
		return;

	if(mSelectedCell.lock() != nullptr)
		mSelectedCell.lock()->SetSelected(false);
	mSelectedCell = cell;
	mSelectedCell.lock()->SetSelected(true);

	mSelectedColliderIndex = colliderIndex;
	RefreshButtons();
	RefreshProperties();
}

void ColliderView::OnNewButton()
{
	if(mNewColliderDialog != nullptr)
		return;

	mNewColliderDialog = std::make_shared<NewColliderDialog>(mAnimationRegister, std::bind(&ColliderView::OnNewColliderDialogDone, this, _1), std::bind(&ColliderView::OnNewColliderDialogClosed, this));
	mNewColliderDialog->Initialize();
	GetChild<Node>("newColliderDialogAnchor")->AddChild(mNewColliderDialog);
	RefreshButtons();
}

void ColliderView::OnNewColliderDialogDone(ColliderConfig newColliderConfig)
{
	if(mSelectedColliderIndex >= 0)
		mColliderConfigs.insert(mColliderConfigs.begin() + mSelectedColliderIndex, newColliderConfig);
	else
		mColliderConfigs.push_back(newColliderConfig);

	OnNewColliderDialogClosed();
}

void ColliderView::OnNewColliderDialogClosed()
{
	mNewColliderDialog = nullptr;
	RefreshColliders();
}

void ColliderView::OnCopyButton()
{
	if(mSelectedColliderIndex < 0 || mNewColliderDialog != nullptr)
		return;

	mColliderConfigs.insert(mColliderConfigs.begin() + mSelectedColliderIndex, mColliderConfigs.at(mSelectedColliderIndex));
	RefreshColliders();
}

void ColliderView::OnDeleteButton()
{
	if(mSelectedColliderIndex < 0 || mNewColliderDialog != nullptr)
		return;

	mColliderConfigs.erase(mColliderConfigs.begin() + mSelectedColliderIndex);
	RefreshColliders();
}
