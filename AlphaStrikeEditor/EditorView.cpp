/*
EditorView

Contains all of the UI elements for the editor.
This class uses the windows load file dialog directly, and is not cross platform. That could be changed in the future.
*/

#include "ArmorView.h"
#include "BehaviorView.h"
#include "Config.h"
#include "Editor.h"
#include "EditorGlobals.h"
#include "EditorView.h"
#include "EnemyPool.h"
#include "EnemyView.h"
#include <fstream>
#include "ColliderView.h"
#include "NewItemDialog.h"
#include "SpawnGroupView.h"
#include "TableView.h"
#include "TextField.h"
#include "TurretView.h"
#include "PlatformUtil.h"
#include "UtilityServiceLocator.h"
#include "WeaponMapView.h"
#include "WeaponView.h"

using std::string;
using namespace std::placeholders;

EditorView::EditorView(EnemyPool& enemyPool, AnimationRegister& animationRegister, InputSystem& inputSystem, Editor& editor):
	View(animationRegister),
	mEnemyPool(enemyPool),
	mEditor(editor),
	mFileLoaded(false),
	mInputSystem(inputSystem),
	mUIVisible(true),
	mGlobalUIVisible(false),
	mSelectedSpawnGroupIndex(-1),
	mSelectedTurretGroupIndex(-1)
{
}

EditorView::~EditorView()
{
}

void EditorView::Initialize()
{
	LoadFromJson("assets\\screens\\editorView.json", PIXELS_IN_UNIT);

	auto enemyPoolNode = GetChild<Node>("enemyPoolNode");

	mSpawnButton = enemyPoolNode->GetChild<Button>("spawnButton");
	mEnemyButton = enemyPoolNode->GetChild<Button>("enemyButton");
	mBehaviorButton = enemyPoolNode->GetChild<Button>("behaviorButton");
	mTurretButton = enemyPoolNode->GetChild<Button>("turretButton");
	mWeaponButton = enemyPoolNode->GetChild<Button>("weaponButton");
	mColliderButton = enemyPoolNode->GetChild<Button>("colliderButton");

	mSpawnButton->SetMouseReleaseCallback(std::bind(&EditorView::OnSpawnButton, this));
	mEnemyButton->SetMouseReleaseCallback(std::bind(&EditorView::OnEnemyButton, this));
	mBehaviorButton->SetMouseReleaseCallback(std::bind(&EditorView::OnBehaviorButton, this));
	mWeaponButton->SetMouseReleaseCallback(std::bind(&EditorView::OnWeaponButton, this));
	mTurretButton->SetMouseReleaseCallback(std::bind(&EditorView::OnTurretButton, this));
	mColliderButton->SetMouseReleaseCallback(std::bind(&EditorView::OnColliderButton, this));

	auto globalNode = GetChild<Node>("globalNode");

	mPrimaryWeaponButton = globalNode->GetChild<Button>("primaryButton");
	mSecondaryWeaponButton = globalNode->GetChild<Button>("secondaryButton");
	mArmorButton = globalNode->GetChild<Button>("armorButton");
	mUpgradeButton = globalNode->GetChild<Button>("upgradeButton");

	mPrimaryWeaponButton->SetMouseReleaseCallback(std::bind(&EditorView::OnPrimaryWeaponsButton, this));
	mSecondaryWeaponButton->SetMouseReleaseCallback(std::bind(&EditorView::OnSecondaryWeaponsButton, this));
	mArmorButton->SetMouseReleaseCallback(std::bind(&EditorView::OnArmorButton, this));
	mUpgradeButton->SetMouseReleaseCallback(std::bind(&EditorView::OnUpgradeButton, this));

	enemyPoolNode->GetChild<Button>("newFileButton")->SetMouseReleaseCallback(std::bind(&EditorView::OnNewFileButton, this));
	GetChild<Button>("saveButton")->SetMouseReleaseCallback(std::bind(&EditorView::OnSaveButton, this));
	enemyPoolNode->GetChild<Button>("loadButton")->SetMouseReleaseCallback(std::bind(&EditorView::OnLoadButton, this));
	GetChild<Button>("quitButton")->SetMouseReleaseCallback(std::bind(&EditorView::OnQuitButton, this));
	GetChild<Button>("hideUIButton")->SetMouseReleaseCallback(std::bind(&EditorView::ToggleUIVisible, this));
	GetChild<Button>("globalPoolToggleButton")->SetMouseReleaseCallback(std::bind(&EditorView::OnGlobalPoolToggle, this));

	GetChild<Button>("respawnButton")->SetMouseReleaseCallback(std::bind(&EditorView::OnRespawnButton, this));

	SetPoolButtonsDisabled(true);

	// Selection table init
	mSelectionView = std::make_shared<View>(mAnimationRegister);
	mSelectionView->LoadFromJson("assets\\screens\\components\\selectionView.json", PIXELS_IN_UNIT);
	GetChild<Node>("selectionViewAnchor")->AddChild(mSelectionView);
	mSelectionTable = mSelectionView->GetChild<TableView>("selectionTable");
	mSelectionView->SetVisible(false);

	OnEnemySelected("");
	UpdateGlobalPoolNodes();
}

// This is called when the mode is changed to avoid duplicated code - it also sets the behavior set view invisible
void EditorView::OnNewTypeSelected()
{
	mSpawnButton->SetSelected(false);
	mEnemyButton->SetSelected(false);
	mBehaviorButton->SetSelected(false);
	mTurretButton->SetSelected(false);
	mWeaponButton->SetSelected(false);
	mColliderButton->SetSelected(false);
	mSelectionView->SetVisible(false);
	
	if(mTypeView != nullptr)
	{
		mTypeView->RemoveFromParentAndCleanup();
		mTypeView = nullptr;
	}
}

void EditorView::SetPoolButtonsDisabled(bool disabled)
{
	mSpawnButton->SetDisabled(disabled);
	mEnemyButton->SetDisabled(disabled);
	mBehaviorButton->SetDisabled(disabled || mSelectedEnemyId == "");
	mTurretButton->SetDisabled(disabled || mSelectedEnemyId == "");
	mWeaponButton->SetDisabled(disabled || mSelectedEnemyId == "");
	mColliderButton->SetDisabled(disabled || mSelectedEnemyId == "");
}

void EditorView::OnSpawnButton()
{
	OnNewTypeSelected();
	mSpawnButton->SetSelected(true);

	std::shared_ptr<SpawnGroupView> sgv = std::make_shared <SpawnGroupView>(mEnemyPool, mAnimationRegister, mInputSystem, std::bind(&EditorView::OnSpawnGroupSelected, this, _1));
	sgv->Initialize(mSelectedSpawnGroupIndex);
	AddChild(sgv);
	mTypeView = sgv;
	RefreshButtons();
}

void EditorView::OnSpawnGroupSelected(int spawnGroupIndex)
{
	mSelectedSpawnGroupIndex = spawnGroupIndex;
	RefreshButtons();
}

void EditorView::OnEnemyButton()
{
	OnNewTypeSelected();
	mEnemyButton->SetSelected(true);
	
	std::shared_ptr<EnemyView> ev = std::make_shared<EnemyView>(mEnemyPool, mAnimationRegister, mInputSystem, std::bind(&EditorView::OnEnemySelected, this, _1));
	ev->Initialize(mSelectedEnemyId);
	AddChild(ev);
	mTypeView = ev;
	RefreshButtons();
}

void EditorView::OnEnemySelected(const std::string& enemyId)
{
	mSelectedEnemyId = enemyId;
	mSelectedTurretGroupIndex = -1;
	RefreshButtons();
}

void EditorView::OnTurretGroupSelected(int turretGroupIndex)
{
	mSelectedTurretGroupIndex = turretGroupIndex;
	RefreshButtons();
}

bool EditorView::TurretViewOpen()
{
	return std::dynamic_pointer_cast<TurretView>(mTypeView) != nullptr;
}

void EditorView::RefreshButtons()
{
	mBehaviorButton->SetDisabled(mSelectedEnemyId == "");
	mTurretButton->SetDisabled(mSelectedEnemyId == "");
	mWeaponButton->SetDisabled(TurretViewOpen() ? mSelectedTurretGroupIndex < 0 : mSelectedEnemyId == "");
	mColliderButton->SetDisabled(TurretViewOpen() ? mSelectedTurretGroupIndex < 0 : mSelectedEnemyId == "");

	bool inSpawnView = std::dynamic_pointer_cast<SpawnGroupView>(mTypeView) != nullptr;
	GetChild<Button>("respawnButton")->SetDisabled((!inSpawnView && mSelectedEnemyId == "") || (inSpawnView && mSelectedSpawnGroupIndex < 0));
}

void EditorView::OnBehaviorButton()
{
	OnNewTypeSelected();
	mBehaviorButton->SetSelected(true);

	if(mSelectedEnemyId != "")
	{
		std::shared_ptr<BehaviorView> bv = std::make_shared<BehaviorView>(mAnimationRegister, mInputSystem, mEnemyPool.GetEditableEnemyConfigs().at(mSelectedEnemyId));
		bv->Initialize();
		mTypeView = bv;
		AddChild(mTypeView);
		RefreshButtons();
	}
}

void EditorView::OnTurretButton()
{
	OnNewTypeSelected();
	mTurretButton->SetSelected(true);

	if(mSelectedEnemyId != "")
	{
		auto tv = std::make_shared<TurretView>(mAnimationRegister, mInputSystem, mEnemyPool.GetEditableEnemyConfigs().at(mSelectedEnemyId), std::bind(&EditorView::OnTurretGroupSelected, this, _1));
		tv->Initialize();
		mTypeView = tv;
		AddChild(mTypeView);
		RefreshButtons();
	}
}

bool EditorView::TryOpenTurretSubview(std::function<std::shared_ptr<View>(bool)> openCallback)	// open callback will be passed true if in turret mode
{
	bool turretMode = TurretViewOpen() && mSelectedTurretGroupIndex >= 0;
	bool enemyMode = !TurretViewOpen() && mSelectedEnemyId != "";

	OnNewTypeSelected();
	mTurretButton->SetSelected(turretMode);

	if(turretMode || enemyMode)
	{
		std::shared_ptr<View> v = openCallback(turretMode);
		mTypeView = v;
		AddChild(mTypeView);
		RefreshButtons();
		return true;
	}
	return false;
}

void EditorView::OnWeaponButton()
{
	auto openCallback = [this](bool turretMode){
		std::shared_ptr<WeaponView> wv = nullptr;
		if(turretMode)
			wv = std::make_shared<WeaponView>(mAnimationRegister, mInputSystem, mEnemyPool.GetEditableEnemyConfigs().at(mSelectedEnemyId).mTurretGroupConfigs.at(mSelectedTurretGroupIndex).mWeapon);
		else
			wv = std::make_shared<WeaponView>(mAnimationRegister, mInputSystem, &mEnemyPool.GetEditableEnemyConfigs().at(mSelectedEnemyId).mWeapons);
		wv->Initialize();
		return wv;
	};

	if(TryOpenTurretSubview(openCallback))
	{
		mWeaponButton->SetSelected(true);
	}
}

void EditorView::OnColliderButton()
{
	auto openCallback = [this](bool turretMode){
		std::shared_ptr<ColliderView> cv = nullptr;
		if(turretMode)
			cv = std::make_shared<ColliderView>(mEnemyPool.GetEditableEnemyConfigs().at(mSelectedEnemyId).mTurretGroupConfigs.at(mSelectedTurretGroupIndex).mColliderConfigs, mAnimationRegister, mInputSystem);
		else
			cv = std::make_shared<ColliderView>(mEnemyPool.GetEditableEnemyConfigs().at(mSelectedEnemyId).mColliderConfigs, mAnimationRegister, mInputSystem);
		cv->Initialize();
		return cv;
	};

	if(TryOpenTurretSubview(openCallback))
	{
		mColliderButton->SetSelected(true);
	}
}


// Global editing below
void EditorView::OnNewGlobalTypeSelected()
{
	mPrimaryWeaponButton->SetSelected(false);
	mSecondaryWeaponButton->SetSelected(false);
	mArmorButton->SetSelected(false);
	mUpgradeButton->SetSelected(false);

	if(mGlobalTypeView != nullptr)
	{
		mGlobalTypeView->RemoveFromParentAndCleanup();
		mGlobalTypeView = nullptr;
	}
}

void EditorView::InitWeaponView(std::map<std::string, Weapon>& weaponMap, ItemType weaponType)
{
	OnNewGlobalTypeSelected();

	auto wv = std::make_shared<WeaponMapView>(mAnimationRegister, mInputSystem, weaponMap, mEditor.GetWorld().GetPlayerShip(), weaponType);
	wv->Initialize();
	mGlobalTypeView = wv;
	AddChild(mGlobalTypeView);
}

void EditorView::OnPrimaryWeaponsButton()
{
	InitWeaponView(mEditor.mConfig.mPrimaryWeapons, ItemType::primaryWeapon);
	mPrimaryWeaponButton->SetSelected(true);
}

void EditorView::OnSecondaryWeaponsButton()
{
	InitWeaponView(mEditor.mConfig.mSecondaryWeapons, ItemType::secondaryWeapon);
	mSecondaryWeaponButton->SetSelected(true);
}

void EditorView::OnArmorButton()
{
	OnNewGlobalTypeSelected();

	auto av = std::make_shared<ArmorView>(mAnimationRegister, mInputSystem, mEditor.GetWorld().GetPlayerShip(), mEditor.mConfig.mArmors);
	av->Initialize();
	mGlobalTypeView = av;
	AddChild(mGlobalTypeView);
}

void EditorView::OnUpgradeButton()
{

}


// File stuff below
void EditorView::OnNewFileButton()
{
	OnSaveButton();
	if(mNewFileDialog != nullptr)
		return;

	mNewFileDialog = std::make_shared<NewItemDialog>(mAnimationRegister, std::bind(&EditorView::OnNewFileDialogDone, this, _1), std::bind(&EditorView::OnNewFileDialogClosed, this));
	mNewFileDialog->Initialize("NEW ENEMY POOL");
	GetChild<Node>("newFileDialogAnchor")->AddChild(mNewFileDialog);
}

bool EditorView::OnNewFileDialogDone(std::string filename)
{
	if(filename.length() == 0)
	{
		return false;
	}
	else if(filename.find('.') != string::npos)
	{
		mNewFileDialog->SetTitle("DO NOT ADD EXTENSION");
		return false;
	}

	string fullFilename = mEditor.GetSaveDirectory("EnemyPools\\") + filename + ".epl";

	if(UtilityServiceLocator::GetPlatformUtil().FileExists(fullFilename))
	{
		mNewFileDialog->SetTitle("FILE ALREADY EXISTS");
		return false;
	}

	mFullEnemyPoolFilename = fullFilename;

	ClearEnemyPool();
	OnSaveButton();

	SetPoolButtonsDisabled(false);
	mNewFileDialog = nullptr;
	return true;
}

void EditorView::OnNewFileDialogClosed()
{
	mNewFileDialog = nullptr;
}

void EditorView::OnSaveButton()
{
	std::ofstream stream;
	stream.open(mEditor.GetSaveDirectory() + CONFIG_FILENAME, std::ios::out | std::ios::binary);
	cereal::BinaryOutputArchive configArchive(stream);
	configArchive(mEditor.mConfig);
	stream.close();

	if(mFullEnemyPoolFilename == "")
		return;

	stream.open(mFullEnemyPoolFilename, std::ios::out | std::ios::binary);
	cereal::BinaryOutputArchive archive(stream);
	archive(mEnemyPool);
	stream.close();
}

void EditorView::OnLoadButton()
{
	OnNewTypeSelected();
	mSelectionView->SetVisible(true);
	mSelectionTable->ClearTableNodes();
	std::vector<FileInfo> files = UtilityServiceLocator::GetPlatformUtil().GetFilesInDirectory(mEditor.GetSaveDirectory("EnemyPools\\") + "*");

	for(auto& file : files)
	{
		if(file.mIsDirectory)
			continue;

		auto cell = CreateSmallCell();
		cell->GetChild<TextField>("label")->SetText(file.mFilename);
		cell->GetChild<Button>("btn")->SetMouseReleaseCallback([file, this](){LoadFromFile(file.mFilename);});
		cell->Resize(0.0f, cell->GetChild<Button>("btn")->GetHeight());	// need to give the node itself it's height for the tableview
		mSelectionTable->AddTableNode(cell);
	}
}

void EditorView::LoadFromFile(const string& filename)
{
	if(filename == "")
		return;

	OnSaveButton();
	mFullEnemyPoolFilename = mEditor.GetSaveDirectory("EnemyPools\\") + filename;
	std::ifstream stream;
	stream.open(mFullEnemyPoolFilename, std::ios::in | std::ios::binary);
	cereal::BinaryInputArchive archive(stream);

	ClearEnemyPool();
	archive(mEnemyPool);

	SetPoolButtonsDisabled(false);
	mSelectionTable->ClearTableNodes();
	mSelectionView->SetVisible(false);
}

void EditorView::OnGlobalPoolToggle()
{
	mGlobalUIVisible = !mGlobalUIVisible;
	if(mTypeView != nullptr)
		mTypeView->SetVisible(!mGlobalUIVisible && mUIVisible);
	if(mGlobalTypeView != nullptr)
		mGlobalTypeView->SetVisible(mGlobalUIVisible && mUIVisible);
	UpdateGlobalPoolNodes();
}

void EditorView::UpdateGlobalPoolNodes()
{
	GetChild<Node>("enemyPoolNode")->SetVisible(!mGlobalUIVisible);
	GetChild<Node>("globalNode")->SetVisible(mGlobalUIVisible);
	GetChild<TextField>("l13")->SetText(mGlobalUIVisible ? "POOLS" : "GLOBALS");
}

void EditorView::OnQuitButton()
{
	mEditor.Quit();
}

std::shared_ptr<View> EditorView::CreateSmallCell()
{
	std::shared_ptr<View> cell = std::make_shared<View>(mAnimationRegister);
	cell->LoadFromJson("assets\\screens\\components\\selectionCell.json", PIXELS_IN_UNIT);
	return cell;
}

void EditorView::ClearEnemyPool()
{
	mEnemyPool = EnemyPool();
	OnEnemySelected("");
}

void EditorView::OnRespawnButton()
{
	bool inSpawnView = std::dynamic_pointer_cast<SpawnGroupView>(mTypeView) != nullptr;
	string behaviorSetId = GetChild<TextField>("respawnBehaviorSetTextField")->GetText();
	if((inSpawnView && mSelectedSpawnGroupIndex < 0) || (!inSpawnView && mSelectedEnemyId == "") || (!inSpawnView && !mEnemyPool.GetEnemyConfig(mSelectedEnemyId).HasBehaviorSet(behaviorSetId)))
		return;

	World& world = mEditor.GetWorld();
	auto& enemyShips = world.GetEnemyShips();
	for(auto enemyShip : enemyShips)
		enemyShip->Kill();

	if(inSpawnView)
		world.SpawnEnemyGroup(mEnemyPool.GetEditableEnemySpawnGroups().at(mSelectedSpawnGroupIndex));
	else
		world.SpawnEnemy(mSelectedEnemyId, behaviorSetId, Vector2(0.5f, -0.2f));
}

void EditorView::ToggleUIVisible()
{
	mUIVisible = !mUIVisible;

	if(!mUIVisible)
	{
		GetChild<Node>("enemyPoolNode")->SetVisible(false);
		GetChild<Node>("globalNode")->SetVisible(false);
	}
	else
	{
		UpdateGlobalPoolNodes();
	}
	
	if(mTypeView != nullptr)
		mTypeView->SetVisible(mUIVisible && !mGlobalUIVisible);
	if(mGlobalTypeView != nullptr)
		mGlobalTypeView->SetVisible(mUIVisible && mGlobalUIVisible);
	if(mNewFileDialog != nullptr)
		mNewFileDialog->SetVisible(mUIVisible);
}