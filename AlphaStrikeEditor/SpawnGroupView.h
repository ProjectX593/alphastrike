#pragma once

#include "PropertyEditorView.h"

class EnemyPool;
class LeaveConfigView;
class SelectionCell;
class SpawnView;

class SpawnGroupView : public PropertyEditorView
{
public:
	SpawnGroupView(EnemyPool& enemyPool, AnimationRegister& animationRegister, InputSystem& inputSystem, std::function<void(int)> spawnGroupSelectedCallback);
	~SpawnGroupView();

	void Initialize(int selectedSpawnGroupIndex);
	void RefreshButtons();
	void OnSpawnsButton();
	void OnLeaveButton();
	void NewSpawnGroup();
	void CopySpawnGroup();
	void DeleteSpawnGroup();
	void RefreshSpawnGroups();
	void AddSpawnGroupProperties();
	void SelectSpawnGroup(int index);

	std::vector<std::unique_ptr<EnemySpawn>>* GetSpawns();
	std::vector<SpawnLeaveConfig>* GetLeaveConfigs();

private:
	EnemyPool& mEnemyPool;
	int mSelectedSpawnGroupIndex;
	bool mSpawnsVisible;
	std::function<void(int)> mSpawnGroupSelectedCallback;
	std::shared_ptr<SpawnView> mSpawnView;
	std::shared_ptr<LeaveConfigView> mLeaveConfigView;
	std::shared_ptr<TableView> mSpawnGroupTable;

	void OnUpButton();
	void OnDownButton();
};

