/*
Editor

The main editor class. It contains the UI and all classes needed to run the editor.
*/

#include "Editor.h"
#include "EditorGlobals.h"
#include "EditorView.h"
#include "InputSystem.h"
#include "Layer.h"
#include "PlayerShip.h"
#include "RendererLocator.h"
#include "Scene.h"
#include "UtilityServiceLocator.h"

using namespace std::placeholders;

Editor::Editor(InputSystem& inputSystem):
	mInputSystem(inputSystem),
	mScene(std::make_shared<Scene>()),
	mFrontendLayer(std::make_shared<Layer>(2.0f)),
	mGameplayLayer(std::make_shared<Layer>(1.0f)),
	mBackgroundLayer(std::make_shared<Layer>(0.0f)),
	mAnimationRegister(UtilityServiceLocator::GetPlatformUtil().GetWorkingDirectory() + "assets\\textures\\", UtilityServiceLocator::GetPlatformUtil().GetWorkingDirectory() + "assets\\animations\\"),
	mItemManager(mAnimationRegister, &mConfig),
	mProgressionState(mItemManager, mRandomGenerator, mConfig),
	mWorld(mProgressionState, mFrontendLayer, mGameplayLayer, mBackgroundLayer, mItemManager, mInputSystem, mRandomGenerator, &mConfig),
	mQuit(false)
{
	RendererLocator::GetRenderer().SetSampleMode(SampleMode::smNearestNeighbor);

	std::ifstream stream;
	stream.open(GetSaveDirectory() + CONFIG_FILENAME, std::ios::out | std::ios::binary);
	cereal::BinaryInputArchive archive(stream);
	archive(mConfig);
	stream.close();

	bool saveConfig = mConfig.VerifyItemIds();
	if(saveConfig)
	{
		std::ofstream stream;
		stream.open(GetSaveDirectory() + CONFIG_FILENAME, std::ios::out | std::ios::binary);
		cereal::BinaryOutputArchive archive(stream);
		archive(mConfig);
		stream.close();
	}

	mProgressionState.StartNewGame();
	mScene->AddChild(mFrontendLayer);
	mScene->AddChild(mGameplayLayer);
	mProgressionState.GetCurrencyManager().AddScrap(50000);
	mProgressionState.GetCurrencyManager().ConvertScrapToEnergy(50000);
	mProgressionState.GetCurrencyManager().ConvertEnergyToAmmo(10000);
	mProgressionState.SelectPlanet(0);
	auto& primaryWeapons = mProgressionState.GetItemsOfType(ItemType::primaryWeapon);
	for(auto& wep : primaryWeapons)
	{
		mProgressionState.SetEquippedItem(wep.second->GetGUID(), ItemType::primaryWeapon);
		break;
	}

	mWorld.StartEditorMode();
	mWorld.Reset();
	mWorld.SetEnemyPool(&mEnemyPool);
	mWorld.GetPlayerShip()->SetInvulnerable(true);

	mEditorView = std::make_shared<EditorView>(mEnemyPool, mAnimationRegister, mInputSystem, *this);
	mEditorView->Initialize();
	mFrontendLayer->AddChild(mEditorView);

	// Pass input to the frontend layer only, the game will query for button states during it's run step
	mInputSystem.SetKeyDownCallback(std::bind(&Node::OnKeyDown, mFrontendLayer.get(), _1));
	mInputSystem.SetKeyUpCallback(std::bind(&Node::OnKeyUp, mFrontendLayer.get(), _1));
	mInputSystem.SetCharInputCallback(std::bind(&Node::OnCharInput, mFrontendLayer.get(), _1));
}

Editor::~Editor()
{
}

bool Editor::Run(float timestepSeconds)
{
	mScene->Run(timestepSeconds);
	mWorld.Simulate(timestepSeconds);

	Renderer& renderer = RendererLocator::GetRenderer();

	renderer.BeginFrame();
	mScene->Render();
	renderer.EndFrame();

	return mInputSystem.GetKeyStatus(ButtonType::frontendBack) != 0.0f || mQuit;
}

void Editor::OnMouseDown(const Vector2& pos)
{
	Vector2 screenCoords = RendererLocator::GetRenderer().PixelCoordsToScreenCoords(pos);
	mScene->OnMouseDown(screenCoords);
}

void Editor::OnMouseUp()
{
	mScene->OnMouseUp();
}

void Editor::OnMouseMove(const Vector2& pos)
{
	Vector2 screenCoords = RendererLocator::GetRenderer().PixelCoordsToScreenCoords(pos);
	mScene->OnMouseMove(screenCoords);
}

void Editor::OnMouseWheel(float delta)
{
	mScene->OnMouseWheel(delta);
}

void Editor::Quit()
{
	mQuit = true;
}

std::string Editor::GetSaveDirectory(const std::string& subDirectory)
{
	auto& pu = UtilityServiceLocator::GetPlatformUtil();
	std::string fileDir = pu.GetWorkingDirectory() + subDirectory;
	pu.CreateDir(fileDir);
	return fileDir;
}