/*
WindowsKeyboardMapping

Maps windows key presses to ButtonTypes. Not well written and should be redone
if this engine is ever used elsewhere.
*/

#include <Windows.h>

#include "InputSystem.h"
#include "WindowsKeyboardMapping.h"

// WindowsKeyboardMapping - Constructor
WindowsKeyboardMapping::WindowsKeyboardMapping(InputSystem& inputSystem):
mInputSystem(inputSystem)
{
	ResetDefaultMappings();
}

// ~WindowsKeyboardMapping - Destructor
WindowsKeyboardMapping::~WindowsKeyboardMapping(void)
{
}

// ResetDefaultMappings - Resets keyboard mappings to the default.
// This is the all important location where the hard coded defaults are maintained.
void WindowsKeyboardMapping::ResetDefaultMappings()
{
	// Frontend
	mMappings[ButtonType::showConsole] = VK_OEM_3;
	mMappings[ButtonType::frontendBack] = VK_ESCAPE;
	mMappings[ButtonType::frontendConfirm] = VK_RETURN;
	mMappings[ButtonType::frontendBackspace] = VK_BACK;
	mMappings[ButtonType::frontendUp] = VK_UP;
	mMappings[ButtonType::frontendDown] = VK_DOWN;
	mMappings[ButtonType::frontendLeft] = VK_LEFT;
	mMappings[ButtonType::frontendRight] = VK_RIGHT;
	mMappings[ButtonType::gameUp] = 'W';
	mMappings[ButtonType::gameDown] = 'S';
	mMappings[ButtonType::gameLeft] = 'A';
	mMappings[ButtonType::gameRight] = 'D';
	mMappings[ButtonType::gameAttack1] = VK_SPACE;
	mMappings[ButtonType::gameAttack2] = 'E';
}

// OnKeyDown - Function that gets called when a key is pressed.
void WindowsKeyboardMapping::OnKeyDown(UINT key)
{
	ButtonType genericKey = FindKey(key);
	if(genericKey != ButtonType::keyNotFound)
		mInputSystem.OnKeyDown(genericKey);
}

// OnKeyUp - Function that gets called when a key is released.
void WindowsKeyboardMapping::OnKeyUp(UINT key)
{
	ButtonType genericKey = FindKey(key);
	if(genericKey != ButtonType::keyNotFound)
		mInputSystem.OnKeyUp(genericKey);
}

void WindowsKeyboardMapping::OnCharInput(wchar_t c)
{
	mInputSystem.OnCharInput(c);
}

// FindKey - Finds the index (in buttons.h) of the key with the given windows
// key ID. Returns -1 if the key is not found, ie. the key is mapped to nothing.
ButtonType WindowsKeyboardMapping::FindKey(UINT key)
{
	// all we can do is a linear search
	for(auto& mapping : mMappings)
	{
		if(mapping.second == key)
			return mapping.first;
	}

	return ButtonType::keyNotFound;
}
