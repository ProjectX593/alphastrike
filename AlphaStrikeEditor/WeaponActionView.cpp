/*
WeaponActionView

The view used for editing weapon actions.
*/

#include "BulletBehaviorView.h"
#include "Button.h"
#include "CreateBulletRingDialog.h"
#include "EditorGlobals.h"
#include "SelectionCell.h"
#include "TableView.h"
#include "TextField.h"
#include "Weapon.h"
#include "WeaponActionView.h"

WeaponActionView::WeaponActionView(AnimationRegister& animationRegister, InputSystem& inputSystem, std::shared_ptr<BulletBehaviorView> bulletBehaviorView, std::shared_ptr<TableView> propertyTable):
	PropertyEditorView(animationRegister, inputSystem),
	mBulletBehaviorView(bulletBehaviorView),
	mSelectedActionIndex(-1),
	mWeapon(nullptr),
	mRingDialogOpen(false)
{
	SetPropertyTable(propertyTable);
}

WeaponActionView::~WeaponActionView()
{
}

void WeaponActionView::Initialize()
{
	LoadFromJson("assets\\screens\\components\\mediumEditorView.json", PIXELS_IN_UNIT);

	GetChild<TextField>("titleLabel")->SetText("Weapon Actions");
	GetChild<Button>("newButton")->SetMouseReleaseCallback(std::bind(&WeaponActionView::OnNewButton, this));
	GetChild<Button>("copyButton")->SetMouseReleaseCallback(std::bind(&WeaponActionView::OnCopyButton, this));
	GetChild<Button>("deleteButton")->SetMouseReleaseCallback(std::bind(&WeaponActionView::OnDeleteButton, this));
	GetChild<Button>("upButton")->SetMouseReleaseCallback(std::bind(&WeaponActionView::OnUpButton, this));
	GetChild<Button>("downButton")->SetMouseReleaseCallback(std::bind(&WeaponActionView::OnDownButton, this));
	GetChild<Button>("ringButton")->SetMouseReleaseCallback(std::bind(&WeaponActionView::OnRingButton, this));

	mActionTable = GetChild<TableView>("contentTable");
	RefreshButtons();
}

void WeaponActionView::SetWeapon(Weapon* weapon)
{
	mWeapon = weapon;
	mSelectedActionIndex = -1;
	RefreshActions();
	RefreshButtons();

	ClearProperties();

	mBulletBehaviorView->SelectWeaponAction(nullptr);
}

void WeaponActionView::RefreshActions()
{
	mActionTable->ClearTableNodes();
	mSelectedCell = std::weak_ptr<SelectionCell>();

	if(mWeapon == nullptr)
		return;

	auto& actions = mWeapon->GetEditableActions();
	for(int i = 0; i < (int)actions.size(); i++)
	{
		auto cell = std::make_shared<SelectionCell>(mAnimationRegister);
		cell->Initialize("Action " + std::to_string(i), std::bind(&WeaponActionView::SelectAction, this, i, cell), "", DEFAULT_ANIMATION, "assets\\screens\\components\\mediumSelectionCell.json");
		mActionTable->AddTableNode(cell);

		if(i == mSelectedActionIndex)
		{
			cell->SetSelected(true);
			mSelectedCell = cell;
		}
	}
}

void WeaponActionView::RefreshButtons()
{
	GetChild<Button>("newButton")->SetDisabled(mWeapon == nullptr || mRingDialogOpen);
	GetChild<Button>("ringButton")->SetDisabled(mWeapon == nullptr || mRingDialogOpen);
	bool canCopyDelete = mWeapon == nullptr || mSelectedActionIndex < 0 || mRingDialogOpen;
	GetChild<Button>("copyButton")->SetDisabled(canCopyDelete);
	GetChild<Button>("deleteButton")->SetDisabled(canCopyDelete);
	GetChild<Button>("upButton")->SetDisabled(canCopyDelete);
	GetChild<Button>("downButton")->SetDisabled(canCopyDelete);
}

void WeaponActionView::RefreshProperties()
{
	ClearProperties();
	auto& action = mWeapon->GetEditableActions().at(mSelectedActionIndex);

	AddIntProperty("dmg", "Damage", action.mDamage, [&action](int damage){action.mDamage = damage; });
	AddVector2Property("centerPos", "Center Position (Pixels)", action.mCenterPosInPixels, [&action](const Vector2& centerPos){action.mCenterPosInPixels = centerPos; });
	AddVector2Property("initialVelocity", "Initial Velocity", action.mInitialVelocity, [&action](const Vector2& initialVelocity){action.mInitialVelocity = initialVelocity; });
	AddBoolProperty("rotates", "Bullet Sprite Rotates", action.mRotates, [&action](bool rotates){action.mRotates = rotates; });
	AddFloatProperty("delay", "Delay", action.mDelay, [&action](float delay){action.mDelay = delay; });
	AddStringProperty("bulletId", "Bullet ID", action.mBulletId, [&action](const std::string& bulletId){action.mBulletId = bulletId; });
}

void WeaponActionView::SelectAction(int actionIndex, std::weak_ptr<SelectionCell> cell)
{
	if(cell.lock() != nullptr)
	{
		if(mSelectedCell.lock() != nullptr)
			mSelectedCell.lock()->SetSelected(false);
		mSelectedCell = cell;
		mSelectedCell.lock()->SetSelected(true);
	}

	mSelectedActionIndex = actionIndex;

	if(mSelectedActionIndex >= 0)
		mBulletBehaviorView->SelectWeaponAction(&mWeapon->GetEditableActions().at(mSelectedActionIndex));
	else
		mBulletBehaviorView->SelectWeaponAction(nullptr);

	RefreshProperties();
	RefreshButtons();
}

void WeaponActionView::OnNewButton()
{
	if(mWeapon == nullptr)
		return;

	auto& actions = mWeapon->GetEditableActions();
	if(mSelectedActionIndex < 0)
	{
		actions.push_back(WeaponAction());
		mSelectedActionIndex = actions.size() - 1;
	}
	else
	{
		actions.insert(actions.begin() + mSelectedActionIndex, WeaponAction());
	}
	
	RefreshActions();
	SelectAction(mSelectedActionIndex);
}

void WeaponActionView::OnCopyButton()
{
	if(mWeapon == nullptr || mSelectedActionIndex < 0)
		return;

	auto& actions = mWeapon->GetEditableActions();
	actions.push_back(actions.at(mSelectedActionIndex));

	RefreshActions();
	SelectAction(mSelectedActionIndex);
}

void WeaponActionView::OnDeleteButton()
{
	if(mWeapon == nullptr || mSelectedActionIndex < 0)
		return;

	auto& actions = mWeapon->GetEditableActions();
	actions.erase(actions.begin() + mSelectedActionIndex);
	if(mSelectedActionIndex == actions.size())
		mSelectedActionIndex--;

	RefreshActions();
	if(mSelectedActionIndex >= 0)
		SelectAction(mSelectedActionIndex);
}

void WeaponActionView::OnUpButton()
{
	if(mWeapon == nullptr || mSelectedActionIndex <= 0)
		return;

	auto& actions = mWeapon->GetEditableActions();
	auto action = actions.at(mSelectedActionIndex);
	actions.erase(actions.begin() + mSelectedActionIndex);
	mSelectedActionIndex--;
	actions.insert(actions.begin() + mSelectedActionIndex, action);
	
	RefreshActions();
}

void WeaponActionView::OnDownButton()
{
	if(mWeapon == nullptr || mSelectedActionIndex < 0 || mSelectedActionIndex == mWeapon->GetEditableActions().size() - 1)
		return;

	auto& actions = mWeapon->GetEditableActions();
	auto action = actions.at(mSelectedActionIndex);
	actions.erase(actions.begin() + mSelectedActionIndex);
	mSelectedActionIndex++;
	actions.insert(actions.begin() + mSelectedActionIndex, action);

	RefreshActions();
}

void WeaponActionView::OnRingButton()
{
	if(mWeapon == nullptr)
		return;

	auto brd = std::make_shared<CreateBulletRingDialog>(mAnimationRegister, mInputSystem, mWeapon, std::bind(&WeaponActionView::OnRingDialogClosed, this));
	brd->Initialize();
	GetParent().lock()->GetParent().lock()->GetChild<Node>("newWeaponDialogAnchor")->AddChild(brd);
	mRingDialogOpen = true;
	RefreshButtons();
}

void WeaponActionView::OnRingDialogClosed()
{
	mRingDialogOpen = false;
	RefreshButtons();
	RefreshActions();
}