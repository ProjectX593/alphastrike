#pragma once

#include "Buttons.h"
#include "KeyMapping.h"
#include <map>

class InputSystem;
enum ButtonTypes;

class WindowsKeyboardMapping : IKeyMapping
{
public:
	WindowsKeyboardMapping(InputSystem& inputSystem);
	~WindowsKeyboardMapping();

	// IKeyMapping functions
	void ResetDefaultMappings();

	// Other functions
	void OnKeyDown(UINT key);
	void OnKeyUp(UINT key);
	void OnCharInput(wchar_t c);

private:
	std::map<ButtonType, UINT> mMappings;
	InputSystem& mInputSystem;

	ButtonType FindKey(UINT key);
};

