/*
ArmorView

A very simple view for editing global armor items.
*/

#include "AnimationRegister.h"
#include "ArmorView.h"
#include "EditorGlobals.h"
#include "Item.h"
#include "NewItemDialog.h"
#include "PlayerShip.h"
#include "TableView.h"
#include "SelectionCell.h"

using std::map;
using std::shared_ptr;
using std::string;

using namespace std::placeholders;

ArmorView::ArmorView(AnimationRegister& animationReigster, InputSystem& inputSystem, std::shared_ptr<PlayerShip> playerShip, map<string, Armor>& armors):
	PropertyEditorView(animationReigster, inputSystem),
	mArmors(armors),
	mPlayerShip(playerShip)
{
}

ArmorView::~ArmorView()
{
}

void ArmorView::Initialize()
{
	LoadFromJson("assets\\screens\\components\\armorView.json", PIXELS_IN_UNIT);

	GetChild<Button>("newButton")->SetMouseReleaseCallback(std::bind(&ArmorView::OnNewButton, this));
	GetChild<Button>("copyButton")->SetMouseReleaseCallback(std::bind(&ArmorView::OnCopyButton, this));
	GetChild<Button>("deleteButton")->SetMouseReleaseCallback(std::bind(&ArmorView::OnDeleteButton, this));
	GetChild<Button>("equipButton")->SetMouseReleaseCallback(std::bind(&ArmorView::OnEquipButton, this));

	mArmorTable = GetChild<TableView>("armorTable");

	SetPropertyTable(GetChild<TableView>("detailsTable"));
	RefreshArmors();
}

void ArmorView::RefreshArmors()
{
	mArmorTable->ClearTableNodes();
	RefreshButtons();
	mSelectedCell = shared_ptr<SelectionCell>();

	for(auto& armor : mArmors)
		AddArmorCell(armor.first);
}

void ArmorView::AddArmorCell(const string& armorId)
{
	std::shared_ptr<SelectionCell> cell = std::make_shared<SelectionCell>(mAnimationRegister);
	cell->Initialize(armorId, std::bind(&ArmorView::SelectArmor, this, armorId, cell));
	mArmorTable->AddTableNode(cell);

	if(armorId == mSelectedArmorId)
	{
		mSelectedCell = cell;
		cell->SetSelected(true);
	}
}

void ArmorView::SelectArmor(const string& armorId, std::weak_ptr<SelectionCell> cell)
{
	if(cell.lock() != nullptr)
	{
		if(mSelectedCell.lock() != nullptr)
			mSelectedCell.lock()->SetSelected(false);
		mSelectedCell = cell;
		mSelectedCell.lock()->SetSelected(true);
	}

	mSelectedArmorId = armorId;
	RefreshButtons();
	RefreshProperties();
}

void ArmorView::RefreshProperties()
{
	ClearProperties();

	Armor* armor = GetSelectedArmor();
	if(armor == nullptr)
		return;

	AddStringProperty("id", "ID", mSelectedArmorId, [this](const std::string& id){
		Armor tmp = *GetSelectedArmor();
		mArmors.erase(mArmors.find(mSelectedArmorId));
		mSelectedArmorId = id;
		mArmors[mSelectedArmorId] = tmp;
		RefreshArmors();
	});
	AddIntProperty("hp", "HP", armor->mHP, [armor](int hp){armor->mHP = hp; });
	AddFloatProperty("speed", "Speed Multiplier", armor->mSpeedFactor, [armor](float speedFactor){armor->mSpeedFactor = speedFactor; });
	AddIntProperty("recVal", "Recycle Value", armor->mRecycleValue, [armor](int recycleValue){armor->mRecycleValue = recycleValue; });
	AddStringProperty("name", "Name", armor->mName, [armor](const std::string& name){armor->mName = name; });
	AddStringProperty("desc", "Description", armor->mDescription, [armor](const std::string& description){armor->mDescription = description; });
	AddStringProperty("thumb", "Thumbnail Img", armor->mThumbnail, [armor](const std::string& thumbnail){armor->mThumbnail = thumbnail; });
}

void ArmorView::OnEquipButton()
{
	mPlayerShip->SetArmor(std::make_shared<Armor>(*GetSelectedArmor()));
}

Armor* ArmorView::GetSelectedArmor()
{
	if(mArmors.find(mSelectedArmorId) == mArmors.end())
		return nullptr;
	return &mArmors.at(mSelectedArmorId);
}

void ArmorView::RefreshButtons()
{
	bool disabled = mArmors.find(mSelectedArmorId) == mArmors.end();
	GetChild<Button>("copyButton")->SetDisabled(disabled);
	GetChild<Button>("deleteButton")->SetDisabled(disabled);
}

void ArmorView::ShowNewArmorDialog(Armor* weapon, const std::string& title)
{
	auto nid = std::make_shared<NewItemDialog>(mAnimationRegister, std::bind(&ArmorView::OnNewArmorDialogDone, this, _1, weapon));
	nid->Initialize(title);
	GetChild<Node>("newArmorDialogAnchor")->AddChild(nid);
}

bool ArmorView::OnNewArmorDialogDone(const std::string& armorId, Armor* armorToCopy)
{
	if(mArmors.find(armorId) != mArmors.end())
		return false;

	if(armorToCopy == nullptr)
		mArmors[armorId] = Armor(-1, "", "", "", 0, 0, 1.0);
	else
		mArmors[armorId] = *armorToCopy;

	RefreshArmors();
	SelectArmor(mSelectedArmorId);

	return true;
}

void ArmorView::OnNewButton()
{
	ShowNewArmorDialog(nullptr, "NEW ARMOR");
}

void ArmorView::OnCopyButton()
{
	ShowNewArmorDialog(GetSelectedArmor(), "NEW ARMOR");
}

void ArmorView::OnDeleteButton()
{
	if(GetSelectedArmor() == nullptr)
		return;

	mArmors.erase(mSelectedArmorId);
	mSelectedArmorId = "";

	RefreshArmors();
}