#pragma once

#include "View.h"

class Editor;
class EnemyPool;
class NewItemDialog;

class EditorView : public View
{
public:
	EditorView(EnemyPool& enemyPool, AnimationRegister& animationRegister, InputSystem& inputSystem, Editor& editor);
	~EditorView();

	void Initialize();

private:
	Editor& mEditor;
	EnemyPool& mEnemyPool;
	int mSelectedSpawnGroupIndex;
	std::string mSelectedEnemyId;
	int mSelectedTurretGroupIndex;
	InputSystem& mInputSystem;

	std::string mFullEnemyPoolFilename;
	bool mFileLoaded;
	bool mUIVisible;
	bool mGlobalUIVisible;

	std::shared_ptr<Button> mSpawnButton;
	std::shared_ptr<Button> mEnemyButton;
	std::shared_ptr<Button> mBehaviorButton;
	std::shared_ptr<Button> mTurretButton;
	std::shared_ptr<Button> mWeaponButton;
	std::shared_ptr<Button> mColliderButton;
	std::shared_ptr<NewItemDialog> mNewFileDialog;
	std::shared_ptr<View> mSelectionView;
	std::shared_ptr<TableView> mSelectionTable;
	std::shared_ptr<View> mTypeView;

	std::shared_ptr<Button> mPrimaryWeaponButton;
	std::shared_ptr<Button> mSecondaryWeaponButton;
	std::shared_ptr<Button> mArmorButton;
	std::shared_ptr<Button> mUpgradeButton;
	std::shared_ptr<View> mGlobalTypeView;

	void OnNewTypeSelected();
	void SetPoolButtonsDisabled(bool disabled);
	void OnSpawnButton();
	void OnSpawnGroupSelected(int spawnGroupIndex);
	void OnEnemyButton();
	void OnEnemySelected(const std::string& enemyId);
	void OnTurretGroupSelected(int turretGroupIndex);
	bool TurretViewOpen();
	void RefreshButtons();
	void OnBehaviorButton();
	void OnTurretButton();
	bool TryOpenTurretSubview(std::function<std::shared_ptr<View>(bool)> openCallback);
	void OnWeaponButton();
	void OnColliderButton();

	void OnNewGlobalTypeSelected();
	void InitWeaponView(std::map<std::string, Weapon>& weaponMap, ItemType weaponType);
	void OnPrimaryWeaponsButton();
	void OnSecondaryWeaponsButton();
	void OnArmorButton();
	void OnUpgradeButton();

	void OnNewFileButton();
	bool OnNewFileDialogDone(std::string filename);
	void OnNewFileDialogClosed();
	void OnSaveButton();
	void OnLoadButton();
	void LoadFromFile(const std::string& filename);
	void OnGlobalPoolToggle();
	void UpdateGlobalPoolNodes();
	void OnQuitButton();
	void ToggleUIVisible();

	std::shared_ptr<View> CreateSmallCell();
	void ClearEnemyPool();

	void OnRespawnButton();
};

