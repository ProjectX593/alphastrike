#pragma once

#include "PropertyEditorView.h"

class EnemyConfig;
class SelectionCell;
class TurretPositionView;

class TurretView : public PropertyEditorView
{
public:
	TurretView(AnimationRegister& animationRegister, InputSystem& inputSystem, EnemyConfig& enemyConfig, std::function<void(int)> turretGroupSelectedCallback);
	~TurretView();

	void Initialize();

private:
	EnemyConfig& mEnemyConfig;
	std::shared_ptr<TableView> mTurretGroupTable;
	std::shared_ptr<TurretPositionView> mTurretPositionView;
	int mSelectedTurretGroupIndex;
	std::weak_ptr<SelectionCell> mSelectedCell;
	std::function<void(int)> mTurretGroupSelectedCallback;

	void RefreshTurretGroups();
	void RefreshButtons();
	void RefreshProperties();
	void SelectTurretGroup(int turretGroupIndex, std::weak_ptr<SelectionCell> cell = std::weak_ptr<SelectionCell>());
	void OnNewButton();
	void OnCopyButton();
	void OnDeleteButton();
};

