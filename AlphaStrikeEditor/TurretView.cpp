/*
TurretView

The view for editing the turrets on an enemy ship.
This has a lot of duplicated code from WeaponView... I should consider a common base class at some point, but for now I just need this editor to be done.
*/

#include "BeamWeapon.h"
#include "EditorGlobals.h"
#include "EnemyConfig.h"
#include "SelectionCell.h"
#include "TableView.h"
#include "TurretPositionView.h"
#include "TurretView.h"

TurretView::TurretView(AnimationRegister& animationRegister, InputSystem& inputSystem, EnemyConfig& enemyConfig, std::function<void(int)> turretGroupSelectedCallback):
	PropertyEditorView(animationRegister, inputSystem),
	mEnemyConfig(enemyConfig),
	mSelectedTurretGroupIndex(-1),
	mTurretGroupSelectedCallback(turretGroupSelectedCallback)
{
}

TurretView::~TurretView()
{
}

void TurretView::Initialize()
{
	LoadFromJson("assets\\screens\\components\\turretView.json", PIXELS_IN_UNIT);

	mTurretGroupTable = GetChild<TableView>("turretGroupTable");
	SetPropertyTable(GetChild<TableView>("detailsTable"));

	GetChild<Button>("newButton")->SetMouseReleaseCallback(std::bind(&TurretView::OnNewButton, this));
	GetChild<Button>("copyButton")->SetMouseReleaseCallback(std::bind(&TurretView::OnCopyButton, this));
	GetChild<Button>("deleteButton")->SetMouseReleaseCallback(std::bind(&TurretView::OnDeleteButton, this));

	mTurretPositionView = std::make_shared<TurretPositionView>(mAnimationRegister, mInputSystem);
	mTurretPositionView->Initialize();
	GetChild<Node>("turretPositionView")->AddChild(mTurretPositionView);

	RefreshTurretGroups();
}

void TurretView::RefreshTurretGroups()
{
	mTurretGroupTable->ClearTableNodes();
	mSelectedCell = std::weak_ptr<SelectionCell>();

	for(int i = 0; i < (int)mEnemyConfig.mTurretGroupConfigs.size(); i++)
	{
		std::shared_ptr<SelectionCell> cell = std::make_shared<SelectionCell>(mAnimationRegister);
		cell->Initialize(mEnemyConfig.mTurretGroupConfigs.at(i).mAnimationsName, std::bind(&TurretView::SelectTurretGroup, this, i, cell));
		mTurretGroupTable->AddTableNode(cell);

		if(i == mSelectedTurretGroupIndex)
		{
			mSelectedCell = cell;
			cell->SetSelected(true);
		}
	}

	RefreshButtons();
}

void TurretView::SelectTurretGroup(int turretGroupIndex, std::weak_ptr<SelectionCell> cell)
{
	if(turretGroupIndex == mSelectedTurretGroupIndex)
		return;

	if(mSelectedCell.lock() != nullptr)
		mSelectedCell.lock()->SetSelected(false);
	mSelectedCell = cell;
	mSelectedCell.lock()->SetSelected(true);

	mSelectedTurretGroupIndex = turretGroupIndex;
	RefreshButtons();
	RefreshProperties();

	mTurretPositionView->SetTurretPositions(mSelectedTurretGroupIndex < 0 ? nullptr : &mEnemyConfig.mTurretGroupConfigs.at(mSelectedTurretGroupIndex).mPositionsInPixels);
	mTurretGroupSelectedCallback(mSelectedTurretGroupIndex);
}

void TurretView::RefreshButtons()
{
	GetChild<Button>("copyButton")->SetDisabled(mSelectedTurretGroupIndex < 0);
	GetChild<Button>("deleteButton")->SetDisabled(mSelectedTurretGroupIndex < 0);
}

void TurretView::RefreshProperties()
{
	ClearProperties();
	if(mSelectedTurretGroupIndex < 0)
		return;

	TurretGroupConfig& config = mEnemyConfig.mTurretGroupConfigs.at(mSelectedTurretGroupIndex);
	AddIntProperty("health", "Max Health", config.mMaxHealth, [&config](int maxHealth){config.mMaxHealth = maxHealth; });
	AddStringProperty("animName", "Animations Name", config.mAnimationsName, [&config, this](const std::string& animationsName){
		config.mAnimationsName = animationsName; 
		RefreshTurretGroups();
	});
	AddStringProperty("idleAnim", "Idle Animation", config.mIdleAnimation, [&config](const std::string& idleAnimation){config.mIdleAnimation = idleAnimation; });
	AddStringProperty("chargeAnim", "Charge Animation", config.mChargeAnimation, [&config](const std::string& chargeAnimation){config.mChargeAnimation = chargeAnimation; });
	AddStringProperty("fireAnim", "Fire Animation", config.mFireAnimation, [&config](const std::string& fireAnimation){config.mFireAnimation = fireAnimation; });
	AddStringProperty("dischargeAnim", "Discharge Animation", config.mDischargeAnimation, [&config](const std::string& dischargeAnimation){config.mDischargeAnimation = dischargeAnimation; });
	AddBoolProperty("faces", "Faces Player", config.mFacesPlayer, [&config](bool facesPlayer){config.mFacesPlayer = facesPlayer; });
	AddBoolProperty("inv", "Invulnerable", config.mInvulnerable, [&config](bool invlunerable){config.mInvulnerable = invlunerable; });
	AddBoolProperty("vis", "Visible", config.mVisible, [&config](bool visible){config.mVisible = visible; });
	AddBoolProperty("hasBeam", "Has Beam Weapon", std::dynamic_pointer_cast<BeamWeapon>(config.mWeapon) != nullptr, [&config](bool hasBeamWeapon){
		bool isBeam = std::dynamic_pointer_cast<BeamWeapon>(config.mWeapon) != nullptr;
		if(hasBeamWeapon && !isBeam)
			config.mWeapon = std::make_shared<BeamWeapon>();
		else if(!hasBeamWeapon && isBeam)
			config.mWeapon = std::make_shared<Weapon>();
	});
}

void TurretView::OnNewButton()
{
	auto& configs = mEnemyConfig.mTurretGroupConfigs;
	if(mSelectedTurretGroupIndex >= 0)
		configs.insert(configs.begin() + mSelectedTurretGroupIndex, TurretGroupConfig());
	else
		configs.push_back(TurretGroupConfig());
	RefreshTurretGroups();
}

void TurretView::OnCopyButton()
{
	if(mSelectedTurretGroupIndex < 0)
		return;

	auto& configs = mEnemyConfig.mTurretGroupConfigs;
	configs.insert(configs.begin() + mSelectedTurretGroupIndex, configs.at(mSelectedTurretGroupIndex));
	RefreshTurretGroups();
}

void TurretView::OnDeleteButton()
{
	if(mSelectedTurretGroupIndex < 0)
		return;

	auto& configs = mEnemyConfig.mTurretGroupConfigs;
	configs.erase(configs.begin() + mSelectedTurretGroupIndex);
	RefreshTurretGroups();
}