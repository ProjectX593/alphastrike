/*
BehaviorView

This view is displayed when the user wnats to edit behaviors. It takes the current enemy config, as well as the selection
table so the EditorView doesn't need to contain the logic to display all of the different behavior cells.
*/

#include <algorithm>
#include "AnimationRegister.h"
#include "BehaviorSetView.h"
#include "BehaviorView.h"
#include "EditorGlobals.h"
#include "EnemyBehavior.h"
#include "EnemyConfig.h"
#include "NewBehaviorDialog.h"
#include "SelectionCell.h"
#include "TableView.h"
#include "TextField.h"
#include "TurretVolleyView.h"

#include "ActivateTurretBehavior.h"
#include "AnimateBehavior.h"
#include "ArriveBehavior.h"
#include "BulletCircleBehavior.h"
#include "DriftBehavior.h"
#include "ExplodeBehavior.h"
#include "FollowBehavior.h"
#include "MoveBehavior.h"
#include "ProximityBehavior.h"
#include "ReflectBehavior.h"
#include "SetChangeBehavior.h"
#include "SequenceBehavior.h"
#include "ShootBehavior.h"
#include "SpawnEnemyBehavior.h"
#include "StrafeBehavior.h"
#include "SuicideBehavior.h"
#include "TurretVisibilityBehavior.h"
#include "TurretVolleyBehavior.h"
#include "WanderBehavior.h"

using std::string;
using namespace std::placeholders;
using std::shared_ptr;
using std::vector;

BehaviorView::BehaviorView(AnimationRegister& animationRegister, InputSystem& inputSystem, EnemyConfig& enemyConfig):
	PropertyEditorView(animationRegister, inputSystem),
	mEnemyConfig(enemyConfig)
{
}

BehaviorView::~BehaviorView()
{
}

void BehaviorView::Initialize()
{
	LoadFromJson("assets\\screens\\components\\behaviorView.json", PIXELS_IN_UNIT);

	// Set up behavior set view
	mBehaviorSetView = std::make_shared<BehaviorSetView>(mEnemyConfig, std::bind(&BehaviorView::RefreshBehaviorButtons, this, _1), std::bind(&BehaviorView::RefreshBehaviors, this, true), mAnimationRegister);
	mBehaviorSetView->Initialize();
	GetChild<Node>("behaviorSetViewAnchor")->AddChild(mBehaviorSetView);

	mSelectionTable = GetChild<TableView>("behaviorTable");
	SetPropertyTable(GetChild<TableView>("behaviorDetailsTable"));

	GetChild<Button>("newButton")->SetMouseReleaseCallback(std::bind(&BehaviorView::OnNewBehaviorButton, this));
	GetChild<Button>("copyButton")->SetMouseReleaseCallback(std::bind(&BehaviorView::OnCopyBehaviorButton, this));
	GetChild<Button>("deleteButton")->SetMouseReleaseCallback(std::bind(&BehaviorView::OnDeleteBehaviorButton, this));
	GetChild<Button>("upButton")->SetMouseReleaseCallback(std::bind(&BehaviorView::OnUpButton, this));
	GetChild<Button>("downButton")->SetMouseReleaseCallback(std::bind(&BehaviorView::OnDownButton, this));

	RefreshBehaviors(false);
}

void BehaviorView::AddSequenceCells(std::shared_ptr<SequenceBehavior> sequence, vector<int>& selectionIndices)
{
	auto& behaviors = sequence->GetEditableBehaviors();

	if(behaviors.size() == 0)
	{
		selectionIndices.push_back(-1);

		std::shared_ptr<SelectionCell> emptyCell = std::make_shared<SelectionCell>(mAnimationRegister);
		emptyCell->Initialize("No Groups", std::bind(&BehaviorView::SelectCell, this, selectionIndices, emptyCell), "sequenceCell1");
		mSelectionTable->AddTableNode(emptyCell);

		if(selectionIndices == mSelectionIndices)
			emptyCell->SetSelected(true);

		selectionIndices.pop_back();
	}

	for(unsigned int i = 0; i < behaviors.size(); i++)
	{
		selectionIndices.push_back(i);

		std::shared_ptr<SelectionCell> beginCell = std::make_shared<SelectionCell>(mAnimationRegister);
		std::function<void()> selectCallback = std::bind(&BehaviorView::SelectCell, this, selectionIndices, beginCell);
		std::string name = "Group " + std::to_string(i);
		beginCell->Initialize(name, selectCallback, "sequenceCell0");
		mSelectionTable->AddTableNode(beginCell);

		if(selectionIndices == mSelectionIndices)
			beginCell->SetSelected(true);

		AddBehaviorCells(behaviors.at(i), selectionIndices);

		std::shared_ptr<SelectionCell> endCell = std::make_shared<SelectionCell>(mAnimationRegister);
		endCell->Initialize(name + " End", selectCallback, "sequenceCell0");
		mSelectionTable->AddTableNode(endCell);

		selectionIndices.pop_back();
	}
}

// selectionLevel 0 is the behaviors in the config, all others are nested sequence behaviors
void BehaviorView::AddBehaviorCells(const std::vector<std::shared_ptr<EnemyBehavior>>& behaviors, vector<int>& selectionIndices)
{
	if(behaviors.size() == 0)
	{
		selectionIndices.push_back(-1);

		std::shared_ptr<SelectionCell> emptyCell = std::make_shared<SelectionCell>(mAnimationRegister);
		emptyCell->Initialize("No Behaviors", std::bind(&BehaviorView::SelectCell, this, selectionIndices, emptyCell), "sequenceCell2");
		mSelectionTable->AddTableNode(emptyCell);

		if(selectionIndices == mSelectionIndices)
			emptyCell->SetSelected(true);

		selectionIndices.pop_back();
	}

	for(unsigned int i = 0; i < behaviors.size(); i++)
	{
		selectionIndices.push_back(i);

		auto& behavior = behaviors.at(i);
		auto& sequence = std::dynamic_pointer_cast<SequenceBehavior>(behavior);
		std::shared_ptr<SelectionCell> cell = std::make_shared<SelectionCell>(mAnimationRegister);
		std::function<void()> selectCallback = std::bind(&BehaviorView::SelectCell, this, selectionIndices, cell);
		cell->Initialize(behavior->GetDisplayString(), selectCallback);
		mSelectionTable->AddTableNode(cell);

		if(sequence != nullptr)
		{
			cell->SetBackgroundAnimation("sequenceCell3");

			AddSequenceCells(sequence, selectionIndices);

			std::shared_ptr<SelectionCell> endCell = std::make_shared<SelectionCell>(mAnimationRegister);
			endCell->Initialize("End Sequence", selectCallback, "sequenceCell3");
			mSelectionTable->AddTableNode(endCell);
		}

		if(selectionIndices == mSelectionIndices)
			cell->SetSelected(true);

		selectionIndices.pop_back();
	}
}

// Takes a bool indicating the caller wants to open a dialog, or false indicating they are closing or not showing one
// Returns true in any case if this is safe to do, false if not
bool BehaviorView::RefreshBehaviorButtons(bool showDialog)
{
	if(mNewBehaviorDialog != nullptr)
		return false;

	string bsid = mBehaviorSetView->GetSelectedBehaviorSetId();
	GetChild<Button>("newButton")->SetDisabled(bsid == "" || showDialog);
	bool cannotCopyDelete = bsid == "" || mSelectionIndices.size() == 0 || showDialog || mSelectionIndices.at(mSelectionIndices.size() - 1) < 0;
	GetChild<Button>("copyButton")->SetDisabled(cannotCopyDelete);
	GetChild<Button>("deleteButton")->SetDisabled(cannotCopyDelete);
	GetChild<Button>("upButton")->SetDisabled(cannotCopyDelete);
	GetChild<Button>("downButton")->SetDisabled(cannotCopyDelete);

	return true;
}

void BehaviorView::RefreshBehaviors(bool clearBehaviorSelection)
{
	if(clearBehaviorSelection)
		mSelectionIndices.clear();

	RefreshBehaviorButtons(false);
	mSelectionTable->ClearTableNodes();
	if(mBehaviorSetView->GetSelectedBehaviorSetId() == "")
		return;

	auto& selectedBehaviorSet = mBehaviorSetView->GetSelectedBehaviorSet();
	AddBehaviorCells(selectedBehaviorSet, vector<int>());
	RefreshBehaviorProperties();
}

void BehaviorView::OnNewBehaviorButton()
{
	if(mBehaviorSetView->NewBehaviorSetDialogVisible())
		return;

	if(SequenceGroupSelected())
	{
		auto& behaviors = std::dynamic_pointer_cast<SequenceBehavior>(GetSelectedBehavior())->GetEditableBehaviors();
		int ind = mSelectionIndices.at(mSelectionIndices.size() - 1);
		if(ind < 0)
		{
			mSelectionIndices.at(mSelectionIndices.size() - 1) = 0;
			behaviors.push_back({});
		}
		else
		{
			behaviors.insert(behaviors.begin() + ind, std::vector<std::shared_ptr<EnemyBehavior>>());
		}
		
		RefreshBehaviors(false);
	}
	else
	{
		mNewBehaviorDialog = std::make_shared<NewBehaviorDialog>(mAnimationRegister, std::bind(&BehaviorView::OnNewBehaviorDialogDone, this, _1), std::bind(&BehaviorView::OnNewBehaviorDialogClosed, this));
		mNewBehaviorDialog->Initialize();
		GetChild<Node>("newBehaviorDialogAnchor")->AddChild(mNewBehaviorDialog);
		mBehaviorSetView->SetButtonsDisabled(true);
	}
}

SequenceSelection BehaviorView::GetSelectedSequence()
{
	SequenceSelection selection;

	if(mSelectionIndices.size() < 3)
		return selection;

	selection.mSequence = std::dynamic_pointer_cast<SequenceBehavior>(mBehaviorSetView->GetSelectedBehaviorSet().at(mSelectionIndices.at(0)));
	selection.mGroupIndex = mSelectionIndices.at(1);
	selection.mBehaviorIndex = mSelectionIndices.at(2);

	// get the index of the sequence we are inserting into, NOT the currently selected behavior
	for(unsigned int i = 1; i < mSelectionIndices.size() - 3; i += 2)	// -1 to ignore group selections, -2 to advance only if there is another sequence ahead we are inserting into
	{
		selection.mSequence = std::dynamic_pointer_cast<SequenceBehavior>(selection.mSequence->GetEditableBehaviors().at(mSelectionIndices.at(i)).at(mSelectionIndices.at(i + 1))); // advance to the next nested sequence
		selection.mGroupIndex = mSelectionIndices.at(i + 2);		// the index of this sequence's selected group
		selection.mBehaviorIndex = mSelectionIndices.at(i + 3);	// the index we are inserting into in that group
	}

	return selection;
}

void BehaviorView::InsertBehavior(shared_ptr<EnemyBehavior> behavior)
{
	auto& bs = mBehaviorSetView->GetSelectedBehaviorSet();
	if(mSelectionIndices.size() == 0 || mSelectionIndices.at(0) < 0)
	{
		bs.push_back(behavior);
	}
	else if(mSelectionIndices.size() <= 2)
	{
		bs.insert(bs.begin() + mSelectionIndices.at(0), behavior);
	}
	else
	{
		SequenceSelection selection = GetSelectedSequence();

		auto& behaviors = selection.mSequence->GetEditableBehaviors().at(selection.mGroupIndex);
		if(selection.mBehaviorIndex < 0)
		{
			behaviors.push_back(behavior);
			mSelectionIndices.at(mSelectionIndices.size() - 1) = 0;
		}
		else
		{
			behaviors.insert(behaviors.begin() + selection.mBehaviorIndex, behavior);
		}
	}
}

void BehaviorView::OnNewBehaviorDialogDone(std::shared_ptr<EnemyBehavior> behavior)
{
	InsertBehavior(behavior);

	mNewBehaviorDialog = nullptr;
	RefreshBehaviors(false);
	mBehaviorSetView->SetButtonsDisabled(false);
}

void BehaviorView::OnNewBehaviorDialogClosed()
{
	mNewBehaviorDialog = nullptr;
	mBehaviorSetView->SetButtonsDisabled(false);
}

void BehaviorView::OnCopyBehaviorButton()
{
	if(SequenceGroupSelected())
	{
		int ind = mSelectionIndices.at(mSelectionIndices.size() - 1);
		if(ind < 0)
			return;
		auto sequence = std::dynamic_pointer_cast<SequenceBehavior>(GetSelectedBehavior());
		auto& behaviors = sequence->GetEditableBehaviors();
		
		auto& group = behaviors.at(ind);
		std::vector<std::shared_ptr<EnemyBehavior>> newBehaviors;
		for(auto& behavior : group)
			newBehaviors.push_back(behavior->Copy());
		behaviors.insert(behaviors.begin() + ind, newBehaviors);
	}
	else
	{
		if(GetSelectedBehavior() == nullptr)
			return;
		InsertBehavior(GetSelectedBehavior()->Copy());
	}

	RefreshBehaviors(false);
}

void BehaviorView::OnDeleteBehaviorButton()
{
	//TODO: Take into account selection level
	if(mSelectionIndices.size() == 0)
		return;

	if(SequenceGroupSelected())
	{
		int ind = mSelectionIndices.at(mSelectionIndices.size() - 1);
		if(ind < 0)
			return;

		auto& sequence = std::dynamic_pointer_cast<SequenceBehavior>(GetSelectedBehavior());
		sequence->GetEditableBehaviors().erase(sequence->GetEditableBehaviors().begin() + ind);
	}
	else if(mSelectionIndices.size() <= 2)
	{
		auto& bs = mBehaviorSetView->GetSelectedBehaviorSet();
		bs.erase(bs.begin() + mSelectionIndices.at(0));
	}
	else
	{
		SequenceSelection selection = GetSelectedSequence();

		if(selection.mBehaviorIndex < 0)
			return;

		auto& behaviors = selection.mSequence->GetEditableBehaviors().at(selection.mGroupIndex);
		behaviors.erase(behaviors.begin() + selection.mBehaviorIndex);
	}

	RefreshBehaviors(true);
}

void BehaviorView::MoveSelectedItem(int offset)
{
	int oldIndex = mSelectionIndices.at(mSelectionIndices.size() - 1);
	if(oldIndex < 0)
		return;

	if(offset < 0)
	{
		offset = -std::min(oldIndex, std::abs(offset));
	}
	else if(offset > 0)
	{
		if(SequenceGroupSelected())
		{
			auto& sequence = std::dynamic_pointer_cast<SequenceBehavior>(GetSelectedBehavior());
			offset = std::min(offset, (int)sequence->GetBehaviors().size() - 1 - oldIndex);
		}
		else if(mSelectionIndices.size() == 1)
		{
			offset = std::min(offset, (int)mBehaviorSetView->GetSelectedBehaviorSet().size() - 1 - oldIndex);
		}
		else
		{
			SequenceSelection selection = GetSelectedSequence();
			offset = std::min(offset, (int)selection.mSequence->GetBehaviors().at(selection.mGroupIndex).size() - 1 - oldIndex);
		}
	}

	if(offset == 0)
		return;


	if(SequenceGroupSelected())
	{
		shared_ptr<SequenceBehavior> sequence = std::dynamic_pointer_cast<SequenceBehavior>(GetSelectedBehavior());
		auto& behaviorGroups = sequence->GetEditableBehaviors();
		auto behaviorGroup = behaviorGroups.at(oldIndex);
		behaviorGroups.erase(behaviorGroups.begin() + oldIndex);
		behaviorGroups.insert(behaviorGroups.begin() + oldIndex + offset, behaviorGroup);
	}
	else if(mSelectionIndices.size() == 1)	// a behavior in the base behavior set is selected
	{
		auto& behaviorSet = mBehaviorSetView->GetSelectedBehaviorSet();
		auto behavior = behaviorSet.at(oldIndex);
		behaviorSet.erase(behaviorSet.begin() + oldIndex);
		behaviorSet.insert(behaviorSet.begin() + oldIndex + offset, behavior);
	}
	else // a behavior in a sequence group is selected
	{
		SequenceSelection sequence = GetSelectedSequence();
		auto& behaviorGroup = sequence.mSequence->GetEditableBehaviors().at(sequence.mGroupIndex);
		auto behavior = behaviorGroup.at(oldIndex);
		behaviorGroup.erase(behaviorGroup.begin() + oldIndex);
		behaviorGroup.insert(behaviorGroup.begin() + oldIndex + offset, behavior);
	}

	mSelectionIndices.at(mSelectionIndices.size() - 1) += offset;
}

void BehaviorView::OnUpButton()
{
	if(mSelectionIndices.size() == 0 || mSelectionIndices.at(mSelectionIndices.size() - 1) < 0)
		return;

	MoveSelectedItem(-1);
	RefreshBehaviors(false);
}

void BehaviorView::OnDownButton()
{
	if(mSelectionIndices.size() == 0 || mSelectionIndices.at(mSelectionIndices.size() - 1) < 0)
		return;

	MoveSelectedItem(1);
	RefreshBehaviors(false);
}

void BehaviorView::SelectCell(const std::vector<int>& selectionIndices, std::weak_ptr<SelectionCell> cell)
{
	mSelectionIndices = selectionIndices;

	if(mSelectedBehaviorCell.lock() != nullptr)
		mSelectedBehaviorCell.lock()->SetSelected(false);
	cell.lock()->SetSelected(true);
	mSelectedBehaviorCell = cell;
	RefreshBehaviorProperties();
	RefreshBehaviorButtons(false);
}

bool BehaviorView::SequenceGroupSelected()
{
	return mSelectionIndices.size() > 0 && mSelectionIndices.size() % 2 == 0;
}

std::shared_ptr<EnemyBehavior> BehaviorView::GetSelectedBehavior()
{
	if(mSelectionIndices.size() == 0 || mSelectionIndices.at(0) < 0)
		return nullptr;

	std::shared_ptr<EnemyBehavior> behavior = mBehaviorSetView->GetSelectedBehaviorSet().at(mSelectionIndices.at(0));
	for(unsigned int i = 1; i < mSelectionIndices.size() - 1; i += 2)	// we do size-1 to prevent going a level deeper when we have a sequence group selected
	{
		if(mSelectionIndices.at(i + 1) < 0)
			return nullptr;	// we probably found -1, indicating an empty cell was selected.

		std::shared_ptr<SequenceBehavior> sequence = std::dynamic_pointer_cast<SequenceBehavior>(behavior);
		behavior = sequence->GetBehaviors().at(mSelectionIndices.at(i)).at(mSelectionIndices.at(i + 1));
	}

	return behavior;
}

void BehaviorView::RefreshBehaviorProperties()
{
	ClearProperties();
	if(mPropertyView != nullptr)
	{
		mPropertyView->RemoveFromParentAndCleanup();
		mPropertyView = nullptr;
	}

	auto& sb = GetSelectedBehavior();

	// there is probably a better way of doing this...
	if(sb == nullptr)
		return;
	else if(std::dynamic_pointer_cast<ActivateTurretBehavior>(sb) != nullptr)
		AddActivateTurretProperties(std::dynamic_pointer_cast<ActivateTurretBehavior>(sb));
	else if(std::dynamic_pointer_cast<AnimateBehavior>(sb) != nullptr)
		AddAnimateProperties(std::dynamic_pointer_cast<AnimateBehavior>(sb));
	else if(std::dynamic_pointer_cast<ArriveBehavior>(sb) != nullptr)
		AddArriveProperties(std::dynamic_pointer_cast<ArriveBehavior>(sb));
	else if(std::dynamic_pointer_cast<BulletCircleBehavior>(sb) != nullptr)
		AddBulletCircleProperties(std::dynamic_pointer_cast<BulletCircleBehavior>(sb));
	else if(std::dynamic_pointer_cast<DriftBehavior>(sb) != nullptr)
		AddDriftProperties(std::dynamic_pointer_cast<DriftBehavior>(sb));
	else if(std::dynamic_pointer_cast<ExplodeBehavior>(sb) != nullptr)
		AddExplodeProperties(std::dynamic_pointer_cast<ExplodeBehavior>(sb));
	else if(std::dynamic_pointer_cast<FollowBehavior>(sb) != nullptr)
		AddFollowProperties(std::dynamic_pointer_cast<FollowBehavior>(sb));
	else if(std::dynamic_pointer_cast<MoveBehavior>(sb) != nullptr)
		AddMoveProperties(std::dynamic_pointer_cast<MoveBehavior>(sb));
	else if(std::dynamic_pointer_cast<ProximityBehavior>(sb) != nullptr)
		AddProximityProperties(std::dynamic_pointer_cast<ProximityBehavior>(sb));
	else if(std::dynamic_pointer_cast<ReflectBehavior>(sb) != nullptr)
		AddReflectProperties(std::dynamic_pointer_cast<ReflectBehavior>(sb));
	else if(std::dynamic_pointer_cast<SetChangeBehavior>(sb) != nullptr)
		AddSetChangeProperties(std::dynamic_pointer_cast<SetChangeBehavior>(sb));
	else if(std::dynamic_pointer_cast<SequenceBehavior>(sb) != nullptr)
		AddSequenceProperties(std::dynamic_pointer_cast<SequenceBehavior>(sb));
	else if(std::dynamic_pointer_cast<ShootBehavior>(sb) != nullptr)
		AddShootProperties(std::dynamic_pointer_cast<ShootBehavior>(sb));
	else if(std::dynamic_pointer_cast<SpawnEnemyBehavior>(sb) != nullptr)
		AddSpawnEnemyProperties(std::dynamic_pointer_cast<SpawnEnemyBehavior>(sb));
	else if(std::dynamic_pointer_cast<StrafeBehavior>(sb) != nullptr)
		AddStrafeProperties(std::dynamic_pointer_cast<StrafeBehavior>(sb));
	else if(std::dynamic_pointer_cast<SuicideBehavior>(sb) != nullptr)
		AddSuicideProperties(std::dynamic_pointer_cast<SuicideBehavior>(sb));
	else if(std::dynamic_pointer_cast<TurretVisibilityBehavior>(sb) != nullptr)
		AddTurretVisibilityProperties(std::dynamic_pointer_cast<TurretVisibilityBehavior>(sb));
	else if(std::dynamic_pointer_cast<TurretVolleyBehavior>(sb) != nullptr)
		AddTurretVolleyProperties(std::dynamic_pointer_cast<TurretVolleyBehavior>(sb));
	else if(std::dynamic_pointer_cast<WanderBehavior>(sb) != nullptr)
		AddWanderProperties(std::dynamic_pointer_cast<WanderBehavior>(sb));
	else if(std::dynamic_pointer_cast<TimedBehavior>(sb) != nullptr)
		AddTimedBehaviorProperties(std::dynamic_pointer_cast<TimedBehavior>(sb));
}

void BehaviorView::AddActivateTurretProperties(std::shared_ptr<ActivateTurretBehavior> behavior)
{
	AddVectorIntProperty("turretsToShoot", "Turrets to Shoot", behavior->GetTurretsToActivate(), [behavior](const std::vector<int>& turretsToActivate){behavior->SetTurretsToActivate(turretsToActivate);});
	AddFloatProperty("duration", "Duration", behavior->GetDuration(), [behavior](float duration){behavior->SetDuration(duration);});
	AddBoolProperty("infinite", "Infinite", behavior->GetInfinite(), [behavior](bool infinite){behavior->SetInfinite(infinite);});
}

void BehaviorView::AddAnimateProperties(std::shared_ptr<AnimateBehavior> behavior)
{
	AddStringProperty("anim", "Animation Name", behavior->mAnimation, [behavior](const std::string& animation){behavior->mAnimation = animation; });
	AddStringProperty("finishAnim", "Finish Animation Name", behavior->mFinishAnimation, [behavior](const std::string& finishAnim){behavior->mFinishAnimation = finishAnim;});
	AddBoolProperty("block", "Block Until Finished", behavior->mBlockUntilComplete, [behavior](bool blockUntilComplete){behavior->mBlockUntilComplete = blockUntilComplete;});
}

void BehaviorView::AddArriveProperties(std::shared_ptr<ArriveBehavior> behavior)
{
	AddFloatProperty("travelDist", "Travel Distance", behavior->GetTravelDistance(), [behavior](float travelDist){behavior->SetTravelDistance(travelDist); });
	AddFloatProperty("initialSpeed", "InitialSpeed", behavior->GetInitialSpeed(), [behavior](float initialSpeed){behavior->SetInitialSpeed(initialSpeed); });
}

void BehaviorView::AddBulletCircleProperties(std::shared_ptr<BulletCircleBehavior> behavior)
{
	AddVector2Property("center", "Center Position (Pixels)", behavior->GetCenterPos(), [behavior](const Vector2& centerPos){behavior->SetCenterPos(centerPos); });
	AddFloatProperty("vel", "Bullet Velocity", behavior->GetVelocity(), [behavior](float velocity){behavior->SetVelocity(velocity);});
	AddIntProperty("bc", "Bullet Count", behavior->GetBulletCount(), [behavior](int bulletCount){behavior->SetBulletCount(bulletCount); });
	AddIntProperty("dam", "Damage", behavior->GetDamage(), [behavior](int damage){behavior->SetDamage(damage); });
	AddStringProperty("bid", "Bullet ID", behavior->GetBulletId(), [behavior](const std::string& bulletId){behavior->SetBulletId(bulletId); });
	AddFloatProperty("cd", "Cooldown", behavior->GetCooldown(), [behavior](float cooldown){behavior->SetCooldown(cooldown); });
	AddFloatProperty("offsRad", "Offset Radians", behavior->GetOffsetRadians(), [behavior](float offsetRadians){behavior->SetOffsetRadians(offsetRadians); });
}

void BehaviorView::AddDriftProperties(std::shared_ptr<DriftBehavior> behavior)
{
	AddFloatProperty("minSp", "Min Speed", behavior->mMinSpeed, [behavior](float minSpeed){behavior->mMinSpeed = minSpeed; });
	AddFloatProperty("maxSp", "Max Speed", behavior->mMaxSpeed, [behavior](float maxSpeed){behavior->mMaxSpeed = maxSpeed; });
}

void BehaviorView::AddExplodeProperties(std::shared_ptr<ExplodeBehavior> behavior)
{
	AddFloatProperty("radius", "Radius", behavior->GetRadius(), [behavior](float radius){behavior->SetRadius(radius); });
	AddStringProperty("expAnimName", "Explosion Animation Name", behavior->GetExplosionAnimationName(), [behavior](const std::string& animName){behavior->SetExplosionAnimationName(animName); });
}

void BehaviorView::AddFollowProperties(std::shared_ptr<FollowBehavior> behavior)
{
	AddFloatProperty("maxDist", "Max Distance", behavior->mMaxDistance, [behavior](float maxDist){behavior->mMaxDistance = maxDist; });
	AddFloatProperty("maxVel", "Max Velocity", behavior->mMaxVelocity, [behavior](float maxVel){behavior->mMaxVelocity = maxVel; });
	AddFloatProperty("acc", "Acceleration", behavior->mAcceleration, [behavior](float acceleration){behavior->mAcceleration = acceleration; });
	AddBoolProperty("xOnly", "X Only", behavior->mXOnly, [behavior](bool xOnly){behavior->mXOnly = xOnly; });
	AddBoolProperty("backw", "Moves Backwards", behavior->mBackwards, [behavior](bool backwards){behavior->mBackwards = backwards; });
}

void BehaviorView::AddMoveProperties(std::shared_ptr<MoveBehavior> behavior)
{
	AddVector2Property("movement", "Movement", behavior->GetMovement(), [behavior](const Vector2& movement){behavior->SetMovement(movement);});
	AddFloatProperty("duration", "Duration", behavior->GetDuration(), [behavior](float speed){behavior->SetDuration(speed);});
	AddFloatProperty("sinStart", "Lerp Sin Start", behavior->GetLerpSinStart(), [behavior](float lerpSinStart){behavior->SetLerpSinStart(lerpSinStart); });
	AddFloatProperty("sinEnd", "Lerp Sin End", behavior->GetLerpSinEnd(), [behavior](float lerpSinEnd){behavior->SetLerpSinEnd(lerpSinEnd); });
}

void BehaviorView::AddProximityProperties(std::shared_ptr<ProximityBehavior> behavior)
{
	AddFloatProperty("dist", "Detection Distance", behavior->mDetectionDistance, [behavior](float dist){behavior->mDetectionDistance = dist; });
	AddFloatProperty("dArcSt", "Detection Arc Start (Radians)", behavior->mDetectionArcStart, [behavior](float dArcStart){behavior->mDetectionArcStart = dArcStart; });
	AddFloatProperty("dArcEd", "Detection Arc End (Radians)", behavior->mDetectionArcEnd, [behavior](float dArcEnd){behavior->mDetectionArcEnd = dArcEnd; });
	AddStringProperty("bSetId", "Behavior Set ID", behavior->mBehaviorSetId, [behavior](const std::string& behaviorSetId){behavior->mBehaviorSetId = behaviorSetId; });
}

void BehaviorView::AddReflectProperties(std::shared_ptr<ReflectBehavior> behavior)
{
	AddFloatProperty("duration", "Duration", behavior->GetDuration(), [behavior](float duration){behavior->SetDuration(duration);});
}

void BehaviorView::AddSetChangeProperties(std::shared_ptr<SetChangeBehavior> behavior)
{
	AddStringProperty("newSetId", "New Behavior Set Id", behavior->GetNewBehaviorSetId(), [behavior](const std::string& newBehaviorSetId){behavior->SetNewBehaviorSetId(newBehaviorSetId); });
	AddIntProperty("healthThrehold", "Health Threhold", behavior->GetHealthThreshold(), [behavior](int healthThreshold){behavior->SetHealthThreshold(healthThreshold); });
	AddBoolProperty("changeOnTurretsDead", "Change On Turrets Dead", behavior->GetChangeWhenTurretsDead(), [behavior](bool changeOnTurretsDead){behavior->SetChangeWhenTurretsDead(changeOnTurretsDead); });
}

void BehaviorView::AddSequenceProperties(std::shared_ptr<SequenceBehavior> behavior)
{
	AddIntProperty("runCount", "Run Count", behavior->GetRunCount(), [behavior](int runCount){behavior->SetRunCount(runCount);});
	AddBoolProperty("isRandom", "Randomized", behavior->GetIsRandom(), [behavior](bool isRandom){behavior->SetIsRandom(isRandom);});
}

void BehaviorView::AddShootProperties(std::shared_ptr<ShootBehavior> behavior)
{
	AddIntProperty("weaponIndex", "Weapon Index", behavior->GetWeaponIndex(), [behavior](int weaponIndex){behavior->SetWeaponIndex(weaponIndex);});
	AddFloatProperty("duration", "Duration", behavior->GetDuration(), [behavior](float duration){behavior->SetDuration(duration);});
}

void BehaviorView::AddSpawnEnemyProperties(std::shared_ptr<SpawnEnemyBehavior> behavior)
{
	AddStringProperty("enemyId", "Enemy ID", behavior->GetEnemyId(), [behavior](const std::string& enemyId){behavior->SetEnemyId(enemyId);});
	AddStringProperty("initialBehaviorSetId", "Initial Behavior Set ID", behavior->GetInitialBehaviorSetId(), [behavior](const std::string& ibsid){behavior->SetInitialBehaviorSetId(ibsid);});
	AddVector2Property("relativePos", "Relative Position (Pixels)", behavior->GetRelativePos(), [behavior](const Vector2& relativePos){behavior->SetRelativePos(relativePos);});
	AddIntProperty("reqTurInd", "Required Turret Index", behavior->GetRequiredTurretIndex(), [behavior](int reqTurInd){behavior->SetRequiredTurretIndex(reqTurInd);});
}

void BehaviorView::AddStrafeProperties(std::shared_ptr<StrafeBehavior> behavior)
{
	AddFloatProperty("xRange", "X Range", behavior->GetXRange(), [behavior](float xRange){behavior->SetXRange(xRange);});
	AddFloatProperty("yRange", "Y Range", behavior->GetYRange(), [behavior](float yRange){behavior->SetYRange(yRange);});
	AddFloatProperty("speed", "Speed", behavior->GetSpeed(), [behavior](float speed){behavior->SetSpeed(speed);});
	AddBoolProperty("isRandDir", "Random Start Direction", behavior->GetIsRandomStartDirection(), [behavior](bool isRandDir){behavior->SetIsRandomStartDirection(isRandDir);});
}

void BehaviorView::AddSuicideProperties(std::shared_ptr<SuicideBehavior> behavior)
{
	AddFloatProperty("dist", "Suicide Distance", behavior->GetSuicideDistance(), [behavior](float dist){behavior->SetSuicideDistance(dist);});
	AddIntProperty("damage", "Damage", behavior->GetDamage(), [behavior](int damage){behavior->SetDamage(damage);});
	AddStringProperty("expAnimName", "Explosion Animation Name", behavior->GetExplosionAnimationName(), [behavior](const std::string& expAN){behavior->SetExplosionAnimationName(expAN);});
}

void BehaviorView::AddTurretVolleyProperties(std::shared_ptr<TurretVolleyBehavior> behavior)
{
	AddFloatProperty("duration", "Duration", behavior->GetDuration(), [behavior](float duration){behavior->SetDuration(duration); });
	auto view = std::make_shared<TurretVolleyView>(behavior, mAnimationRegister, mInputSystem);
	view->Initialize(mPropertyTable);
	GetChild<Node>("turretVolleyViewAnchor")->AddChild(view);
	mPropertyView = view;
}

void BehaviorView::AddWanderProperties(std::shared_ptr<WanderBehavior> behavior)
{
	AddFloatProperty("vel", "Velocity", behavior->GetVelocity(), [behavior](float velocity){behavior->SetVelocity(velocity);});
	AddFloatProperty("dirCngInt", "Direction Change Interval", behavior->GetDirectionChangeInterval(), [behavior](float dirChgInt){behavior->SetDirectionChangeInterval(dirChgInt);});
	AddFloatProperty("dirChgFac", "Direction Change Factor", behavior->GetDirectionChangeFactor(), [behavior](float dirChgFac){behavior->SetDirectionChangeFactor(dirChgFac);});
	AddFloatProperty("lowYBndy", "Lower Y Boundary", behavior->GetLowerYBoundary(), [behavior](float lowerYBoundary){behavior->SetLowerYBoundary(lowerYBoundary);});
}

void BehaviorView::AddTimedBehaviorProperties(std::shared_ptr<TimedBehavior> behavior)
{
	AddFloatProperty("dur", "Duration", behavior->GetDuration(), [behavior](float duration){behavior->SetDuration(duration); });
}

void BehaviorView::AddTurretVisibilityProperties(std::shared_ptr<TurretVisibilityBehavior> behavior)
{
	AddVectorIntProperty("turrets", "Turret Indices", behavior->GetTurretsToChange(), [behavior](const std::vector<int>& turretsToChange){behavior->SetTurretsToChange(turretsToChange); });
	AddBoolProperty("visible", "Visible", behavior->GetVisible(), [behavior](bool visible){behavior->SetVisible(visible); });
}