/*
GenericListView

A small view that lets the user create, copy, and delete elements. This class isn't useful by itself, and subclasses must be used.
*/

#include "Button.h"
#include "EditorGlobals.h"
#include "GenericListView.h"
#include "SelectionCell.h"
#include "TableView.h"
#include "TextField.h"

GenericListView::GenericListView(AnimationRegister& animationRegister, InputSystem& inputSystem):
	PropertyEditorView(animationRegister, inputSystem),
	mSelectedIndex(-1),
	mEnabled(true)
{
}

GenericListView::~GenericListView()
{
}

void GenericListView::Initialize(std::shared_ptr<TableView> propertyTable)
{
	SetPropertyTable(propertyTable);

	LoadFromJson("assets\\screens\\components\\genericListView.json", PIXELS_IN_UNIT);
	GetChild<TextField>("titleLabel")->SetText("UNINITIALIZED");
	GetChild<Button>("newButton")->SetMouseReleaseCallback(std::bind(&GenericListView::OnNewButton, this));
	GetChild<Button>("copyButton")->SetMouseReleaseCallback(std::bind(&GenericListView::OnCopyButton, this));
	GetChild<Button>("deleteButton")->SetMouseReleaseCallback(std::bind(&GenericListView::OnDeleteButton, this));
	mTable = GetChild<TableView>("table");

	SetPropertyTable(propertyTable);
	Refresh();
}

void GenericListView::Refresh()
{
	mTable->ClearTableNodes();
	AddItems();

	ClearProperties();
	AddProperties();

	bool selected = mSelectedIndex >= 0;
	GetChild<Button>("newButton")->SetDisabled(!mEnabled);
	GetChild<Button>("copyButton")->SetDisabled(!selected);
	GetChild<Button>("deleteButton")->SetDisabled(!selected);
}

void GenericListView::AddItem(const std::string& text, int index)
{
	auto cell = std::make_shared<SelectionCell>(mAnimationRegister);
	cell->Initialize(text, std::bind(&GenericListView::SelectItem, this, index));
	mTable->AddTableNode(cell);

	if(index == mSelectedIndex)
		cell->SetSelected(true);
}

void GenericListView::OnNewButton()
{
	Refresh();
}

void GenericListView::OnCopyButton()
{
	Refresh();
}

void GenericListView::OnDeleteButton()
{
	Refresh();
}

void GenericListView::SelectItem(int index)
{
	mSelectedIndex = index;
	Refresh();
}