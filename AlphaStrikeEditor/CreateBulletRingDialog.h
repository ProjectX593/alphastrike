#pragma once

#include "PropertyEditorView.h"

class Weapon;

class CreateBulletRingDialog : public PropertyEditorView
{
public:
	CreateBulletRingDialog(AnimationRegister& animationRegister, InputSystem& inputSystem, Weapon* weapon, std::function<void()> closeCallback);
	~CreateBulletRingDialog();

	void Initialize();

private:
	Weapon* mWeapon;
	std::function<void()> mCloseCallback;
	int mDamage;
	int mBulletCount;
	Vector2 mCenter;
	float mVelocity;
	bool mRotates;
	std::string mBulletId;
	float mStartAngleRadians;
	float mEndAngleRadians;

	void OnCreateButton();
	void OnCancelButton();
};

