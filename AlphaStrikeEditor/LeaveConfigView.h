#pragma once

#include "GenericListView.h"

struct SpawnLeaveConfig;

class LeaveConfigView : public GenericListView
{
public:
	LeaveConfigView(std::vector<SpawnLeaveConfig>* leaveConfigs, AnimationRegister& animationRegister, InputSystem& inputSystem);
	~LeaveConfigView();

	virtual void Initialize(std::shared_ptr<TableView> propertyTable);
	void SetLeaveConfigs(std::vector<SpawnLeaveConfig>* leaveConfigs);

private:
	std::vector<SpawnLeaveConfig>* mLeaveConfigs;

	virtual void AddItems();
	virtual void AddProperties();
	virtual void OnNewButton();
	virtual void OnCopyButton();
	virtual void OnDeleteButton();
};

