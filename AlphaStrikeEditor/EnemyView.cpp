/*
EnemyView

This view handles all of the logic of creating and modifying the most basic properties of enemies.
*/

#include "Button.h"
#include "EditorGlobals.h"
#include "EnemyConfig.h"
#include "EnemyPool.h"
#include "EnemyView.h"
#include "NewItemDialog.h"
#include "TableView.h"
#include "TextField.h"

using std::string;
using namespace std::placeholders;

const string DEFAULT_ID = "unnamed";
const string PROPERTY_HEALTH = "health";
const string PROPERTY_DIFFICULTY = "difficulty";
const string PROPERTY_SCRAP_DROP = "scrapDrop";
const string PROPERTY_MAX_VELOCITY = "maxVelocity";
const string PROPERTY_ACCELERATION = "acceleration";
const string PROPERTY_IS_BOSS = "isBoss";
const string PROPERTY_LEAVES = "leaves";
const string PROPERTY_ANIMATION_NAME = "animationName";
const string PROPERTY_INITIAL_ANIMATION = "initialAnimation";
const string PROPERTY_DROP_TABLE_ID = "dropTableId";
const string PROPERTY_DROP_CHANCE = "dropChance";

EnemyView::EnemyView(EnemyPool& enemyPool, AnimationRegister& animationRegister, InputSystem& inputSystem, std::function<void(const std::string&)> selectEnemyIdCallback):
	PropertyEditorView(animationRegister, inputSystem),
	mEnemyPool(enemyPool),
	mNewEnemyView(nullptr),
	mCopyEnemy(false),
	mSelectEnemyIdCallback(selectEnemyIdCallback)
{
}

EnemyView::~EnemyView()
{
}

void EnemyView::Initialize(const std::string& enemyToSelect)
{
	LoadFromJson("assets\\screens\\components\\enemyView.json", PIXELS_IN_UNIT);

	GetChild<Button>("newButton")->SetMouseReleaseCallback(std::bind(&EnemyView::CreateNewEnemyDialog, this, false));
	GetChild<Button>("copyButton")->SetMouseReleaseCallback(std::bind(&EnemyView::CreateNewEnemyDialog, this, true));
	GetChild<Button>("deleteButton")->SetMouseReleaseCallback(std::bind(&EnemyView::OnDeleteButton, this));

	mEnemyTable = GetChild<TableView>("enemyTable");

	SetPropertyTable(GetChild<TableView>("enemyDetailsTable"));
	SetSelectedEnemyConfigId(enemyToSelect);
	RefreshEnemyTable();
}

void EnemyView::CreateNewEnemyDialog(bool copyEnemy)
{
	string title = copyEnemy ? "COPY ENEMY" : "NEW ENEMY";

	if(mNewEnemyView != nullptr && mCopyEnemy == copyEnemy)
	{
		return;
	}
	else if(mNewEnemyView != nullptr)
	{
		mNewEnemyView->SetTitle(title);
		mCopyEnemy = copyEnemy;
		return;
	}

	mCopyEnemy = copyEnemy;

	mNewEnemyView = std::make_shared<NewItemDialog>(mAnimationRegister, std::bind(&EnemyView::OnNewEnemyViewDone, this, _1), std::bind(&EnemyView::OnNewEnemyViewClosed, this));
	mNewEnemyView->Initialize(title);
	GetChild<Node>("newEnemyDialogAnchor")->AddChild(mNewEnemyView);
}

bool EnemyView::OnNewEnemyViewDone(std::string enemyId)
{
	if(mEnemyPool.EnemyConfigExists(enemyId))
	{
		mNewEnemyView->SetTitle("ENEMY ALREADY EXISTS");
		return false;
	}

	if(mCopyEnemy)
		mEnemyPool.AddEnemyConfig(enemyId, GetSelectedEnemy());
	else
		mEnemyPool.AddEnemyConfig(enemyId, EnemyConfig(1, 1, ""));
	
	RefreshEnemyTable();
	mNewEnemyView = nullptr;

	return true;
}

void EnemyView::OnNewEnemyViewClosed()
{
	mNewEnemyView = nullptr;
}

void EnemyView::SetSelectedEnemyConfigId(const std::string& enemyConfigId)
{
	mSelectedEnemyConfigId = enemyConfigId;
	GetChild<Button>("copyButton")->SetDisabled(mSelectedEnemyConfigId == "");
	GetChild<Button>("deleteButton")->SetDisabled(mSelectedEnemyConfigId == "");

	if(mSelectedEnemyConfigId == "")
	{
		RemovePropertyCells();
	}
	else
	{
		if(GetPropertyCount() == 0)
			AddPropertyCells();
		else
			UpdatePropertyCells();
	}

	mSelectEnemyIdCallback(mSelectedEnemyConfigId);
}

void EnemyView::OnDeleteButton()
{
	mEnemyPool.DeleteEnemyConfig(mSelectedEnemyConfigId);

	SetSelectedEnemyConfigId("");

	RefreshEnemyTable();
}

void EnemyView::RefreshEnemyTable()
{
	mEnemyTable->ClearTableNodes();
	mSelectedCell = std::weak_ptr<View>();
	
	for(auto& enemyConfig : mEnemyPool.GetEditableEnemyConfigs())
	{
		std::shared_ptr<View> cell = std::make_shared<View>(mAnimationRegister);
		cell->LoadFromJson("assets\\screens\\components\\selectionCell.json", PIXELS_IN_UNIT);
		cell->Resize(0.0f, cell->GetChild<Button>("btn")->GetHeight());
		std::weak_ptr<View> weakCell = cell;
		cell->GetChild<Button>("btn")->SetMouseReleaseCallback( [&enemyConfig, weakCell, this](){SelectEnemyConfig(enemyConfig.first, weakCell);} );
		cell->GetChild<TextField>("label")->SetText(enemyConfig.first);
		mEnemyTable->AddTableNode(cell);

		if(enemyConfig.first == mSelectedEnemyConfigId)
		{
			cell->GetChild<Button>("btn")->SetSelected(true);
			mSelectedCell = cell;
		}
	}
}

void EnemyView::SelectEnemyConfig(const string& enemyConfigId, std::weak_ptr<View> cell)
{
	if(enemyConfigId == mSelectedEnemyConfigId)
		return;

	if(mSelectedCell.lock() != nullptr)
		mSelectedCell.lock()->GetChild<Button>("btn")->SetSelected(false);
	mSelectedCell = cell;
	mSelectedCell.lock()->GetChild<Button>("btn")->SetSelected(true);

	SetSelectedEnemyConfigId(enemyConfigId);
}

EnemyConfig& EnemyView::GetSelectedEnemy()
{
	return mEnemyPool.GetEditableEnemyConfigs().at(mSelectedEnemyConfigId);
}

void EnemyView::AddPropertyCells()
{
	EnemyConfig& config = GetSelectedEnemy();
	AddIntProperty(PROPERTY_HEALTH, "Health", config.mMaxHealth, std::bind(&EnemyView::OnHealthChanged, this, _1));
	AddIntProperty(PROPERTY_DIFFICULTY, "Difficulty", config.mDifficulty, std::bind(&EnemyView::OnDifficultyChanged, this, _1));
	AddIntProperty(PROPERTY_SCRAP_DROP, "Scrap Drop", config.mScrapDrop, std::bind(&EnemyView::OnScrapDropChanged, this, _1));
	AddFloatProperty(PROPERTY_MAX_VELOCITY, "Max Velocity", config.mMaxVelocity, std::bind(&EnemyView::OnMaxVelocityChanged, this, _1));
	AddFloatProperty(PROPERTY_ACCELERATION, "Acceleration", config.mAcceleration, std::bind(&EnemyView::OnAccelerationChanged, this, _1));
	AddBoolProperty(PROPERTY_IS_BOSS, "Is Boss", config.mIsBoss, std::bind(&EnemyView::OnIsBossChanged, this, _1));
	AddBoolProperty(PROPERTY_LEAVES, "Leaves", config.mLeaves, std::bind(&EnemyView::OnLeavesChanged, this, _1));
	AddStringProperty(PROPERTY_ANIMATION_NAME, "Animation Name", config.mAnimationName, std::bind(&EnemyView::OnAnimationNameChanged, this, _1));
	AddStringProperty(PROPERTY_INITIAL_ANIMATION, "Initial Animation", config.mInitialAnimation, std::bind(&EnemyView::OnInitialAnimationChanged, this, _1));	
	AddStringProperty(PROPERTY_DROP_TABLE_ID, "Drop Table ID", config.mDropTableId, std::bind(&EnemyView::OnDropTableIdChanged, this, _1));
	AddFloatProperty(PROPERTY_DROP_CHANCE, "Drop Chance", config.mDropChance, std::bind(&EnemyView::OnDropChanceChanged, this, _1));
}

void EnemyView::UpdatePropertyCells()
{
	EnemyConfig& config = GetSelectedEnemy();
	SetPropertyValue(PROPERTY_HEALTH, std::to_string(config.mMaxHealth));
	SetPropertyValue(PROPERTY_DIFFICULTY, std::to_string(config.mDifficulty));
	SetPropertyValue(PROPERTY_SCRAP_DROP, std::to_string(config.mScrapDrop));
	SetPropertyValue(PROPERTY_MAX_VELOCITY, std::to_string(config.mMaxVelocity));
	SetPropertyValue(PROPERTY_ACCELERATION, std::to_string(config.mAcceleration));
	SetPropertyValue(PROPERTY_IS_BOSS, config.mIsBoss ? "true" : "false");
	SetPropertyValue(PROPERTY_LEAVES, config.mLeaves ? "true" : "false");
	SetPropertyValue(PROPERTY_ANIMATION_NAME, config.mAnimationName);
	SetPropertyValue(PROPERTY_INITIAL_ANIMATION, config.mInitialAnimation);
	SetPropertyValue(PROPERTY_DROP_TABLE_ID, config.mDropTableId);
	SetPropertyValue(PROPERTY_DROP_CHANCE, std::to_string(config.mDropChance));
}

void EnemyView::RemovePropertyCells()
{
	ClearProperties();
}

void EnemyView::OnHealthChanged(int health)
{
	GetSelectedEnemy().mMaxHealth = health;
}

void EnemyView::OnDifficultyChanged(int difficulty)
{
	GetSelectedEnemy().mDifficulty = difficulty;
}

void EnemyView::OnScrapDropChanged(int scrapDrop)
{
	GetSelectedEnemy().mScrapDrop = scrapDrop;
}

void EnemyView::OnMaxVelocityChanged(float maxVelocity)
{
	GetSelectedEnemy().mMaxVelocity = maxVelocity;
}

void EnemyView::OnAccelerationChanged(float acceleration)
{
	GetSelectedEnemy().mAcceleration = acceleration;
}

void EnemyView::OnIsBossChanged(bool isBoss)
{
	GetSelectedEnemy().mIsBoss = isBoss;
}

void EnemyView::OnLeavesChanged(bool leaves)
{
	GetSelectedEnemy().mLeaves = leaves;
}

void EnemyView::OnAnimationNameChanged(const std::string& animationName)
{
	GetSelectedEnemy().mAnimationName = animationName;
}

void EnemyView::OnInitialAnimationChanged(const std::string& initialAnimation)
{
	GetSelectedEnemy().mInitialAnimation = initialAnimation;
}

void EnemyView::OnDropTableIdChanged(const std::string& dropTableId)
{
	GetSelectedEnemy().mDropTableId = dropTableId;
}

void EnemyView::OnDropChanceChanged(float dropChance)
{
	GetSelectedEnemy().mDropChance = dropChance;
}