#pragma once

#include "EnemyConfig.h"
#include "View.h"

class NewItemDialog;
class SelectionCell;

class BehaviorSetView : public View
{
public:
	BehaviorSetView(EnemyConfig& enemyConfig, std::function<bool(bool)> onPopupCallback, std::function<void()> refreshBehaviorsCallback, AnimationRegister& animationRegister);
	~BehaviorSetView();

	void Initialize();
	BehaviorSet& GetSelectedBehaviorSet();
	const std::string& GetSelectedBehaviorSetId() { return mSelectedBehaviorSetId; }
	bool NewBehaviorSetDialogVisible();
	void SetButtonsDisabled(bool disabled);

private:
	EnemyConfig& mEnemyConfig;
	std::function<bool(bool)> mOnPopupCallback;
	std::function<void()> mRefreshBehaviorsCallback;
	std::shared_ptr<NewItemDialog> mNewBehaviorSetDialog;
	std::weak_ptr<SelectionCell> mSelectedBehaviorSetCell;
	bool mCopyBehaviorSet;
	bool mButtonsDisabled;
	std::string mSelectedBehaviorSetId;

	void CreateNewBehaviorSetDialog(bool copy);
	bool OnNewBehaviorSetDialogDone(std::string behaviorSetName);
	void OnNewBehaviorSetDialogClosed();
	void OnDeleteBehaviorSetButton();
	void RefreshBehaviorSets();
	void OnBehaviorSetSelected(const std::string &behaviorSetId, std::weak_ptr<SelectionCell> behaviorSetCell);
	void SetSelectedBehaviorSetId(const std::string& behaviorSetId);
	void RefreshButtons();
};

