#pragma once

#include "PropertyEditorView.h"

class TurretPositionView : public PropertyEditorView
{
public:
	TurretPositionView(AnimationRegister& animationRegister, InputSystem& inputSystem);
	~TurretPositionView();

	void Initialize();
	void SetTurretPositions(std::vector<Vector2>* turretPositions);

private:
	std::vector<Vector2>* mTurretPositions;
	std::weak_ptr<View> mSelectedCell;
	int mSelectedPositionIndex;

	void SetSelectedTurretPositionIndex(int index);
	void RefreshTurretPositions();
	void OnTurretPositionSelected(int index, std::weak_ptr<View> cell);
	void RefreshButtons();
	void OnNewButton();
	void OnCopyButton();
	void OnDeleteButton();
};

