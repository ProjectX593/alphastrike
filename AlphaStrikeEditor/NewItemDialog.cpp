/*
NewItemDialog

A small dialog used to ask the user for a name for a new item. The done callback will be called
with the value the user enters when they click the create button. If they click cancel, the done callback
is called instead.
The done callback needs to return true if it is safe for the dialog to close, or false if the dialog should not close.
*/

#include "EditorGlobals.h"
#include "NewItemDialog.h"
#include "TextField.h"

using std::string;

NewItemDialog::NewItemDialog(AnimationRegister& animationRegister, std::function<bool(std::string)> doneCallback, std::function<void()> closeCallback):
	View(animationRegister),
	mDoneCallback(doneCallback),
	mCloseCallback(closeCallback)
{
}

NewItemDialog::~NewItemDialog()
{
}

void NewItemDialog::Initialize(const string& title)
{
	LoadFromJson("assets\\screens\\components\\newItemDialog.json", PIXELS_IN_UNIT);

	SetTitle(title);
	GetChild<Button>("createButton")->SetMouseReleaseCallback(std::bind(&NewItemDialog::OnCreateButton, this));
	GetChild<Button>("cancelButton")->SetMouseReleaseCallback(std::bind(&NewItemDialog::OnCancelButton, this));
}

void NewItemDialog::SetTitle(const string& title)
{
	GetChild<TextField>("titleLabel")->SetText(title);
}

void NewItemDialog::OnCreateButton()
{
	if(mDoneCallback(GetChild<TextField>("inputTextBox")->GetText()))
		RemoveFromParentAndCleanup();
}

void NewItemDialog::OnCancelButton()
{
	if(mCloseCallback != nullptr)
		mCloseCallback();
	RemoveFromParentAndCleanup();
}