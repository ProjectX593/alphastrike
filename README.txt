Alpha Strike is a working title for a space shooter / roguelike game I've been working on in my spare time. It's nowhere near completion, and contains a lot of debug art and incomplete features, but it serves as a good example of my code. It consists of three projects:

- Draw2D: The 2D game engine I've created.
- AlphaStrike: The game itself.
- AlphaStrikeEditor: An editor used to create enemy pools, which contain enemy ship and spawn configs.

The game and editor are already compiled and can be found at AlphaStrikeEditor/Release for the editor, and AlphaStrike/Release for the game itself.
To play the game, simply start it up and hit 'Continue' - there is already a save file ready to go. Next click 'Bridge', then 'Launch'. The controls are WASD to move, space to fire, and E to fire the secondary weapon.
To use the editor, refer to the separate readme file I've included to keep this one brief.

As for the code, I've written the majority of the functionality myself. The game makes use of OpenGL, LodePNG, Cereal, jsoncpp, and Freetype (hopefully I haven't forgotten anything!).
Draw2D is a 2D graphics engine that uses a scene graph to determine where to place everything on the screen. It contains a handful of simple UI elements as well as sprites which all extend the Node class which contains most of the scene graph logic. The rendering itself is extremely simple, I simply draw textured quads onto the screen; no shaders or anything fancy.
As for the game itself, there are numerous features in different states of completion including items, drops, levels, enemies and their behaviors etc.

Feel free to explore the code and ask me any questions you might have about it!